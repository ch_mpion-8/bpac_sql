CREATE TABLE [HangFire].[List]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Key] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[ExpireAt] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [HangFire].[List] ADD CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List] ([ExpireAt]) INCLUDE ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_List_Key] ON [HangFire].[List] ([Key]) INCLUDE ([ExpireAt], [Value]) ON [PRIMARY]
GO
