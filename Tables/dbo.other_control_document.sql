CREATE TABLE [dbo].[other_control_document]
(
[document_id] [int] NOT NULL IDENTITY(1, 1),
[document_no] [varchar] (20) COLLATE Thai_CI_AS NULL,
[document_type] [int] NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NULL,
[receiver] [varchar] (10) COLLATE Thai_CI_AS NULL,
[receiver_name] [nvarchar] (120) COLLATE Thai_CI_AS NULL,
[contact_person] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[contact_person_id] [int] NULL,
[contact_person_type] [int] NULL,
[method] [varchar] (20) COLLATE Thai_CI_AS NULL,
[document_date] [datetime] NULL,
[user_code] [varchar] (50) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[update_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[reference_guid] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[insert_other_document_no]
   ON  [dbo].[other_control_document]
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    Declare @count bigint 
	select @count= count(*) from dbo.other_control_document o group by Year(o.document_date) having Year(o.document_date) = Year((select document_date from inserted))
	
	Declare @running  varchar(7)
	set @running = Right('000000'+convert(varchar(7),@count),6);
	Declare @otherdoc_no varchar(20)
	Declare @comcode varchar(40)
	select @comcode = company_code from inserted

	Declare @type int =null
	select @type = document_type from inserted

	if @type =	5
	begin
		set @otherdoc_no = 'CS'+Right(CONVERT(varchar(4),year((select document_date from inserted))),2)+'-'+@comcode+'-'+@running
	end
	else
	begin
		set @otherdoc_no = 'O'+Right(CONVERT(varchar(4),year((select document_date from inserted))),2)+'-'+@comcode+'-'+@running
	end
	
	
	update dbo.other_control_document 
	set document_no = @otherdoc_no
	where document_id = (select document_id from inserted)
	
	
END
GO
ALTER TABLE [dbo].[other_control_document] ADD CONSTRAINT [PK_other_control_document] PRIMARY KEY CLUSTERED  ([document_id]) ON [PRIMARY]
GO
