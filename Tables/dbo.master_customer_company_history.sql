CREATE TABLE [dbo].[master_customer_company_history]
(
[history_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NOT NULL,
[old_user_name] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[new_user_name] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_company_history] ADD CONSTRAINT [PK_master_customer_company_history] PRIMARY KEY CLUSTERED  ([history_id]) ON [PRIMARY]
GO
