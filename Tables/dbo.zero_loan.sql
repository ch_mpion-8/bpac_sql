CREATE TABLE [dbo].[zero_loan]
(
[zero_loan_id] [int] NOT NULL IDENTITY(1, 1),
[zero_loan_date] [date] NOT NULL,
[company_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[submit_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[summary] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zero_loan] ADD CONSTRAINT [PK_zero_loan] PRIMARY KEY CLUSTERED  ([zero_loan_id], [zero_loan_date], [company_code]) ON [PRIMARY]
GO
