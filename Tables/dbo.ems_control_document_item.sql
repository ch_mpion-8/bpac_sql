CREATE TABLE [dbo].[ems_control_document_item]
(
[ems_detail_id] [int] NOT NULL IDENTITY(1, 1),
[ems_control_document_id] [int] NULL,
[customer_code] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[ems_tracking_no] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[delete_customer_in_ems]
   ON  [dbo].[ems_control_document_item] 
   AFTER  DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    set nocount on;
 
    declare @control_id int
			,@updated_by nvarchar(50)
			,@action VARCHAR(50)
			,@description nvarchar(max)
			,@customer_code varchar(10)
 


	select @control_id = deleted.ems_control_document_id   
			,@customer_code = deleted.customer_code
			,@updated_by = deleted.updated_by   
    from deleted

	
	SET @action = 'Remove'
	set @description = 'Remove customer code "' + @customer_code + '"'

 
	if @action is not null and @description is not null
	begin
	   exec [dbo].[insert_ems_control_history]
		@ems_control_document_id = @control_id
		,@description = @description
		,@action = @action
		,@updated_by = @updated_by
	end
 

END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[update_ems]
   ON  [dbo].[ems_control_document_item] 
   AFTER  UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    set nocount on;
 
    declare @control_id int
			,@updated_by nvarchar(50)
			,@action VARCHAR(50)
			,@description nvarchar(max)
			,@remark nvarchar(2000)
			,@old_remark nvarchar(2000)
			,@customer_code varchar(10)
			,@is_active bit
			,@status nvarchar(50)
 
    select @control_id = inserted.ems_control_document_id   
			,@updated_by = inserted.updated_by   
			,@is_active = inserted.is_active
			,@remark = inserted.remark
			,@customer_code = inserted.customer_code
    from inserted

	select @status = [status]
	from [dbo].[ems_control_document]
	where ems_control_document_id = @control_id

	select @old_remark = deleted.remark 
    from deleted

	IF UPDATE([remark]) and @remark <> @old_remark
    BEGIN
          SET @action = 'Updated Remark'
		  --set @description = 'Customer ' + @customer_code +' change remark from "' + @old_remark + '"'
          SET @description = 'Updated Remark'
    END

	IF UPDATE([is_active])
    BEGIN
		if @is_active = 0 and @status <>  'CANCEL'
		begin
          SET @action = 'Remove'
		  set @description = 'Remove customer code "' + @customer_code + '"'
		end
    END
 
	if @action is not null and @description is not null
	begin
	   exec [dbo].[insert_ems_control_history]
		@ems_control_document_id = @control_id
		,@description = @description
		,@action = @action
		,@updated_by = @updated_by
	end
 

END

GO
ALTER TABLE [dbo].[ems_control_document_item] ADD CONSTRAINT [PK_ems_control_document_item] PRIMARY KEY CLUSTERED  ([ems_detail_id]) ON [PRIMARY]
GO
