CREATE TABLE [dbo].[master_email_config]
(
[customer_company_id] [int] NOT NULL,
[sales_org] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[sold_to] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[ship_to] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[number_of_copies] [int] NULL,
[output_type_text] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[output_type_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[send_email_type] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL CONSTRAINT [DF_master_email_config_is_active] DEFAULT ((1)),
[created_date] [datetime] NULL,
[created_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_email_config] ADD CONSTRAINT [PK_master_email_config_1] PRIMARY KEY CLUSTERED  ([customer_company_id]) ON [PRIMARY]
GO
