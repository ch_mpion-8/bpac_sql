CREATE TABLE [dbo].[master_other_contact_person]
(
[other_contact_id] [bigint] NOT NULL IDENTITY(1, 1),
[customer_code] [varchar] (20) COLLATE Thai_CI_AS NULL,
[customer_name] [varchar] (100) COLLATE Thai_CI_AS NULL,
[title] [varchar] (100) COLLATE Thai_CI_AS NULL,
[telephone] [varchar] (100) COLLATE Thai_CI_AS NULL,
[contact_person] [varchar] (100) COLLATE Thai_CI_AS NULL,
[remark] [varchar] (1000) COLLATE Thai_CI_AS NULL,
[e_mail] [varchar] (100) COLLATE Thai_CI_AS NULL,
[created_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[updated_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_other_contact_person] ADD CONSTRAINT [PK_master_other_contact_person] PRIMARY KEY CLUSTERED  ([other_contact_id]) ON [PRIMARY]
GO
