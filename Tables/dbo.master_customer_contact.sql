CREATE TABLE [dbo].[master_customer_contact]
(
[contact_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NOT NULL,
[contact_name] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[position] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[email] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[telephone] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[fax] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[is_show_in_ems] [bit] NULL,
[is_show_in_other_document] [bit] NULL,
[copy_from_id] [int] NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_contact] ADD CONSTRAINT [PK_master_customer_contact] PRIMARY KEY CLUSTERED  ([contact_id]) ON [PRIMARY]
GO
