CREATE TABLE [dbo].[invoice_document_detail]
(
[invoice_no] [varchar] (18) COLLATE Thai_CI_AS NOT NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[year] [int] NOT NULL,
[document_type] [nvarchar] (20) COLLATE Thai_CI_AS NOT NULL,
[document_name] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[document_id] [uniqueidentifier] NULL,
[print_count] [int] NULL CONSTRAINT [DF_invoice_document_detail_print_count] DEFAULT ((0)),
[is_active] [bit] NULL CONSTRAINT [DF_invoice_document_detail_is_active] DEFAULT ((1)),
[created_date] [datetime] NULL,
[created_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[invoice_document_detail] ADD CONSTRAINT [PK_invoice_document_detail_1] PRIMARY KEY CLUSTERED  ([invoice_no], [company_code], [year], [document_type], [document_name]) ON [PRIMARY]
GO
