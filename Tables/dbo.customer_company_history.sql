CREATE TABLE [dbo].[customer_company_history]
(
[history_id] [int] NOT NULL IDENTITY(1, 1),
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[user_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[user_name] [varchar] (200) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customer_company_history] ADD CONSTRAINT [PK_customer_company_history] PRIMARY KEY CLUSTERED  ([history_id]) ON [PRIMARY]
GO
