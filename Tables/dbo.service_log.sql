CREATE TABLE [dbo].[service_log]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[webkey] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[message] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[date] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
