CREATE TABLE [dbo].[AuditLog]
(
[Id] [uniqueidentifier] NOT NULL,
[Timestamp] [datetime] NOT NULL,
[EventName] [nvarchar] (max) COLLATE Thai_CI_AS NOT NULL,
[EventDetail] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[ActionUser] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLog] ADD CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
