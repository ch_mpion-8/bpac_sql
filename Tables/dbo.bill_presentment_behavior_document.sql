CREATE TABLE [dbo].[bill_presentment_behavior_document]
(
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[valid_date_from] [date] NOT NULL,
[payment_term_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[document_code] [nvarchar] (20) COLLATE Thai_CI_AS NOT NULL,
[custom_document] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bill_presentment_behavior_document] ADD CONSTRAINT [PK_bill_presentment_behavior_document] PRIMARY KEY CLUSTERED  ([company_code], [customer_code], [valid_date_from], [payment_term_code], [document_code]) ON [PRIMARY]
GO
