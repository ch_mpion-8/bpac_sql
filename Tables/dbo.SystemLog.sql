CREATE TABLE [dbo].[SystemLog]
(
[Id] [uniqueidentifier] NOT NULL,
[Timestamp] [datetime] NOT NULL,
[LogLevel] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[LogMessage] [nvarchar] (250) COLLATE Thai_CI_AS NOT NULL,
[LogDetail] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[ActionUser] [nvarchar] (100) COLLATE Thai_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemLog] ADD CONSTRAINT [PK_SystemLog] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
