CREATE TABLE [HangFire].[Job]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StateId] [int] NULL,
[StateName] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[InvocationData] [nvarchar] (max) COLLATE Thai_CI_AS NOT NULL,
[Arguments] [nvarchar] (max) COLLATE Thai_CI_AS NOT NULL,
[CreatedAt] [datetime] NOT NULL,
[ExpireAt] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [HangFire].[Job] ADD CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job] ([ExpireAt]) INCLUDE ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job] ([StateName]) ON [PRIMARY]
GO
