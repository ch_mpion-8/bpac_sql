CREATE TABLE [dbo].[zero_loan_item]
(
[zero_loan_detail_id] [int] NOT NULL IDENTITY(1, 1),
[zero_loan_id] [int] NOT NULL,
[reference_document_no] [nvarchar] (16) COLLATE Thai_CI_AS NOT NULL,
[cheque_control_id] [int] NOT NULL,
[zero_loan_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[create_date] [datetime] NULL,
[create_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zero_loan_item] ADD CONSTRAINT [PK_zero_loan_item] PRIMARY KEY CLUSTERED  ([zero_loan_detail_id]) ON [PRIMARY]
GO
