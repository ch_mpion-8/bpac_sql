CREATE TABLE [dbo].[master_contact_person]
(
[contact_person_id] [int] NOT NULL IDENTITY(1, 1),
[contact_person_name] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[contact_group_id] [int] NOT NULL,
[contact_person_phone] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[contact_person_email] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[contact_person_remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_contact_person] ADD CONSTRAINT [PK_master_contact_person] PRIMARY KEY CLUSTERED  ([contact_person_id]) ON [PRIMARY]
GO
