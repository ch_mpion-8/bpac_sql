CREATE TABLE [dbo].[pyc_customer_group]
(
[pyc_customer_group_id] [int] NOT NULL IDENTITY(1, 1),
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[customer_group] [int] NOT NULL,
[customer_group_order] [int] NOT NULL
) ON [PRIMARY]
GO
