CREATE TABLE [dbo].[cheque_invoice_control_document_item]
(
[cheque_control_detail_id] [int] NOT NULL IDENTITY(1, 1),
[cheque_control_id] [int] NULL,
[invoice_no] [varchar] (16) COLLATE Thai_CI_AS NULL,
[amount] [decimal] (15, 2) NULL,
[tracking_date] [datetime] NULL,
[cheque_no] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[cheque_amount] [decimal] (15, 2) NULL,
[cheque_date] [date] NULL,
[cheque_currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[cheque_bank] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[cheque_remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[document_year] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cheque_invoice_control_document_item] ADD CONSTRAINT [PK_cheque_invoice_control_document_item] PRIMARY KEY CLUSTERED  ([cheque_control_detail_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cheque_invoice_control_document_item] ADD CONSTRAINT [FK_cheque_invoice_control_document_item] FOREIGN KEY ([cheque_control_id]) REFERENCES [dbo].[cheque_invoice_control_document] ([cheque_control_id])
GO
