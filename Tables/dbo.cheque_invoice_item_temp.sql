CREATE TABLE [dbo].[cheque_invoice_item_temp]
(
[temp_detail_id] [bigint] NOT NULL IDENTITY(1, 1),
[temp_id] [int] NULL,
[invoice_no] [varchar] (16) COLLATE Thai_CI_AS NULL,
[amount] [decimal] (15, 2) NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[document_year] [int] NULL
) ON [PRIMARY]
GO
