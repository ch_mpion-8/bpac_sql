CREATE TABLE [dbo].[log_manual_update_bill_date]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[invoice_no] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[company_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[old_billing_date] [datetime] NULL,
[new_billing_date] [datetime] NULL,
[remark] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_time] [datetime] NULL,
[is_manual_update] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
