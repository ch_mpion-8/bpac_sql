CREATE TABLE [dbo].[files_temp]
(
[id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_files_temp_id] DEFAULT (newid()),
[file_name] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[file_type] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[file_content] [varbinary] (max) NULL,
[document_type] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[size] [int] NULL,
[is_active] [bit] NULL CONSTRAINT [DF_files_temp_is_active] DEFAULT ((1)),
[created_date] [datetime] NULL,
[created_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[files_temp] ADD CONSTRAINT [PK_files_temp] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
