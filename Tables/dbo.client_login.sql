CREATE TABLE [dbo].[client_login]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ip] [varchar] (12) COLLATE Thai_CI_AS NULL,
[fail_count] [int] NULL,
[last_date] [datetime] NULL,
[last_user] [varchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[client_login] ADD CONSTRAINT [PK_client_login] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
