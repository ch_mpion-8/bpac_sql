CREATE TABLE [dbo].[clearing]
(
[clearing_document_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[invoice_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[amount] [decimal] (10, 2) NULL,
[clearing_date] [datetime] NULL,
[document_date] [datetime] NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[clearing_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
