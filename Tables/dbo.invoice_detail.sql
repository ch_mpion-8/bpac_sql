CREATE TABLE [dbo].[invoice_detail]
(
[invoice_no] [varchar] (18) COLLATE Thai_CI_AS NOT NULL,
[invoice_item] [varchar] (6) COLLATE Thai_CI_AS NOT NULL,
[dp_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[load_date] [date] NULL,
[order_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[bid_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[po_no] [varchar] (20) COLLATE Thai_CI_AS NULL,
[material_no] [varchar] (20) COLLATE Thai_CI_AS NULL,
[net_value] [decimal] (15, 2) NULL,
[tax_value] [decimal] (13, 2) NULL,
[created_date] [date] NULL,
[created_time] [time] NULL,
[cost] [decimal] (13, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[invoice_detail] ADD CONSTRAINT [PK_invoice_detail] PRIMARY KEY CLUSTERED  ([invoice_no], [invoice_item]) ON [PRIMARY]
GO
