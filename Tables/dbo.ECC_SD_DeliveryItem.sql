CREATE TABLE [dbo].[ECC_SD_DeliveryItem]
(
[TABT] [nvarchar] (28) COLLATE Thai_CI_AS NULL,
[VKORG] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[VSTEL] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[VBELN] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VBTYP] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[LFART] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ABLAD] [nvarchar] (25) COLLATE Thai_CI_AS NULL,
[KUNNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[KUNAG] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[ROUTA] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[INCO1] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[INCO2] [nvarchar] (28) COLLATE Thai_CI_AS NULL,
[FAKSK] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[LIFSK] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[VKOIV] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[VTWIV] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[SPAIV] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[WADAT_IST] [date] NULL,
[LFDAT] [date] NULL,
[ERDAT] [date] NULL,
[AEDAT] [date] NULL,
[TRATY] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[FKDAT] [date] NULL,
[LAND1] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[LZONE] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[TRSTA] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[WBSTK] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[POSNR] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[VGBEL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VGPOS] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[WERKS] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[LGORT] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[MATNR] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[CHARG] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VTWEG] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[SPART] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[LFIMG] [numeric] (13, 3) NULL,
[VRKME] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[PSTYV] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ARKTX] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[PRODH] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[TDLINE] [nvarchar] (132) COLLATE Thai_CI_AS NULL,
[DGJAHR] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[QDOCNO] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[QDOCITEM] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[QDATE] [date] NULL,
[CDOCNO] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[CDOCITEM] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[CDATE] [date] NULL,
[SHPNUM] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[SHPITM] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[SHPDAT] [date] NULL,
[MBDAT] [date] NULL,
[MODLV] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[ETDDAT] [date] NULL,
[SIGNI] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[EXTI1] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[VENDORID] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PLATENO] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[ETADAT] [date] NULL,
[DUEDAT] [date] NULL,
[AUGRU] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[PARVWSP] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARVWSF] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARVWHL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARVWSA] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARVWSG] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARVWEC] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[LPRIO] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[ORDDATE] [date] NULL,
[DEL_TEXT] [nvarchar] (60) COLLATE Thai_CI_AS NULL,
[BWTAR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VKGRP] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[TNDR_TRKID] [nvarchar] (35) COLLATE Thai_CI_AS NULL,
[KVGR2] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[ZMARK] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[FKSTK] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[FKSTA] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[CTDATE] [date] NULL,
[ABRVW] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MODLV2] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[BUKRS] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ProcessDate] [datetime] NULL
) ON [PRIMARY]
GO
