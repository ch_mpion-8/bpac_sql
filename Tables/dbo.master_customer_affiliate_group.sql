CREATE TABLE [dbo].[master_customer_affiliate_group]
(
[affiliate_id] [int] NOT NULL IDENTITY(1, 1),
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_affiliate_group] ADD CONSTRAINT [PK_master_customer_affiliate_group] PRIMARY KEY CLUSTERED  ([affiliate_id]) ON [PRIMARY]
GO
