CREATE TABLE [dbo].[master_customer_collection_behavior]
(
[collection_behavior_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NOT NULL,
[valid_date_from] [date] NOT NULL,
[valid_date_to] [date] NULL,
[payment_term_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[address_id] [int] NULL,
[time] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[collection_condition_date] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[period_from] [int] NOT NULL,
[period_to] [int] NOT NULL,
[description] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[condition_by] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[payment_day] [int] NULL,
[is_business_day] [bit] NULL,
[date_from] [int] NULL,
[date_to] [int] NULL,
[last_x_date] [int] NULL,
[last_x_business_day] [int] NULL,
[custom_date] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[custom_day] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[condition_week_by] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[custom_week] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[next_x_week] [int] NULL,
[condition_month_by] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[custom_month] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[next_x_month] [int] NULL,
[is_next_collection_date] [bit] NULL,
[payment_method] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[receipt_method] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[bank] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[notify_day] [smallint] NULL,
[is_skip_customer_holiday] [bit] NULL,
[is_skip_scg_holiday] [bit] NULL,
[document] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[custom_document] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[group_id] [int] NULL,
[custom_date_next_x_month] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_collection_behavior] ADD CONSTRAINT [PK_master_customer_collection_behavior] PRIMARY KEY CLUSTERED  ([collection_behavior_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_collection_behavior] ADD CONSTRAINT [FK_master_customer_collection_behavior_master_customer_company] FOREIGN KEY ([customer_company_id]) REFERENCES [dbo].[master_customer_company] ([customer_company_id])
GO
