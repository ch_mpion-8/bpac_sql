CREATE TABLE [dbo].[email_document_control]
(
[id] [uniqueidentifier] NOT NULL,
[document_id] [int] NULL,
[document_type] [varchar] (30) COLLATE Thai_CI_AS NULL,
[email_type] [varchar] (30) COLLATE Thai_CI_AS NULL,
[email_to] [varchar] (max) COLLATE Thai_CI_AS NULL,
[email_cc] [varchar] (max) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[used] [bit] NULL,
[used_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[used_date] [datetime] NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[email_document_control] ADD CONSTRAINT [PK_email_document_control] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
