CREATE TABLE [dbo].[log_interface_detail]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[log_interface_header_id] [int] NULL,
[web_key] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[step_name] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[start_date_time] [datetime] NULL,
[end_date_time] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (max) COLLATE Thai_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[log_interface_detail] ADD CONSTRAINT [PK_log_interface_detail] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[log_interface_detail] ADD CONSTRAINT [FK_log_interface_detail_log_interface_detail] FOREIGN KEY ([log_interface_header_id]) REFERENCES [dbo].[log_interface_header] ([id])
GO
