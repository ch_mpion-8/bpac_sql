CREATE TABLE [dbo].[VBAK_Temp]
(
[MANDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBELN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERZET] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERNAM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ANGDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BNDDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBTYP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TRVOG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUART] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUGRU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GWLDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SUBMI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LIFSK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FAKSK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[NETWR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WAERK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKORG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VTWEG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SPART] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKGRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKBUR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GSBER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GSKST] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GUEBG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GUEEN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KNUMV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VDATU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VPRGR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUTLF] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBKLA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBKLT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KALSM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VSBED] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FKARA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AWAHR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KTEXT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BSTNK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BSARK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BSTDK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BSTZD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[IHREZ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BNAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TELF1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MAHZA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MAHDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KUNNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOSTL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STAFO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STWAE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AEDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KNUMA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOKRS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PS_PSP_PNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KURST] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KKBER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KNKLI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GRUPP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SBGRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CTLPC] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMWAE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMFRE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMNUP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMNGV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AMTBL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[HITYP_PR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABRVW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABDIS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGBEL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[OBJNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BUKRS_VF] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK6] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK7] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK8] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXK9] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[XBLNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZUONR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGTYP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KALSM_CH] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AGRZR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUFNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[QMNUM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBELN_GRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SCHEME_GRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABRUF_PART] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABHOD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABHOV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABHOB] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[RPLNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VZEIT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STCEG_L] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LANDTX] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[XEGDR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ENQUEUE_GRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DAT_FZAU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FMBDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VSNMR_V] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[HANDLE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PROLI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CONT_DG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CRM_GUID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UPD_TMSTMP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MSR_ID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TM_CTRL_KEY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PSM_BUDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SWENR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SMENR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PHASE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MTLAUR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STAGE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[HB_CONT_REASON] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[HB_EXPDATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[HB_RESDATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MILL_APPL_ID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LOGSYSB] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KALCD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MULTI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SPPAYM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WTYSC_CLM_HDR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[YYKUNNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZ_KTEXT_MC] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[YYTRCKRESV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[YYTRCKREQ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[YYTRCKCONF] [nvarchar] (500) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
