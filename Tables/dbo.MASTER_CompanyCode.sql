CREATE TABLE [dbo].[MASTER_CompanyCode]
(
[CompanyCode] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[CountryKey] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[CurrencyKey] [nvarchar] (5) COLLATE Thai_CI_AS NULL,
[ChartOfAccounts] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[CreditControlArea] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[FiscalYearVariant] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[Company] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[Language] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[CompanyName] [nvarchar] (25) COLLATE Thai_CI_AS NULL,
[ProcessDate] [datetime] NULL
) ON [PRIMARY]
GO
