CREATE TABLE [dbo].[jv_company]
(
[company_id] [int] NOT NULL IDENTITY(1, 1),
[company_name] [varchar] (20) COLLATE Thai_CI_AS NULL,
[company_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[company_full_name] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[status] [varchar] (20) COLLATE Thai_CI_AS NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jv_company] ADD CONSTRAINT [PK_jv_company] PRIMARY KEY CLUSTERED  ([company_id]) ON [PRIMARY]
GO
