CREATE TABLE [dbo].[menu_access]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[role_name] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[menu_access] [nvarchar] (max) COLLATE Thai_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
