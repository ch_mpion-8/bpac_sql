CREATE TABLE [dbo].[master_company]
(
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[company_name] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[company_address] [varchar] (500) COLLATE Thai_CI_AS NULL,
[company_logo] [varchar] (500) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_require_document] [bit] NULL CONSTRAINT [DF_master_company_is_require_document] DEFAULT ((0)),
[is_waiting_all_dp] [bit] NULL CONSTRAINT [DF_master_company_is_waiting_all_dp] DEFAULT ((0)),
[company_name_th] [nvarchar] (500) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_company] ADD CONSTRAINT [PK_master_company] PRIMARY KEY CLUSTERED  ([company_code]) ON [PRIMARY]
GO
