CREATE TABLE [dbo].[DNLOAD]
(
[VBELN] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[LFART] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[LFDAT] [datetime] NULL,
[KUNNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[LPRIO] [nvarchar] (10) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
