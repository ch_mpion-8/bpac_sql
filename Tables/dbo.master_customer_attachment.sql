CREATE TABLE [dbo].[master_customer_attachment]
(
[attachment_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NOT NULL,
[title] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[reference_guid] [uniqueidentifier] NULL,
[copy_from_id] [int] NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[file_name] [nvarchar] (100) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_attachment] ADD CONSTRAINT [PK_master_customer_attachment] PRIMARY KEY CLUSTERED  ([attachment_id]) ON [PRIMARY]
GO
