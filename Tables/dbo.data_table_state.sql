CREATE TABLE [dbo].[data_table_state]
(
[date_state_id] [int] NOT NULL IDENTITY(1, 1),
[key] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[data_table_state] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[user] [varchar] (50) COLLATE Thai_CI_AS NULL,
[created_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[updated_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[data_table_state] ADD CONSTRAINT [PK_data_table_state] PRIMARY KEY CLUSTERED  ([date_state_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_data_table_state] ON [dbo].[data_table_state] ([key], [user]) ON [PRIMARY]
GO
