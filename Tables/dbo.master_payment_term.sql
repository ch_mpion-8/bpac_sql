CREATE TABLE [dbo].[master_payment_term]
(
[payment_term_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[payment_term_name] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_payment_term] ADD CONSTRAINT [PK_master_payment_term] PRIMARY KEY CLUSTERED  ([payment_term_code]) ON [PRIMARY]
GO
