CREATE TABLE [dbo].[bill_presentment_behavior_custom_date]
(
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[valid_date_from] [date] NOT NULL,
[payment_term_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[date] [date] NOT NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bill_presentment_behavior_custom_date] ADD CONSTRAINT [PK_bill_presentment_behavior_custom_date] PRIMARY KEY CLUSTERED  ([company_code], [customer_code], [valid_date_from], [payment_term_code], [date]) ON [PRIMARY]
GO
