CREATE TABLE [dbo].[cheque_invoice_control_document]
(
[cheque_control_id] [int] NOT NULL IDENTITY(1, 1),
[cheque_control_no] [nvarchar] (14) COLLATE Thai_CI_AS NULL,
[customer_company_id] [int] NULL,
[method] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[job_date] [datetime] NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[cheque_control_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[collection_date] [date] NULL,
[reference_guid] [uniqueidentifier] NULL,
[is_manual] [bit] NULL CONSTRAINT [DF_cheque_invoice_control_document_is_manual] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[generate_cheque_invoice_control_no]
   ON  [dbo].[cheque_invoice_control_document]
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @result varchar(14)  --B17-0480-00001
		,@thisyear varchar(2)
		,@id varchar(5)
		,@n int
		,@com_code as varchar(4) 
		,@inserted_id as int
		,@is_manual as bit
		
	declare @mDocNo as nvarchar(14)
	declare @mInvDocNo as nvarchar(14)
    select @thisyear = right(year(getdate()),2)
	--select @thisyear

	select  @inserted_id = cheque_control_id
			,@com_code = c.company_code
			,@is_manual = i.is_manual
	from inserted i
	inner join dbo.master_customer_company c on i.customer_company_id = c.customer_company_id

	if @is_manual = 1 
	begin
	
		select @mDocNo = max(substring(cheque_control_no,10,5))
			from [cheque_control_document] with(nolock)
			where cheque_control_no like 'M' + @thisyear + '-' + @com_code + '-%'


		select @mInvDocNo = max(substring(cheque_control_no,10,5))
			from [cheque_invoice_control_document] with(nolock)
			where cheque_control_no like 'M' + @thisyear + '-' + @com_code + '-%'

		if ( @mDocNo is not null and @mDocNo <> '') or ( @mInvDocNo is not null and @mInvDocNo <> '')
		begin
			set @id = case when iif(@mDocNo = '' , 0 ,isnull(@mDocNo, 0)) > iif(@mInvDocNo = '' , 0 ,isnull(@mInvDocNo, 0)) 
							then  RIGHT( '0000' + convert(varchar(5),convert(int,@mDocNo)+1),5)
							else  RIGHT( '0000' + convert(varchar(5),convert(int,@mInvDocNo)+1),5)
							end

		end
		else
			set @id =  '00001'  

		--if exists(select 1 from dbo.[cheque_invoice_control_document] where cheque_control_no like 'M' + @thisyear + '-%')
		--begin
		--	select @id = max(substring(cheque_control_no,10,5))
		--	from [cheque_invoice_control_document] with(nolock)
		--	where cheque_control_no like 'M' + @thisyear + '-%'

		--	set @n = convert(int,@id)+1
		--	set @id = RIGHT( '0000' + convert(varchar(5),@n),5)
		--end
		--else
		--begin
		--	-- make a new series of id values for new year 
		--	set @id =  '00001'  
		--end
		set @result = 'M' + @thisyear+ '-'+ @com_code + '-' + @id

	end
	else
	begin

		select @mDocNo = max(substring(cheque_control_no,10,5))
			from [cheque_control_document] with(nolock)
			where cheque_control_no like 'C' + @thisyear  + '-' + @com_code + '-%'


		select @mInvDocNo = max(substring(cheque_control_no,10,5))
			from [cheque_invoice_control_document] with(nolock)
			where cheque_control_no like 'C' + @thisyear  + '-' + @com_code + '-%'

		if ( @mDocNo is not null and @mDocNo <> '') or ( @mInvDocNo is not null and @mInvDocNo <> '')
		begin
			set @id = case when iif(@mDocNo = '' , 0 ,isnull(@mDocNo, 0)) > iif(@mInvDocNo = '' , 0 ,isnull(@mInvDocNo, 0)) 
							then  RIGHT( '0000' + convert(varchar(5),convert(int,@mDocNo)+1),5)
							else  RIGHT( '0000' + convert(varchar(5),convert(int,@mInvDocNo)+1),5)
							end

		end
		else
			set @id =  '00001'  

	
		--if exists(select * from dbo.[cheque_invoice_control_document] where cheque_control_no like 'C' + @thisyear + '-%')
		--begin
		--	select @id = max(substring(cheque_control_no,10,5))
		--	from [cheque_invoice_control_document] with(nolock)
		--	where cheque_control_no like 'C' + @thisyear + '-%'

		--	set @n = convert(int,@id)+1
		--	set @id = RIGHT( '0000' + convert(varchar(5),@n),5)
		--end
		--else
		--begin
		--	-- make a new series of id values for new year 
		--	set @id =  '00001'  
		--end
		set @result = 'C' + @thisyear+ '-'+ @com_code + '-' + @id


	end


    --select @result

	--select  @inserted_id = cheque_control_id
	--from inserted 

	update dbo.[cheque_invoice_control_document]
	set cheque_control_no = @result
	where cheque_control_id = @inserted_id

END


GO
ALTER TABLE [dbo].[cheque_invoice_control_document] ADD CONSTRAINT [PK_cheque_invoice_control_document] PRIMARY KEY CLUSTERED  ([cheque_control_id]) ON [PRIMARY]
GO
