CREATE TABLE [dbo].[master_company_cost_center]
(
[cost_center_id] [int] NOT NULL IDENTITY(1, 1),
[company] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[company_type] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[department] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[cost_center] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[cost_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL CONSTRAINT [DF_master_company_cost_center_is_active] DEFAULT ((1)),
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_company_cost_center] ADD CONSTRAINT [PK_master_company_cost_center] PRIMARY KEY CLUSTERED  ([cost_center_id]) ON [PRIMARY]
GO
