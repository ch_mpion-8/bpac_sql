CREATE TABLE [dbo].[master_customer_holiday]
(
[customer_holiday_id] [int] NOT NULL IDENTITY(1, 1),
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[holiday_year] [smallint] NOT NULL,
[holiday_month] [smallint] NOT NULL,
[holiday_day] [smallint] NOT NULL,
[holiday_name] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[compensate_date] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_holiday] ADD CONSTRAINT [PK_master_customer_holiday] PRIMARY KEY CLUSTERED  ([customer_holiday_id]) ON [PRIMARY]
GO
