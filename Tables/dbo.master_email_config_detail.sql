CREATE TABLE [dbo].[master_email_config_detail]
(
[email_config_detail_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NULL,
[document_type] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[email_type] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[email_address] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL CONSTRAINT [DF_master_email_config_detail_is_active] DEFAULT ((1)),
[created_date] [datetime] NULL,
[created_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (200) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_email_config_detail] ADD CONSTRAINT [PK_master_email_config_detail] PRIMARY KEY CLUSTERED  ([email_config_detail_id]) ON [PRIMARY]
GO
