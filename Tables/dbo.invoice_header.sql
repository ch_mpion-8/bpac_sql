CREATE TABLE [dbo].[invoice_header]
(
[invoice_no] [varchar] (18) COLLATE Thai_CI_AS NOT NULL,
[invoice_type] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[invoice_date] [date] NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[payment_term] [varchar] (4) COLLATE Thai_CI_AS NULL,
[invoice_amount] [decimal] (15, 2) NULL,
[tax_amount] [decimal] (13, 2) NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[referenece_document_no] [varchar] (16) COLLATE Thai_CI_AS NULL,
[assignment_no] [varchar] (18) COLLATE Thai_CI_AS NULL,
[bill_presentment_date] [date] NULL,
[bill_presentment_notify_date] [date] NULL,
[bill_presentment_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[bill_presentment_calculation_log] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[bill_presentment_remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[collection_date] [date] NULL,
[collection_notify_date] [date] NULL,
[collection_calculation_log] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[collection_amount] [decimal] (15, 2) NULL,
[collection_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[collection_remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[due_date] [date] NULL,
[net_due_date] [date] NULL,
[bill_presentment_behavior_id] [int] NULL,
[collection_behavior_id] [int] NULL,
[clearing_document_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[created_date_time] [datetime] NULL CONSTRAINT [DF_invoice_header_created_date_time] DEFAULT (getdate()),
[created_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date_time] [datetime] NULL,
[updated_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[remaining_amount] [decimal] (15, 2) NULL,
[invoice_qty] [decimal] (15, 3) NULL,
[qty_unit] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[vat_type] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[is_pdc] [bit] NULL,
[require_document_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[require_document] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[email_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[email_receive_date] [datetime] NULL,
[print_document_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[insert_log_date]
   ON   [dbo].[invoice_header]
   AFTER  UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    set nocount on;
 
    declare @invoice_no  nvarchar(50)
			,@updated_by nvarchar(50)
			,@new_collection_date datetime
			,@old_collection_date datetime
			,@old_billing_date datetime
			,@new_billing_date datetime
			,@old_updated_by nvarchar(50)
			,@old_updated_time datetime
			,@is_manual_update bit = 0
			,@bill_status nvarchar(50)
			,@col_status nvarchar(50)
			,@company_no  nvarchar(50)
			,@bill_remark  nvarchar(max)
			,@coll_remark  nvarchar(max)
			--,@old_bill_status nvarchar(50)
			--,@old_col_status nvarchar(50)
 
	DECLARE TrigTempUpdate_Cursor CURSOR FOR

	 select inserted.invoice_no   
			, inserted.updated_by   
			,inserted.collection_date
			,inserted.bill_presentment_date
			,inserted.bill_presentment_status
			,inserted.collection_status
			,inserted.[company_code]
			,inserted.bill_presentment_remark
			,inserted.collection_remark
			,d.collection_date
			,d.bill_presentment_date
    from inserted inserted
	join deleted d on inserted.invoice_no = d.invoice_no
			and inserted.[company_code] = d.[company_code]
	
	OPEN TrigTempUpdate_Cursor;

	FETCH NEXT FROM TrigTempUpdate_Cursor INTO @invoice_no ,@updated_by ,@new_collection_date,@new_billing_date ,@bill_status
											,@col_status,@company_no,@bill_remark,@coll_remark,@old_collection_date,@old_billing_date

	WHILE @@FETCH_STATUS = 0

	BEGIN
		
		--select @old_updated_by = deleted.updated_by   
		--		,@old_updated_time = deleted.updated_date_time   
		--		,@old_collection_date = deleted.collection_date
		--		,@old_billing_date = deleted.bill_presentment_date
		--		--,@old_bill_status = deleted.bill_presentment_status
		--		--,@old_col_status = deleted.collection_status
		--from deleted
		--where deleted.invoice_no = @invoice_no
		--		and deleted.company_code = @company_no

	if UPDATE(bill_presentment_date) 
	begin
		if isnull(@bill_status,'') = 'MANUAL'
			set @is_manual_update = 1

		INSERT INTO [dbo].[log_manual_update_bill_date]
           ([invoice_no]
           ,[company_code]
           ,[old_billing_date]
           ,[new_billing_date]
           ,[remark]
           ,[updated_by]
           ,[updated_time]
           ,[is_manual_update])
		 VALUES
			   (@invoice_no
			   ,@company_no
			   ,@old_billing_date
			   ,@new_billing_date
			   ,@bill_remark
			   ,@updated_by
			   ,getdate()
			   ,@is_manual_update)
 
	end
	
	if (UPDATE(collection_date) )
	begin
		if isnull(@col_status,'') = 'MANUAL'
			set @is_manual_update = 1

		
		INSERT INTO [dbo].[log_manual_update_collection_date]
           ([invoice_no]
           ,[company_code]
           ,[old_collection_date]
           ,[new_collection_date]
           ,[remark]
           ,[updated_by]
           ,[updated_time]
           ,[is_manual_update])
		 VALUES
			   (@invoice_no
			   ,@company_no
			   ,@old_collection_date
			   ,@new_collection_date
			   ,@coll_remark
			   ,@updated_by
			   ,getdate()
			   ,@is_manual_update)
	end

	
	FETCH NEXT FROM TrigTempUpdate_Cursor INTO @invoice_no ,@updated_by ,@new_collection_date,@new_billing_date ,@bill_status
											,@col_status,@company_no,@bill_remark,@coll_remark,@old_collection_date,@old_billing_date

	END;

	CLOSE TrigTempUpdate_Cursor;

	DEALLOCATE TrigTempUpdate_Cursor;

END
GO
ALTER TABLE [dbo].[invoice_header] ADD CONSTRAINT [PK_invoice_header_1] PRIMARY KEY CLUSTERED  ([invoice_no], [company_code]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-notify-date] ON [dbo].[invoice_header] ([bill_presentment_notify_date]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-collection-notify-date] ON [dbo].[invoice_header] ([collection_notify_date]) ON [PRIMARY]
GO
