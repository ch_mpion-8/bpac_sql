CREATE TABLE [dbo].[mismatch_history]
(
[history_id] [int] NOT NULL IDENTITY(1, 1),
[mismatch_id] [int] NULL,
[action] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
