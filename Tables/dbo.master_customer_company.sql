CREATE TABLE [dbo].[master_customer_company]
(
[customer_company_id] [int] NOT NULL IDENTITY(1, 1),
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[user_name] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[user_display_name] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_company] ADD CONSTRAINT [PK_master_customer_company] PRIMARY KEY CLUSTERED  ([customer_company_id]) ON [PRIMARY]
GO
