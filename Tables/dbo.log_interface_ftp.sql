CREATE TABLE [dbo].[log_interface_ftp]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[log_date] [datetime] NULL,
[log_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[log_message] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[file_name] [nvarchar] (2000) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
