CREATE TABLE [dbo].[master_other_contact]
(
[contact_id] [int] NOT NULL IDENTITY(1, 1),
[title] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[contact_name] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[address] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[sub_district] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[district] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[province] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[post_code] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_other_contact] ADD CONSTRAINT [PK_master_other_contact] PRIMARY KEY CLUSTERED  ([contact_id]) ON [PRIMARY]
GO
