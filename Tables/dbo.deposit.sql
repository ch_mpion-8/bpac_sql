CREATE TABLE [dbo].[deposit]
(
[order_no] [nvarchar] (10) COLLATE Thai_CI_AS NOT NULL,
[year] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[order_date] [date] NULL,
[request_date] [date] NULL,
[product_id] [nvarchar] (50) COLLATE Thai_CI_AS NOT NULL,
[total_quantity] [decimal] (13, 2) NULL,
[dp_no] [nvarchar] (10) COLLATE Thai_CI_AS NOT NULL,
[charg] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[lfimg] [decimal] (13, 2) NULL,
[company_code] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[customer_code] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[invoice_date] [date] NULL,
[invoice_no] [nvarchar] (10) COLLATE Thai_CI_AS NOT NULL,
[created_date_time] [datetime] NULL CONSTRAINT [DF_deposit_created_date_time] DEFAULT (getdate()),
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL CONSTRAINT [DF_deposit_created_by] DEFAULT (N'SSIS'),
[updated_date_time] [datetime] NULL CONSTRAINT [DF_deposit_updated_date_time] DEFAULT (getdate()),
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL CONSTRAINT [DF_deposit_updated_by] DEFAULT (N'SSIS')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[after_update_deposit]
ON [dbo].[deposit]
AFTER UPDATE AS
	Begin
		Set nocount on; 
			Update t
			Set t.updated_date_time = getdate()
			From deposit t inner join inserted 
				On t.order_no = inserted.order_no
					and t.product_id = inserted.product_id
					and t.dp_no = inserted.dp_no
	End

GO
