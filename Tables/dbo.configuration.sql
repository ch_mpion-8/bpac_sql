CREATE TABLE [dbo].[configuration]
(
[configuration_key] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[configuration_code] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[configuration_value] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[sequence_no] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[configuration] ADD CONSTRAINT [PK_configuration] PRIMARY KEY CLUSTERED  ([configuration_key], [configuration_code]) ON [PRIMARY]
GO
