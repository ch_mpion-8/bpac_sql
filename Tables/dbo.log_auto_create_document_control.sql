CREATE TABLE [dbo].[log_auto_create_document_control]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[log_date] [datetime] NULL,
[log_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[log_message] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[data] [nvarchar] (2000) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
