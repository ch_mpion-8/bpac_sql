CREATE TABLE [dbo].[ZFI_0090_ADVREC_Temp]
(
[MANDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[COM_CODE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BUSINESS_PLACE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZMARK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ADV_REC_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ADV_REC_YEAR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ADV_REC_DATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CUSTOMER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FI_DOCUMENT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FI_GJAHR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BILLING_PRES] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DOC_TRANFER_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DOC_TRANFER_YEAR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[USER_ID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CREATE_DATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AMOUNT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CURRENCY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LANGU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CANCEL_BY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CANCEL_DATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CANCEL_REASON] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CHANGE_BY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CHANGE_DATE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ADV_REC_NO_NEW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ADV_REC_YEAR_NEW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[IS_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VE_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AP_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ISID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VEID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[APID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[REMARK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[REFERENCE_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[RSTGR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of MANDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of COM_CODE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of BUSINESS_PLACE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of ZMARK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of ADV_REC_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[conversion_ADV_REC_YEAR_Int] [int] NULL,
[conversion_ADV_REC_Date] [datetime] NULL,
[Copy of CUSTOMER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of FI_DOCUMENT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[conversion_FI-GJAHR_Int] [int] NULL,
[conversion_DOC_TRANFER_YEAR_Int] [int] NULL,
[conversion_CREATE_DATE_Date] [datetime] NULL,
[conversion_AMOUNT_Decimal] [decimal] (28, 13) NULL,
[conversion_CANCEL_DATE_Date] [datetime] NULL,
[conversion_CHANGE_DATE_Date] [datetime] NULL,
[conversion_ADV_REC_YEAR_NEW_Int] [int] NULL,
[Copy of BILLING_PRES] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of DOC_TRANFER_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of USER_ID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of CURRENCY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of LANGU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of CANCEL_BY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of CANCEL_REASON] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of CHANGE_BY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of ADV_REC_NO_NEW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of IS_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of VE_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of AP_NAME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of ISID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of VEID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of APID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of REMARK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of REFERENCE_NO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[Copy of RSTGR] [nvarchar] (500) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
