CREATE TABLE [dbo].[VBRP_Temp]
(
[MANDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBELN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[POSNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UEPOS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FKIMG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VRKME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UMVKZ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UMVKN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MEINS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SMENG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FKLMG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LMENG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[NTGEW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BRGEW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GEWEI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VOLUM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VOLEH] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GSBER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PRSDT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FBUDA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KURSK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[NETWR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VBELV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[POSNV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGBEL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGPOS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGTYP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUBEL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUPOS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUREF] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MATNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ARKTX] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PMATN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CHARG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MATKL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PSTYV] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[POSAR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PRODH] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VSTEL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ATPKZ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SPART] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[POSPA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WERKS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ALAND] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WKREG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WKCOU] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WKCTY] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM6] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM7] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM8] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TAXM9] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOWRR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PRSFD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SKTOF] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SKFBP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KONDM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KTGRM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOSTL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BONUS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PROVG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[EANNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKGRP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKBUR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SPARA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SHKZG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERNAM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERDAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ERZET] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BWTAR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LGORT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STAFO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WAVWR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZWI6] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STCUR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UVPRS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UVALL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[EAN11] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PRCTR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KVGR5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MVGR1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MVGR2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MVGR3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MVGR4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MVGR5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MATWA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BONBA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOKRS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PAOBJNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PS_PSP_PNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUFNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[TXJCD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMPRE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMPNT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CUOBJ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CUOBJ_CH] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KOUPD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UECHA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[XCHAR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABRVW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SERNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BZIRK_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDGRP_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KONDA_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LLAND_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MPROK] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PLTYP_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[REGIO_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKORG_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VTWEG_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABRBG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PROSA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UEPVW] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUTYP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[STADAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FPLNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FPLTR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AKTNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KNUMA_PI] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KNUMA_AG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PREFE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MWSBP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUGRU_AUFT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FAREG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UPMAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[UKONM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CMPRE_FLT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABFOR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ABGES] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1ARFZ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1AREGIO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1AGICD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1ADTYP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1ATXREL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BCFOP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTXSDC] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BRTWR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WKTNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WKTPS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[RPLNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KURSK_DAT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WGRU1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WGRU2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDKG1] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDKG2] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDKG3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDKG4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KDKG5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VKAUS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1AINDXP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1AIDATEP] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KZFME] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MWSKZ] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VERTT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VERTN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[SGTXT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DELCO] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[BEMOT] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[RRREL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AKKUR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[WMINR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGBEL_EX] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGPOS_EX] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[LOGSYS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[VGTYP_EX] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW3] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW4] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW5] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MSR_ID] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MSR_REFUND_CODE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[MSR_RET_REASON] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[NRAB_KNUMH] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[NRAB_VALUE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DISPUTE_CASE] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FUND_USAGE_ITEM] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CLAIMS_TAXATION] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[KURRF_DAT_ORIG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUFPL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[APLZL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DPCNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DCPNR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[DPNRB] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PEROP_BEG] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PEROP_END] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FONDS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FISTL] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[FKBER] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[GRANT_NBR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PRS_WORK_PERIOD] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PPRCTR] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[PARGB] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[AUFPL_OAA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[APLZL_OAA] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[CAMPAIGN] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[COMPREAS] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI7] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI8] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI9] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI10] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI11] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI12] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI13] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI14] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI15] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI16] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI17] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI18] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI19] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[ZZWI20] [nvarchar] (500) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
