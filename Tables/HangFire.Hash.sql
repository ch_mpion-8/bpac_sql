CREATE TABLE [HangFire].[Hash]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Key] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[Field] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[ExpireAt] [datetime2] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [HangFire].[Hash] ADD CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash] ([ExpireAt]) INCLUDE ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_Key] ON [HangFire].[Hash] ([Key]) INCLUDE ([ExpireAt]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Hash_Key_Field] ON [HangFire].[Hash] ([Key], [Field]) ON [PRIMARY]
GO
