CREATE TABLE [dbo].[EMPGDCCHEM]
(
[Person ID] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Personnel Number] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[SCG Employee ID] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Name prefix (Thai)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[First Name (Thai)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Last Name (Thai)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Title (English)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[First Name (English)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Last Name (English)] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to Personnel Number] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to Name] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to Position] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to Position Name] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to Email] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Report to User ID] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager Personnel Number] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager Name] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager Position] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager Position Name] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager SCG EMP ID] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager's Email] [nvarchar] (255) COLLATE Thai_CI_AS NULL,
[Manager's User ID] [nvarchar] (255) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
