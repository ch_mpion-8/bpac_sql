CREATE TABLE [dbo].[master_contact_group]
(
[contact_group_id] [int] NOT NULL IDENTITY(1, 1),
[contact_group_value] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_contact_group] ADD CONSTRAINT [PK_master_contact_group] PRIMARY KEY CLUSTERED  ([contact_group_id]) ON [PRIMARY]
GO
