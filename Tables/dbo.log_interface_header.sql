CREATE TABLE [dbo].[log_interface_header]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[web_key] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[request_data] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[response_data] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[request_date_time] [datetime] NULL,
[request_by] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[response_date_time] [datetime] NULL,
[response_by] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[log_request] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[log_response] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[INF_ID] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[CPUDT_FROM] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[CPUDT_TO] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[CPUTM_FROM] [nvarchar] (8) COLLATE Thai_CI_AS NULL,
[CPUTM_TO] [nvarchar] (8) COLLATE Thai_CI_AS NULL,
[BUKRS] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[BLART] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[PI_MSG_ID_RES] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[PI_MSG_ID_REQ] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[STATUS_TYPE] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[STATUS_ID] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[STATUS_NUMBER] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[STATUS_MESSAGE] [nvarchar] (220) COLLATE Thai_CI_AS NULL,
[BKPF_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[BSEG_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[BSID_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[BSAD_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[VBRK_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[VBRP_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[VBAK_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[BIDNO_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[DNLOAD_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[ZFI_0090_ADVREC_COUNT] [nvarchar] (6) COLLATE Thai_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[log_interface_header] ADD CONSTRAINT [PK_log_interface_header] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
