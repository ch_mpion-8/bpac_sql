CREATE TABLE [dbo].[cheque_control_document_history]
(
[history_id] [int] NOT NULL IDENTITY(1, 1),
[cheque_control_id] [int] NULL,
[action] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[bpac_due_date] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cheque_control_document_history] ADD CONSTRAINT [PK_cheque_control_document_history] PRIMARY KEY CLUSTERED  ([history_id]) ON [PRIMARY]
GO
