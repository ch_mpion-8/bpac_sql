CREATE TABLE [dbo].[ems_sub_item]
(
[ems_sub_detail_id] [int] NOT NULL IDENTITY(1, 1),
[ems_detail_id] [int] NULL,
[reference_id] [int] NULL,
[reference_type] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ems_sub_item] ADD CONSTRAINT [check_active_ems_count_constraint] CHECK (([dbo].[check_active_ems_count]([reference_id],[reference_type])<=(1)))
GO
ALTER TABLE [dbo].[ems_sub_item] ADD CONSTRAINT [PK_ems_sub_item] PRIMARY KEY CLUSTERED  ([ems_sub_detail_id]) ON [PRIMARY]
GO
