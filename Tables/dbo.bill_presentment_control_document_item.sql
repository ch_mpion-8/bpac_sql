CREATE TABLE [dbo].[bill_presentment_control_document_item]
(
[bill_presentment_detail_id] [bigint] NOT NULL IDENTITY(1, 1),
[bill_presentment_id] [int] NULL,
[invoice_no] [varchar] (18) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bill_presentment_control_document_item] ADD CONSTRAINT [check_active_bill_doc_count_constraint] CHECK ((NOT ([dbo].[check_active_bill_doc_count]([invoice_no],[bill_presentment_id])>(1) AND isnull([is_active],(1))=(1))))
GO
ALTER TABLE [dbo].[bill_presentment_control_document_item] ADD CONSTRAINT [PK_bill_presentment_control_document_item] PRIMARY KEY CLUSTERED  ([bill_presentment_detail_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bill_presentment_control_document_item] ADD CONSTRAINT [FK_bill_presentment_control_document_item] FOREIGN KEY ([bill_presentment_id]) REFERENCES [dbo].[bill_presentment_control_document] ([bill_presentment_id])
GO
