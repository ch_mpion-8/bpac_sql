CREATE TABLE [dbo].[master_customer_affiliate_list]
(
[affiliate_detail_id] [int] NOT NULL IDENTITY(1, 1),
[affiliate_id] [int] NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_affiliate_list] ADD CONSTRAINT [PK_master_customer_affiliate_list] PRIMARY KEY CLUSTERED  ([affiliate_detail_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_affiliate_list] ADD CONSTRAINT [FK_master_customer_affiliate_list_master_customer_affiliate_group] FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[master_customer_affiliate_group] ([affiliate_id])
GO
