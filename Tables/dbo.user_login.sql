CREATE TABLE [dbo].[user_login]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[username] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[fail_count] [int] NOT NULL,
[reference_datetime] [datetime] NOT NULL
) ON [PRIMARY]
GO
