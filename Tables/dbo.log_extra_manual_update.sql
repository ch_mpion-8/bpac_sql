CREATE TABLE [dbo].[log_extra_manual_update]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[invoice_no] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[old_collection_date] [datetime] NULL,
[new_collection_date] [datetime] NULL,
[old_billing_date] [datetime] NULL,
[new_billing_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_time] [datetime] NULL,
[old_updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[old_updated_time] [datetime] NULL,
[is_manual_update] [bit] NULL,
[company_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
