CREATE TABLE [dbo].[master_bank]
(
[bank_id] [int] NOT NULL IDENTITY(1, 1),
[bank_name] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_bank] ADD CONSTRAINT [PK_master_bank] PRIMARY KEY CLUSTERED  ([bank_id]) ON [PRIMARY]
GO
