CREATE TABLE [dbo].[scg_pyc_process]
(
[process_id] [int] NOT NULL IDENTITY(1, 1),
[reference_no_id] [int] NOT NULL,
[reference_type] [int] NOT NULL,
[trip_date] [date] NOT NULL,
[count_trip] [int] NOT NULL,
[is_skip] [bit] NOT NULL,
[remark] [varchar] (max) COLLATE Thai_CI_AS NULL,
[status] [nchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [varchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [varchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
