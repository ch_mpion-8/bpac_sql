CREATE TABLE [dbo].[bill_presentment_control_document]
(
[bill_presentment_id] [int] NOT NULL IDENTITY(1, 1),
[bill_presentment_no] [varchar] (14) COLLATE Thai_CI_AS NULL,
[customer_company_id] [int] NULL,
[method] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[bill_presentment_date] [date] NULL,
[bill_presentment_status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[reference_guid] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[generate_bill_presentment_no]
   ON  [dbo].[bill_presentment_control_document]
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @result varchar(14)  --B17-0480-00001
		,@thisyear varchar(2)
		,@id varchar(5)
		,@n int
		,@com_code as varchar(4) 
		,@inserted_id as int

    select @thisyear = right(year(getdate()),2)
	--select @thisyear
	select  @inserted_id = bill_presentment_id
			,@com_code = c.company_code
	from inserted i
	inner join dbo.master_customer_company c on i.customer_company_id = c.customer_company_id


    if exists(select * from dbo.bill_presentment_control_document where bill_presentment_no like 'B' + @thisyear + '-%')
    begin
        select @id = max(substring(bill_presentment_no,10,5))
        from bill_presentment_control_document with(nolock)
        where bill_presentment_no like 'B' + @thisyear + '-%'

        set @n = convert(int,@id)+1
        set @id = RIGHT( '0000' + convert(varchar(5),@n),5)
    end
    else
    begin
        -- make a new series of id values for new year 
        set @id =  '00001'  
    end
    set @result = 'B' + @thisyear+ '-'+ @com_code + '-' + @id
    --select @result

	--select  @inserted_id = bill_presentment_id
	--from inserted 

	update dbo.bill_presentment_control_document
	set bill_presentment_no = @result
	where bill_presentment_id = @inserted_id

END


GO
ALTER TABLE [dbo].[bill_presentment_control_document] ADD CONSTRAINT [PK_bill_presentment_control_document] PRIMARY KEY CLUSTERED  ([bill_presentment_id]) ON [PRIMARY]
GO
