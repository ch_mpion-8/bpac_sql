CREATE TABLE [dbo].[other_control_document_item]
(
[document_id] [int] NOT NULL,
[reference_no] [varchar] (500) COLLATE Thai_CI_AS NOT NULL,
[year] [varchar] (4) COLLATE Thai_CI_AS NULL,
[action_type] [int] NOT NULL,
[document_status] [int] NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[sequence_no] [int] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[other_control_document_item] ADD CONSTRAINT [CK_other_control_document_item] CHECK ((NOT [dbo].[check_active_other_doc_count]([document_id],[reference_no],[action_type])>(1)))
GO
ALTER TABLE [dbo].[other_control_document_item] ADD CONSTRAINT [PK_other_control_document_item] PRIMARY KEY CLUSTERED  ([document_id], [reference_no], [action_type]) ON [PRIMARY]
GO
