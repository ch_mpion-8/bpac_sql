CREATE TABLE [dbo].[other_control_document_history]
(
[history_id] [int] NOT NULL IDENTITY(1, 1),
[document_id] [int] NOT NULL,
[action] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[other_control_document_history] ADD CONSTRAINT [PK_other_control_document_history] PRIMARY KEY CLUSTERED  ([history_id], [document_id]) ON [PRIMARY]
GO
