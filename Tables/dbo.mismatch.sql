CREATE TABLE [dbo].[mismatch]
(
[mismatch_id] [int] NOT NULL IDENTITY(1, 1),
[mismatch_date] [date] NULL,
[company_code] [nvarchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[customer_company_id] [int] NULL,
[mismatch_status] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[amount] [decimal] (18, 2) NULL,
[interest_day] [int] NULL,
[interest_rate] [decimal] (4, 2) NULL,
[reason] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[solution] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[submit_by] [varchar] (100) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[reference_guid] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
