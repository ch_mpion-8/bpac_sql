CREATE TABLE [HangFire].[JobQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[JobId] [int] NOT NULL,
[Queue] [nvarchar] (50) COLLATE Thai_CI_AS NOT NULL,
[FetchedAt] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [HangFire].[JobQueue] ADD CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HangFire_JobQueue_QueueAndFetchedAt] ON [HangFire].[JobQueue] ([Queue], [FetchedAt]) ON [PRIMARY]
GO
