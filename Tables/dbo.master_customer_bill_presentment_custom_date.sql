CREATE TABLE [dbo].[master_customer_bill_presentment_custom_date]
(
[bill_presentment_custom_date_id] [int] NOT NULL IDENTITY(1, 1),
[bill_presentment_behavior_id] [int] NOT NULL,
[date] [date] NOT NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_bill_presentment_custom_date] ADD CONSTRAINT [PK_master_customer_bill_presentment_custom_date] PRIMARY KEY CLUSTERED  ([bill_presentment_custom_date_id]) ON [PRIMARY]
GO
