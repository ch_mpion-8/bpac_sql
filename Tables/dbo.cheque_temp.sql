CREATE TABLE [dbo].[cheque_temp]
(
[temp_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NULL,
[collection_date] [date] NULL,
[job_date] [date] NULL,
[method] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[payment_term] [varchar] (4) COLLATE Thai_CI_AS NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
