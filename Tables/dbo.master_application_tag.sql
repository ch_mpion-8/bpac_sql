CREATE TABLE [dbo].[master_application_tag]
(
[application_tag_id] [int] NOT NULL IDENTITY(1, 1),
[application_tag_name] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_application_tag] ADD CONSTRAINT [PK_master_application_tag] PRIMARY KEY CLUSTERED  ([application_tag_id]) ON [PRIMARY]
GO
