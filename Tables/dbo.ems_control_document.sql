CREATE TABLE [dbo].[ems_control_document]
(
[ems_control_document_id] [int] NOT NULL IDENTITY(1, 1),
[ems_control_document_no] [varchar] (9) COLLATE Thai_CI_AS NULL,
[company_code] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[department] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[cost_center] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[cost_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[send_date] [date] NULL,
[reference_guid] [uniqueidentifier] NULL,
[is_manual] [bit] NULL CONSTRAINT [DF_ems_control_document_is_manual] DEFAULT ((0)),
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[generate_ems_control_no]
   ON  [dbo].[ems_control_document]
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @result varchar(14)  --
		,@thisyear varchar(2)
		,@id varchar(5)
		,@n int
		,@com_code as varchar(4) 
		,@inserted_id as int

    select @thisyear = right(year(getdate()),2)
	--select @thisyear

	select  @inserted_id = i.ems_control_document_id   
	from inserted i

 if exists(select * from dbo.[ems_control_document] where ems_control_document_no like 'E' + @thisyear + '-%')
    begin
        select @id = max(substring(ems_control_document_no,5,5))
        from [ems_control_document] with(nolock)
        where ems_control_document_no like 'E' + @thisyear + '-%'

        set @n = convert(int,@id)+1
        set @id = RIGHT( '0000' + convert(varchar(5),@n),5)
    end
    else
    begin
        -- make a new series of id values for new year 
        set @id =  '00001'  
    end
    set @result = 'E' + @thisyear+ '-' + @id

	update dbo.[ems_control_document]
	set ems_control_document_no = @result
	where ems_control_document_id = @inserted_id


END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[insert_ems]
   ON  [dbo].[ems_control_document] 
   AFTER  INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    set nocount on;
 
    declare @control_id int
			,@updated_by nvarchar(50)
 
    select @control_id = inserted.ems_control_document_id   
			,@updated_by = inserted.updated_by    
    from inserted
 
    exec [dbo].[insert_ems_control_history]
		@ems_control_document_id = @control_id
		,@description = 'Create EMS document control.'
		,@action = 'Create'
		,@updated_by = @updated_by

END
GO
ALTER TABLE [dbo].[ems_control_document] ADD CONSTRAINT [PK_ems_control_document] PRIMARY KEY CLUSTERED  ([ems_control_document_id]) ON [PRIMARY]
GO
