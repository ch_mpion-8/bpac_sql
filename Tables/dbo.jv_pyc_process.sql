CREATE TABLE [dbo].[jv_pyc_process]
(
[process_id] [int] NOT NULL IDENTITY(1, 1),
[document_date] [date] NULL,
[company_id] [int] NULL,
[count_trip] [int] NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jv_pyc_process] ADD CONSTRAINT [PK_jv_pyc_process] PRIMARY KEY CLUSTERED  ([process_id]) ON [PRIMARY]
GO
