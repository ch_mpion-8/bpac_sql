CREATE TABLE [dbo].[master_customer_bill_presentment_document]
(
[bill_presentment_document_id] [int] NOT NULL IDENTITY(1, 1),
[bill_presentment_behavior_id] [int] NOT NULL,
[document_code] [nvarchar] (20) COLLATE Thai_CI_AS NOT NULL,
[custom_document] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_bill_presentment_document] ADD CONSTRAINT [PK_master_customer_bill_presentment_document] PRIMARY KEY CLUSTERED  ([bill_presentment_document_id]) ON [PRIMARY]
GO
