CREATE TABLE [dbo].[receipt]
(
[clearing_document_no] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[document_type] [varchar] (2) COLLATE Thai_CI_AS NULL,
[year] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[assignment_no] [varchar] (18) COLLATE Thai_CI_AS NULL,
[posting_date] [date] NULL,
[document_date] [date] NULL,
[created_date] [date] NULL,
[reference_document_no] [varchar] (16) COLLATE Thai_CI_AS NOT NULL,
[document_amount] [decimal] (13, 2) NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[base_line_due_date] [date] NULL,
[payment_term] [varchar] (4) COLLATE Thai_CI_AS NULL,
[day_term] [decimal] (3, 0) NULL,
[other_document_no] [varchar] (14) COLLATE Thai_CI_AS NULL,
[other_document_date] [date] NULL,
[other_document_status] [varchar] (10) COLLATE Thai_CI_AS NULL,
[receipt_type] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL CONSTRAINT [DF_receipt_is_active] DEFAULT ((1)),
[cancel_by] [nvarchar] (12) COLLATE Thai_CI_AS NULL,
[cancel_date] [date] NULL,
[cancel_reason] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[is_pdc] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[receipt] ADD CONSTRAINT [PK_receipt] PRIMARY KEY CLUSTERED  ([clearing_document_no], [year], [company_code], [reference_document_no]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-assignment_no] ON [dbo].[receipt] ([assignment_no]) ON [PRIMARY]
GO
