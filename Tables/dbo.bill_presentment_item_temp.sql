CREATE TABLE [dbo].[bill_presentment_item_temp]
(
[temp_detail_id] [bigint] NOT NULL IDENTITY(1, 1),
[temp_id] [int] NULL,
[invoice_no] [varchar] (18) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL
) ON [PRIMARY]
GO
