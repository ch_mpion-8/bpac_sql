CREATE TABLE [dbo].[master_customer_address]
(
[address_id] [int] NOT NULL IDENTITY(1, 1),
[customer_company_id] [int] NOT NULL,
[title] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[address] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[sub_district] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[district] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[province] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[post_code] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[is_ems] [bit] NULL,
[is_allowed_edit] [bit] NULL CONSTRAINT [DF_master_customer_address_is_allowed_edit] DEFAULT ((1)),
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[copy_from_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_customer_address] ADD CONSTRAINT [PK_master_customer_address] PRIMARY KEY CLUSTERED  ([address_id]) ON [PRIMARY]
GO
