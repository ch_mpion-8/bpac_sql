CREATE TABLE [dbo].[VBRP]
(
[MANDT] [varchar] (3) COLLATE Thai_CI_AS NULL,
[VBELN] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[POSNR] [int] NULL,
[UEPOS] [int] NULL,
[FKIMG] [decimal] (13, 3) NULL,
[VRKME] [int] NULL,
[UMVKZ] [decimal] (5, 0) NULL,
[UMVKN] [decimal] (5, 0) NULL,
[MEINS] [int] NULL,
[SMENG] [decimal] (13, 3) NULL,
[FKLMG] [decimal] (13, 3) NULL,
[LMENG] [decimal] (13, 3) NULL,
[NTGEW] [decimal] (15, 3) NULL,
[BRGEW] [decimal] (15, 3) NULL,
[GEWEI] [int] NULL,
[VOLUM] [decimal] (15, 3) NULL,
[VOLEH] [int] NULL,
[GSBER] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[PRSDT] [datetime] NULL,
[FBUDA] [datetime] NULL,
[KURSK] [decimal] (9, 5) NULL,
[NETWR] [decimal] (15, 2) NULL,
[VBELV] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[POSNV] [int] NULL,
[VGBEL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VGPOS] [int] NULL,
[VGTYP] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[AUBEL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[AUPOS] [int] NULL,
[AUREF] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[MATNR] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[ARKTX] [nvarchar] (40) COLLATE Thai_CI_AS NULL,
[PMATN] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[CHARG] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[MATKL] [nvarchar] (9) COLLATE Thai_CI_AS NULL,
[PSTYV] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[POSAR] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[PRODH] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[VSTEL] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ATPKZ] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[SPART] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[POSPA] [int] NULL,
[WERKS] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ALAND] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[WKREG] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[WKCOU] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[WKCTY] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[TAXM1] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM2] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM3] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM4] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM5] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM6] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM7] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM8] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[TAXM9] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[KOWRR] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[PRSFD] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[SKTOF] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[SKFBP] [decimal] (13, 2) NULL,
[KONDM] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KTGRM] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KOSTL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[BONUS] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[PROVG] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[EANNR] [nvarchar] (13) COLLATE Thai_CI_AS NULL,
[VKGRP] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[VKBUR] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[SPARA] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[SHKZG] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[ERNAM] [nvarchar] (12) COLLATE Thai_CI_AS NULL,
[ERDAT] [datetime] NULL,
[ERZET] [time] NULL,
[BWTAR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[LGORT] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[STAFO] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[WAVWR] [decimal] (13, 2) NULL,
[KZWI1] [decimal] (13, 2) NULL,
[KZWI2] [decimal] (13, 2) NULL,
[KZWI3] [decimal] (13, 2) NULL,
[KZWI4] [decimal] (13, 2) NULL,
[KZWI5] [decimal] (13, 2) NULL,
[KZWI6] [decimal] (13, 2) NULL,
[STCUR] [decimal] (9, 5) NULL,
[UVPRS] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[UVALL] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[EAN11] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[PRCTR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[KVGR1] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[KVGR2] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[KVGR3] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[KVGR4] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[KVGR5] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MVGR1] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MVGR2] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MVGR3] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MVGR4] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MVGR5] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MATWA] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[BONBA] [decimal] (13, 2) NULL,
[KOKRS] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[PAOBJNR] [int] NULL,
[PS_PSP_PNR] [int] NULL,
[AUFNR] [nvarchar] (12) COLLATE Thai_CI_AS NULL,
[TXJCD] [nvarchar] (15) COLLATE Thai_CI_AS NULL,
[CMPRE] [decimal] (11, 2) NULL,
[CMPNT] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[CUOBJ] [int] NULL,
[CUOBJ_CH] [int] NULL,
[KOUPD] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[UECHA] [int] NULL,
[XCHAR] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[ABRVW] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[SERNR] [nvarchar] (8) COLLATE Thai_CI_AS NULL,
[BZIRK_AUFT] [nvarchar] (6) COLLATE Thai_CI_AS NULL,
[KDGRP_AUFT] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KONDA_AUFT] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[LLAND_AUFT] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MPROK] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[PLTYP_AUFT] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[REGIO_AUFT] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[VKORG_AUFT] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[VTWEG_AUFT] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[ABRBG] [datetime] NULL,
[PROSA] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[UEPVW] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[AUTYP] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[STADAT] [datetime] NULL,
[FPLNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[FPLTR] [int] NULL,
[AKTNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[KNUMA_PI] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[KNUMA_AG] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PREFE] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[MWSBP] [decimal] (13, 2) NULL,
[AUGRU_AUFT] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[FAREG] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[UPMAT] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[UKONM] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[CMPRE_FLT] [decimal] (16, 16) NULL,
[ABFOR] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[ABGES] [decimal] (16, 16) NULL,
[J_1ARFZ] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[J_1AREGIO] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1AGICD] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[J_1ADTYP] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[J_1ATXREL] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[J_1BCFOP] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW1] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW2] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1BTXSDC] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[BRTWR] [decimal] (15, 2) NULL,
[WKTNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[WKTPS] [int] NULL,
[RPLNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[KURSK_DAT] [datetime] NULL,
[WGRU1] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[WGRU2] [nvarchar] (18) COLLATE Thai_CI_AS NULL,
[KDKG1] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KDKG2] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KDKG3] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KDKG4] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[KDKG5] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[VKAUS] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1AINDXP] [nvarchar] (5) COLLATE Thai_CI_AS NULL,
[J_1AIDATEP] [datetime] NULL,
[KZFME] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[MWSKZ] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[VERTT] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[VERTN] [nvarchar] (13) COLLATE Thai_CI_AS NULL,
[SGTXT] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[DELCO] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[BEMOT] [nvarchar] (2) COLLATE Thai_CI_AS NULL,
[RRREL] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[AKKUR] [decimal] (9, 5) NULL,
[WMINR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VGBEL_EX] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VGPOS_EX] [int] NULL,
[LOGSYS] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[VGTYP_EX] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW3] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW4] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[J_1BTAXLW5] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MSR_ID] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[MSR_REFUND_CODE] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[MSR_RET_REASON] [nvarchar] (3) COLLATE Thai_CI_AS NULL,
[NRAB_KNUMH] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[NRAB_VALUE] [decimal] (13, 2) NULL,
[DISPUTE_CASE] [uniqueidentifier] NULL,
[FUND_USAGE_ITEM] [uniqueidentifier] NULL,
[CLAIMS_TAXATION] [nvarchar] (1) COLLATE Thai_CI_AS NULL,
[KURRF_DAT_ORIG] [datetime] NULL,
[AUFPL] [int] NULL,
[APLZL] [int] NULL,
[DPCNR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[DCPNR] [int] NULL,
[DPNRB] [int] NULL,
[PEROP_BEG] [datetime] NULL,
[PEROP_END] [datetime] NULL,
[FONDS] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[FISTL] [nvarchar] (16) COLLATE Thai_CI_AS NULL,
[FKBER] [nvarchar] (16) COLLATE Thai_CI_AS NULL,
[GRANT_NBR] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[PRS_WORK_PERIOD] [int] NULL,
[PPRCTR] [nvarchar] (10) COLLATE Thai_CI_AS NULL,
[PARGB] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[AUFPL_OAA] [int] NULL,
[APLZL_OAA] [int] NULL,
[CAMPAIGN] [uniqueidentifier] NULL,
[COMPREAS] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[ZZWI7] [decimal] (15, 2) NULL,
[ZZWI8] [decimal] (15, 2) NULL,
[ZZWI9] [decimal] (15, 2) NULL,
[ZZWI10] [decimal] (15, 2) NULL,
[ZZWI11] [decimal] (15, 2) NULL,
[ZZWI12] [decimal] (15, 2) NULL,
[ZZWI13] [decimal] (15, 2) NULL,
[ZZWI14] [decimal] (15, 2) NULL,
[ZZWI15] [decimal] (15, 2) NULL,
[ZZWI16] [decimal] (15, 2) NULL,
[ZZWI17] [decimal] (15, 2) NULL,
[ZZWI18] [decimal] (15, 2) NULL,
[ZZWI19] [decimal] (15, 2) NULL,
[ZZWI20] [decimal] (15, 2) NULL
) ON [PRIMARY]
GO
