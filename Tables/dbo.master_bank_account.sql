CREATE TABLE [dbo].[master_bank_account]
(
[bank_account_id] [int] NOT NULL IDENTITY(1, 1),
[bank_id] [int] NOT NULL,
[bank_account_no] [nvarchar] (100) COLLATE Thai_CI_AS NOT NULL,
[bank_account_name] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[bank_account_type] [nvarchar] (200) COLLATE Thai_CI_AS NOT NULL,
[branch] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[is_active] [bit] NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[master_bank_account] ADD CONSTRAINT [PK_master_bank_account] PRIMARY KEY CLUSTERED  ([bank_account_id]) ON [PRIMARY]
GO
