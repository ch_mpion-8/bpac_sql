CREATE TABLE [dbo].[master_customer]
(
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[customer_name] [nvarchar] (160) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[application_tag_id] [int] NULL,
[is_active] [bit] NULL,
[street] [nvarchar] (500) COLLATE Thai_CI_AS NULL,
[district] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[city] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[post_code] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[country] [varchar] (5) COLLATE Thai_CI_AS NULL,
[is_ignore] [bit] NULL CONSTRAINT [DF_master_customer_is_ignore] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[insert_or_update_master_customer]
   ON  [dbo].[master_customer]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @customer_company_id as int =null
		,@address as nvarchar(200) = null

	DECLARE customer_cursor CURSOR FAST_FORWARD FOR
		SELECT  customer_company_id, 
					inserted.street+' ' +inserted.district +' ' +inserted.city +' ' +inserted.post_code  as 'address'
		from master_customer_company a, inserted
		where a.customer_code = inserted.customer_code
	OPEN customer_cursor;

	FETCH NEXT from customer_cursor 
		INTO @customer_company_id, @address

	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (EXISTS(select 1 from master_customer_address b 
						where b.customer_company_id = @customer_company_id
						and b.is_allowed_edit = 0))
				BEGIN
					update b
					set b.address = @address
					from master_customer_address b
						where b.customer_company_id = @customer_company_id
						and b.is_allowed_edit = 0
				END
			ELSE
				BEGIN
					insert into master_customer_address(
						customer_company_id, 
						title,
						address,
						is_allowed_edit,
						is_active,
						created_by,
						created_date)
					values(
						@customer_company_id,
						'ภพ.20',
						@address,
						0,
						1,
						'SYSTEM',
						getdate()
					)
				END

				 if not exists(select 1 from [dbo].master_customer_address 
						where customer_company_id = @customer_company_id and isnull(is_ems,0) = 1)
					begin
						update dbo.master_customer_address
						set is_ems = 1
							,updated_date = getdate()
							,updated_by = 'SYSTEM'
						from dbo.master_customer_address
						where customer_company_id = @customer_company_id
						and title = 'ภพ.20'
					end 

			FETCH NEXT from customer_cursor 
				INTO @customer_company_id, @address
		END

	CLOSE customer_cursor;
	DEALLOCATE customer_cursor;

END
GO
ALTER TABLE [dbo].[master_customer] ADD CONSTRAINT [PK_master_customer] PRIMARY KEY CLUSTERED  ([customer_code]) ON [PRIMARY]
GO
