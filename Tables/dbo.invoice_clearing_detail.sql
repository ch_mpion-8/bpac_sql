CREATE TABLE [dbo].[invoice_clearing_detail]
(
[invoice_no] [nchar] (18) COLLATE Thai_CI_AS NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[assignment_no] [varchar] (18) COLLATE Thai_CI_AS NULL,
[year] [varchar] (4) COLLATE Thai_CI_AS NOT NULL,
[accounting_document_no] [varchar] (10) COLLATE Thai_CI_AS NOT NULL,
[clearing_document_no] [varchar] (10) COLLATE Thai_CI_AS NULL,
[line_no] [varchar] (3) COLLATE Thai_CI_AS NOT NULL,
[clearing_date] [date] NULL,
[posting_date] [date] NULL,
[document_date] [date] NULL,
[created_date] [date] NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[reference_document] [varchar] (16) COLLATE Thai_CI_AS NULL,
[reference_clearing_document] [varchar] (10) COLLATE Thai_CI_AS NULL,
[indicator] [varchar] (10) COLLATE Thai_CI_AS NULL,
[document_type] [varchar] (2) COLLATE Thai_CI_AS NULL,
[period] [varchar] (2) COLLATE Thai_CI_AS NULL,
[local_amount] [decimal] (13, 2) NULL,
[document_amount] [decimal] (13, 2) NULL,
[gl_account] [varchar] (10) COLLATE Thai_CI_AS NULL,
[base_line_due_date] [date] NULL,
[payment_reference] [varchar] (30) COLLATE Thai_CI_AS NULL,
[bsad_move] [varchar] (10) COLLATE Thai_CI_AS NULL,
[is_reverse] [bit] NULL,
[is_actived] [bit] NULL,
[is_count] [bit] NULL,
[created_date_time] [datetime] NULL CONSTRAINT [DF_invoice_clearing_detail_created_date_time] DEFAULT (getdate()),
[updated_date_time] [datetime] NULL CONSTRAINT [DF_invoice_clearing_detail_updated_date_time] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[invoice_clearing_detail] ADD CONSTRAINT [PK_invoice_clearing_detail] PRIMARY KEY CLUSTERED  ([company_code], [year], [accounting_document_no], [line_no]) ON [PRIMARY]
GO
