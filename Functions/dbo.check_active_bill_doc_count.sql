SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[check_active_bill_doc_count](
 @invoice_no  nvarchar(18),
 @bill_presentment_id as int 
) returns int as
begin

	declare @ret as int,@company_code nvarchar(4)

	select @company_code = c.company_code
	from dbo.bill_presentment_control_document d
	inner join dbo.master_customer_company c on d.customer_company_id = c.customer_company_id
	where bill_presentment_id = @bill_presentment_id

	select @ret = count(1) 
	from dbo.bill_presentment_control_document_item i
	inner join dbo.bill_presentment_control_document d on i.bill_presentment_id = d.bill_presentment_id
	inner join dbo.master_customer_company c on d.customer_company_id = c.customer_company_id
	where c.company_code = @company_code
			and invoice_no = @invoice_no 
			and i.is_active = 1
	
	if(@ret is null)
		set @ret = 0

	return @ret
end
GO
