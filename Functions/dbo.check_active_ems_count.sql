SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[check_active_ems_count](
 @ref_id  int 
,@ref_type as nvarchar(10) 
) returns int as
begin

	declare @ret as int
	select @ret = count(1) 
	from dbo.ems_sub_item
	where [reference_id] = @ref_id 
			and reference_type = @ref_type 
			and is_active = 1
	if(@ret is null)
		set @ret = 0

	return @ret
end
GO
