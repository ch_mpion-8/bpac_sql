SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[check_active_other_doc_count](
 @document_id int
 ,@reference_no  nvarchar(18)
 ,@action_type as int
) returns int as
begin

	declare @ret as int,@document_type int  ,@company_code varchar(4),@customer_code varchar(30) ,@year varchar(4)

	select @document_type = d.document_type,@company_code = d.company_code,@customer_code = receiver
	from dbo.[other_control_document] d
	where document_id = @document_id

	select @year = [year]
	from dbo.other_control_document_item 
	where document_id = @document_id
	and reference_no = @reference_no
	and action_type =@action_type

	if @document_type = 1
	begin
		select @ret = count(1) 
		from dbo.[other_control_document_item] i 
		inner join dbo.[other_control_document] d on i.document_id = d.document_id
		where i.reference_no = @reference_no
				and d.document_type = 1
				and i.action_type = @action_type 
				and d.company_code = @company_code
				and d.receiver = @customer_code
				and i.[year] = @year
				and isnull(d.status,'') <> 'CANCEL'
	end
	else if @document_type <> 4 -- 4 --> ใบคุมเอกสารอื่นๆ ไม่จำเป็นต้องเช็ค
	begin
		select @ret = count(1) 
		from dbo.[other_control_document_item] i 
		inner join dbo.[other_control_document] d on i.document_id = d.document_id
		where i.reference_no = @reference_no
				and d.document_type = @document_type
				and i.action_type = @action_type 
				and d.company_code = @company_code
				and isnull(d.status,'') <> 'CANCEL'
	end
			
	if(@ret is null)
		set @ret = 0

	return  @ret
end
GO
