SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[get_next_holiday]
(
	-- Add the parameters for the function here
	@date date = null--'20161231'
	,@holiday_type nvarchar(20) = null --'SCG'
	,@customer_code nvarchar(20) = null --'2007211' 
)
RETURNS date
AS
BEGIN
	-- Declare the return variable here
	
	declare @newdate as date
		,@numDays int = 0

	if datepart(dw, @date) = 1   set @numdays = @numdays + 1
	else if datepart(dw, @date) = 7   set @numdays = @numdays + 2
	set @newdate =  DATEADD(d, @numDays, @date)


	--while exists ( select 1 from dbo.master_holiday 
	--					where holiday_year = year(@newdate) 
	--						and holiday_month = month(@newdate) 
	--						and holiday_day = day(@newdate)
	--						and holiday_type = @holiday_type
	--						and [status] = 'ACTIVE'
	--						and datediff(day,compensate_date,@date) >0
	--					)
	--begin
	--set @newdate =  DATEADD(d, 1, @newdate)	
	--end

	
	if(@holiday_type = 'CUSTOMER')
		begin
			while exists ( select 1 from dbo.master_customer_holiday 
								where holiday_year = year(@newdate) 
									and holiday_month = month(@newdate) 
									and holiday_day = day(@newdate)
									and customer_code = @customer_code
									and [is_active] = 1
									and datediff(day,compensate_date,@date) >0
								)
			begin
			set @newdate =  DATEADD(d, 1, @newdate)	
			end
		end
	else
		begin
			while exists ( select 1 from dbo.master_holiday 
								where holiday_year = year(@newdate) 
									and holiday_month = month(@newdate) 
									and holiday_day = day(@newdate)
									and holiday_type = @holiday_type
									and [is_active] = 1
									and datediff(day,compensate_date,@date) >0
								)
			begin
			set @newdate =  DATEADD(d, 1, @newdate)	
			end
		end

	return @newdate


END



GO
