SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[check_active_cheque_doc_count](
 @cheque_control_id int 
 ,@receipt_no  nvarchar(18) 
 ,@year as int
) returns int as
begin

	declare @ret as int,@company_code nvarchar(4)

	select @company_code = c.company_code
	from dbo.cheque_control_document d
	inner join dbo.master_customer_company c on d.customer_company_id = c.customer_company_id
	where cheque_control_id = @cheque_control_id

	select @ret = count(1) 
	from dbo.cheque_control_document_item i 
	inner join dbo.cheque_control_document d on i.cheque_control_id = d.cheque_control_id
	inner join dbo.master_customer_company c on d.customer_company_id = c.customer_company_id
	where c.company_code = @company_code
			and reference_document_no = @receipt_no 
			and i.document_year = @year
			and i.is_active = 1
			
	if(@ret is null)
		set @ret = 0

	return @ret
end
GO
