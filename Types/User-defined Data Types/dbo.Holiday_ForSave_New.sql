CREATE TYPE [dbo].[Holiday_ForSave_New] AS TABLE
(
[holiday_year] [int] NULL,
[holiday_month] [int] NULL,
[holiday_day] [int] NULL,
[holiday_name] [varchar] (255) COLLATE Thai_CI_AS NULL
)
GO
