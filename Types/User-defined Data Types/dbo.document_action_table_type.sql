CREATE TYPE [dbo].[document_action_table_type] AS TABLE
(
[reference_no] [varchar] (500) COLLATE Thai_CI_AS NULL,
[action_type] [int] NULL,
[type] [varchar] (10) COLLATE Thai_CI_AS NULL
)
GO
