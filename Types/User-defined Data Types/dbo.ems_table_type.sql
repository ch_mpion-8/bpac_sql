CREATE TYPE [dbo].[ems_table_type] AS TABLE
(
[ems_detail_id] [int] NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[reference_no_list] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
