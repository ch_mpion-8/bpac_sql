CREATE TYPE [dbo].[JvPycTable] AS TABLE
(
[document_date] [date] NULL,
[tprc] [int] NULL,
[smpc] [int] NULL,
[roc] [int] NULL,
[moc] [int] NULL
)
GO
