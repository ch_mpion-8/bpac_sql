CREATE TYPE [dbo].[OtherDocumentList] AS TABLE
(
[document_no] [varchar] (20) COLLATE Thai_CI_AS NULL,
[document_type] [int] NULL,
[action_type] [int] NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NULL,
[receiver] [varchar] (10) COLLATE Thai_CI_AS NULL,
[method] [varchar] (20) COLLATE Thai_CI_AS NULL,
[document_date] [datetime] NULL,
[user_code] [varchar] (50) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[status] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[created_date] [datetime] NULL,
[created_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[updated_date] [datetime] NULL,
[updated_by] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[document_receive] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[document_send] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[document_sendreceive] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
