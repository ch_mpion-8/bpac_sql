CREATE TYPE [dbo].[ZeroloanList_New] AS TABLE
(
[zero_loan_detail_id] [int] NULL,
[zero_loan_id] [int] NULL,
[reference_document_no] [nvarchar] (50) COLLATE Thai_CI_AS NULL,
[cheque_control_id] [int] NULL,
[zero_loan_status] [int] NULL,
[created_by] [datetime] NULL,
[updated_by] [datetime] NULL,
[company_code] [nvarchar] (4) COLLATE Thai_CI_AS NULL,
[amount] [decimal] (18, 0) NULL
)
GO
