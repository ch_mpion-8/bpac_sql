CREATE TYPE [dbo].[e_tax_allowance] AS TABLE
(
[exchange_document_id] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[charge_indicator] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[actual_amount] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
