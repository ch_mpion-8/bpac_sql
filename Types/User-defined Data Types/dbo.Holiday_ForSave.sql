CREATE TYPE [dbo].[Holiday_ForSave] AS TABLE
(
[holiday_year] [varchar] (255) COLLATE Thai_CI_AS NULL,
[holiday_month] [varchar] (255) COLLATE Thai_CI_AS NULL,
[holiday_day] [varchar] (255) COLLATE Thai_CI_AS NULL,
[holiday_name] [varchar] (255) COLLATE Thai_CI_AS NULL
)
GO
