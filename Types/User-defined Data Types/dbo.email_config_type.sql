CREATE TYPE [dbo].[email_config_type] AS TABLE
(
[document_type] [nvarchar] (50) COLLATE Thai_CI_AS NOT NULL,
[email_type] [nvarchar] (50) COLLATE Thai_CI_AS NOT NULL,
[email_address] [nvarchar] (50) COLLATE Thai_CI_AS NOT NULL
)
GO
