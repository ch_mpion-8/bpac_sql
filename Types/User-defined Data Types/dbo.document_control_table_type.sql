CREATE TYPE [dbo].[document_control_table_type] AS TABLE
(
[customer_company_id] [int] NULL,
[date] [date] NULL,
[method] [nvarchar] (20) COLLATE Thai_CI_AS NULL,
[payment_term] [varchar] (4) COLLATE Thai_CI_AS NULL,
[currency] [varchar] (5) COLLATE Thai_CI_AS NULL,
[remark] [nvarchar] (2000) COLLATE Thai_CI_AS NULL,
[reference_no_list] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
