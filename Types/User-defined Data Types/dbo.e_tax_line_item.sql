CREATE TYPE [dbo].[e_tax_line_item] AS TABLE
(
[exchange_document_id] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[line_item_no] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[product_name] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[gross_price] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[line_trade_delivery] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[monetary_tax_total_amount] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[monetary_net_line_amount] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[monetary_net_include_amount] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
