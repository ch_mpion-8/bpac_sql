CREATE TYPE [dbo].[document_table_type] AS TABLE
(
[document_code] [nvarchar] (200) COLLATE Thai_CI_AS NULL,
[custom_document] [nvarchar] (max) COLLATE Thai_CI_AS NULL
)
GO
