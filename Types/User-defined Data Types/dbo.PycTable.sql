CREATE TYPE [dbo].[PycTable] AS TABLE
(
[document_no] [varchar] (50) COLLATE Thai_CI_AS NULL,
[document_date] [datetime] NULL,
[company_code] [varchar] (4) COLLATE Thai_CI_AS NULL,
[customer_code] [varchar] (10) COLLATE Thai_CI_AS NULL,
[count_trip] [int] NULL,
[trip_date] [date] NULL,
[is_skip] [bit] NULL,
[document_id] [int] NULL,
[reference_type] [int] NULL,
[process_id] [int] NULL,
[remark] [varchar] (max) COLLATE Thai_CI_AS NULL
)
GO
