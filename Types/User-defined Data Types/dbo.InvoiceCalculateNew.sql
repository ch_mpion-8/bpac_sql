CREATE TYPE [dbo].[InvoiceCalculateNew] AS TABLE
(
[invoice_no] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[bill_prement_date] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[bill_prement_notify_date] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[bill_prement_log] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[currency] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[collection_prement_date] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[collection_prement_notify_date] [nvarchar] (100) COLLATE Thai_CI_AS NULL,
[collection_prement_log] [nvarchar] (max) COLLATE Thai_CI_AS NULL,
[billing_behavior_id] [int] NULL,
[collection_behavior_id] [int] NULL
)
GO
