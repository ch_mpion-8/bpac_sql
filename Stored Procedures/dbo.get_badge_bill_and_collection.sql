SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_badge_bill_and_collection]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
declare @user_name as nvarchar(50) = 'panaphak'

-- Bill Following
select *
from dbo.invoice_header ih
inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
inner join dbo.master_customer cus on ih.customer_code = cus.customer_code	and isnull(cus.is_ignore,0) = 0
left join dbo.master_customer_bill_presentment_behavior cbb 
		on cbb.bill_presentment_behavior_id = ih.bill_presentment_behavior_id					
WHERE (
			(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
			or ih.invoice_type in ('RC','RD','DA')
		)
		and isnull(cbb.is_bill_presentment,1) = 1
		and (ih.bill_presentment_notify_date is null or ih.bill_presentment_notify_date <= getdate() )
		and isnull(ih.bill_presentment_status,'WAITING') in ('WAITING','ERROR','MANUAL')
		and  (@user_name is null or cc.user_name = @user_name) 


-- Bill Control
select *
from dbo.bill_presentment_control_document b
inner join  dbo.master_customer_company cc on b.customer_company_id = cc.customer_company_id 
where bill_presentment_status = 'WAITINGRECEIVED'
		and  (@user_name is null or cc.user_name = @user_name) 




-- Collection Following
select *
from dbo.invoice_header ih
inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
inner join dbo.master_customer cus on ih.customer_code = cus.customer_code	and isnull(cus.is_ignore,0) = 0					
WHERE  (ih.collection_notify_date is null or ih.collection_notify_date <= getdate() )
		and isnull(ih.collection_status,'WAITING') in ('WAITING','ERROR','MANUAL')
		and  (@user_name is null or cc.user_name = @user_name) 


-- Cheque Control --Waiting -- Red
select *
	from dbo.receipt re
	inner join  dbo.master_customer_company cc on re.customer_code = cc.customer_code and re.company_code = cc.company_code
	inner join dbo.master_customer cus on re.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	left join (dbo.cheque_control_document_item cci 
		 JOIN dbo.cheque_control_document ccd 
			on cci.cheque_control_id = ccd.cheque_control_id
			and ccd.cheque_control_status <> 'CANCEL'
			)
	on re.reference_document_no = cci.reference_document_no
	 and cci.is_active = 1 
	 and cc.customer_company_id = ccd.customer_company_id
	where re.receipt_type = 'advance'
			and  (@user_name is null or cc.user_name = @user_name)
			and ccd.cheque_control_id is null


-- Cheque Control --Waiting -- Yellow
select *
from dbo.cheque_control_document c
inner join  dbo.master_customer_company cc on c.customer_company_id = cc.customer_company_id 
where cheque_control_status = 'WAITINGRECEIVED'
			and  (@user_name is null or cc.user_name = @user_name)
END

GO
