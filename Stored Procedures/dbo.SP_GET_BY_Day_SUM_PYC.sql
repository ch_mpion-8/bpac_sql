SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_BY_Day_SUM_PYC]
	-- Add the parameters for the stored procedure here
	@start_date varchar(30)=null,
	@end_date varchar(30)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
set LANGUAGE us_english
	

	;with other_doc as (
	select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.document_id as [document_id],o.document_no,mcom.company_code,mcus.customer_code,o.document_date,document_date as trip_date ,1 as reference_type 
	from [dbo].[other_control_document] o left join master_company mcom
	on o.company_code = mcom.company_code
	left join master_customer mcus on mcus.customer_code = o.receiver
	left join dbo.scg_pyc_process pyc on pyc.reference_no_id = o.document_id and pyc.reference_type = 1
	where o.method = 'PYC' 
	),
		cheque as (
	select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.cheque_control_id as [document_id],o.[cheque_control_no] as document_no,mcc.company_code,mcc.customer_code,o.collection_date as document_date,o.collection_date as trip_date,2 as reference_type from cheque_control_document o left join master_customer_company mcc
	on o.customer_company_id = mcc.customer_company_id 
	left join dbo.scg_pyc_process pyc
	on pyc.reference_no_id = o.cheque_control_id
	and pyc.reference_type = 2
	where o.method = 'PYC'
	),
		bill as (
	select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.bill_presentment_id as [document_id],o.bill_presentment_no as document_no,mcc.company_code,mcc.customer_code,bill_presentment_date  as document_date,o.bill_presentment_date as trip_date,3 as reference_type from bill_presentment_control_document o left join master_customer_company mcc
	on o.customer_company_id = mcc.customer_company_id 
	left join dbo.scg_pyc_process pyc
	on pyc.reference_no_id = o.bill_presentment_id
	and pyc.reference_type =3
	where o.method = 'PYC'
	)
	,
		all_doc as (
		select * from other_doc 
		union all
		select * from cheque
		union all
		select * from bill
	)

	select t.trip_date,sum(t.performance) as performance,sum(t.plastic) as plastic,sum(t.ico) as ico,sum(t.moc) as moc,
	sum(t.roc) as roc,sum(t.smpc) as smpc,sum(t.tprc) as tprc
	from (
		select pv.trip_date,isnull(pv.performance,0) as performance,isnull(pv.plastic,0) as plastic,isnull(pv.ico,0) as ico
		,isnull(pv.tprc,0) as tprc,isnull(pv.smpc,0) as smpc,isnull(pv.roc,0) as roc,isnull(pv.moc,0)  as moc 
		from (
				select 
				convert(date,a.trip_date) as trip_date , 0 as count_trip
				,case a.company_code
					when 230 then 'performance' 
					when 2220 then 'ico'
					when 480 then 'plastic'
					when 470 then 'roc'
					when 7850 then 'moc'
					end as company
				from all_doc a 
				where year(a.trip_date) >= year(@start_date) and year(a.trip_date)<=year(@end_date)
				 and ( year(a.trip_date) < year(@end_date) or month(a.trip_date) >= month(@start_date) and month(a.trip_date)<=month(@end_date))

		) as t
		pivot (
			count(count_trip) for company in (performance,plastic,ico,tprc,smpc,roc,moc)
		) as PV

		union

		select pv.document_date,isnull(pv.performance,0) as performance,isnull(pv.plastic,0) as plastic,isnull(pv.ico,0) as ico
		,isnull(pv.tprc,0) as tprc,isnull(pv.smpc,0) as smpc,isnull(pv.roc,0) as roc,isnull(pv.moc,0)  as moc 
		from (
				select j.document_date,j.count_trip,c.company_name 
				from dbo.jv_pyc_process j
				inner join dbo.jv_company c
				on c.company_id = j.company_id
				and year(j.document_date) >= year(@start_date) and year(j.document_date)<=year(@end_date)
				and ( year(j.document_date)<Year(@end_date) or month(j.document_date) >= month(@start_date) and month(j.document_date)<=month(@end_date))
		) as t
		pivot (
			sum(count_trip) for company_name in (performance,plastic,ico,tprc,smpc,roc,moc)
		) as PV

	) as t
	group by t.trip_date


END
GO
