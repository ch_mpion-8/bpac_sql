SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[get_cheque_document_control_draft]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select bt.[temp_id]
      ,bt.[customer_company_id]
	  ,cc.company_code
	  ,cc.customer_code
	  ,cus.customer_name
      ,bt.collection_date
      ,bt.[method] as receive_method
      ,bt.[payment_term]
      ,bt.[currency]
      ,bt.[remark]
	  ,cc.user_display_name
	  ,subReceipt.receipt_no as receipt_list
	  ,sumamount.receipt_amount as receipt_amount
      ,bt.[created_date]
      ,bt.[created_by]
      ,bt.[updated_date]
      ,bt.[updated_by]
  from [dbo].cheque_temp bt
  inner join dbo.master_customer_company cc on bt.customer_company_id = cc.customer_company_id
  inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
  inner join 
  (
		select  main.temp_id,	stuff(
					(
						select ', ' + reference_document_no
							from dbo.cheque_item_temp sub
							where sub.temp_id = main.temp_id and sub.[is_active] = 1
							for xml path('')
					) ,1,1,'') as receipt_no
					from dbo.cheque_temp main
  ) subReceipt on bt.temp_id = subReceipt.temp_id
  inner join 
  (
		select temp_id, sum(subih.document_amount) as receipt_amount
		from dbo.cheque_item_temp sub
		inner join dbo.receipt subih on sub.reference_document_no = subih.reference_document_no
		where sub.[is_active] = 1
		group by temp_id
	) sumamount on bt.temp_id = sumamount.temp_id


END


GO
