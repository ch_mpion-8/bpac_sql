SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_ems_document_control_for_print]
	-- Add the parameters for the stored procedure here
	@ems_control_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
	select distinct e.ems_control_document_id
			,e.ems_control_document_no
			,0 as ems_detail_id --ei.ems_detail_id
			,ei.customer_code as customer_code
			,ei.remark
			,case when e.is_manual = 0 then isnull(isnull(ot.receiver_name, c.customer_name),ei.customer_code)
				else isnull(c.customer_name, oc.contact_name) end as customer_name
			,convert(varchar, e.send_date, 103) as send_date
			,e.department
			,e.cost_center
			,e.cost_code
			,isnull(com.company_code,j.company_code) as company_code
			,isnull(com.company_name,j.company_full_name) as company_name
	from dbo.ems_control_document e
	inner join dbo.ems_control_document_item ei on e.ems_control_document_id = ei.ems_control_document_id --and ei.is_active = 1
	left join dbo.master_customer c on ei.customer_code = c.customer_code
	left join dbo.master_company com on e.company_code = com.company_code
	left join dbo.jv_company j on e.company_code = j.company_code
	left join dbo.master_other_contact oc on ei.customer_code = cast(oc.contact_id as varchar(10))
	left join dbo.ems_sub_item esi on  ei.ems_detail_id = esi.ems_detail_id and esi.reference_type = 'O'  --and esi.is_active = 1
	left join dbo.other_control_document ot on esi.reference_id = ot.document_id
	where e.ems_control_document_id in (select item from dbo.SplitString(@ems_control_list,','))
			and (ei.is_active = 1 or (e.[status] = 'CANCEL' and ei.is_active = 0))

END
GO
