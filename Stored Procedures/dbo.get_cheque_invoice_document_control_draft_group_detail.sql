SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cheque_invoice_document_control_draft_group_detail]
	@invoice_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET NOCOUNT ON;
	;with docList as (
		select substring(item,0,charindex('|', item)) as customer_company_id
					-- ,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as invoice_no 
			 ,right(item,charindex('|', reverse(item)) -1 ) as invoice_no
					from dbo.SplitString(@invoice_list,',')
	)
		select distinct cc.customer_company_id
				,ih.company_code
				,com.company_name
				,ih.customer_code
				,cus.customer_name
				, isnull(cbb.receipt_method ,'PYC') as receipt_method
				, ih.currency
				, ih.invoice_no as invoice_list
				, ih.remaining_amount as document_amount
				, ih.invoice_date as document_date
				,year(ih.invoice_date) as document_year --r.[year]  as document_year
				, ih.invoice_amount + isnull(ih.tax_amount,0)  as original_amount
		from dbo.invoice_header ih
		inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
		inner join dbo.master_customer cus on ih.customer_code = cus.customer_code
		inner join dbo.master_company com on ih.company_code = com.company_code
		left join dbo.master_customer_collection_behavior cbb 
				on cc.customer_company_id = cbb.customer_company_id
						and ih.collection_behavior_id = cbb.collection_behavior_id
		inner join docList d on cc.customer_company_id = d.customer_company_id 
					and ih.invoice_no = d.invoice_no

		union 
		select distinct ct.customer_company_id
				,cc.company_code
				,com.company_name
				,cc.customer_code
				,cus.customer_name
				, ct.method as receipt_method
				, ct.currency
				, ci.invoice_no as invoice_list
				, ih.remaining_amount as document_amount
				,ih.invoice_date AS document_date
				,ci.document_year as document_year --r.[year]  as document_year
				, ih.invoice_amount + isnull(ih.tax_amount,0)  as original_amount
		from dbo.[cheque_invoice_item_temp] ci
		inner join dbo.[cheque_invoice_temp] ct on ci.temp_id = ct.temp_id
		inner join dbo.master_customer_company cc on ct.customer_company_id = cc.customer_company_id
		inner join dbo.master_company com on cc.company_code = com.company_code
		inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
		inner join dbo.invoice_header ih on ci.invoice_no = ih.invoice_no
		where ci.temp_id in (
			select t.temp_id
				from dbo.[cheque_temp] t
				inner join dbo.[cheque_item_temp] bi on t.temp_id = bi.temp_id
				where bi.reference_document_no in (select docList.invoice_no from docList)
		) and ci.is_active = 1
END
GO
