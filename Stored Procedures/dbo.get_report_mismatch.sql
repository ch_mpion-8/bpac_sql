SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_report_mismatch]
	-- Add the parameters for the stored procedure here
	@start_date datetime = null,
	@end_date datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from zero_loan inner join zero_loan_item
	on zero_loan.zero_loan_id = zero_loan_item.zero_loan_id
	inner join cheque_control_document_item cci on cci.cheque_control_detail_id = zero_loan_item.cheque_control_id
	inner join cheque_control_document ccd on cci.cheque_control_id = ccd.cheque_control_id
	inner  join master_customer_company mcc on ccd.customer_company_id = mcc.customer_company_id
	inner join (
			
			select  reference_document_no,sum(abs(document_amount)) as document_amount,customer_code,company_code,currency
			from dbo.receipt
			--where document_type = 'RA'
			group by reference_document_no,customer_code,company_code,currency
	) re on cci.reference_document_no = re.reference_document_no
		and mcc.customer_code = re.customer_code
	and mcc.company_code = re.company_code
	inner join master_customer mc on mc.customer_code = mcc.customer_code
	inner join master_company mcom on mcc.company_code = mcom.company_code
	where 
	((ccd.collection_date Between @start_date AND @end_date) OR @start_date is null OR @end_date is null )
	and 
	zero_loan_item.zero_loan_status = 3
	order by zero_loan.zero_loan_date,ccd.collection_date,ccd.cheque_control_no


END

GO
