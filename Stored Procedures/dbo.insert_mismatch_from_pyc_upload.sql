SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[insert_mismatch_from_pyc_upload]
	-- Add the parameters for the stored procedure here
	@mismatch_amount decimal(18, 2) =null,
	@date date =null,
	@submited_by varchar(50) =null,
	@company_code varchar(4) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if exists (select 1 from dbo.mismatch where company_code = @company_code and mismatch_date = @date)
		update dbo.mismatch
		set amount = @mismatch_amount,
		updated_by = @submited_by,
		updated_date = getdate()
		where company_code = @company_code and mismatch_date = @date
	else
		insert into mismatch(mismatch_date,mismatch_status,amount,submit_by,created_by,created_date,company_code)
		values(@date,1,@mismatch_amount,@submited_by,@submited_by,getdate(),@company_code)
	
	select convert(bit,1) as valid
END
GO
