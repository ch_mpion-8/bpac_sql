SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_zero_loan_amount]
	-- Add the parameters for the stored procedure here
	@date date =null,
	@company_code varchar(4) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if exists(select 1  from zero_loan
	where zero_loan_date = @date
	and company_code = @company_code)
	begin
		select sum(document_amount) as zero_loan_amount 
		from dbo.zero_loan_item i
		inner join dbo.zero_loan h
			on h.zero_loan_id = i.zero_loan_id
		inner join dbo.cheque_control_document_item ch
			on ch.cheque_control_id = i.cheque_control_id
		inner join dbo.cheque_control_document_item ci
			on ci.cheque_control_id = ch.cheque_control_id
			and ci.is_active = 1
			and ci.reference_document_no = i.reference_document_no
		inner join dbo.receipt r
			on r.reference_document_no = i.reference_document_no
			and r.[year] = ci.document_year
			and r.company_code = h.company_code
		where h.company_code = @company_code
			and h.zero_loan_date = @date
		--select sum(document_amount) as zero_loan_amount from dbo.zero_loan_item i
		--inner join dbo.zero_loan h
		--	on h.zero_loan_id = i.zero_loan_id
		--inner join dbo.receipt r
		--	on r.reference_document_no = i.reference_document_no
		--where h.company_code = @company_code
		--	and h.zero_loan_date = @date
		--	and r.company_code = @company_code
	end	
	else
		select null as zero_loan_amount
END
GO
