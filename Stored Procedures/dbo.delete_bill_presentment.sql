SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[delete_bill_presentment]
	-- Add the parameters for the stored procedure here
	 @bill_presentment_id as int = null--1
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update dbo.bill_presentment_control_document
	set bill_presentment_status = 'CANCEL'
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 'CANCEL',*
	from dbo.bill_presentment_control_document
	where bill_presentment_id = @bill_presentment_id

	update dbo.bill_presentment_control_document_item
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 0,*
	from dbo.bill_presentment_control_document_item
	where bill_presentment_id = @bill_presentment_id

		exec dbo.update_invoice_bill_presentment_status
		@invoice_no = null
		,@bill_presentment_id = @bill_presentment_id
		,@status  = 'WAITING'
		,@updated_by  = @updated_by

		-- Moom Added 20-12-2017
	UPDATE dbo.invoice_header set collection_calculation_log = '',collection_date = null,collection_notify_date = null
	where invoice_no in (
	select ih.invoice_no from dbo.bill_presentment_control_document b
	inner join dbo.bill_presentment_control_document_item bi on
	bi.bill_presentment_id = b.bill_presentment_id
	inner join dbo.invoice_header ih on
	ih.invoice_no = bi.invoice_no
	inner join master_customer_company mcc
	on 
	ih.company_code = mcc.company_code
	and
	ih.customer_code = mcc.customer_code
	inner join master_customer_collection_behavior mcb
	on
	mcb.payment_term_code = ih.payment_term
	and mcb.customer_company_id = mcc.customer_company_id
	where mcb.collection_condition_date = 'Billing Date'
	and b.bill_presentment_id = @bill_presentment_id
	)
	--End Moom Added 


	if exists(select 1 from dbo.bill_presentment_control_document where bill_presentment_id = @bill_presentment_id and convert(date,getdate())<convert(date,bill_presentment_date) and method = 'PYC')
			begin
				if exists (select 1 from dbo.scg_pyc_process where reference_no_id = @bill_presentment_id and reference_type = 3 )
				begin
					update dbo.scg_pyc_process
					set remark = '-.. . .-.. . - . -..',
					updated_by = @updated_by,
					updated_date = getdate(),
					is_skip = 1
					where reference_no_id = @bill_presentment_id
					and reference_type = 3
				end
				else
				begin
					
					declare @trip_date as date = null
					select @trip_date = bill_presentment_date
					from dbo.bill_presentment_control_document where bill_presentment_id = @bill_presentment_id 

						insert into scg_pyc_process(
							reference_no_id,
							reference_type,
							remark,
							created_by,
							created_date,
							count_trip,
							is_skip,
							trip_date
						)
						values(
							@bill_presentment_id,
							3,
							'-.. . .-.. . - . -..',
							@updated_by,
							getdate(),
							1,
							1,
							@trip_date)
				end

			end

			
	exec dbo.insert_bill_presentment_history
			@bill_presentment_id = @bill_presentment_id
			,@description =  'Cancel Document Control'
			,@action  = 'Update status'
			,@updated_by  = @updated_by

END
GO
