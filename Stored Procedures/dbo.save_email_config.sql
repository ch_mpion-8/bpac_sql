SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_email_config]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(4) = null
	,@customer_code as varchar(50) = null
	,@sales_org as nvarchar(10) = null
	,@sold_to as nvarchar(10) = null
	,@ship_to as nvarchar(10) = null
	,@number_of_copies as int = null
	,@output_type_text as nvarchar(50) = null
	,@output_type_code as nvarchar(50) = null
	,@send_email_type as nvarchar(50) = null
	,@is_active as bit = null
	,@updated_by as nvarchar(200) = null
	,@email_config_detail as email_config_type readonly
	,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @customer_company_id as int = null

	select @customer_company_id = customer_company_id
	from master_customer_company
	where  right('0000000000' + customer_code,10) = right('0000000000' + @customer_code,10) and company_code = @company_code

	if(@customer_company_id is null or @customer_company_id = 0)
	begin 
		set @error_message = 'Cannot find customer or company in database'
		return;
	end

	if (exists (select 1 from master_email_config where customer_company_id = @customer_company_id))
	begin
		update master_email_config
		set sales_org = @sales_org
			,sold_to = @sold_to
			,ship_to = @ship_to
			,number_of_copies = @number_of_copies
			,output_type_text = @output_type_text
			,output_type_code = @output_type_code
			,send_email_type = @send_email_type
			,is_active = @is_active
			,updated_by = @updated_by
			,updated_date = getdate()
		where customer_company_id = @customer_company_id
	end
	else
	begin
		insert into master_email_config
		(
			customer_company_id
			, sales_org
			, sold_to
			, ship_to
			, number_of_copies
			, output_type_text
			, output_type_code
			, send_email_type
			, is_active
			, created_date
			, created_by
			, updated_date
			, updated_by
		)
		values(
			@customer_company_id
			, @sales_org
			, @sold_to
			, @ship_to
			, @number_of_copies
			, @output_type_text
			, @output_type_code
			, @send_email_type
			, @is_active
			, getdate()
			, @updated_by
			, getdate()
			, @updated_by
		)
	end

	update ed
	set is_active = 0
		, updated_by = @updated_by
		, updated_date = getdate()
	from master_email_config_detail ed
	left join @email_config_detail temp on ed.document_type = temp.document_type
											and ed.email_type = temp.email_type
											and ed.email_address = temp.email_address
	where ed.customer_company_id = @customer_company_id and temp.email_address is null

	
	update ed
	set is_active = 1
		, updated_by = @updated_by
		, updated_date = getdate()
	from master_email_config_detail ed
	inner join @email_config_detail temp on ed.document_type = temp.document_type
											and ed.email_type = temp.email_type
											and ed.email_address = temp.email_address
	where ed.customer_company_id = @customer_company_id 

	insert into master_email_config_detail
	select @customer_company_id
			,temp.document_type
			,temp.email_type
			,temp.email_address
			,1
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from @email_config_detail temp
	left join master_email_config_detail ed on  ed.document_type = temp.document_type
											and ed.email_type = temp.email_type
											and ed.email_address = temp.email_address
	where ed.email_address is null

END

GO
