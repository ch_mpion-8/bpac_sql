SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_client_login]
	-- Add the parameters for the stored procedure here
	@ip varchar(12) =null,
	@username varchar(50) =null,
	@is_success bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @date datetime = getdate()

    if @is_success = 1 

	begin
		update dbo.client_login
		set fail_count = 0
		,last_user = @username
		where [ip] = @ip
		and DATEADD(minute,10,last_date) > @date
	end
	else
	begin
		if exists (select 1 from dbo.client_login where [ip]=@ip and DATEADD(minute,10,last_date) > @date)
			update dbo.client_login
			set fail_count = fail_count+1
			,last_user = @username
			,last_date = getdate()
			where [ip]=@ip and DATEADD(minute,10,last_date) > @date
		else
			insert into dbo.client_login(
			[ip],
			last_date,
			last_user,
			fail_count)
			values(
			@ip,
			@date,
			@username,
			1
			)
	end
END
GO
