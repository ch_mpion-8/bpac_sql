SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SP_GET_PYC]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200 ) = null,
	@customer_code as varchar(20) = null,
	@customer_name as varchar(100) = null,
	@document_no as varchar(20) = null,
	@date_from as varchar(20) = null,
	@date_to as varchar(20) = null,
	@is_skip as varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @deleted_remark as nvarchar(50) = '-.. . .-.. . - . -..'
	if @is_skip = 'COUNT' or @is_skip is null 
	begin
		;with other_doc as 
		(
			select o.document_id as [document_id]
					,o.document_no
					,mcom.company_code
					,mcus.customer_code
					,o.document_date
					,document_date as trip_date 
					,1 as reference_type 
			from [dbo].[other_control_document] o 
			left join master_company mcom on o.company_code = mcom.company_code
			left join master_customer mcus on mcus.customer_code = o.receiver
			--left join dbo.scg_pyc_process p on o.document_id = p.reference_no_id and p.reference_type = 1
			where o.method = 'PYC' 
					--and isnull(p.remark,'') <> @deleted_remark
		),
		 cheque as (
			select o.cheque_control_id as [document_id]
					,o.[cheque_control_no] as document_no
					,mcc.company_code
					,mcc.customer_code
					,o.collection_date as document_date
					,o.collection_date as trip_date
					,2 as reference_type 
			from cheque_control_document o 
			left join master_customer_company mcc 	on o.customer_company_id = mcc.customer_company_id 
			--left join dbo.scg_pyc_process p on o.cheque_control_id = p.reference_no_id and p.reference_type = 2
			where o.method = 'PYC' 
					--and isnull(p.remark,'') <> @deleted_remark
		),
		 bill as (
			select o.bill_presentment_id as [document_id]
				,o.bill_presentment_no as document_no
				,mcc.company_code
				,mcc.customer_code
				,bill_presentment_date  as document_date
				,o.bill_presentment_date as trip_date
				,3 as reference_type 
			from bill_presentment_control_document o 
			left join master_customer_company mcc 	on o.customer_company_id = mcc.customer_company_id 
			--left join dbo.scg_pyc_process p on o.bill_presentment_id = p.reference_no_id and p.reference_type = 3
			where o.method = 'PYC' 
					--and isnull(p.remark,'') <> @deleted_remark
		),
		 chequeInvoice as (
			select o.cheque_control_id as [document_id]
					,o.[cheque_control_no] as document_no
					,mcc.company_code
					,mcc.customer_code
					,o.collection_date as document_date
					,o.collection_date as trip_date
					,4 as reference_type 
			from cheque_invoice_control_document o 
			left join master_customer_company mcc 	on o.customer_company_id = mcc.customer_company_id 
			--left join dbo.scg_pyc_process p on o.cheque_control_id = p.reference_no_id and p.reference_type = 2
			where o.method = 'PYC' 
					--and isnull(p.remark,'') <> @deleted_remark
		),
		 all_doc as (
			select * from other_doc 
			union all
			select * from cheque
			union all
			select * from bill
			union all
			select * from chequeInvoice
		)
		select distinct document_id
			,document_no
			--,isnull(p.remark,'')
			,a.company_code
			,a.customer_code
			,a.document_date
			,a.trip_date
			,mcus.customer_name
			,mcom.company_name
			,a.reference_type
			,0 as is_skip
			,null as remark
			,null as process_id
			, pcg.*
			,(select top 1 [address] from dbo.master_customer_address addr where addr.customer_company_id = mcc.customer_company_id) as [address]
		from all_doc a 
		left join pyc_customer_group pcg on pcg.customer_code = a.customer_code
		left join master_customer mcus on mcus.customer_code = a.customer_code
		left join master_company mcom on mcom.company_code = a.company_code
		left join dbo.scg_pyc_process p on  a.document_id = p.reference_no_id and a.reference_type =  p.reference_type
		left join master_customer_company mcc on mcc.company_code = a.company_code and mcc.customer_code = a.customer_code 
		where 
			( @company_code is null or a.company_code  in ( select item from dbo.SplitString(@company_code,',')))

			and 
			(
				@customer_code is not null AND a.customer_code = @customer_code
				OR
				@customer_code is null AND  (a.customer_code like '%' + @customer_name + '%' or mcus.customer_name like '%' + @customer_name +'%')
			)
			and ( @document_no is null or a.document_no like '%' + @document_no + '%')
			and ( @date_from is null or @date_to is null or (a.document_date between convert(datetime, LEFT(@date_from,10), 103)  and convert(datetime, LEFT(@date_to,10), 103) ))
			and isnull(p.remark,'') <> @deleted_remark
		
	order by reference_type,document_id
	end
	--else begin
	--select null 
	----as document_id,null as document_no
	----,null as company_code,null as customer_code,null as document_date,null as trip_date
	----,null as customer_name,null as company_name
	----,null as reference_type,null as is_skip,null as remark,null as process_id
	--end


END
GO
