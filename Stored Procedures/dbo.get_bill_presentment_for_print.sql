SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[get_bill_presentment_for_print]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as nvarchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select bcd.bill_presentment_id
			,bcd.bill_presentment_no
			,bcd.customer_company_id
			,cc.company_code
			,com.company_name
			,cc.customer_code
			,cus.customer_name
			,isnull(conf.[configuration_value], bcd.method) as bill_presentment_method
			,bcd.bill_presentment_date
			,bcd.remark
			,cc.user_display_name
			,bcd.updated_date
	from dbo.bill_presentment_control_document bcd
	inner join dbo.master_customer_company cc on bcd.customer_company_id = cc.customer_company_id
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
	inner join dbo.master_company com on cc.company_code = com.company_code
	left join dbo.configuration conf on bcd.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where bill_presentment_id in ( select item from dbo.SplitString( @bill_presentment_id,','))

	select bcdi.bill_presentment_detail_id
			,bcdi.bill_presentment_id
			--,bcdi.invoice_no  /*Aof change if DA substring year*/
			, case ih.invoice_type 
						WHEN 'DA' then 
							substring(bcdi.invoice_no, 1,12)
						WHEN 'DC' then 
							substring(bcdi.invoice_no, 1,12)
						else
							 bcdi.invoice_no 
						end as [invoice_no]
			,convert(varchar, ih.invoice_date, 103) as invoice_date
			, case when c.company_code in  ('7850','0470') then '-' else  t.payment_term_name end as payment_term
			, case when c.company_code in  ('7850','0470') 
				then
					case when ih.net_due_date is not null then convert(varchar, ih.net_due_date , 103) else '' end
				else
					case when ih.due_date is not null then convert(varchar, ih.due_date , 103) else '' end 
				end as  due_date
			,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
			,ih.currency
			,case ih.invoice_type when 'RD' then 'debit'
							when 'RC' then 'credit'
				else 'invoice'
				end as invoice_type
			,cc.payment_method
	from dbo.bill_presentment_control_document_item bcdi
	inner join dbo.bill_presentment_control_document bcd on bcdi.bill_presentment_id = bcd.bill_presentment_id
	inner join dbo.master_customer_company c on bcd.customer_company_id = c.customer_company_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no and c.company_code = ih.company_code
	inner join dbo.master_payment_term t on ih.payment_term = t.payment_term_code
	outer apply(
		select top 1 *
		from dbo.master_customer_collection_behavior ccb
		where 
		(
			ih.collection_behavior_id is not null
			and ccb.collection_behavior_id = ih.collection_behavior_id
		)
		or
		(
			ih.collection_behavior_id is null
			and ccb.customer_company_id = bcd.customer_company_id
			and ccb.is_active = 1
			and ccb.collection_condition_date = 'BPAC Bill Date'
			and bcd.bill_presentment_date between ccb.valid_date_from and isnull(ccb.valid_date_to,bcd.bill_presentment_date)
			and DATEPART(day,bcd.bill_presentment_date) between ccb.period_from and ccb.period_to
		)
	)as cc
	where bcdi.bill_presentment_id  in ( select item from dbo.SplitString( @bill_presentment_id,','))
		and bcdi.is_active = 1
	ORDER BY case ih.invoice_type 
				when 'RA' then 1
				when 'DA' then 2
				when 'RD' then 3
				when 'RC' then 4
				when 'DC' then 5
		END, ih.invoice_date


END
GO
