SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_INVOICE_AND_COLLECTION_BY_INVOICE] 
	
@invoice_no as nvarchar(max)  = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @bill TABLE(
    bill_id INT
    );

    select 
	ih.invoice_no,
	ih.invoice_type,
	ih.invoice_date,
	(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)
	as load_date,
	ih.bill_presentment_date,
	ih.due_date,
	ih.collection_date,
	ih.company_code,
	ih.customer_code,
	ih.payment_term,
	ih.invoice_amount,
	ih.tax_amount,
	ih.currency,
	ih.referenece_document_no,
	--	ih.bill_presentment_behavior_id,
	ih.collection_behavior_id,
	ih.assignment_no,
	--mccb.collection_behavior_id,
	mccb.customer_company_id as collection_customer_company_id,
	mccb.collection_condition_date,
	mccb.condition_by,
	mccb.condition_month_by as collection_condition_month_by,
	mccb.condition_week_by as collection_condition_week_by,
	mccb.custom_date as collection_custom_date,
	mccb.custom_day as collection_custom_day,
	mccb.custom_month as collection_custom_month,
	mccb.custom_week as collection_custom_week,
	mccb.date_from as collection_date_from,
	mccb.date_to as collection_date_to,
	mccb.is_skip_customer_holiday  as collection_is_skip_customer_holiday,
	mccb.is_skip_scg_holiday  as collection_is_skip_scg_holiday,
	mccb.is_business_day as collection_is_business_day,
	mccb.last_x_date as collection_last_x_date,
	mccb.next_x_month as collection_next_x_month,
	mccb.notify_day as collection_notify_date,
	mccb.period_from as collection_period_from,
	mccb.period_to as collection_period_to,
	mccb.payment_term_code as collection_payment_term_code,
	mccb.is_next_collection_date,
	mccb.payment_day,
    mccb.next_x_week,
	mccb.last_x_business_day

		into #temp1
		from invoice_header ih 
		--left join master_customer_collection_behavior mccb
		--on ih.collection_behavior_id = mccb.collection_behavior_id and mccb.is_active = 1
		left join master_customer_company mcc 
		on ih.company_code = mcc.company_code 
		and ih.customer_code =  mcc.customer_code
		left join master_customer_collection_behavior mccb
		on mcc.customer_company_id = mccb.customer_company_id
		and ih.payment_term = mccb.payment_term_code
		AND (
		(mccb.collection_condition_date = 'Due Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.due_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
		and DAY(ih.due_date) between mccb.period_from and mccb.period_to)
		or
		(mccb.collection_condition_date = 'Billing Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.invoice_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
		and DAY(ih.invoice_date) between mccb.period_from and mccb.period_to)
		or
		(mccb.collection_condition_date = 'BPAC Bill Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.bill_presentment_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
		and DAY(ih.bill_presentment_date) between mccb.period_from and mccb.period_to)
		or
		(mccb.collection_condition_date = 'Delivery Date' 
		AND (DATEDIFF(DAY,mccb.valid_date_from,
		(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)) >= 0)
		and DAY((Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))
		 between mccb.period_from and mccb.period_to)
		)

		where ih.invoice_no in (select item from dbo.SplitString(@invoice_no,','))
	
	select * from #temp1 
	--select * from master_customer_collection_custom_date
	--where collection_behavior_id in ( select collection_behavior_id  from #temp1)
	--and is_active =1

	select h.customer_holiday_id as holiday_id 
			,h.customer_code
			,h.holiday_year
			,h.holiday_month
			,h.holiday_day
			,h.holiday_name
			,'CUSTOMER' as holiday_type
			,h.is_active
			,h.created_date
			,h.created_by
			,h.updated_date
			,h.updated_by
	from dbo.master_customer_holiday h
	where   customer_code in (select distinct customer_code from #temp1 where collection_is_skip_customer_holiday is null or collection_is_skip_customer_holiday = 0)
			and is_active = 1

	

	drop table #temp1
	--exec SP_INSERT_CUSTOMER_TO_MASTER_CUSTOMER_COMPANY

END
GO
