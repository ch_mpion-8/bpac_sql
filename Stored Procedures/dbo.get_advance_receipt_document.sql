SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_advance_receipt_document]
    -- Add the parameters for the stored procedure here
    @company_code AS VARCHAR(200) = 'All',
    @customer_name AS VARCHAR(120) = NULL,
    @clearing_document_no AS VARCHAR(10) = NULL,
    @document_date_start AS DATE = NULL,
    @document_date_end AS DATE = NULL,
    @status AS VARCHAR(20) = 'All',
    @otherdoc_no AS VARCHAR(20) = NULL,
    @otherdoc_date_start AS DATE = NULL,
    @otherdoc_date_end AS DATE = NULL,
    @reference_doc_no VARCHAR(50) = NULL,
    @collection_user VARCHAR(100) = NULL,
    @customer_code AS VARCHAR(20) = NULL,
    @method AS VARCHAR(20) = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    IF @company_code = 'All'
        SET @company_code = NULL;
    IF @status = 'All'
        SET @status = NULL;

    --unti sql sniff
    DECLARE @com VARCHAR(200) = @company_code,
            @cus_code VARCHAR(20) = @customer_code,
            @cus_name VARCHAR(120) = @customer_name,
            @met VARCHAR(10) = @method;



    --search cancel
    SELECT DISTINCT
           r.customer_code,
           r.company_code,
           cus.customer_name,
           r.clearing_document_no,
           r.document_date,
           mcc.user_display_name AS 'collection_user',
           r.currency,
           r.[year],
           r.document_amount,
           r.reference_document_no,
           r.is_active,
           com.company_name,
           bh.billing_method
    INTO #temp
    FROM dbo.other_control_document o
        INNER JOIN dbo.other_control_document_item i
            ON i.document_id = o.document_id
        INNER JOIN dbo.receipt r
            ON r.reference_document_no = i.reference_no
               AND r.company_code = o.company_code
               AND r.customer_code = o.receiver
               AND r.[year] = i.[year]
        INNER JOIN dbo.master_customer_company mcc
            ON mcc.company_code = r.company_code
               AND mcc.customer_code = r.customer_code
        INNER JOIN dbo.master_customer cus
            ON cus.customer_code = r.customer_code
        INNER JOIN dbo.master_company com
            ON com.company_code = r.company_code
        LEFT JOIN dbo.invoice_header inv
            ON inv.invoice_no = r.assignment_no
               AND inv.company_code = com.company_code
               AND inv.customer_code = cus.customer_code
        LEFT JOIN dbo.master_customer_bill_presentment_behavior bh
            ON bh.customer_company_id = mcc.customer_company_id
               AND bh.payment_term_code = inv.payment_term
    WHERE o.document_type = 1
          AND o.[status] = 'CANCEL'
          AND ISNULL(r.is_active, 1) = 1
          AND r.receipt_type = 'normal'
          AND
          (
              SELECT TOP 1
                     1
              FROM dbo.other_control_document ao
                  INNER JOIN dbo.other_control_document_item ai
                      ON ao.document_id = ai.document_id
              WHERE ai.reference_no = r.reference_document_no
                    AND ao.company_code = r.company_code
                    AND ao.receiver = r.customer_code
                    AND ao.[status] <> 'CANCEL'
          ) IS NULL
          AND r.reference_document_no LIKE '%' + ISNULL(@reference_doc_no, '') + '%'
          AND
          (
              @cus_code IS NOT NULL
              AND r.customer_code = @cus_code
              OR @cus_code IS NULL
                 AND cus.customer_name LIKE '%' + ISNULL(@cus_name, '') + '%'
          )
          AND
          (
              @com IS NULL
              OR r.company_code IN
                 (
                     SELECT Item FROM dbo.SplitString(@com, ',')
                 )
          )
          AND
          (
              (r.document_date
          BETWEEN @document_date_start AND @document_date_end
              )
              OR @document_date_start IS NULL
              OR @document_date_end IS NULL
          )
          AND r.clearing_document_no LIKE '%' + ISNULL(@clearing_document_no, '') + '%'
          AND 'WC' = ISNULL(@status, 'WC')
          AND ISNULL(mcc.user_display_name, '') LIKE '%' + ISNULL(@collection_user, '') + '%'
          AND ISNULL(@otherdoc_no, '') = ''
          AND
          (
              @met IS NULL
              OR o.method = @met
          );






    SELECT DISTINCT
           r.customer_code,
           r.company_code,
           cus.customer_name,
           r.clearing_document_no,
           r.document_date,
           c.user_display_name AS 'collection_user',
           r.currency,
           r.[year],
           r.document_amount,
           r.reference_document_no,
           o.[status],
           ISNULL(conf.[configuration_value], o.method) AS method,
           o.created_by,
           o.document_no,
           o.document_date AS other_document_date,
           CONVERT(VARCHAR(20), o.document_id) AS document_id,
           o.reference_no,
           r.is_active,
           com.company_name,
           o.reference_guid,
           bh.billing_method
    FROM dbo.receipt r
        INNER JOIN dbo.master_customer_company c
            ON r.customer_code = c.customer_code
               AND r.company_code = c.company_code
        INNER JOIN dbo.master_customer cus
            ON cus.customer_code = c.customer_code
        INNER JOIN dbo.master_company com
            ON com.company_code = r.company_code
        OUTER APPLY
    (
        SELECT o.document_id,
               o.document_no,
               o.document_date,
               o.created_by,
               o.created_date,
               o.method,
               o.[status],
               o.reference_guid,
               i.reference_no,
               o.receiver,
               o.company_code
        FROM dbo.other_control_document o
            INNER JOIN dbo.other_control_document_item i
                ON i.document_id = o.document_id
        WHERE o.document_type = 1
              AND o.company_code = r.company_code
              AND o.receiver = r.customer_code
              AND i.[year] = r.[year]
              AND i.reference_no = r.reference_document_no
    ) o
        LEFT JOIN dbo.invoice_header inv
            ON inv.invoice_no = r.assignment_no
               AND inv.company_code = com.company_code
               AND inv.customer_code = cus.customer_code
        LEFT JOIN dbo.master_customer_bill_presentment_behavior bh
            ON bh.customer_company_id = c.customer_company_id
               AND bh.payment_term_code = inv.payment_term
        LEFT JOIN dbo.configuration conf
            ON o.method = conf.[configuration_code]
               AND conf.[configuration_key] = 'Method'
    WHERE r.receipt_type = 'normal'
          AND
          (
              o.[status] IS NULL
              AND ISNULL(r.is_active, 1) = 1
              OR o.[status] IS NOT NULL
          )
          AND
          (
              (r.document_date
          BETWEEN @document_date_start AND @document_date_end
              )
              OR @document_date_start IS NULL
              OR @document_date_end IS NULL
          )
          AND
          (
              @com IS NULL
              OR r.company_code IN
                 (
                     SELECT Item FROM dbo.SplitString(@com, ',')
                 )
          )
          AND
          (
              @met IS NULL
              OR o.method = @met
          )
          AND r.reference_document_no LIKE '%' + ISNULL(@reference_doc_no, '') + '%'
          AND
          (
              @cus_code IS NOT NULL
              AND r.customer_code = @cus_code
              OR @cus_code IS NULL
                 AND cus.customer_name LIKE '%' + ISNULL(@cus_name, '') + '%'
          )
          AND r.clearing_document_no LIKE '%' + ISNULL(@clearing_document_no, '') + '%'
          AND ISNULL(o.document_no, '') LIKE '%' + ISNULL(@otherdoc_no, '') + '%'
          AND ISNULL(CONVERT(DATE, o.document_date), '') >= ISNULL(
                                                                      @otherdoc_date_start,
                                                                      ISNULL(CONVERT(DATE, o.document_date), '')
                                                                  )
          AND ISNULL(CONVERT(DATE, o.document_date), '') <= ISNULL(
                                                                      @otherdoc_date_end,
                                                                      ISNULL(CONVERT(DATE, o.document_date), '')
                                                                  )
          AND ISNULL(o.[status], 'WC') = ISNULL(@status, ISNULL(o.[status], 'WC'))
          AND ISNULL(c.user_display_name, '') LIKE '%' + ISNULL(@collection_user, '') + '%'
    UNION ALL
    SELECT customer_code,
           company_code,
           customer_name,
           clearing_document_no,
           document_date,
           collection_user,
           currency,
           year,
           document_amount,
           reference_document_no,
           NULL AS [status],
           NULL AS method,
           NULL AS created_by,
           NULL AS document_no,
           NULL AS other_document_date,
           NULL AS document_id,
           NULL AS reference_no,
           is_active,
           company_name,
           NULL AS reference_guid,
           billing_method
    FROM #temp;

    DROP TABLE #temp;

END;
GO
