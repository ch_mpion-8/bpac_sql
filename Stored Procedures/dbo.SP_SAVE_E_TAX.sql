SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_SAVE_E_TAX]
    @e_tax_list [dbo].[e_tax] READONLY,
    @e_tax_line_item [dbo].[e_tax_line_item] READONLY,
    @e_tax_allowance [dbo].[e_tax_allowance] READONLY
--,
--   @error_message NVARCHAR(200) OUTPUT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.

    SET NOCOUNT ON;
    DECLARE @document_id NVARCHAR(MAX);
    DECLARE @exchange_document_id NVARCHAR(MAX);
    DECLARE @exchange_document_name NVARCHAR(MAX);
    DECLARE @exchange_doucment_type_code NVARCHAR(MAX);
    DECLARE @issue_date NVARCHAR(MAX);
    DECLARE @created_date NVARCHAR(MAX);
    DECLARE @seller_name NVARCHAR(MAX);
    DECLARE @seller_tax_id NVARCHAR(MAX);
    DECLARE @seller_post_code NVARCHAR(MAX);
    DECLARE @seller_street NVARCHAR(MAX);
    DECLARE @seller_city NVARCHAR(MAX);
    DECLARE @seller_city_sub_division NVARCHAR(MAX);
    DECLARE @seller_building_number NVARCHAR(MAX);
    DECLARE @buyer_name NVARCHAR(50);
    DECLARE @buyer_tax_id NVARCHAR(MAX);
    DECLARE @buyer_post_code NVARCHAR(MAX);
    DECLARE @buyer_street NVARCHAR(MAX);
    DECLARE @buyer_city NVARCHAR(MAX);
    DECLARE @buyer_city_sub_division NVARCHAR(MAX);
    DECLARE @buyer_country NVARCHAR(MAX);
    DECLARE @buyer_country_sub_division NVARCHAR(MAX);
    DECLARE @buyer_building_number NVARCHAR(MAX);
    DECLARE @invoice_currency NVARCHAR(MAX);
    DECLARE @tax_type NVARCHAR(MAX);
    DECLARE @tax_calculate_rate NVARCHAR(MAX);
    DECLARE @tax_basis_amount NVARCHAR(MAX);
    DECLARE @summation_amount NVARCHAR(MAX);
    DECLARE @allowance_total_amount NVARCHAR(MAX);
    DECLARE @charge_total_amount NVARCHAR(MAX);
    DECLARE @tax_basis_total_amount NVARCHAR(MAX);
    DECLARE @tax_total_amount NVARCHAR(MAX);
    DECLARE @grand_total_amount NVARCHAR(MAX);



    DECLARE _cur CURSOR FOR
    SELECT document_id,
           exchange_document_id,
           exchange_document_name,
           exchange_doucment_type_code,
           issue_date,
           created_date,
           seller_name,
           seller_tax_id,
           seller_post_code,
           seller_street,
           seller_city,
           seller_city_sub_division,
           seller_building_number,
           buyer_name,
           buyer_tax_id,
           buyer_post_code,
           buyer_street,
           buyer_city,
           buyer_city_sub_division,
           buyer_country,
           buyer_country_sub_division,
           buyer_building_number,
           invoice_currency,
           tax_type,
           tax_calculate_rate,
           tax_basis_amount,
           summation_amount,
           allowance_total_amount,
           charge_total_amount,
           tax_basis_total_amount,
           tax_total_amount,
           grand_total_amount
    FROM @e_tax_list;

    OPEN _cur;
    FETCH NEXT FROM _cur
    INTO @document_id,
         @exchange_document_id,
         @exchange_document_name,
         @exchange_doucment_type_code,
         @issue_date,
         @created_date,
         @seller_name,
         @seller_tax_id,
         @seller_post_code,
         @seller_street,
         @seller_city,
         @seller_city_sub_division,
         @seller_building_number,
         @buyer_name,
         @buyer_tax_id,
         @buyer_post_code,
         @buyer_street,
         @buyer_city,
         @buyer_city_sub_division,
         @buyer_country,
         @buyer_country_sub_division,
         @buyer_building_number,
         @invoice_currency,
         @tax_type,
         @tax_calculate_rate,
         @tax_basis_amount,
         @summation_amount,
         @allowance_total_amount,
         @charge_total_amount,
         @tax_basis_total_amount,
         @tax_total_amount,
         @grand_total_amount;

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        IF NOT EXISTS
        (
            SELECT 1
            FROM e_tax_xml
            WHERE exchange_document_id = @exchange_document_id
        )
        BEGIN
            INSERT INTO dbo.e_tax_xml
            (
                document_id,
                exchange_document_id,
                exchange_document_name,
                exchange_doucment_type_code,
                issue_date,
                created_date,
                seller_name,
                seller_tax_id,
                seller_post_code,
                seller_street,
                seller_city,
                seller_city_sub_division,
                seller_building_number,
                buyer_name,
                buyer_tax_id,
                buyer_post_code,
                buyer_street,
                buyer_city,
                buyer_city_sub_division,
                buyer_country,
                buyer_country_sub_division,
                buyer_building_number,
                invoice_currency,
                tax_type,
                tax_calculate_rate,
                tax_basis_amount,
                summation_amount,
                allowance_total_amount,
                charge_total_amount,
                tax_basis_total_amount,
                tax_total_amount,
                grand_total_amount
            )
            VALUES
            (@document_id, @exchange_document_id, @exchange_document_name, @exchange_doucment_type_code, @issue_date,
             @created_date, @seller_name, @seller_tax_id, @seller_post_code, @seller_street, @seller_city,
             @seller_city_sub_division, @seller_building_number, @buyer_name, @buyer_tax_id, @buyer_post_code,
             @buyer_street, @buyer_city, @buyer_city_sub_division, @buyer_country, @buyer_country_sub_division,
             @buyer_building_number, @invoice_currency, @tax_type, @tax_calculate_rate, @tax_basis_amount,
             @summation_amount, @allowance_total_amount, @charge_total_amount, @tax_basis_total_amount,
             @tax_total_amount, @grand_total_amount);
        END;

        FETCH NEXT FROM _cur
        INTO @document_id,
         @exchange_document_id,
         @exchange_document_name,
         @exchange_doucment_type_code,
         @issue_date,
         @created_date,
         @seller_name,
         @seller_tax_id,
         @seller_post_code,
         @seller_street,
         @seller_city,
         @seller_city_sub_division,
         @seller_building_number,
         @buyer_name,
         @buyer_tax_id,
         @buyer_post_code,
         @buyer_street,
         @buyer_city,
         @buyer_city_sub_division,
         @buyer_country,
         @buyer_country_sub_division,
         @buyer_building_number,
         @invoice_currency,
         @tax_type,
         @tax_calculate_rate,
         @tax_basis_amount,
         @summation_amount,
         @allowance_total_amount,
         @charge_total_amount,
         @tax_basis_total_amount,
         @tax_total_amount,
         @grand_total_amount;
    END;
    CLOSE _cur;
    DEALLOCATE _cur;

    INSERT INTO dbo.e_tax__xml_line_item
    (
        exchange_document_id,
        line_item_no,
        product_name,
        gross_price,
        line_trade_delivery,
        monetary_tax_total_amount,
        monetary_net_line_amount,
        monetary_net_include_amount
    )
    SELECT exchange_document_id,
           line_item_no,
           product_name,
           gross_price,
           line_trade_delivery,
           monetary_tax_total_amount,
           monetary_net_line_amount,
           monetary_net_include_amount
    FROM @e_tax_line_item;

    INSERT INTO dbo.e_tax_xml_allowance
    (
        exchange_document_id,
        charge_indicator,
        actual_amount
    )
    SELECT exchange_document_id,
           charge_indicator,
           actual_amount
    FROM @e_tax_allowance;

END;

GO
