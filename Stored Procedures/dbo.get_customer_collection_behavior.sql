SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_customer_collection_behavior] 
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null 
	,@customer_company_id as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	select distinct cb.group_id as [collection_behavior_id]
      ,cb.[customer_company_id]
	  ,cc.company_code
	  ,com.company_name
	  ,cc.customer_code
      ,[valid_date_from]
	  ,[valid_date_to]
      --,cb.[payment_term_code]
	 ,(
				select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_collection_behavior] b
						where b.customer_company_id = cb.customer_company_id and b.group_id = cb.group_id
									and b.is_active = cb.is_active
									--and b.is_active = 1
								for xml path('')) ,1,1,'') as txt
				) as [payment_term_code]
	  ,(
				select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_collection_behavior] b
						where b.customer_company_id = cb.customer_company_id and b.group_id = cb.group_id
									and b.is_active = cb.is_active
									--and b.is_active = 1
								for xml path('')) ,1,1,'') as txt
				)  as [payment_term_name]

	  --,term.payment_term_name
      ,cb.[address_id]
	  ,ad.title as address_title
      ,[time]
      ,con.configuration_value as [collection_condition_date]
      ,cast([period_from]as nvarchar(50)) + ' - ' + cast([period_to]as nvarchar(50)) as period
	  ,[period_from]
	  ,[period_to]
	  --,cb.condition_by
	  --,cb.custom_date
	  --,cb.last_x_date
	  --,cb.last_x_business_day
	  ,case when cb.condition_by = 'ชำระภายใน,EVERYDATE' then 'ชำระภายใน ' + cast(cb.payment_day as nvarchar(10)) + ' วัน'
			when cb.condition_by = 'ชำระหลัง,EVERYDATE' then 'ชำระหลัง ' + cast(cb.payment_day as nvarchar(10))+ ' วัน'
			when cb.condition_by like 'ชำระภายใน%' then 'ชำระภายใน ' + cast(cb.payment_day as nvarchar(10)) + ' วัน,'
			when cb.condition_by like 'ชำระหลัง%' then 'ชำระหลัง ' + cast(cb.payment_day as nvarchar(10))+ ' วัน,'

			else '' end +
		case when cb.condition_by =  'EVERYDATE' then 'Every day'
			when cb.condition_by like  '%EVERYDATE' then ''
			when cb.condition_by like  '%CUSTOMDATE' 
				then 
					case when cb.custom_date like '%-1%' 
						then case when cb.last_x_date is not null and cb.last_x_date = 1 then replace(cb.custom_date,'-1','Last day')
								 when cb.last_x_date is not null and cb.last_x_date = 2 then replace(cb.custom_date,'-1','Last 2 days')
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then replace(cb.custom_date,'-1','Last business day')
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then replace(cb.custom_date,'-1','Last 2 business days')
								 else '' end
					else 'วันที่ ' + cb.custom_date end
			when cb.condition_by like  '%PERIOD' then 'Period ' 
						+ case when cb.date_from = -1 
								then case when cb.last_x_date is not null and cb.last_x_date = 1 then 'Last day'
								 when cb.last_x_date is not null and cb.last_x_date = 2 then 'Last 2 days'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then 'Last business day'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then 'Last 2 business days'
								 else '' end
							else cast(cb.date_from as nvarchar(10)) 
							end
						+ ' - ' 
						+ case when cb.date_to =-1 
						then case when cb.last_x_date is not null and cb.last_x_date = 1 then 'Last day'
								 when cb.last_x_date is not null and cb.last_x_date = 2 then 'Last 2 days'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then 'Last business day'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then 'Last 2 business days'
								 else '' end
						else cast(cb.date_to as nvarchar(10)) 
						end

			when cb.condition_by like  '%LASTDAY' then 'Last ' + cast(cb.last_x_date as nvarchar(10)) + ' day(s)'
			when cb.condition_by like  '%LASTBUSINESSDAY' then 'Last ' + cast(cb.last_x_business_day as nvarchar(10)) + ' business day(s)'
			when cb.condition_by like  '%CUSTOMDAY' then 'ทุกวัน ' + (
														select stuff(
																(
																	select ',' + dayName 
																		from (
																		select dayname = configuration_value
																		from dbo.SplitString(cb.custom_day,',') s
																		inner join  dbo.configuration  c 
																			on s.item = c.configuration_code and c.configuration_key = 'Day'


																	) a
																		for xml path('')
																) ,1,1,'') as txt
														)
			else ''
			end as [date]
		, case when cb.condition_week_by = 'EVERYWEEK' then 'Every week' else  (
										select stuff(
												(
													select ',' + mon 
														from (
														select mon = 'week ' + item
														from dbo.SplitString(cb.custom_week ,',') s
													) a
														for xml path('')
												) ,1,1,'') as txt
										)
					
					end as [week] 
		, case when cb.condition_week_by = 'EVERYMONTH' then 'Every month' else cb.custom_month end as [month]
	   , case when ad.[address] is null then '' else  ad.[address] +' ' end
		+ case when ad.sub_district is null then '' else  ad.sub_district +' ' end
		+ case when ad.district is null then '' else  ad.district +' ' end
		+ case when ad.province is null then '' else  ad.province +' ' end
		+ case when ad.post_code is null then '' else  ad.post_code end as [address]
	  ,cb.payment_method 
	  ,isnull(conf.[configuration_value],cb.receipt_method ) as receipt_method 
	  ,cb.notify_day 
	  ,doc.document
	  ,cb.bank 
	  ,bank.bank_name
	  ,cb.remark
	  ,cb.[description]
      ,cb.[is_active]
      --,cb.[created_date]
      --,cb.[created_by]
      --,cb.[updated_date]
      --,cb.[updated_by]

  from [scgbpac].[dbo].[master_customer_collection_behavior] cb
	inner join  dbo.master_customer_company cc on cb.customer_company_id = cc.customer_company_id
	--inner join dbo.master_payment_term term on cb.payment_term_code = term.payment_term_code
	inner join dbo.master_company com on cc.company_code = com.company_code
	left join dbo.configuration con on cb.[collection_condition_date] = con.configuration_code 
				and configuration_key = 'CollectionConditionDate'
	left join dbo.master_customer_address ad on cb.address_id = ad.address_id
	left join (
					select  main.collection_behavior_id, stuff(
					(
						select ',' + bank_name 
							from dbo.SplitString(main.bank,',') s
							inner join dbo.master_bank b on s.Item = b.bank_id
							for xml path('')
					) ,1,1,'') as bank_name
					from dbo.master_customer_collection_behavior main
				) bank on cb.collection_behavior_id = bank.collection_behavior_id	
	left join (
		select  main.collection_behavior_id, stuff(
		(
			select ',' + con.configuration_value  
					+ case when main.document is null or s.Item <> '3' then '' else ' ' + main.custom_document end
				from dbo.SplitString(main.document,',') s
				inner join dbo.configuration con on s.Item = con.configuration_code and con.configuration_key = 'CollectionDocument'
				where con.is_active =1 
				for xml path('')
		) ,1,1,'') as document
		from dbo.master_customer_collection_behavior main
	) doc on cb.collection_behavior_id = doc.collection_behavior_id	
	left join dbo.configuration conf on cb.receipt_method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where (@customer_code is null or customer_code = @customer_code )
		and (@customer_company_id is null or cb.customer_company_id = @customer_company_id)
		and cb.is_active = 1 
		--and (cb.is_active = 1 
		--			or ( cb.is_active = 0 
		--					and not exists ( select 1 from  dbo.master_customer_collection_behavior 
		--										where group_id = cb.group_id and is_active = 1)))

END
GO
