SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_other_contact_person]
	-- Add the parameters for the stored procedure here
	@contact_person varchar(100) =null,
	@customer_name varchar(100) =null,
	@customer_code varchar(20)=null,
	@telephone varchar(100)=null,
	@status bit = null,
	@title varchar(100) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select * from dbo.master_other_contact_person
	where (@contact_person is null or contact_person like '%'+isnull(@contact_person,'')+'%' )
	and  (@customer_name is null or customer_name like '%'+isnull(@customer_name,'')+'%')
	and  (@customer_code is null or isnull(customer_code,'') like '%'+isnull(@customer_code,'')+'%')
	and  (@telephone is null or telephone like '%'+isnull(@telephone,'')+'%')
	and  (@status is null or isnull(is_active,1) = isnull(@status,isnull(is_active,1)))
	and  (@title is null or upper(title)  like '%'+isnull(upper(@title),'')+'%')
END

GO
