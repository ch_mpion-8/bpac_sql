SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_invoice_for_manual_cheque]
	-- Add the parameters for the stored procedure here
	 @cheque_detail_id as nvarchar(2000) = null -- 1016
		,@invoice_list as nvarchar(2000) =  null --'157|1480173290,121|1480173291'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @cheque_detail_id is not null and @cheque_detail_id <> ''
	begin
    	select distinct cdi.invoice_no as reference_document_no
			,c.company_code + ' - ' + com.company_name as company_code
			,c.customer_code
			,cus.customer_name
			,c.customer_company_id
			,cd.cheque_control_no
			,cdi.cheque_no
			,cdi.cheque_amount
			,cdi.cheque_date
			,cdi.cheque_bank
			,cdi.cheque_currency
			,cdi.cheque_remark
			,cdi.cheque_control_detail_id
			,cdi.amount
			,cdi.document_year
		from dbo.cheque_invoice_control_document_item cdi
		inner join dbo.cheque_invoice_control_document cd on cdi.cheque_control_id = cd.cheque_control_id
		inner join dbo.master_customer_company c on cd.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		inner join dbo.master_company com on c.company_code = com.company_code
		where cdi.is_active = 1 and cdi.cheque_control_detail_id in (select item from dbo.SplitString(@cheque_detail_id,','))
	end
	else
	begin

		;with a as(
					select substring(item,0,charindex('|', item)) as customer_company_id
					,right(item,charindex('|', reverse(item)) -1 ) as invoice_no
					from dbo.SplitString(@invoice_list,',')
		)
		select distinct a.invoice_no as reference_document_no
			,c.company_code + ' - ' + com.company_name as company_code
			,c.customer_code
			,cus.customer_name
			,cast(a.customer_company_id as int) customer_company_id
			/*,cd.cheque_control_no
			,cdi.cheque_no
			,cdi.cheque_amount
			,cdi.cheque_date
			,cdi.cheque_bank
			,cdi.cheque_currency
			,cdi.cheque_remark
			,cdi.cheque_control_detail_id*/
			,null as 'cheque_control_no'
			,null as 'cheque_amount'
			,null as 'cheque_date'
			,null as 'cheque_bank'
			,null as 'cheque_currency'
			,null as 'cheque_remark'
			,null as 'cheque_control_detail_id'
			,ih.remaining_amount AS amount
			,null as document_year
		from a
		inner join dbo.master_customer_company c on a.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		inner join dbo.master_company com on c.company_code = com.company_code
		INNER JOIN dbo.invoice_header ih ON a.invoice_no = ih.invoice_no 
		/*left join dbo.cheque_control_document_item cdi  
				on cdi.is_active = 1
					and a.receipt_no = cdi.reference_document_no
		left join dbo.cheque_control_document cd 
				on a.customer_company_id = cd.customer_company_id 
					and cdi.cheque_control_id = cd.cheque_control_id
					and cd.cheque_control_status = 'WAITINGRECEIVED'*/
		
	end
END
GO
