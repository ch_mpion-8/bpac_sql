SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_bill_presentment_detail]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	


	select bcdi.bill_presentment_detail_id
			,bcdi.bill_presentment_id
			,bcdi.invoice_no
			,convert(varchar, ih.invoice_date, 103) as invoice_date
			,ih.payment_term
			,case when ih.due_date is not null then convert(varchar, ih.due_date , 103) 
				else '' end as  due_date
			,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
			,ih.currency
			,'' as payment_method
			,(select stuff((
						select distinct ', ' + sub.dp_no 
							from dbo.invoice_detail sub
							where sub.invoice_no = ih.invoice_no
							for xml path('')
					) ,1,1,'') ) as dp_no
	from dbo.bill_presentment_control_document_item bcdi
	inner join dbo.bill_presentment_control_document bcd on bcdi.bill_presentment_id = bcd.bill_presentment_id
	inner join dbo.master_customer_company c on bcd.customer_company_id = c.customer_company_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no and c.company_code = ih.company_code
	where bcdi.bill_presentment_id = @bill_presentment_id
		and (bcd.bill_presentment_status = 'CANCEL' or bcdi.is_active = 1)


	select distinct bdd.document_code
			,con.configuration_value as value
			,bdd.custom_document
	from dbo.bill_presentment_control_document bcd
	inner join dbo.master_customer_company cc on bcd.customer_company_id = cc.customer_company_id
	inner join dbo.bill_presentment_control_document_item bcdi on bcd.bill_presentment_id = bcdi.bill_presentment_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no
	left join dbo.master_customer_bill_presentment_behavior cbb 
				on cc.customer_company_id = cbb.customer_company_id
					and cbb.payment_term_code = ih.payment_term
					and ih.invoice_date between cbb.valid_date_from and isnull(cbb.valid_date_to,'9999-12-31')		
	left join dbo.master_customer_bill_presentment_document bdd on cbb.bill_presentment_behavior_id = bdd.bill_presentment_behavior_id
	left join dbo.configuration con on bdd.document_code = con.configuration_code and con.configuration_key = 'BillingDocument'
	where bcd.bill_presentment_id = @bill_presentment_id and con.configuration_value is not null



	select [action]
			,[description]
			,convert(varchar, [bpac_due_date], 103) as [bpac_due_date] 
			,convert(varchar, [created_date], 103) + ' '  + convert(varchar(8), [created_date], 14)as [created_date]
			,[created_by]
	from dbo.bill_presentment_control_document_history
	where bill_presentment_id = @bill_presentment_id

	
declare @payment_term as varchar(4)
			,@bill_date as date
			,@currency as varchar(5)
			,@customer_company_id as int


	select @payment_term = ih.payment_term
			,@bill_date = ih.bill_presentment_date
			,@currency = ih.currency
			,@customer_company_id = bcd.customer_company_id
	from dbo.bill_presentment_control_document bcd
	inner join dbo.bill_presentment_control_document_item bcdi on bcd.bill_presentment_id = bcdi.bill_presentment_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no
	where bcd.bill_presentment_id = @bill_presentment_id


	select ih.invoice_no as value
	from dbo.invoice_header ih
	inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
	where cc.customer_company_id = @customer_company_id 
			and ih.payment_term = @payment_term
			and ih.currency = @currency
			--and (ih.bill_presentment_date is null or ih.bill_presentment_date = @bill_date)
			and (ih.bill_presentment_status is null or ih.bill_presentment_status in ('MANUAL','WAITING','ERROR'))

END



GO
