SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[reverse_otherdocument]
	-- Add the parameters for the stored procedure here
	@document_id int =null
	, @submited_by varchar(50)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Declare @status varchar(10)=null,@type int =null

	select @status = [status],@type = document_type from dbo.other_control_document where document_id = @document_id

	if @status = 'C'
	begin
		if @type = 2
		begin
			update dbo.other_control_document
			set [status] = 'WSR',
			update_date = getdate(),
			updated_by = @submited_by
			where document_id = @document_id
		end
		else if @type = 4
		begin
			update dbo.other_control_document
			set [status] = 'Waiting',
			update_date = getdate(),
			updated_by = @submited_by
			where document_id = @document_id
		end
		else
		begin
			update dbo.other_control_document
			set [status] = 'WR',
			update_date = getdate(),
			updated_by = @submited_by
			where document_id = @document_id
		end

			insert into dbo.other_control_document_history ([action],[description],[created_by],[created_date],[document_id])
			values(
			'Reverse',
			'Reverse Otherdocument Id '+CONVERT(varchar(10),@document_id),
			@submited_by,
			getdate(),
			@document_id)
	end
END
GO
