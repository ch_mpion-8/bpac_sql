SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[update_ems_document_control_remark]
	-- Add the parameters for the stored procedure here
	@control_list  as [dbo].[ems_table_type] readonly
	 ,@control_id as int = null 
	 ,@send_date as date = null
	 ,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.ems_control_document
	set send_date = @send_date
		,updated_by = @updated_by
		,updated_date = getdate()
	from dbo.ems_control_document
	where ems_control_document_id = @control_id


	update dbo.ems_control_document_item
	set remark = c.remark
		,updated_by = @updated_by
		,updated_date = getdate()
	from @control_list c
	inner join dbo.ems_control_document_item i on c.[ems_detail_id] = i.[ems_detail_id]
	
	
	--exec dbo.insert_ems_control_history
	--		@ems_control_document_id = @control_id
	--		,@description = 'Upload ems document.'
	--		,@action  = 'Update'
	--		,@updated_by  = @updated_by


END




GO
