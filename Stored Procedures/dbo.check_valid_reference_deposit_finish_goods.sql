SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[check_valid_reference_deposit_finish_goods]
	@reference_no varchar(50)=null,
	@document_id int=null,
	@error_message nvarchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    if @reference_no is null or @reference_no =''
	begin 
		set @error_message = 'โปรดกรอก Reference No.'
		return
	end
    Declare @d_customer_code varchar(20),@d_company_code varchar(10),@invoice_no varchar(50)
	Declare @customer_code varchar(20),@company_code varchar(10),@customer_name nvarchar(50),@invoice_date datetime,@dp_no varchar(20)=null
    
	Select @company_code = company_code,@customer_code = customer_code,@invoice_date = invoice_date,@dp_no = dp_no from dbo.deposit
	where dp_no = @reference_no 

	if @dp_no is null OR @dp_no =''
	begin
		set @error_message = 'ไม่พบ DP No ที่ระบุ'
		return
	end

	----check dupicate referecnce in otherdoc.document_type = 1
	--if exists(select 1 from dbo.other_control_document o 
	--	inner join dbo.other_control_document_item i 
	--	on i.document_id=o.document_id
	--	and i.reference_no = @reference_no
	--	where o.document_type = 3)
	--begin
	--	set @error_message ='Reference No: '+@reference_no+' มีอยู่แล้วใน OtherDocument นี้'
	--	return
	--end


	Select TOP 1 
		@invoice_no = d.invoice_no,
		@d_company_code = d.company_code,
		@d_customer_code = d.customer_code
	from dbo.other_control_document o
	inner join dbo.other_control_document_item i
	on i.document_id=o.document_id
	inner join dbo.deposit d
	on d.dp_no = i.reference_no
	where o.document_id = @document_id

	

	if @d_company_code =@company_code AND @customer_code=@d_customer_code
	begin
		Declare @temp Table(
			customer_code varchar(20),
			customer_name nvarchar(50),
			company_code varchar(10),
			dp_no varchar(20),
			invoice_date datetime,
			reference_no varchar(50),
			invoice_no varchar(50)
		)

		select @customer_name = customer_name from dbo.master_customer where customer_code = @customer_code

		insert into @temp values(
		@customer_code,
		@customer_name,
		@company_code,
		@dp_no,
		@invoice_date,
		@reference_no,
		@invoice_no)

		select * from @temp
	end
	ELSE
	begin
		set @error_message ='CompanyCode CustomerCode ของ Reference No : '+@reference_no+' ไม่ตรงกัน'
	end	
END
GO
