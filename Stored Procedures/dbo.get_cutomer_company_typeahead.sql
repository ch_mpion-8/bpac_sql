SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cutomer_company_typeahead]
	@company_code as varchar(4)= null,
	@search_word as varchar(50) = null
AS
BEGIN 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@search_word is null)
		set @search_word = ''
	
   select o.customer_code, c.customer_name as 'user_display_name' from
	dbo.master_customer_company o, master_customer c
	where o.company_code = @company_code
	and (c.customer_name LIKE '%'+@search_word +'%'  OR o.customer_code LIKE '%'+@search_word+'%')
	and o.customer_code = c.customer_code


	
END
GO
