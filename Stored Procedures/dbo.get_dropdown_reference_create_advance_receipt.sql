SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_dropdown_reference_create_advance_receipt]
	-- Add the parameters for the stored procedure here
	@currency varchar(5)null,
	@company_code varchar(4) =null,
	@customer_code varchar(4) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select r.clearing_document_no,r.reference_document_no,cus.customer_code,cus.customer_name,r.company_code,r.document_date from dbo.receipt r
	inner join dbo.master_customer cus
	on cus.customer_code = r.customer_code
	left join (select i.reference_no,o.receiver,o.company_code from dbo.other_control_document o inner join dbo.other_control_document_item i on o.document_id = i.document_id where o.document_type=1) o
	on o.reference_no = r.reference_document_no
	and o.company_code = r.company_code
	and o.receiver = r.customer_code
	where r.currency = @currency
	and r.customer_code = @customer_code
	and r.company_code = @company_code
	and o.reference_no is null
	and r.receipt_type='normal'
	
END
GO
