SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_bill_presentment_document_control]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@user_name as nvarchar(50) = null
		,@invoice_date_from as date = null
		,@invoice_date_to as date = null
		,@payment_term as nvarchar(200) = null
		,@status as nvarchar(50) = null
		,@bill_presentment_date_from as date = null
		,@bill_presentment_date_to as date = null
		,@bill_presentment_method as nvarchar(50) = null
		,@invoice_no as nvarchar(50) = null
		,@bill_presentment_no as nvarchar(14) = null
		,@output_type as nvarchar(50) = null
		,@email_status as nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	

select  distinct cc.customer_company_id
		,ih.company_code
		,com.company_name
		,ih.customer_code
		,cus.customer_name
		,bdc.bill_presentment_id
		,bdc.bill_presentment_detail_id
		,ih.invoice_no
		,ih.invoice_date
		--,ih.due_date
		,case when cc.company_code in  ('7850','0470') then ih.net_due_date else ih.due_date end as due_date
		,ih.net_due_date
		,bdc.bill_presentment_date   as bill_presentment_date
		, bdc.bill_presentment_no    as bill_presentment_no
		,case when cc.company_code in  ('7850','0470') then '' else ih.payment_term END  as [payment_term_name]
		--,term.payment_term_name
		,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
		,ih.currency
		,isnull(conf.[configuration_value], bdc.method) as bill_presentment_method
		, isnull(bdc.bill_presentment_status,'WAITING')as bill_presentment_status
		,replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING') as invoice_bill_status
		,cc.user_display_name
		, btemp.temp_id
		, case when btemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		,cbb.bill_presentment_behavior_id
		,bdc.reference_guid
		,bdc.is_active
		,cbh.payment_method
		,ec.output_type_code
		,ih.email_status
		,ih.email_receive_date
		into #main
	from dbo.invoice_header ih
	--left join  dbo.invoice_detail id on ih.invoice_no = id.invoice_no
	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code		and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on ih.company_code = com.company_code		
	--left join dbo.bill_presentment_control_document_item bdci on ih.invoice_no = bdci.invoice_no --and bdci.is_active = 1
	--left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
	left join (
		select bdc.bill_presentment_id,bdc.bill_presentment_no,bdc.bill_presentment_date,bdc.bill_presentment_status
				, bdc.method
				,bdc.reference_guid,bdc.remark,cc.company_code
				,bdci.bill_presentment_detail_id,bdci.invoice_no,bdci.is_active
		from dbo.bill_presentment_control_document bdc 
		left join dbo.bill_presentment_control_document_item bdci on  bdc.bill_presentment_id = bdci.bill_presentment_id
		inner join  dbo.master_customer_company cc on bdc.customer_company_id = cc.customer_company_id 
	
		where (isnull(bdci.is_active,1) = 1 or bdc.bill_presentment_status = 'CANCEL' )
	) bdc on ih.invoice_no = bdc.invoice_no and ih.company_code = bdc.company_code
	left join dbo.master_customer_bill_presentment_behavior cbb 
			on ih.bill_presentment_behavior_id = cbb.bill_presentment_behavior_id --and cbb.is_bill_presentment = 1
	left join dbo.bill_presentment_item_temp btemp on ih.invoice_no = btemp.invoice_no and btemp.is_active = 1 
	left join [dbo].[master_customer_collection_behavior] cbh on ih.collection_behavior_id = cbh.collection_behavior_id 
		left join dbo.configuration conf on bdc.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	LEFT JOIN dbo.master_email_config ec ON cc.customer_company_id = ec.customer_company_id
	where (@company_code is null or ih.company_code  in ( select item from dbo.SplitString(@company_code,',')))
			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
			and cus.customer_code = ISNULL(@customer_code,cus.customer_code)
			and (@invoice_date_from is null or 
					(
						@invoice_date_from is not null 
						and @invoice_date_to is not null 
						and ih.invoice_date between @invoice_date_from and @invoice_date_to
					)
				)
			and (@bill_presentment_date_from is null or
					(
						@bill_presentment_date_from is not null 
						and @bill_presentment_date_to is not null 
						and bdc.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
					)
				)
			--and (@status is null 
			--		or (@status = 'WAITING' and replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')  = @status)
			--		or (bdc.bill_presentment_status = @status)
			--	)
			and (@bill_presentment_method is null or bdc.method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
			and (@payment_term is null or ih.payment_term in ( select item from dbo.SplitString(@payment_term,',')))
			and (@user_name is null or cc.user_name = @user_name)
			and (
					(ih.invoice_type = 'RA' and (ih.payment_term not in ('TS00','NT00')or ih.company_code in ('0470','7850')))
					or ih.invoice_type  in ('RC','RD','DA','DC')
				)
			and (@invoice_no is null or ih.invoice_no like '%' +  @invoice_no + '%')
			and (@bill_presentment_no is null or bdc.bill_presentment_no like '%' + @bill_presentment_no + '%')
			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
			and ((bdc.invoice_no is not null ) or isnull(cbb.is_bill_presentment,1) = 1)
			and (((bdc.invoice_no is not null and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')) or replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL')
			--and ((isnull(bdc.is_active,1) = 0 and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')  or isnull(bdci.is_active,1)  = 1 )
			and ( replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL' or bdc.invoice_no is not null)
			AND (@output_type IS NULL OR ISNULL(ec.output_type_code,'') = @output_type)
			AND (@email_status IS NULL OR ISNULL(ih.email_status,'WAITING') = @email_status)

	select *
	into #sub
	from
	(
	select *
	from #main
	union
	select main.customer_company_id
		,main.company_code
		,main.company_name
		,main.customer_code
		,main.customer_name
		,bdci.bill_presentment_id
		,bdci.bill_presentment_detail_id
		,main.invoice_no
		,main.invoice_date
		,main.due_date
		,main.net_due_date
		,bdc.bill_presentment_date as bill_presentment_date
		,bdc.bill_presentment_no
		,main.payment_term_name as payment_term_name
		--,term.payment_term_name
		,main.invoice_amount /*add by Aof*/
		,main.currency
		,isnull(conf.[configuration_value], bdc.method)  as bill_presentment_method
		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
		,main.invoice_bill_status
		,main.user_display_name
		, main.temp_id
		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		,main.bill_presentment_behavior_id
		,main.reference_guid
		,main.is_active
		,main.payment_method
		,main.output_type_code
		,main.email_status
		,main.email_receive_date
	from #main main 
	left join dbo.bill_presentment_control_document_item bdci on main.invoice_no = bdci.invoice_no and bdci.is_active = 1
	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
	left join dbo.configuration conf on bdc.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where invoice_bill_status <> 'CANCEL' and  
				main.invoice_no not in (select invoice_no from #main where bill_presentment_status in( 'COMPLETE' ,'WAITINGRECEIVED')  
			and isnull(is_active,1) = 1 )
	) sub
			
	select * 
	from #sub sub
	where (@status is null 
					--or (@status = 'WAITING' and sub.invoice_bill_status  = @status)
					or (sub.bill_presentment_status = @status)
				)
		and (@bill_presentment_date_from is null or
				(
					@bill_presentment_date_from is not null 
					and @bill_presentment_date_to is not null 
					and sub.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
				)
			)
		and (@bill_presentment_no is null or bill_presentment_no like '%' + @bill_presentment_no + '%')
		and (@bill_presentment_method is null or sub.bill_presentment_method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
		order by customer_name,invoice_date	
			
			drop table #main
			drop table #sub

--	--V4

--;with main as (
--select  distinct cc.customer_company_id
--		,ih.company_code
--		,com.company_name
--		,ih.customer_code
--		,cus.customer_name
--		,bdc.bill_presentment_id
--		,bdc.bill_presentment_detail_id
--		,ih.invoice_no
--		,ih.invoice_date
--		,ih.due_date
--		,ih.net_due_date
--		,bdc.bill_presentment_date   as bill_presentment_date
--		, bdc.bill_presentment_no    as bill_presentment_no
--		,ih.payment_term as payment_term_name
--		--,term.payment_term_name
--		,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
--		,ih.currency
--		, bdc.method as bill_presentment_method
--		, isnull(bdc.bill_presentment_status,'WAITING')as bill_presentment_status
--		,replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING') as invoice_bill_status
--		,cc.user_display_name
--		, btemp.temp_id
--		, case when btemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,cbb.bill_presentment_behavior_id
--		,bdc.reference_guid
--		,bdc.is_active
--	from dbo.invoice_header ih
--	--left join  dbo.invoice_detail id on ih.invoice_no = id.invoice_no
--	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
--	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code		and isnull(cus.is_ignore,0) = 0
--	inner join dbo.master_company com on ih.company_code = com.company_code		
--	--left join dbo.bill_presentment_control_document_item bdci on ih.invoice_no = bdci.invoice_no --and bdci.is_active = 1
--	--left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	left join (
--		select bdc.bill_presentment_id,bdc.bill_presentment_no,bdc.bill_presentment_date,bdc.bill_presentment_status
--				,bdc.method,bdc.reference_guid,bdc.remark,cc.company_code
--				,bdci.bill_presentment_detail_id,bdci.invoice_no,bdci.is_active
--		from dbo.bill_presentment_control_document bdc 
--		left join dbo.bill_presentment_control_document_item bdci on  bdc.bill_presentment_id = bdci.bill_presentment_id
--		inner join  dbo.master_customer_company cc on bdc.customer_company_id = cc.customer_company_id 
	
--		where (isnull(bdci.is_active,1) = 1 or bdc.bill_presentment_status = 'CANCEL' )
--	) bdc on ih.invoice_no = bdc.invoice_no and ih.company_code = bdc.company_code
--	left join dbo.master_customer_bill_presentment_behavior cbb 
--			on ih.bill_presentment_behavior_id = cbb.bill_presentment_behavior_id --and cbb.is_bill_presentment = 1
--	left join dbo.bill_presentment_item_temp btemp on ih.invoice_no = btemp.invoice_no and btemp.is_active = 1 
--	where (@company_code is null or ih.company_code = @company_code)
--			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
--			and cus.customer_code = ISNULL(@customer_code,cus.customer_code)
--			and (@invoice_date_from is null or 
--					(
--						@invoice_date_from is not null 
--						and @invoice_date_to is not null 
--						and ih.invoice_date between @invoice_date_from and @invoice_date_to
--					)
--				)
--			and (@bill_presentment_date_from is null or
--					(
--						@bill_presentment_date_from is not null 
--						and @bill_presentment_date_to is not null 
--						and bdc.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--					)
--				)
--			--and (@status is null 
--			--		or (@status = 'WAITING' and replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')  = @status)
--			--		or (bdc.bill_presentment_status = @status)
--			--	)
--			and (@bill_presentment_method is null or bdc.method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
--			and (@payment_term is null or ih.payment_term in ( select item from dbo.SplitString(@payment_term,',')))
--			and (@user_name is null or cc.user_name = @user_name)
--			and (
--					(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
--					or ih.invoice_type  in ('RC','RD','DA')
--				)
--			and (@invoice_no is null or ih.invoice_no like '%' +  @invoice_no + '%')
--			and (@bill_presentment_no is null or bdc.bill_presentment_no like '%' + @bill_presentment_no + '%')
--			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
--			and ((bdc.invoice_no is not null ) or isnull(cbb.is_bill_presentment,1) = 1)
--			and (((bdc.invoice_no is not null and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')) or replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL')
--			--and ((isnull(bdc.is_active,1) = 0 and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')  or isnull(bdci.is_active,1)  = 1 )
--			and ( replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL' or bdc.invoice_no is not null)
--	),sub as (
--	select *
--	from main
--	union
--	select main.customer_company_id
--		,main.company_code
--		,main.company_name
--		,main.customer_code
--		,main.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,main.invoice_no
--		,main.invoice_date
--		,main.due_date
--		,main.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,main.payment_term_name as payment_term_name
--		--,term.payment_term_name
--		,main.invoice_amount /*add by Aof*/
--		,main.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,main.invoice_bill_status
--		,main.user_display_name
--		, main.temp_id
--		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,main.bill_presentment_behavior_id
--		,main.reference_guid
--		,main.is_active
--	from main 
--	left join dbo.bill_presentment_control_document_item bdci on main.invoice_no = bdci.invoice_no and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	where invoice_bill_status <> 'CANCEL' and  
--				main.invoice_no not in (select invoice_no from main where bill_presentment_status in( 'COMPLETE' ,'WAITINGRECEIVED')  
--			and isnull(main.is_active,1) = 1 )


--	)
--	select * 
--	from sub
--	where (@status is null 
--					--or (@status = 'WAITING' and sub.invoice_bill_status  = @status)
--					or (sub.bill_presentment_status = @status)
--				)
--		and (@bill_presentment_date_from is null or
--				(
--					@bill_presentment_date_from is not null 
--					and @bill_presentment_date_to is not null 
--					and sub.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--				)
--			)
--		and (@bill_presentment_no is null or bill_presentment_no like '%' + @bill_presentment_no + '%')
--		and (@bill_presentment_method is null or sub.bill_presentment_method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
--		order by customer_name,invoice_date	



--	--#v3

--;with main as (
--select  distinct cc.customer_company_id
--		,ih.company_code
--		,com.company_name
--		,ih.customer_code
--		,cus.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,ih.invoice_no
--		,ih.invoice_date
--		,ih.due_date
--		,ih.net_due_date
--		,bdc.bill_presentment_date   as bill_presentment_date
--		, bdc.bill_presentment_no    as bill_presentment_no
--		,ih.payment_term as payment_term_name
--		--,term.payment_term_name
--		,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
--		,ih.currency
--		, bdc.method as bill_presentment_method
--		, isnull(bdc.bill_presentment_status,'WAITING')as bill_presentment_status
--		,replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING') as invoice_bill_status
--		,cc.user_display_name
--		, btemp.temp_id
--		, case when btemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,cbb.bill_presentment_behavior_id
--		,bdc.reference_guid
--		,bdci.is_active
--	from dbo.invoice_header ih
--	left join  dbo.invoice_detail id on ih.invoice_no = id.invoice_no
--	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
--	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code		and isnull(cus.is_ignore,0) = 0
--	inner join dbo.master_company com on ih.company_code = com.company_code		
--	left join dbo.bill_presentment_control_document_item bdci on ih.invoice_no = bdci.invoice_no --and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	left join dbo.master_customer_bill_presentment_behavior cbb 
--			on ih.bill_presentment_behavior_id = cbb.bill_presentment_behavior_id --and cbb.is_bill_presentment = 1
--	left join dbo.bill_presentment_item_temp btemp on ih.invoice_no = btemp.invoice_no and btemp.is_active = 1 
--	where (@company_code is null or ih.company_code = @company_code)
--			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
--			and cus.customer_code = ISNULL(@customer_code,cus.customer_code)
--			and (@invoice_date_from is null or 
--					(
--						@invoice_date_from is not null 
--						and @invoice_date_to is not null 
--						and ih.invoice_date between @invoice_date_from and @invoice_date_to
--					)
--				)
--			and (@bill_presentment_date_from is null or
--					(
--						@bill_presentment_date_from is not null 
--						and @bill_presentment_date_to is not null 
--						and bdc.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--					)
--				)
--			--and (@status is null 
--			--		or (@status = 'WAITING' and replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')  = @status)
--			--		or (bdc.bill_presentment_status = @status)
--			--	)
--			and (@bill_presentment_method is null or bdc.method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
--			and (@payment_term is null or ih.payment_term in ( select item from dbo.SplitString(@payment_term,',')))
--			and (@user_name is null or cc.user_name = @user_name)
--			and (
--					(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
--					or ih.invoice_type  in ('RC','RD','DA')
--				)
--			and (@invoice_no is null or ih.invoice_no like '%' +  @invoice_no + '%')
--			and (@bill_presentment_no is null or bdc.bill_presentment_no like '%' + @bill_presentment_no + '%')
--			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
--			and ((bdci.invoice_no is not null ) or isnull(cbb.is_bill_presentment,1) = 1)
--			and (((bdci.invoice_no is not null and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')) or replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL')
--			and ((isnull(bdci.is_active,1) = 0 and isnull(bdc.bill_presentment_status,'WAITING') = 'CANCEL')  or isnull(bdci.is_active,1)  = 1)
--	),sub as (
--	select *
--	from main
--	union
--	select main.customer_company_id
--		,main.company_code
--		,main.company_name
--		,main.customer_code
--		,main.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,main.invoice_no
--		,main.invoice_date
--		,main.due_date
--		,main.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,main.payment_term_name as payment_term_name
--		--,term.payment_term_name
--		,main.invoice_amount /*add by Aof*/
--		,main.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,main.invoice_bill_status
--		,main.user_display_name
--		, main.temp_id
--		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,main.bill_presentment_behavior_id
--		,main.reference_guid
--		,main.is_active
--	from main 
--	left join dbo.bill_presentment_control_document_item bdci on main.invoice_no = bdci.invoice_no and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	where invoice_bill_status <> 'CANCEL' and  
--				main.invoice_no not in (select invoice_no from main where bill_presentment_status in( 'COMPLETE' ,'WAITINGRECEIVED')  
--			and isnull(main.is_active,1) = 1 )


--	)
--	select * 
--	from sub
--	where (@status is null 
--					--or (@status = 'WAITING' and sub.invoice_bill_status  = @status)
--					or (sub.bill_presentment_status = @status)
--				)
--		and (@bill_presentment_date_from is null or
--				(
--					@bill_presentment_date_from is not null 
--					and @bill_presentment_date_to is not null 
--					and sub.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--				)
--			)
--		and (@bill_presentment_no is null or bill_presentment_no like '%' + @bill_presentment_no + '%')
--		order by customer_name,invoice_date	
	

--#v2
--;with main as (
--   select  distinct cc.customer_company_id
--		,ih.company_code
--		,com.company_name
--		,ih.customer_code
--		,cus.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,ih.invoice_no
--		,ih.invoice_date
--		,ih.due_date
--		,ih.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,ih.payment_term as payment_term_name
--		--,term.payment_term_name
--		,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
--		,ih.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING') as invoice_bill_status
--		,cc.user_display_name
--		, btemp.temp_id
--		, case when btemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,cbb.bill_presentment_behavior_id
--		,bdc.reference_guid
--	from dbo.invoice_header ih
--	left join  dbo.invoice_detail id on ih.invoice_no = id.invoice_no
--	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
--	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code		and isnull(cus.is_ignore,0) = 0
--	inner join dbo.master_company com on ih.company_code = com.company_code		
--	left join dbo.bill_presentment_control_document_item bdci on ih.invoice_no = bdci.invoice_no --and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	left join dbo.master_customer_bill_presentment_behavior cbb 
--			on ih.bill_presentment_behavior_id = cbb.bill_presentment_behavior_id --and cbb.is_bill_presentment = 1
--	left join dbo.bill_presentment_item_temp btemp on ih.invoice_no = btemp.invoice_no and btemp.is_active = 1 
--	where (@company_code is null or ih.company_code = @company_code)
--			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
--			and cus.customer_code = ISNULL(@customer_code,cus.customer_code)
--			and (@invoice_date_from is null or 
--					(
--						@invoice_date_from is not null 
--						and @invoice_date_to is not null 
--						and ih.invoice_date between @invoice_date_from and @invoice_date_to
--					)
--				)
--			and (@bill_presentment_date_from is null or
--					(
--						@bill_presentment_date_from is not null 
--						and @bill_presentment_date_to is not null 
--						and bdc.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--					)
--				)
--			--and (@status is null 
--			--		or (@status = 'WAITING' and replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')  = @status)
--			--		or (bdc.bill_presentment_status = @status)
--			--	)
--			and (@bill_presentment_method is null or bdc.method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
--			and (@payment_term is null or ih.payment_term in ( select item from dbo.SplitString(@payment_term,',')))
--			and (@user_name is null or cc.user_name = @user_name)
--			and (
--					(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
--					or ih.invoice_type  in ('RC','RD','DA')
--				)
--			and (bdci.invoice_no is not null or replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL')
--			and (@invoice_no is null or ih.invoice_no like '%' +  @invoice_no + '%')
--			and (@bill_presentment_no is null or bdc.bill_presentment_no like '%' + @bill_presentment_no + '%')
--			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
--			and (bdci.invoice_no is not null or isnull(cbb.is_bill_presentment,1) = 1)
--	--order by cus.customer_name,ih.invoice_date
--	),sub as (
--	select *
--	from main
--	union
--	select main.customer_company_id
--		,main.company_code
--		,main.company_name
--		,main.customer_code
--		,main.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,main.invoice_no
--		,main.invoice_date
--		,main.due_date
--		,main.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,main.payment_term_name as payment_term_name
--		--,term.payment_term_name
--		,main.invoice_amount /*add by Aof*/
--		,main.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,main.invoice_bill_status
--		,main.user_display_name
--		, main.temp_id
--		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,main.bill_presentment_behavior_id
--		,main.reference_guid
--	from main 
--	left join dbo.bill_presentment_control_document_item bdci on main.invoice_no = bdci.invoice_no and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	where invoice_bill_status <> 'CANCEL' and  main.invoice_no not in (select invoice_no from main where bill_presentment_status in( 'COMPLETE' ,'WAITINGRECEIVED') )


--	)
--	select * 
--	from sub
--	where (@status is null 
--					--or (@status = 'WAITING' and sub.invoice_bill_status  = @status)
--					or (sub.bill_presentment_status = @status)
--				)
--		and (@bill_presentment_date_from is null or
--				(
--					@bill_presentment_date_from is not null 
--					and @bill_presentment_date_to is not null 
--					and sub.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--				)
--			)
--		and (@bill_presentment_no is null or bill_presentment_no like '%' + @bill_presentment_no + '%')
--		order by customer_name,invoice_date	
  

  --#V1
--;with main as (
--   select  distinct cc.customer_company_id
--		,ih.company_code
--		,com.company_name
--		,ih.customer_code
--		,cus.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,ih.invoice_no
--		,ih.invoice_date
--		,ih.due_date
--		,ih.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,ih.payment_term as payment_term_name
--		--,term.payment_term_name
--		,ih.invoice_amount  + isnull(ih.tax_amount,0) as invoice_amount /*add by Aof*/
--		,ih.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING') as invoice_bill_status
--		,cc.user_display_name
--		, btemp.temp_id
--		, case when btemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,cbb.bill_presentment_behavior_id
--		,bdc.reference_guid
--	from dbo.invoice_header ih
--	left join  dbo.invoice_detail id on ih.invoice_no = id.invoice_no
--	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
--	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code		and isnull(cus.is_ignore,0) = 0
--	inner join dbo.master_company com on ih.company_code = com.company_code		
--	left join dbo.bill_presentment_control_document_item bdci on ih.invoice_no = bdci.invoice_no --and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	left join dbo.master_customer_bill_presentment_behavior cbb 
--			on ih.bill_presentment_behavior_id = cbb.bill_presentment_behavior_id --and cbb.is_bill_presentment = 1
--	left join dbo.bill_presentment_item_temp btemp on ih.invoice_no = btemp.invoice_no and btemp.is_active = 1 
--	where (@company_code is null or ih.company_code = @company_code)
--			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
--			and cus.customer_code = ISNULL(@customer_code,cus.customer_code)
--			and (@invoice_date_from is null or 
--					(
--						@invoice_date_from is not null 
--						and @invoice_date_to is not null 
--						and ih.invoice_date between @invoice_date_from and @invoice_date_to
--					)
--				)
--			and (@bill_presentment_date_from is null or
--					(
--						@bill_presentment_date_from is not null 
--						and @bill_presentment_date_to is not null 
--						and bdc.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--					)
--				)
--			--and (@status is null 
--			--		or (@status = 'WAITING' and replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')  = @status)
--			--		or (bdc.bill_presentment_status = @status)
--			--	)
--			and (@bill_presentment_method is null or bdc.method in ( select item from dbo.SplitString(@bill_presentment_method,',')))
--			and (@payment_term is null or ih.payment_term in ( select item from dbo.SplitString(@payment_term,',')))
--			and (@user_name is null or cc.user_name = @user_name)
--			and (
--					(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
--					or ih.invoice_type  in ('RC','RD','DA')
--				)
--			and (bdci.invoice_no is not null or replace(isnull(ih.bill_presentment_status,'WAITING'),'MANUAL','WAITING')   <> 'CANCEL')
--			and (@invoice_no is null or ih.invoice_no like '%' +  @invoice_no + '%')
--			and (@bill_presentment_no is null or bdc.bill_presentment_no like '%' + @bill_presentment_no + '%')
--			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
--			and (bdci.invoice_no is not null or isnull(cbb.is_bill_presentment,1) = 1)
--	--order by cus.customer_name,ih.invoice_date
--	),sub as (
--	select *
--	from main
--	union
--	select main.customer_company_id
--		,main.company_code
--		,main.company_name
--		,main.customer_code
--		,main.customer_name
--		,bdci.bill_presentment_id
--		,bdci.bill_presentment_detail_id
--		,main.invoice_no
--		,main.invoice_date
--		,main.due_date
--		,main.net_due_date
--		,bdc.bill_presentment_date as bill_presentment_date
--		,bdc.bill_presentment_no
--		,main.payment_term_name as payment_term_name
--		--,term.payment_term_name
--		,main.invoice_amount /*add by Aof*/
--		,main.currency
--		,bdc.method as bill_presentment_method
--		,isnull(bdc.bill_presentment_status,'WAITING') as bill_presentment_status
--		,main.invoice_bill_status
--		,main.user_display_name
--		, main.temp_id
--		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
--		,main.bill_presentment_behavior_id
--		,main.reference_guid
--	from main 
--	left join dbo.bill_presentment_control_document_item bdci on main.invoice_no = bdci.invoice_no and bdci.is_active = 1
--	left join dbo.bill_presentment_control_document bdc on bdci.bill_presentment_id = bdc.bill_presentment_id
--	)
--	select * 
--	from sub
--	where (@status is null 
--					--or (@status = 'WAITING' and sub.invoice_bill_status  = @status)
--					or (sub.bill_presentment_status = @status)
--				)
--		and (@bill_presentment_date_from is null or
--				(
--					@bill_presentment_date_from is not null 
--					and @bill_presentment_date_to is not null 
--					and sub.bill_presentment_date between @bill_presentment_date_from and @bill_presentment_date_to
--				)
--			)
--		and (@bill_presentment_no is null or bill_presentment_no like '%' + @bill_presentment_no + '%')
			
--	order by customer_name,invoice_date

END
GO
