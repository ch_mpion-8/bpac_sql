SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_BULK_SAVE_HOLIDAY]
	-- Add the parameters for the stored procedure here
	@holiday_list as [dbo].[Holiday_ForSave] readonly
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @holiday_year nvarchar(250)
	--DECLARE @holiday_month nvarchar(250)
	--DECLARE @holiday_day nvarchar(250)
	--DECLARE @holiday_name nvarchar(250)

	--DECLARE holiday_cursor CURSOR FOR 
	--SELECT @holiday_year,@holiday_month,@holiday_day,@holiday_name
	--FROM @holiday_list
	
		--insert into [dbo].[holidaytest]
		--		(year
		--		,month
		--		,date
		--		,name
			
		--		)
		--		select holiday_year,holiday_month,holiday_day,holiday_name from @holiday_list
	

	---- Open Cursor
	--OPEN holiday_cursor 
	--FETCH NEXT FROM holiday_cursor 
	--INTO @holiday_year,@holiday_month,@holiday_day,@holiday_name

	---- Loop From Cursor
	--WHILE (@@FETCH_STATUS = 0) 
	--BEGIN 
		
		
		insert into [dbo].[master_holiday]
				(holiday_year
				,holiday_month
				,holiday_day
				,holiday_type
				,holiday_name
				,[is_active]
				,[created_date]
				,[created_by]
				)
				select holiday_year,holiday_month,holiday_day,'SCG',holiday_name,1,GETDATE()
				,'SYSTEM' from @holiday_list
			

	--	FETCH NEXT FROM holiday_cursor -- Fetch next cursor
	--	INTO @holiday_year,@holiday_month,@holiday_day,@holiday_name

	--END
	
	--CLOSE holiday_cursor; 
	--DEALLOCATE holiday_cursor; 


	

END
				

GO
