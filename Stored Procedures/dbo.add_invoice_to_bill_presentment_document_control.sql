SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[add_invoice_to_bill_presentment_document_control]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int
	,@invoice_no as varchar(18)
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists(select 1 from dbo.bill_presentment_control_document_item where bill_presentment_id = @bill_presentment_id and invoice_no = @invoice_no)
	begin
		update  dbo.bill_presentment_control_document_item
		set is_active = 1
			,updated_by = @updated_by
			,updated_date = getdate()
		where bill_presentment_id = @bill_presentment_id 
			and invoice_no = @invoice_no
	end
	else
	begin
		insert into dbo.bill_presentment_control_document_item
		(
			bill_presentment_id
			,invoice_no
			,is_active
			,created_date
			,created_by
			,updated_date
			,updated_by
		)
		values
		(
			@bill_presentment_id
			,@invoice_no
			,1
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
		)
	end

	exec dbo.update_invoice_bill_presentment_status
		@invoice_no = @invoice_no
		,@bill_presentment_id = null
		,@status  = 'COMPLETE'
		,@updated_by  = @updated_by

	
	--declare @description as nvarchar(100)
	--set @description = 'Add invoice ' + @invoice_no

	exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_presentment_id
			,@description = @invoice_no
			,@action  = 'Add Invoice'
			,@updated_by  = @updated_by
END
GO
