SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_customer_receiver_typeahead]
	-- Add the parameters for the stored procedure here
	@company_code varchar(4) = null,
	@search_word varchar(100) =null,
	@had_other_contact bit = null,
	@title varchar(100) = null,
	@customer_code varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if @company_code = ''
		set @company_code = NULL

	if @had_other_contact = 1
	begin
		select ocp.other_contact_id as customer_company_id,customer_code ,ocp.customer_name,0 as [type],ocp.contact_person,ocp.title,ocp.telephone ,null as billing_method
		from dbo.master_other_contact_person ocp
		where 
		(ocp.customer_name like '%'+isnull(@search_word,'')+'%' OR ocp.customer_code like '%'+@search_word+'%')
		and title like '%' + isnull(@title,'')+'%'
		and ocp.is_active = 1
		union
		select convert(bigint,mcc.customer_company_id) as customer_company_id,mcc.customer_code,c.customer_name,1,null,null,null  ,oa.billing_method
		from dbo.master_customer_company mcc
		inner join dbo.master_customer c
			on c.customer_code = mcc.customer_code
		outer apply(
			select top 1  bh.billing_method
			from dbo.master_customer_bill_presentment_behavior bh
			where bh.customer_company_id = mcc.customer_company_id
			order by bh.payment_term_code
		) as oa
		where 
		mcc.company_code = isnull(@company_code,mcc.company_code)
		and (c.customer_code like '%'+@search_word+'%' or c.customer_name like '%'+isnull(@search_word,'')+'%')
		and c.customer_code = isnull(@customer_code,c.customer_code)
		and c.is_active=1
	end
	else
	begin
		select convert(bigint,mcc.customer_company_id) as customer_company_id,mcc.customer_code,c.customer_name,1,null,null,null  ,oa.billing_method
		from dbo.master_customer_company mcc
		inner join dbo.master_customer c
			on c.customer_code = mcc.customer_code
		outer apply(
			select top 1  bh.billing_method
			from dbo.master_customer_bill_presentment_behavior bh
			where bh.customer_company_id = mcc.customer_company_id
			order by bh.payment_term_code
		) as oa
		where mcc.company_code = isnull(@company_code,mcc.company_code)
		and (c.customer_code like '%'+@search_word+'%' or c.customer_name like '%'+@search_word+'%')
		and c.customer_code = isnull(@customer_code,c.customer_code)
		and c.is_active = 1
	end

	

END
GO
