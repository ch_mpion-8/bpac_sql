SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_customer_address]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null -- '2011271' --
	,@customer_company_id as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select address_id
			,ad.customer_company_id	
			,cc.company_code
			,com.company_name
			,customer_code	
			,title	
			,[address]	
			,sub_district	
			,district	
			,province	
			,post_code	
			,is_ems	
			,is_allowed_edit
			,ad.[is_active]
	from dbo.master_customer_address ad
	inner join dbo.master_customer_company  cc on ad.customer_company_id = cc.customer_company_id
	inner join dbo.master_company com on cc.company_code = com.company_code
	where (@customer_code is null or customer_code = @customer_code )--and ad.[status] = 'ACTIVE'
			and (@customer_company_id is null or ad.customer_company_id = @customer_company_id)
			and ad.is_active = 1
END
GO
