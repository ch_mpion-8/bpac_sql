SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[update_cheque_invoice_document_control_remark]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
	,@remark as nvarchar(2000) 
	,@collection_date as date = null
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.cheque_invoice_control_document
	set remark = @remark
		,collection_date = @collection_date
		,updated_by = @updated_by
		,updated_date = getdate()
	where cheque_control_id = @cheque_control_id

	
	declare @description as nvarchar(2000) 
	select @description = 'Collection Date : ' + cast(@collection_date as nvarchar(20)) + '</br>Remark : ' + @remark


	exec dbo.insert_cheque_invoice_control_history
			@cheque_control_id = @cheque_control_id
			,@description = @description
			,@action  = 'Update Collection Date / Remark'
			,@updated_by  = @updated_by


END
GO
