SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SP_GET_MISMATCH_BY_ID]
	-- Add the parameters for the stored procedure here
	@mismatch_id as  int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT 
	mm.mismatch_id,mm.mismatch_date,mm.amount,mm.interest_day,mm.interest_rate,mm.mismatch_status,mm.reason,mm.solution
	,mcom.company_name,mcc.company_code,mc.customer_code,mc.customer_name,mm.reference_guid
	,mcc.user_display_name as [collection_user]
	from mismatch mm 
	left join master_customer_company mcc on mm.customer_code = mcc.customer_code and mm.company_code = mcc.company_code
	left join master_customer mc on mc.customer_code = mcc.customer_code
	left join master_company mcom on mcom.company_code = mcc.company_code
	
	where  mismatch_id = @mismatch_id
END
GO
