SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_contact]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null
	,@customer_company_id as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select ct.contact_id
			,ct.customer_company_id	
			,cc.company_code
			,com.company_name
			,customer_code	
			,ct.contact_name	
			,ct.position
			,ct.email	
			,ct.telephone	
			,ct.fax	
			,ct.remark	
			,ct.is_show_in_ems	
			,ct.is_show_in_other_document
			,ct.[is_active]
	from dbo.master_customer_contact ct
	inner join dbo.master_customer_company  cc on ct.customer_company_id = cc.customer_company_id
	inner join dbo.master_company com on cc.company_code = com.company_code
	where (@customer_code is null or customer_code = @customer_code )--and ad.[status] = 'ACTIVE'
		and (@customer_company_id is null or ct.customer_company_id = @customer_company_id)
		and ct.is_active = 1


END
GO
