SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_DUPLICATE_PYC]
	-- Add the parameters for the stored procedure here
	@document_id int =null,
	@reference_type int=null,
	@old_trip_date date =null,
	@trip_date date =null,
	@count_trip int=null,
	@remark varchar(max)=null,
	@submitby varchar(50)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @old_trip_date = @trip_date
		return

	if exists(select 1 from dbo.scg_pyc_process where reference_no_id = @document_id and reference_type = @reference_type and trip_date = @trip_date)
	return

	insert into dbo.scg_pyc_process(
	reference_no_id,
	reference_type,
	trip_date,
	count_trip,
	remark,
	is_skip,
	created_by,
	created_date)
	values(
	@document_id,
	@reference_type,
	@trip_date,
	@count_trip,
	@remark,
	0,
	@submitby,
	GETDATE()
	)



    
END
GO
