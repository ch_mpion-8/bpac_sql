SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_other_doc_item_add]
	-- Add the parameters for the stored procedure here
	@document_id int =null,
	@reference varchar(500) =null,
	@create_by varchar(50)=null,
	@create_date datetime = null,
	@action_type int =null,
	@type varchar(10) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @doc_type int

	Declare @is_exists bit = 0
	declare @ref_year varchar(4) = null

	select @doc_type = document_type from dbo.other_control_document where document_id = @document_id

	if @doc_type =1 
	begin
		--for advance receipt need to split year 
		 set @ref_year = SUBSTRING(@reference,CHARINDEX('_',@reference)+1,4)
		set @reference =  SUBSTRING(@reference,1,CHARINDEX('_',@reference)-1)
	end

	if (@doc_type = 4 and isnull(@type,'add') = 'update' and  EXISTS(select 1 from dbo.other_control_document_item where document_id = @document_id and reference_no = @reference and action_type = @action_type))
		return

    IF (
		(@doc_type=4 AND isnull(@type,'add') <> 'update' AND  NOT EXISTS(select 1 from dbo.other_control_document_item where document_id = @document_id and reference_no=@reference and action_type = @action_type))
		OR 
	    (@doc_type<>4 AND  NOT EXISTS(select 1 from dbo.other_control_document_item where document_id = @document_id and reference_no=@reference))
	)
	begin
		insert into dbo.other_control_document_item(
		document_id,
		reference_no,
		[year],
		created_by,
		created_date,
		action_type)
		values(
		@document_id,
		@reference,
		@ref_year,
		@create_by,
		@create_date,
		@action_type)
		
		insert into dbo.other_control_document_history(document_id,
		[action],
		created_by,
		created_date,
		[description])
		values(
		@document_id,
		'Add',
		@create_by,
		@create_date,
		'Reference No: '+@reference)
	end
	else
	begin

		--for otherdoc type =4 if user delete send action and update another receive item action  that had same reference no then insert it and fix history to update 
		if isnull(@type,'add') = 'update' and not exists(select 1 from dbo.other_control_document_item where document_id = @document_id and reference_no = @reference)
		begin
			insert into dbo.other_control_document_item(
				document_id,
				reference_no,
				created_by,
				created_date,
				action_type)
				values(
				@document_id,
				@reference,
				@create_by,
				@create_date,
				@action_type)

				insert into dbo.other_control_document_history(document_id,
				[action],
				created_by,
				created_date,
				[description])
				values(
				@document_id,
				'Updated',
				@create_by,
				@create_date,
				'Reference No: '+@reference+' Change Action Type: '+case when @action_type = 0 then 'Send/Receive' 
				when @action_type = 1 then 'Send'
				else 'Receive' end)

			return
		end


		Declare @old int =null
		select @old = action_type from dbo.other_control_document_item where document_id = @document_id and reference_no = @reference
		Declare @sac varchar(20)=null

		if @old != @action_type
		begin
			update dbo.other_control_document_item
				set action_type = @action_type,
				updated_by=@create_by,
				updated_date = @create_date		
				where document_id = @document_id
				and reference_no = @reference

				insert into dbo.other_control_document_history(document_id,
				[action],
				created_by,
				created_date,
				[description])
				values(
				@document_id,
				'Updated',
				@create_by,
				@create_date,
				'Reference No: '+@reference+' Change Action Type: '+case when @action_type = 0 then 'Send/Receive' 
				when @action_type = 1 then 'Send'
				else 'Receive' end)
		end
		
	end
END
GO
