SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_UPDATE_ZEROLOAN_STATUS]
	-- Add the parameters for the stored procedure here
	@status int,
	@cheque_detail_id int,
	@zero_loan_id int,
	@submit_by nvarchar(100),
	@amount decimal
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @id int
	SELECT @id=zero_loan_id FROM zero_loan_item where cheque_control_id = @cheque_detail_id;
	IF @status = 1
	begin
       DELETE FROM zero_loan_item where cheque_control_id = @cheque_detail_id
	   update zero_loan set summary = ISNULL(summary,0) - ISNULL(@amount,0) where zero_loan_id = @id
	end
	ELSE 
	begin
	   update zero_loan_item set zero_loan_status = @status ,updated_by=@submit_by,updated_date=GETDATE()
	   where cheque_control_id = @cheque_detail_id and zero_loan_detail_id = @zero_loan_id
	   
	   update zero_loan set summary = ISNULL(summary,0) + ISNULL(@amount,0) where zero_loan_id = @id
	end
	if((select count(*) from zero_loan_item where zero_loan_id = @id) = 0)
	begin
	  DELETE FROM zero_loan where zero_loan_id = @id
	end
END
GO
