SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_user_in_charge]
	-- Add the parameters for the stored procedure here
	@assign_to_user_name as nvarchar(50) = null --'10002'
		,@assign_to_user_display_name as nvarchar(200) = null --'Arya'
		,@updated_by as nvarchar(50) = null --
		,@customer_company_ids as nvarchar(2000) = null --'6,7,8,9'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    

	update dbo.master_customer_company
	set [user_name] = @assign_to_user_name
		,[user_display_name] = @assign_to_user_display_name
		,updated_by = @updated_by
		,updated_date = GETDATE()
	--select  *
	--from dbo.master_customer_company mas
	where customer_company_id in (select item from dbo.SplitString(@customer_company_ids,','))


END




GO
