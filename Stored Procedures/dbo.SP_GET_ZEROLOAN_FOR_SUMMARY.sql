SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_ZEROLOAN_FOR_SUMMARY]
	-- Add the parameters for the stored procedure here
	@date Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select zl.zero_loan_detail_id,zl.zero_loan_id,cci.cheque_control_detail_id,mcc.company_code,mcom.company_name,mcc.user_display_name as [collection_user]
	,ccd.cheque_control_no,ccd.collection_date,mcc.customer_code,mc.customer_name,cci.reference_document_no,re.document_amount,cci.created_by ,z.submit_by as [submit by]
	,zl.zero_loan_status as [status],re.currency
	from zero_loan_item zl 
		inner join cheque_control_document_item cci on cci.cheque_control_detail_id = zl.cheque_control_id
	inner join cheque_control_document ccd on ccd.cheque_control_id = cci.cheque_control_id
	inner join master_customer_company mcc on ccd.customer_company_id = mcc.customer_company_id
	inner join  (
			select  reference_document_no,sum(ABS(document_amount)) as document_amount,customer_code,company_code,currency
			from dbo.receipt
			where document_type = 'RA'
			and receipt_type = 'ADVANCE' 
			group by reference_document_no,customer_code,company_code,year(document_date),currency
	) re on cci.reference_document_no = re.reference_document_no
		and mcc.customer_code = re.customer_code
	and mcc.company_code = re.company_code

	inner join master_customer mc on mc.customer_code = mcc.customer_code
	inner join master_company mcom on mcc.company_code = mcom.company_code
	inner join zero_loan z on z.zero_loan_id = zl.zero_loan_id
	where  ( @date is null or  datediff(day, @date, z.zero_loan_date) = 0)
			and (( ccd.cheque_control_status = 'CANCEL' and zl.zero_loan_status = 3) or (ccd.cheque_control_status = 'WAITINGRECEIVED'))
				and (cci.is_active = 1 or zl.zero_loan_status = 3)
END
GO
