SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cheque_invoice_document_control_detail]
	@cheque_control_id as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 
      -- Insert statements for procedure here
	select bcdi.cheque_control_detail_id
			,bcdi.cheque_control_id
			,bcdi.invoice_no
			,convert(varchar, ih.invoice_date, 103) as document_date
			,ih.payment_term
			,case when ih.due_date is not null then convert(varchar, ih.due_date , 103) 
				else '' end as  due_date
			,bcdi.amount as document_amount
			,ih.currency
			, ih.invoice_amount + isnull(ih.tax_amount,0)  as original_amount
	from dbo.cheque_invoice_control_document_item bcdi
	inner join dbo.cheque_invoice_control_document bcd on bcdi.cheque_control_id = bcd.cheque_control_id
	inner join dbo.master_customer_company c on bcd.customer_company_id = c.customer_company_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no 
								and c.customer_code = ih.customer_code 
								and c.company_code = ih.company_code
	where bcdi.cheque_control_id = @cheque_control_id
		and (bcd.cheque_control_status = 'CANCEL' or bcdi.is_active = 1)
	--group by bcdi.cheque_control_detail_id
	--		,bcdi.cheque_control_id
	--		,bcdi.reference_document_no
	--		,convert(varchar, r.document_date, 103) 
	--		,r.payment_term
	--		,case when r.base_line_due_date is not null then convert(varchar, r.base_line_due_date , 103) 
	--			else '' end 
	--		,r.currency	
		


	select [action]
			,[description]
			,convert(varchar, [bpac_due_date], 103) as [bpac_due_date] 
			,convert(varchar, [created_date], 103)+ ' '  + convert(varchar(8), [created_date], 14) as [created_date]
			,[created_by]
	from dbo.cheque_invoice_control_document_history
	where cheque_control_id = @cheque_control_id

	
	declare @payment_term as varchar(4)
			,@collection_date as date
			,@currency as varchar(5)
			,@customer_company_id as int


	select @payment_term = ih.payment_term
			,@collection_date = bcd.collection_date
			,@currency = ih.currency
			,@customer_company_id = bcd.customer_company_id
	from dbo.cheque_invoice_control_document bcd
	inner join dbo.cheque_invoice_control_document_item bcdi on bcd.cheque_control_id = bcdi.cheque_control_id
	inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no
	where bcd.cheque_control_id = @cheque_control_id


	select distinct ih.invoice_no +'|' + cast(ih.remaining_amount as nvarchar(50)) as value
	from dbo.invoice_header ih
	inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
	where cc.customer_company_id = @customer_company_id 
			and ih.payment_term = @payment_term
			and ih.currency = @currency
			and ih.remaining_amount > 0
			and ih.invoice_no not in 
				( select bcdi.invoice_no 
					from dbo.cheque_invoice_control_document_item bcdi
					where cheque_control_id = @cheque_control_id and bcdi.is_active = 1
				)


END
GO
