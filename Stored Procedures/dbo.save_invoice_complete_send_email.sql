SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_invoice_complete_send_email]
    -- Add the parameters for the stored procedure here
    @document_id INT = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    UPDATE inv
    SET inv.email_status = 'SENT'
    FROM dbo.bill_presentment_control_document bp
        INNER JOIN dbo.bill_presentment_control_document_item bpi
            ON bp.bill_presentment_id = bpi.bill_presentment_id
        INNER JOIN dbo.invoice_header inv
            ON inv.invoice_no = bpi.invoice_no
    WHERE bp.bill_presentment_id = @document_id
          AND bpi.is_active = 1;
END;
GO
