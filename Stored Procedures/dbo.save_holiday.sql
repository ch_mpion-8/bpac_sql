SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_holiday]
	-- Add the parameters for the stored procedure here
	@date_from as varchar(10) = null--'20170413'
	,@date_to as varchar(10) = null--'20170416'
	,@holiday_type as nvarchar(20) = null--'SCG'
	,@holiday_name as nvarchar(200) = null--'วันสงกรานต์' 
	,@status as bit = null--'ACTIVE'
	,@updated_by as nvarchar(50)  = null--
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 
		select [date]
			,day([date]) as [day]
			,month([date]) as [month]
			,year([date]) as [year]
		into #tmpDate
		from (
		SELECT  TOP (DATEDIFF(DAY, @date_from, @date_to) + 1)
				[date] = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id) - 1, @date_from)

		FROM    sys.all_objects a CROSS JOIN sys.all_objects b
		) a
		--select * from #tmpDate
		
		select item 
		into #tmpType
		from dbo.SplitString(@holiday_type,',')


		

		update [dbo].[master_holiday]
			set holiday_name = @holiday_name
				,[is_active] = @status
				,[updated_date] = getdate()
				,[updated_by] = @updated_by
		--select  *
		from #tmpDate tmp 
		left join dbo.master_holiday h on tmp.[year]  = h.holiday_year
			and  tmp.[month] = h.holiday_month
			and tmp.[day] = h.holiday_day
			and (@holiday_type is null or holiday_type in (select item from #tmpType))
		where h.holiday_year is not null




	

	insert into [dbo].[master_holiday]
				(holiday_year
				,holiday_month
				,holiday_day
				,holiday_type
				,holiday_name
				,[is_active]
				,[created_date]
				,[created_by]
				,[updated_date]
				,[updated_by])

	select 
		a.[year]
		,a.[month]
		,a.[day]
		,b.Item
		,@holiday_name 
		,@status 
		,getdate() 
		,@updated_by
		,getdate()
		,@updated_by
	from 
	(
		select tmp.[year]
				,tmp.[month]
				,tmp.[day]
		from #tmpDate tmp 	
		left join dbo.master_holiday h on tmp.[year]  = h.holiday_year
			and  tmp.[month] = h.holiday_month
			and tmp.[day] = h.holiday_day
			and (@holiday_type is null or holiday_type in (select item from #tmpType))

		where  h.holiday_year is null
	)a , #tmpType b

	--select * from dbo.master_holiday
	--where holiday_year = 2017
	--		and holiday_month = 4
	--		and (@holiday_type is null or holiday_type in (select item from dbo.SplitString(@holiday_type,',')))

	drop table #tmpDate
	drop table #tmpType


END




GO
