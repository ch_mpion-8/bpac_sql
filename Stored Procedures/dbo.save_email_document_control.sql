SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_email_document_control]
	-- Add the parameters for the stored procedure here
	@id UNIQUEIDENTIFIER = NULL OUTPUT,
    @document_id INT =NULL,
	@document_type varchar(30) =NULL,
	@email_type varchar(30) = NULL,
	@email_to VARCHAR(MAX) =NULL,
	@email_cc VARCHAR(MAX) =NULL,
	@action VARCHAR(50) =NULL,
	@submited_by VARCHAR(50) =NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF @id IS NULL 
	BEGIN
		DECLARE @t TABLE (id UNIQUEIDENTIFIER NULL)
		INSERT INTO dbo.email_document_control
		(
		    id,
		    document_id,
		    document_type,
		    email_type,
		    email_to,
		    email_cc,
		    created_date,
		    created_by,
		    used,
		    used_by,
		    used_date,
			is_active
		)
		OUTPUT Inserted.id INTO @t
		VALUES
		(   NEWID(),      -- id - uniqueidentifier
		    @document_id,         -- document_id - int
		    @document_type,        -- document_type - varchar(30)
		    @email_type,        -- email_type - varchar(30)
		    @email_to,        -- email_to - varchar(max)
		    @email_cc,        -- email_cc - varchar(max)
		    GETDATE(), -- created_date - datetime
		    @submited_by,        -- created_by - varchar(50)
		    0,      -- used - bit
		    NULL,        -- used_by - varchar(50)
		    NULL,  -- used_date - datetime,
			1
		    )

		SELECT @id = id FROM @t
	END	
	ELSE IF @action = 'complete'
	BEGIN
		UPDATE dbo.email_document_control
		SET used = 1
		,used_by = @submited_by
		,used_date = GETDATE()
		WHERE id = @id
	END	
END
GO
