SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[get_report_bill_presentment_control_document]
	-- Add the parameters for the stored procedure here
	@company_code varchar(100)=null,
	@document_date date=null,
	@method varchar(100)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Declare @t_method Table(method varchar(15))
	if @company_code = 'All'
		set @company_code = NULL

	insert into @t_method select * from dbo.SplitString(@method,',')
	

	select h.bill_presentment_no,
	h.bill_presentment_date,
	c.customer_code,
	cus.customer_name as user_display_name,
	c.company_code,
	com.company_name,
	inv.invoice_no,
	inv.due_date,
	inv.invoice_date,
	p.payment_term_name,
	inv.invoice_amount+isnull(inv.tax_amount,0) as invoice_amount ,
	inv.currency,
	isnull(conf.[configuration_value], h.method) as method,
	h.created_by,
	h.created_date
	from dbo.bill_presentment_control_document h
	inner join dbo.bill_presentment_control_document_item i on i.bill_presentment_id = h.bill_presentment_id
	inner join dbo.master_customer_company c on c.customer_company_id = h.customer_company_id
	inner join dbo.invoice_header inv on i.invoice_no = inv.invoice_no
		AND inv.company_code = c.company_code
	inner join dbo.master_customer cus on cus.customer_code = c.customer_code
	inner join dbo.master_company com on com.company_code = c.company_code
	inner join dbo.master_payment_term p on p.payment_term_code = inv.payment_term
	left join dbo.configuration conf on h.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where (h.method in (select * from @t_method) or @method is null or @method ='')
	and (@company_code is null OR @company_code ='' or c.company_code in ( select item from dbo.SplitString(@company_code,',')))
	and (convert(date,h.created_date) = @document_date or @document_date is null)
	and h.bill_presentment_status <>'CANCEL'
	and i.is_active =1
	order by h.bill_presentment_date,cus.customer_code,inv.invoice_no,h.bill_presentment_no
END
GO
