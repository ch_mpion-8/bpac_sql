SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[update_ems_upload]
	-- Add the parameters for the stored procedure here
	@ems_control_id as int = null
		,@reference_guid as uniqueidentifier = null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   declare @status as nvarchar(20) = 'COMPLETE'
			,@description as nvarchar(200) = 'Complete by upload ems document control.'
			,@id as nvarchar(200)
			,@type as nvarchar(200)

	update dbo.ems_control_document
	set [status] = @status
		,reference_guid = @reference_guid
		,updated_by = @updated_by
		,updated_date = getdate()
	where ems_control_document_id = @ems_control_id

	
	declare table_cursor cursor for 

		
	select reference_id, reference_type
	from  dbo.ems_control_document e
	inner join dbo.ems_control_document_item i on e.ems_control_document_id = i.ems_control_document_id
	inner join dbo.ems_sub_item s on i.ems_detail_id = s.ems_detail_id
	where i.is_active= 1 and s.is_active = 1 and e.ems_control_document_id = @ems_control_id

	open table_cursor;
	fetch next from table_cursor into @id,@type
	while @@fetch_status = 0
	   begin

			
			if @type = 'B'
			begin
					
				if exists (
					select 1
					from dbo.bill_presentment_control_document
					where bill_presentment_status = 'WAITINGRECEIVED' and bill_presentment_id = @id)
				begin 
					exec [dbo].[update_bill_presentment_upload] 
						@bill_presentment_id  = @id
						,@reference_guid = null
						,@updated_by  = @updated_by
						,@description  = @description
				end
			end
			else if  @type = 'O'
			begin
				if exists (
					select 1
					from dbo.other_control_document
					where status not in ( 'C','CANCEL') and document_id = @id)
				begin 
					declare @updated_date as datetime = null
					select @updated_date =  getdate()
					exec [dbo].[update_attachment_otherdoc]
						@document_id  = @id,
						@guid = null,
						@update_by  =@updated_by,
						@update_date  =  @updated_date,
						@description  = @description
				end
			end

		  fetch next from table_cursor into @id,@type
	   end;

	close table_cursor;
	deallocate table_cursor;

	exec dbo.insert_ems_control_history
			@ems_control_document_id = @ems_control_id
			,@description = 'Upload ems document.'
			,@action  = 'Complete'
			,@updated_by  = @updated_by


END
GO
