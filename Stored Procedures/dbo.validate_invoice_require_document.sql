SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec dbo.validate_invoice_require_document ''
-- =============================================
CREATE PROCEDURE [dbo].[validate_invoice_require_document] 
	@invoice_no as nvarchar(20) = NULL
	,@company_code as varchar(4) = NULL --'0230'
	,@year as int = NULL --2019
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @require_doc as nvarchar(2000) = NULL
			,@is_waiting_all_dp BIT = 0
			,@is_miss_doc BIT = 0
			,@has_data BIT = 0
			,@doc as nvarchar(2000) = ''


			
	DECLARE doc_cursor CURSOR FOR  
		SELECT CASE WHEN c.configuration_code = '1' OR c.configuration_code = '2'  THEN 'INV'
					WHEN c.configuration_code = '4' OR c.configuration_code = '11'  THEN 'PO'
					WHEN c.configuration_code = '8'   THEN 'PL'
					WHEN c.configuration_code = '10'   THEN 'OT'
					ELSE c.configuration_value END AS doc_key
					,1

		FROM dbo.invoice_header ih
		INNER JOIN dbo.master_customer_bill_presentment_document bd ON ih.bill_presentment_behavior_id = bd.bill_presentment_behavior_id
		LEFT JOIN dbo.configuration c ON bd.document_code = c.configuration_code AND c.configuration_key = 'BillingDocument'
			
		WHERE    bd.document_code in ('1','2','3','4','8','10','11')
			AND ih.invoice_no = @invoice_no
			AND ih.company_code = @company_code
			AND YEAR(ih.invoice_date) = @year
  
	OPEN doc_cursor;  
	FETCH NEXT FROM doc_cursor INTO @require_doc, @has_data;  
  
	-- Check @@FETCH_STATUS to see if there are any more rows to fetch.  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
  
	    SET @require_doc = CASE WHEN @require_doc IN ('INV','CN','DN') THEN 'INV' ELSE @require_doc END 

		IF(@require_doc = 'DP')
		BEGIN 
			SELECT @is_waiting_all_dp = is_waiting_all_dp
			FROM dbo.master_company 
			WHERE company_code = @company_code

			IF @is_waiting_all_dp = 0
			BEGIN
				IF NOT EXISTS ( SELECT 1 FROM dbo.invoice_document_detail
									WHERE invoice_no = @invoice_no 
										AND company_code = @company_code
										AND [year] = @year
										AND document_type = @require_doc )
				BEGIN
					SET @is_miss_doc = 1
					SET @doc = CASE WHEN @doc = '' THEN 'DP' ELSE CONCAT(@doc, ', DP') END
				END
            
			END 
			ELSE
            BEGIN
				IF EXISTS ( SELECT 1 FROM dbo.invoice_detail id
				LEFT JOIN dbo.invoice_document_detail  idd ON id.invoice_no = id.invoice_no 
										AND id.dp_no = LEFT(idd.document_name,10)
										AND idd.document_type = @require_doc
				WHERE id.invoice_no = @invoice_no
						AND idd.document_name IS NULL )
				BEGIN
					SET @is_miss_doc = 1
					SET @doc = CASE WHEN @doc = '' THEN 'DP' ELSE CONCAT(@doc, ', DP')  END
				END
				
            
			END 
		END
        ELSE IF(@require_doc = 'PO' OR @require_doc = 'PL')
		BEGIN
			
			IF NOT EXISTS ( SELECT 1 FROM dbo.invoice_document_detail
								WHERE invoice_no = @invoice_no 
									AND company_code = @company_code
									AND [year] = @year
									AND document_type IN ('PO' , 'PL'))
			BEGIN
				SET @is_miss_doc = 1
				SET @doc = CASE WHEN @doc = '' THEN 'PO or PL' ELSE CONCAT(@doc, ', PO or PL')  END
            END
        END
		ELSE
        BEGIN
		
			IF NOT EXISTS ( SELECT 1 FROM dbo.invoice_document_detail
								WHERE invoice_no = @invoice_no 
									AND company_code = @company_code
									AND [year] = @year
									AND document_type = @require_doc)
			BEGIN
				SET @is_miss_doc = 1
				SET @doc = CASE WHEN @doc = '' THEN @require_doc ELSE CONCAT(@doc, ', ',@require_doc)  END
            END
            
        END
        
		
	   -- This is executed as long as the previous fetch succeeds.  
	   FETCH NEXT FROM doc_cursor  INTO @require_doc, @has_data;  
	END  
	CLOSE doc_cursor;  
	DEALLOCATE doc_cursor;  

	IF @has_data = 1
	BEGIN
		
		DECLARE @print_status AS NVARCHAR(20) = NULL
		IF EXISTS(SELECT 1 FROM dbo.invoice_document_detail WHERE ISNULL(print_count,0) <=0  AND invoice_no = @invoice_no AND company_code = @company_code AND [year] = @year)
		BEGIN 
			SET @print_status = 'WAITING'
		END 
		ELSE
		BEGIN 
			SET @print_status = 'COMPLETE'
		END 
        
		update main
		set require_document_status = case when @is_miss_doc = 0 then 'COMPLETE'
										else 'WAITING' end
			, require_document = @doc
			, print_document_status = @print_status
		from dbo.invoice_header main
		WHERE  main.invoice_no = @invoice_no and YEAR(main.invoice_date) = @year and main.company_code = @company_code
	END 


 --   declare @require_doc as nvarchar(2000) = null

	--;with requireDoc as (
	--	select  distinct main.invoice_no,main.company_code , main.invoice_date,	stuff(
	--	(
	--		select ', ' +  c.configuration_value
	--			FROM dbo.invoice_header ih
	--			INNER JOIN dbo.master_customer_bill_presentment_document bd ON ih.bill_presentment_behavior_id = bd.bill_presentment_behavior_id
	--			LEFT JOIN dbo.configuration c ON bd.document_code = c.configuration_code AND c.configuration_key = 'BillingDocument'
	--			left join dbo.invoice_document_detail idd 
	--				on ih.invoice_no = idd.invoice_no and idd.is_active = 1
	--					and c.configuration_code = case when upper(idd.document_type) = 'INV' or upper(idd.document_type) = 'DN' 
	--													or upper(idd.document_type) = 'CN' or upper(idd.document_type) = 'DL' then '1' -- invoice
	--												when upper(idd.document_type) = 'DP' then '3' -- dp
	--												when upper(idd.document_type) = 'PO' then '4' -- po
	--												when upper(idd.document_type) = 'PL' then '8' -- picking list
	--											else '10' -- other doc 
	--											end
	--			WHERE   idd.document_id is null
	--			  AND bd.document_code in ('1','3','4','8','10')
	--			  AND ih.invoice_no = main.invoice_no
	--			  AND ih.company_code = main.company_code
	--			  AND ih.invoice_date = main.invoice_date
			
	--			for xml path('')
	--	) ,1,1,'') as doc
	--	FROM dbo.invoice_header main
	--	WHERE  main.bill_presentment_status NOT IN ('COMPLETE' ,'CANCEL')
	--	  AND main.bill_presentment_behavior_id IS NOT NULL
	--	  AND ISNULL(main.require_document_status,'') <> 'COMPLETE'
	--	  AND main.invoice_no = @invoice_no
	--	-- AND main.invoice_no in ( '1580311650', '1580369810','1580310629','1580309656')
	--)
	--	update main
	--	set require_document_status = case when r.doc is null or r.doc = '' then 'COMPLETE'
	--									else 'WAITING' end
	--		, require_document = r.doc
	--	from invoice_header main
	--	inner join requireDoc r on main.invoice_no = r.invoice_no and main.invoice_date = r.invoice_date and main.company_code = r.company_code

END
GO
