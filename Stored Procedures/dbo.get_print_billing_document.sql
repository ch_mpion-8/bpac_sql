SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_print_billing_document]
	@document_id varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select 
	isnull(conf.[configuration_value], o.method) as method,
	o.document_date as other_doc_date,
	o.document_no,
	o.remark,
	c.customer_code,
	cus.customer_name,
	i.action_type,
	inv.invoice_no,
	inv.invoice_date,
	c.user_display_name,
	com.company_name
	from dbo.other_control_document o
	inner join dbo.other_control_document_item i 	on i.document_id = o.document_id
	inner join dbo.invoice_header inv 	on i.reference_no = inv.invoice_no
	inner join dbo.master_customer_company c on c.customer_code = inv.customer_code and c.company_code = inv.company_code
	inner join dbo.master_customer cus	on cus.customer_code = c.customer_code
	inner join dbo.master_company com	on com.company_code = c.company_code
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where o.document_id = @document_id


END
GO
