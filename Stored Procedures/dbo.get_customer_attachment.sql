SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_attachment]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select [attachment_id]
      ,ca.[customer_company_id]
	  ,cc.company_code
	  ,com.company_name
	  ,cc.customer_code
      ,[title]
      ,[remark]
      ,[reference_guid]
	  ,[file_name]
      ,ca.[is_active]
	from dbo.master_customer_attachment ca
	inner join dbo.master_customer_company  cc on ca.customer_company_id = cc.customer_company_id
	inner join dbo.master_company com on cc.company_code = com.company_code
		where customer_code = @customer_code 
		and ca.is_active = 1
END
GO
