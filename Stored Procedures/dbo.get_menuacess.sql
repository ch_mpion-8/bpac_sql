SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_menuacess]
	-- Add the parameters for the stored procedure here
	@user_role as nvarchar(100) --= 'Admin,Collection'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @Names VARCHAR(8000) 
	SELECT @Names = COALESCE(@Names + ', ', '') + menu_access 
	FROM menu_access 
	where role_name in (select * from dbo.SplitString(@user_role,',')) 
	select @Names as menu_access
END
GO
