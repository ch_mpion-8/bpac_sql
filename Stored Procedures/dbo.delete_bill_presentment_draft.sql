SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_bill_presentment_draft]
	-- Add the parameters for the stored procedure here
	@temp_id as varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from bill_presentment_item_temp
	where temp_id = @temp_id

	
	delete from bill_presentment_temp
	where temp_id = @temp_id


END


GO
