SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_other_doc_detail]
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @temp table(
	document_id int,
	document_no varchar(100),
	document_date datetime,
	reference_no varchar(500),
	customer_name varchar(100),
	company_code varchar(4),
	method varchar(50),
	action_type int,
	remark varchar(max),
	created_by varchar(50),
	customer_code nvarchar(100),
	other_doc_date datetime,
	company_name varchar(200),
	item_create_date datetime,
	sequence_no int
	)

	insert into @temp 
	select 
	o.document_id,
	o.document_no,
	o.document_date,
	i.reference_no,
	null,
	o.company_code,
	o.method,
	i.action_type,
	o.remark,
	o.created_by,
	o.receiver,
	o.document_date,
	com.company_name,
	i.created_date,
	i.sequence_no
	from dbo.other_control_document o
	inner join dbo.master_company com
	on com.company_code = o.company_code
	left join dbo.other_control_document_item i
	on i.document_id = o.document_id
	where o.document_id = @document_id

	Declare @recevier_type int =null,@receiver_name nvarchar(100) =null,@contact_person_name nvarchar(100),@customer_name varchar(100) =null,@customer_code varchar(20) =null
	select @recevier_type = contact_person_type,
	@receiver_name = receiver_name,
	@customer_code = receiver ,
	@contact_person_name = contact_person
	from dbo.other_control_document o
	where o.document_id = @document_id


	if isnull(@recevier_type,1) = 1
	begin
		select @customer_name = customer_name from dbo.master_customer where customer_code = @customer_code
	end
	else
	begin
		set @customer_name = @receiver_name
		update @temp
		set customer_code = @contact_person_name
	end

	update @temp
	set customer_name = @customer_name

	select * from @temp order by item_create_date asc,sequence_no asc

	delete from @temp

END
GO
