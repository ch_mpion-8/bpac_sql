SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[update_other_document_date]
	-- Add the parameters for the stored procedure here
	@document_id int =null,
	@doc_date date =null,
	@submited_by varchar(50) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if @doc_date is not null or @doc_date !=''  or @doc_date !=' '
	begin
		update dbo.other_control_document
		set document_date = @doc_date,
		update_date = getdate(),
		updated_by = @submited_by
		where document_id = @document_id

		insert into dbo.other_control_document_history
		(
			document_id,
			[action],
			[description],
			created_by,
			created_date
		)
		values(
		@document_id,
		'Update',
		'Document Date : '+convert(varchar(20),@doc_date),
		@submited_by,
		getdate()
		)
	end
END
GO
