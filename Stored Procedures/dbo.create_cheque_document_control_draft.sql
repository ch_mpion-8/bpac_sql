SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[create_cheque_document_control_draft]
	-- Add the parameters for the stored procedure here
	 @document_control_list  as [dbo].[document_control_table_type] readonly
	 ,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	declare @customer_company_id [int]  = NULL,
		@collection_date [date] =  NULL,
		@method [nvarchar](20) = NULL,
		@payment_term [varchar](4) = NULL,
		@currency [varchar](5) = NULL,
		@remark [nvarchar](2000) = NULL,
		@reference_no_list [nvarchar](max) = NULL


	declare table_cursor cursor for 

	select [customer_company_id]
			,[date]
			,[method]
			,[payment_term]
			,[currency]
			,[remark]
			,[reference_no_list]
	from @document_control_list

	open table_cursor;
	fetch next from table_cursor into @customer_company_id,@collection_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	while @@fetch_status = 0
	   begin
      
		  declare @tempId as int = null

		  if exists (select 1 from dbo.cheque_temp 
								where customer_company_id = @customer_company_id
									and payment_term = @payment_term
									and method = @method
									and currency = @currency
									and collection_date = @collection_date)
			begin
			
				select @tempId = temp_id from dbo.cheque_temp 
								where customer_company_id = @customer_company_id
									and payment_term = @payment_term
									and method = @method
									and currency = @currency
									and collection_date = @collection_date

				update dbo.cheque_temp
				set remark = @remark
					,updated_by = @updated_by
					,updated_date = getdate()
				where temp_id = @tempId

			end
			else
			begin
			
				insert into  dbo.cheque_temp
				(
					[customer_company_id]
					,collection_date
					,[method]
					,[payment_term]
					,[currency]
					,[remark]
					,created_by
					,created_date
					,updated_by
					,updated_date
				)
				values
				(
					@customer_company_id
					,@collection_date
					,@method
					,@payment_term
					,@currency
					,@remark
					,@updated_by
					,getdate()
					,@updated_by
					,getdate()
				)

				select @tempId = SCOPE_IDENTITY()

			end

			update dbo.cheque_item_temp
			set is_active = 0
				,updated_by = @updated_by
				,updated_date = getdate()	
			from dbo.cheque_item_temp ci 
			inner join dbo.cheque_temp c on ci.temp_id = c.temp_id
			where ci.temp_id <> @tempId 
				and c.customer_company_id = @customer_company_id
				and reference_document_no in (select rtrim(ltrim(item)) as item from dbo.SplitString(@reference_no_list,','))

			update dbo.cheque_item_temp
			set is_active = 1
				,updated_by = @updated_by
				,updated_date = getdate()
			where temp_id = @tempId and reference_document_no in (select rtrim(ltrim(item)) as item from dbo.SplitString(@reference_no_list,','))

			insert into dbo.cheque_item_temp
			(
				temp_id
				,reference_document_no
				,created_by
				,created_date
				,updated_by
				,updated_date
				,is_active
				,document_year
			)
			select @tempId
					,rtrim(ltrim(substring(item,0,charindex('|', item)))) as item
					,@updated_by
					,getdate()
					,@updated_by
					,getdate()
					,1
					,right(item,charindex('|', reverse(item)) -1 )
			from dbo.SplitString(@reference_no_list,',') ss
			left join dbo.cheque_item_temp tmp on rtrim(ltrim(ss.Item)) = tmp.reference_document_no and tmp.temp_id = @tempId
			where tmp.reference_document_no is null


	   
		  fetch next from table_cursor into  @customer_company_id,@collection_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	   end;

	close table_cursor;
	deallocate table_cursor;

END
GO
