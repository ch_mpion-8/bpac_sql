SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_cheque_document_control_for_print]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as nvarchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select bcd.cheque_control_id
			,bcd.cheque_control_no
			,bcd.customer_company_id
			,cc.company_code
			,com.company_name
			,cc.customer_code
			,cus.customer_name
			,isnull(conf.[configuration_value],bcd.method )  as receive_method
			,bcd.collection_date
			,bcd.remark
			,cc.user_display_name
			,bcd.updated_date
	from dbo.cheque_control_document bcd
	inner join dbo.master_customer_company cc on bcd.customer_company_id = cc.customer_company_id
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
	inner join dbo.master_company com on cc.company_code = com.company_code
	left join dbo.configuration conf on bcd.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where cheque_control_id in ( select item from dbo.SplitString( @cheque_control_id,','))

	select bcdi.cheque_control_detail_id
			,bcdi.cheque_control_id
			,bcdi.reference_document_no
			,convert(varchar, ih.document_date, 103) as document_date
			,sum(ih.document_amount) as document_amount
			--,ih.document_amount as document_amount
			,ih.currency
			,bcdi.document_year
	from dbo.cheque_control_document_item bcdi
	inner join dbo.cheque_control_document bcd on bcdi.cheque_control_id = bcd.cheque_control_id
	inner join dbo.master_customer_company c on bcd.customer_company_id = c.customer_company_id
	left join dbo.receipt ih 
	on bcdi.reference_document_no = ih.reference_document_no 
	and c.customer_code = ih.customer_code 
	and c.company_code = ih.company_code
	and year(ih.document_date) = bcdi.[document_year]
	where  bcdi.cheque_control_id in ( select item from dbo.SplitString( @cheque_control_id,','))
		and bcdi.is_active = 1
	group by bcdi.cheque_control_detail_id
			,bcdi.cheque_control_id
			,bcdi.reference_document_no
			,convert(varchar, ih.document_date, 103) 
			,ih.currency
			,bcdi.document_year

END
GO
