SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_report_user_performance]
	-- Add the parameters for the stored procedure here
	@year varchar(4) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--get count bill normal
	select * from (
			select 'm'+convert(varchar(2),month(b.bill_presentment_date)) as m,isnull(user_display_name,'Customer Need User')as user_display_name from (
			select distinct b.bill_presentment_id,b.bill_presentment_date,c.user_display_name from dbo.bill_presentment_control_document b
			inner join dbo.bill_presentment_control_document_item i
			on i.bill_presentment_id = b.bill_presentment_id
			inner join dbo.invoice_header inv
			on inv.invoice_no = i.invoice_no
			inner join dbo.master_customer_bill_presentment_behavior h
			on h.bill_presentment_behavior_id = inv.bill_presentment_behavior_id
			inner join dbo.master_customer_company c
			on c.customer_company_id = b.customer_company_id
			where h.document_type = 'Normal'
			and year(b.bill_presentment_date) = convert(int,@year)
			and i.is_active = 1
			and CHARINDEX('สร้างใบคุมบนSAP',isnull(inv.bill_presentment_remark,'')) = 0
			and c.user_display_name is not null
		) as b
	) as t
	pivot (
	 count(m) for m in (m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12)
	)as pv

	--get count bill special
	select * from (
			select'm'+convert(varchar(2),month(b.bill_presentment_date)) as m,isnull(user_display_name,'Customer Need User') as user_display_name from (
			select distinct b.bill_presentment_id,b.bill_presentment_date,c.user_display_name from dbo.bill_presentment_control_document b
			inner join dbo.bill_presentment_control_document_item i
			on i.bill_presentment_id = b.bill_presentment_id
			inner join dbo.invoice_header inv
			on inv.invoice_no = i.invoice_no
			inner join dbo.master_customer_bill_presentment_behavior h
			on h.bill_presentment_behavior_id = inv.bill_presentment_behavior_id
			inner join dbo.master_customer_company c
			on c.customer_company_id = b.customer_company_id
			where h.document_type = 'Special'
			and year(b.bill_presentment_date) = convert(int,@year)
			and i.is_active = 1
			and c.user_display_name is not null
			and CHARINDEX('สร้างใบคุมบนSAP',isnull(inv.bill_presentment_remark,'')) = 0
		) as b
	) as t
	pivot (
	 count(m) for m in (m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12)
	)as pv


	-- get count invoice bill



	select* from (
	select 'm'+convert(varchar(2),month(b.bill_presentment_date)) as m,c.company_code,i.bill_presentment_detail_id  from dbo.bill_presentment_control_document_item i
	inner join dbo.bill_presentment_control_document b 
	on i.bill_presentment_id = b.bill_presentment_id
	inner join dbo.master_customer_company c
	on c.customer_company_id = b.customer_company_id
	inner join dbo.invoice_header inv
	on inv.invoice_no = i.invoice_no
	where year(b.bill_presentment_date) = convert(int,@year)
	and i.is_active = 1
	and CHARINDEX('สร้างใบคุมบนSAP',isnull(inv.bill_presentment_remark,'')) = 0
	) as t
	pivot (
	 count(bill_presentment_detail_id) for m in (m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12)
	)as pv

	--count clearing
	select* from (
		select c.user_display_name,'m'+convert(varchar(2),month(cl.clearing_date))  as m from dbo.invoice_clearing_detail cl
		inner join dbo.master_customer_company c
		on cl.company_code = c.company_code
		and cl.customer_code = c.customer_code
		where is_count = 1
		and year(cl.clearing_date) = convert(int,@year)
		and cl.document_type not in ('ZY','DB')
		and c.user_display_name is not null
	) as t 
		pivot (
	 count(m) for m in (m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12)
	)as pv

	--amount clearing
	select * from (
		select c.company_code,'m'+convert(varchar(2),month(cl.clearing_date)) as  m,convert(float,cl.document_amount) as amount from dbo.invoice_clearing_detail cl
		inner join dbo.master_company c
		on cl.company_code = c.company_code
		and cl.is_count = 1
		where year(cl.clearing_date) = convert(int,@year)
		and cl.document_type not in ('ZY','DB')
	) as t 
		pivot (
	 sum(amount) for m in (m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12)
	)as pv




END
GO
