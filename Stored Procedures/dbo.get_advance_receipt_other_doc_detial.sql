SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_advance_receipt_other_doc_detial]
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select 
	o.document_id,
	o.remark,
	o.created_by,
	o.method,
	c.company_code,
	c.customer_code,
	cus.customer_name,
	i.clearing_document_no,
	i.document_date,
	i.reference_no,
	o.document_no,
	o.document_date as other_doc_date,
	i.currency,
	i.document_amount,
	com.company_name,
	i.[year],
	isnull(i.is_active,1) as is_active
	from dbo.other_control_document o
	outer apply(
		select 
			r.clearing_document_no,
			r.document_date,
			i.reference_no,
			r.currency,
			r.[year],
			r.document_amount,
			r.is_active
		from dbo.other_control_document_item i
		inner join dbo.receipt r
		on r.reference_document_no = i.reference_no
		and r.[year] = i.[year]
		and r.receipt_type = 'normal'
		and r.customer_code = o.receiver
		and r.company_code =o.company_code
		where i.document_id = o.document_id
	) as i
	inner join dbo.master_customer_company c
	on c.customer_code = o.receiver
	and c.company_code = o.company_code
	inner join dbo.master_customer cus
	on c.customer_code = cus .customer_code
	inner join dbo.master_company com
	on com.company_code = o.company_code
	where o.document_id = @document_id

	--select 
	--o.document_id,
	--o.remark,
	--o.created_by,
	--o.method,
	--c.company_code,
	--c.customer_code,
	--cus.customer_name,
	--r.clearing_document_no,
	--r.document_date,
	--i.reference_no,
	--o.document_no,
	--o.document_date as other_doc_date,
	--r.currency,
	--r.document_amount,
	--com.company_name,
	--isnull(r.is_active,1) as is_active
	-- from dbo.other_control_document o
	--left join dbo.other_control_document_item i
	--on o.document_id=i.document_id
	--inner join dbo.receipt r
	--on r.reference_document_no = i.reference_no
	--and r.customer_code = o.receiver
	--and r.company_code =o.company_code
	--inner join dbo.master_customer_company c
	--on c.customer_code = r.customer_code
	--and c.company_code = r.company_code
	--inner join dbo.master_customer cus
	--on c.customer_code = cus .customer_code
	--inner join dbo.master_company com
	--on com.company_code = r.company_code
	--where o.document_id = @document_id


END

SET ANSI_NULLS ON
GO
