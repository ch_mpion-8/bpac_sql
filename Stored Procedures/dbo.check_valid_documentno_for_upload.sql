SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[check_valid_documentno_for_upload]
	@document_no varchar(30) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if exists (select 1 from dbo.other_control_document o where o.document_no = @document_no)
		select convert(bit,1) as is_valid
	else if exists (select 1 from dbo.bill_presentment_control_document o where o.bill_presentment_no = @document_no)
		select convert(bit,1) as is_valid
	else if exists (select 1 from dbo.ems_control_document o where o.ems_control_document_no = @document_no)
		select convert(bit,1) as is_valid
	else
		select convert(bit,0) as is_valid
END
GO
