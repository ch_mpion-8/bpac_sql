SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_holiday]
	-- Add the parameters for the stored procedure here
	@year as nvarchar(200) = null--'2017,9999'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select h.holiday_id
			,h.holiday_year
			,h.holiday_month
			,h.holiday_day
			,h.holiday_name
			,h.holiday_type
			,h.[is_active]
			,h.created_date
			,h.created_by
			,h.updated_date
			,h.updated_by
	from dbo.master_holiday h
	where (@year is null or CONVERT(varchar(10), h.holiday_year) = @year or CONVERT(varchar(10), h.holiday_year)  in (select * from SplitString(@year,',')))
			and h.holiday_type <> 'DEFAULT'
			and [is_active] = 1
			and holiday_type = 'SCG'
END





GO
