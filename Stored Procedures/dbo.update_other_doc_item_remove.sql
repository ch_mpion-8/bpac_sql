SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_other_doc_item_remove]
	@document_id int =null,
	@reference varchar(500) = null,
	@create_by varchar(20)=null,
	@create_date datetime  = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @doc_type int = (select document_type from dbo.other_control_document where document_id = @document_id)
	if @doc_type =1
	begin
		--for advance receipt need to split year 
		declare @ref_year varchar(4) = SUBSTRING(@reference,CHARINDEX('_',@reference)+1,4)
		set @reference =  SUBSTRING(@reference,1,CHARINDEX('_',@reference)-1)

		IF EXISTS(select 1 from dbo.other_control_document_item where document_id=@document_id and reference_no = @reference)
		begin
			delete from dbo.other_control_document_item 
			where document_id = @document_id
			and reference_no = @reference
			and [year] = @ref_year

			insert into dbo.other_control_document_history([action],
			document_id,
			[description],
			created_by,
			created_date)
			values('Remove',
			@document_id,
			'Reference No:'+@reference,
			@create_by,
			@create_date)
		end
	end
	else
	begin
		IF EXISTS(select 1 from dbo.other_control_document_item where document_id=@document_id and reference_no = @reference)
		begin
			delete from dbo.other_control_document_item 
			where document_id = @document_id
			and reference_no = @reference

			insert into dbo.other_control_document_history([action],
			document_id,
			[description],
			created_by,
			created_date)
			values('Remove',
			@document_id,
			'Reference No:'+@reference,
			@create_by,
			@create_date)
		end
	end

	
END

SET ANSI_NULLS ON
GO
