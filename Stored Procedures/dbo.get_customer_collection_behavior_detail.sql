SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_collection_behavior_detail]
	-- Add the parameters for the stored procedure here
	@collection_behavior_id as int = null
	--,@customer_company_id as int = null --4
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select cb.[group_id] as [collection_behavior_id]
      ,cb.[customer_company_id]
	  ,cc.company_code
	  ,cc.customer_code
      ,[valid_date_from]
      ,cb.[valid_date_to]
      --,cb.[payment_term_code]
	  ,(
					select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_collection_behavior] b
						where b.customer_company_id = cb.customer_company_id 
									and b.group_id = cb.group_id 
									and b.is_active = 1
								for xml path('')) ,1,1,'') as txt
				
				) as [payment_term_code]
	  --,term.payment_term_name
	  ,(
					select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_collection_behavior] b
						where b.customer_company_id = cb.customer_company_id 
									and b.group_id = cb.group_id 
									and b.is_active = 1
								for xml path('')) ,1,1,'') as txt
				) as [payment_term_name]
      ,cb.[address_id]
	  ,ad.title as address_title
      ,[time]
      ,[collection_condition_date]
      ,[period_from]
      ,[period_to]
      ,[description]
      ,[remark]
      ,[condition_by]
      ,abs([payment_day]) as [payment_day]
      ,isnull([is_business_day],0) as [is_business_day]
      ,[date_from]
      ,[date_to]
      ,[last_x_date]
	  ,last_x_business_day
      ,[custom_date]
      ,[custom_day]
      ,[condition_week_by]
      ,[custom_week]
	  , next_x_week
      ,[condition_month_by]
      ,[custom_month]
      ,[next_x_month]
      ,[is_next_collection_date]
      ,[payment_method]
      ,cb.[bank]
	  --,b.bank_name
      ,[notify_day]
      ,[is_skip_customer_holiday]
      ,[is_skip_scg_holiday]
      ,[document]
      ,[custom_document]
	  ,receipt_method
      ,cb.[is_active]
      ,cb.[created_date]
      ,cb.[created_by]
      ,cb.[updated_date]
      ,cb.[updated_by]
	  ,cb.custom_date_next_x_month
	  , cc.[user_name]
  from [dbo].[master_customer_collection_behavior] cb
	inner join  dbo.master_customer_company cc on cb.customer_company_id = cc.customer_company_id
	--inner join dbo.master_payment_term term on cb.payment_term_code = term.payment_term_code
	left join dbo.master_customer_address ad on cb.address_id = ad.address_id
	--inner join dbo.master_bank b on cb.bank_id = b.bank_id
	where cb.[group_id] = @collection_behavior_id
			and cb.is_active = 1

	--select collection_custom_date_id
	--	,[date] as custom_date 
	--from dbo.master_customer_collection_custom_date
	--where collection_behavior_id = @collection_behavior_id and [status] = 'ACTIVE'

	--select address_id
	--		,customer_company_id	
	--		,title	
	--		,[address]	
	--		,sub_district	
	--		,district	
	--		,province	
	--		,post_code	
	--		,is_ems	
	--		,is_allowed_edit
	--		,[status] 
	--from dbo.master_customer_address 
	--where customer_company_id = @customer_company_id
	--	and [status] = 'ACTIVE'
end
GO
