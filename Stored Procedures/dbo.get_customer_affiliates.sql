SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_affiliates]
	-- Add the parameters for the stored procedure here
	 @customer_code as varchar(10) = null --'2007211'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @aff_id as int
	select  @aff_id = affiliate_id
	from dbo.master_customer_affiliate_list 
	where customer_code = @customer_code
			and [is_active] = 1


	select distinct aff.affiliate_id
			,aff.affiliate_detail_id
			,aff.customer_code
			,cus.customer_name
			,aff.[is_active]
			,case when cc.customer_company_id is not null then cast(1 as bit) else cast(0 as bit) end as is_has_com
			,cus2.customer_code as search_customer_code
			,cus2.customer_name as search_customer_name 
	from dbo.master_customer_affiliate_list aff 
	inner join dbo.master_customer cus on aff.customer_code= cus.customer_code
	left join dbo.master_customer_company cc on aff.customer_code = cc.customer_code
	inner join dbo.master_customer cus2 on cus2.customer_code= @customer_code
	where aff.affiliate_id = @aff_id
			and aff.customer_code <> @customer_code
			and aff.is_active = 1
	order by aff.customer_code

END
GO
