SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_other_receiver_title_typeahead]
	-- Add the parameters for the stored procedure here
	@search_word varchar(100) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

	select distinct title from dbo.master_other_contact_person
	where 
		UPPER(title) like '%'+isnull(UPPER(@search_word),'')+'%'

	

END
GO
