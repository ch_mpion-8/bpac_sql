SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[SP_GET_ZEROLOAN]
		 @company_code as varchar(200) = null-- '0230'
		,@customer as nvarchar(100) =null--  '2003137'
		,@recript as nvarchar(100) =  null --'AA0001100003'
		,@cheque_date as varchar(100) = null--'EMS,Messenger'
		,@cheque_no as nvarchar(100) = null--'C17-0480-00005'
		,@user as nvarchar(100) = null
		,@status as int =null-- 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT null as zero_loan_detail_id,null as zero_loan_id,cci.cheque_control_detail_id,mcc.company_code,mcom.company_name,mcc.user_display_name as [collection_user]
	,ccd.cheque_control_no,ccd.collection_date,mcc.customer_code,mc.customer_name,cci.reference_document_no,re.document_amount,cci.created_by,null as [submit by],1 as [Status] 
	,re.currency
	--select *
	from cheque_control_document ccd 
	inner join cheque_control_document_item cci on cci.cheque_control_id = ccd.cheque_control_id
	inner join master_customer_company mcc on ccd.customer_company_id = mcc.customer_company_id
	inner join (
			select  reference_document_no,sum(document_amount) as document_amount,customer_code,company_code,currency
			from dbo.receipt
			where document_type = 'RA'
			and receipt_type = 'ADVANCE' 
			group by reference_document_no,customer_code,company_code,[year],currency
	) re on cci.reference_document_no = re.reference_document_no
		and mcc.customer_code = re.customer_code
	and mcc.company_code = re.company_code
	inner join master_customer mc on mc.customer_code = mcc.customer_code
	inner join master_company mcom on mcc.company_code = mcom.company_code
	where 
	 ccd.cheque_control_status = 'WAITINGRECEIVED'
	and ccd.method = 'PYC'
	--order by cheque_control_detail_id 
	--and re.reference_document_no = 'AA0001100003'
	and (cci.cheque_control_detail_id  not in (select cheque_control_id from zero_loan_item))
	and ( @company_code is null or mcc.company_code  in ( select item from dbo.SplitString(@company_code,',')))
	and ( @customer is null or mcc.customer_code = @customer or mc.customer_name = @customer)
	and ( @recript is null or cci.reference_document_no like '%' +  @recript + '%')
		and ( @cheque_date is null or datediff(day, ccd.collection_date, convert(datetime, LEFT(@cheque_date,10), 103)) = 0)
	and ( @cheque_no is null or ccd.cheque_control_no like '%'+ @cheque_no + '%')
	and ( @user is null or mcc.user_name = @user)
	and ( @status is null or @status = 1)
	and cci.is_active = 1 
	
	union 
	
	select zl.zero_loan_detail_id,zl.zero_loan_id,cci.cheque_control_detail_id,mcc.company_code,mcom.company_name,mcc.user_display_name as [collection_user]
	,ccd.cheque_control_no,ccd.collection_date,mcc.customer_code,mc.customer_name,cci.reference_document_no,re.document_amount,cci.created_by ,z.submit_by,zl.zero_loan_status
	,re.currency
	from zero_loan_item zl 

	inner join cheque_control_document_item cci on cci.cheque_control_detail_id = zl.cheque_control_id
	inner join cheque_control_document ccd on ccd.cheque_control_id = cci.cheque_control_id
	inner join master_customer_company mcc on ccd.customer_company_id = mcc.customer_company_id
	inner join master_customer mc on mc.customer_code = mcc.customer_code
	inner join master_company mcom on mcc.company_code = mcom.company_code
	inner join (
			select  reference_document_no,sum(document_amount) as document_amount,customer_code,company_code,currency
			from dbo.receipt
			where document_type = 'RA'
			and receipt_type = 'ADVANCE' 
			group by reference_document_no,customer_code,company_code,year(document_date),currency
	) re on cci.reference_document_no = re.reference_document_no
		and mcc.customer_code = re.customer_code
		and mcc.company_code = re.company_code
		
	inner join zero_loan z on z.zero_loan_id = zl.zero_loan_id
	where  ( @company_code is null or mcc.company_code  in ( select item from dbo.SplitString(@company_code,',')))
	and  ccd.method = 'PYC'
	and ( @customer is null or mcc.customer_code = @customer or mc.customer_name = @customer)
	and ( @recript is null or cci.reference_document_no like '%' +  @recript + '%')
	and ( @cheque_date is null or datediff(day, ccd.collection_date, convert(datetime, LEFT(@cheque_date,10), 103)) = 0)
    and ( @cheque_no is null or ccd.cheque_control_no like '%'+ @cheque_no + '%')
	and ( @user is null or mcc.user_name= @user)
	and ( @status is null or zero_loan_status =  @status)
	and (( ccd.cheque_control_status = 'CANCEL' and zl.zero_loan_status = 3) or (ccd.cheque_control_status = 'WAITINGRECEIVED'))
		
	and cci.is_active = 1 
END
GO
