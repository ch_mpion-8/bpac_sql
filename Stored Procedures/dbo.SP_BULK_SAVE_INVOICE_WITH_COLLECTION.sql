SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_BULK_SAVE_INVOICE_WITH_COLLECTION]
	-- Add the parameters for the stored procedure here
	@invoice_list as [dbo].[InvoiceCalculateNew_With_Status] readonly
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @row_count int = 0;

	DECLARE @invoice_no nvarchar(100)
	DECLARE @bill_prement_date nvarchar(100)
	DECLARE @bill_prement_notify_date nvarchar(100)
	DECLARE @bill_prement_log nvarchar(100)
	DECLARE @collection_prement_date nvarchar(100)
	DECLARE @collection_prement_notify_date nvarchar(100)
	DECLARE @collection_prement_log nvarchar(100)
	DECLARE @billing_behavior_id int
	DECLARE @collection_behavior_id int

	DECLARE @updated_by as nvarchar(50) = 'SYSTEM'

	DECLARE @billing_status as nvarchar(max)
	DECLARE @collection_status as nvarchar(max)

	DECLARE invoice_cursor CURSOR FOR 
	SELECT invoice_no,bill_prement_date,bill_prement_notify_date,
	bill_prement_log,collection_prement_date,collection_prement_notify_date,
	collection_prement_log,
	billing_behavior_id,
	collection_behavior_id,
	billing_status,
	collection_status 
	FROM @invoice_list
	
	declare @billcount int =  (SELECT COUNT(*) 
	FROM @invoice_list);
	insert into [SCGBPAC].[dbo].[service_log] (webkey,[message],[date]) values ('TEST COUNT BEFORE','BEFORE COLLECTION COUNT' + convert(varchar,(SELECT COUNT(*) 
	FROM @invoice_list)),GETDATE())

	-- Open Cursor
	OPEN invoice_cursor 
	FETCH NEXT FROM invoice_cursor 
	INTO @invoice_no, @bill_prement_date,@bill_prement_notify_date,@bill_prement_log,
	@collection_prement_date,@collection_prement_notify_date,@collection_prement_log
	,@billing_behavior_id
	,@collection_behavior_id
	,@billing_status
	,@collection_status

	-- Loop From Cursor
	WHILE (@@FETCH_STATUS = 0) 
	BEGIN 
		--declare @isManual as bit

		--select @isManual = case when isnull(collection_status,'') = 'MANUAL' then 1 else 0 end
		--from dbo.invoice_header
		--where invoice_no = @invoice_no
		
		
		-- /*add condition if status id manual then update only behavior by nan 20180703*/
		-- IF (@isManual = 1)
		--BEGIN
		--	update dbo.invoice_header
		--		set collection_calculation_log = 'success calculate behavior. update only collection_behavior_id'
		--		,collection_behavior_id = @collection_behavior_id
		--		,updated_date_time = GETDATE()
		--		,updated_by = @updated_by
		--		where invoice_no = @invoice_no

		--		set @row_count = @row_count +1;
		--END
		--ELSE 
		IF (@collection_status = 'MANUAL' and @collection_behavior_id != 0)
		BEGIN
			update dbo.invoice_header
				set collection_calculation_log = @collection_prement_log
					,updated_date_time = GETDATE()
				,collection_behavior_id = @collection_behavior_id
				,updated_by = @updated_by
				where invoice_no = @invoice_no

				set @row_count = @row_count +1;
		END
		ELSE IF (@collection_prement_date is null) or (@collection_prement_notify_date is null)  BEGIN 
			update dbo.invoice_header
				set collection_calculation_log = @collection_prement_log
					,updated_date_time = GETDATE()
					,collection_date = null
				,collection_notify_date = null 
				,collection_behavior_id = @collection_behavior_id,
				collection_status = 'ERROR'
				,updated_by = @updated_by
				where invoice_no = @invoice_no
				and isnull(collection_status,'') not in ('COMPLETE','CANCEL')


				set @row_count = @row_count +1;
		END
		ELSE BEGIN
			update dbo.invoice_header
				set collection_date = convert(datetime, LEFT(@collection_prement_date,10), 103)
				,collection_notify_date = convert(datetime, LEFT(@collection_prement_notify_date,10), 103) 
				,collection_calculation_log = 'success calculate : '  + @collection_prement_date
				,collection_behavior_id = @collection_behavior_id
				,collection_status = null
					,updated_date_time = GETDATE()
				,updated_by = @updated_by
				where invoice_no = @invoice_no
				and isnull(collection_status,'') not in ('COMPLETE','CANCEL') /*add by Aof 13-12-2017 */

				set @row_count = @row_count +1;
		 END

		FETCH NEXT FROM invoice_cursor -- Fetch next cursor
		INTO @invoice_no, @bill_prement_date,@bill_prement_notify_date,@bill_prement_log,
		@collection_prement_date,@collection_prement_notify_date,@collection_prement_log
		,@billing_behavior_id
		,@collection_behavior_id
		,@billing_status
		,@collection_status

	END
	
	CLOSE invoice_cursor; 
	DEALLOCATE invoice_cursor; 

	insert into [SCGBPAC].[dbo].[service_log] (webkey,[message],[date]) values ('TEST COUNT ' + convert(varchar(10),getdate(),120),'AFTER COLLECTION COUNT' + convert(varchar,@row_count),GETDATE())

	--UPDATE invoice_header set collection_calculation_log = '' where invoice_no in (select ih.invoice_no from invoice_header ih
	--				inner join master_customer_collection_behavior mccb
	--				on ih.collection_behavior_id = mccb.collection_behavior_id
	--				left join bill_presentment_control_document_item bi 
	--				on bi.invoice_no = ih.invoice_no
	--				left join bill_presentment_control_document b 
	--				on bi.bill_presentment_id = b.bill_presentment_id
	--				where mccb.collection_condition_date = 'BPAC Bill Date'
	--				and b.bill_presentment_date is  null
	--				and b.bill_presentment_status is null or b.bill_presentment_status = 'CANCEL')

		UPDATE invoice_header set collection_calculation_log = null,collection_status = null where invoice_no in (select ih.invoice_no from invoice_header ih 
									inner join master_customer_company mcc
									on ih.company_code = mcc.company_code 
									and ih.customer_code = mcc.customer_code
									inner join master_customer_collection_behavior mcb
									on mcc.customer_company_id = mcb.customer_company_id
									and mcb.collection_condition_date = 'BPAC Bill Date'
									and ih.payment_term = mcb.payment_term_code
									left join bill_presentment_control_document_item b
									on b.invoice_no = ih.invoice_no
									left join bill_presentment_control_document bd
									on b.bill_presentment_id = bd.bill_presentment_id
									where 
									((bd.bill_presentment_date is  null) or (bd.bill_presentment_date is not null and bd.bill_presentment_status = 'CANCEL'))
									and 
									(isnull(ih.collection_status,'') not in ('COMPLETE','CANCEL') )/*add by Aof 13-12-2017*/
									and ((bd.bill_presentment_status is null or bd.bill_presentment_status = 'CANCEL'))
									)
	

END
				

GO
