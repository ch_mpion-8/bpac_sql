SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[save_receive_cheque]
	-- Add the parameters for the stored procedure here
	 @cheque_control_detail_id as  nvarchar(2000) = null --'AA0001030160,AI0001041407,AA0001030094,AA0001040020'
	,@cheque_no as varchar(50) = null 
	,@cheque_amount as decimal(15,2) = null 
	,@cheque_date as date = null 
	,@cheque_currency as varchar(5) = null 
	,@cheque_bank as nvarchar(2000) = null 
	,@remark as nvarchar(2000) = null 
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update dbo.cheque_control_document_item
    set cheque_no = @cheque_no
		,cheque_amount = @cheque_amount
		,cheque_date = @cheque_date
		,cheque_currency = @cheque_currency
		,cheque_bank = @cheque_bank
		,cheque_remark = @remark
		,updated_by = @updated_by
		,updated_date = getdate()
	where cheque_control_detail_id in ( select item from dbo.SplitString(@cheque_control_detail_id,','))


	
	 declare @cheque_detail_id [int]  = NULL

	declare table_cursor2 cursor for 

	select item 
	from dbo.SplitString(@cheque_control_detail_id,',')

	open table_cursor2;
	fetch next from table_cursor2 into @cheque_detail_id
	while @@fetch_status = 0
	   begin
      
		  	declare @description as nvarchar(100)
			,@cheque_control_id as int

			select  @description = 'Cheque No : ' + @cheque_no
					, @cheque_control_id = cheque_control_id
			from dbo.cheque_control_document_item 
			where cheque_control_detail_id = @cheque_detail_id

			exec dbo.insert_cheque_control_history
					@cheque_control_id = @cheque_control_id
					,@description = @description
					,@action  = 'Update Cheque'
					,@updated_by  = @updated_by

			if not exists ( select 1 from dbo.cheque_control_document_item where cheque_control_id = @cheque_control_id and cheque_no is null and is_active = 1)
			begin
				exec dbo.update_cheque_control_document_status
						@cheque_control_id = @cheque_control_id
						,@status = 'COMPLETE'
						,@updated_by  = @updated_by
			end

		
	   
		  fetch next from table_cursor2 into   @cheque_detail_id
	   end;

	close table_cursor2;
	deallocate table_cursor2;
END
GO
