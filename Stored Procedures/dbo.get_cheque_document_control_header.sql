SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_cheque_document_control_header]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	select bcd.cheque_control_id
			,bcd.cheque_control_no
			,bcd.customer_company_id
			,cc.company_code
			,com.company_name
			,cc.customer_code
			,cus.customer_name
			,isnull(isnull(conf.[configuration_value],bcd.method),'-') as receive_method
			,bcd.collection_date
			,bcd.remark
			,bcd.cheque_control_status
	from dbo.cheque_control_document bcd
	inner join dbo.master_customer_company cc on bcd.customer_company_id = cc.customer_company_id
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
	inner join dbo.master_company com on cc.company_code = com.company_code
	left join dbo.configuration conf on bcd.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where cheque_control_id = @cheque_control_id


END
GO
