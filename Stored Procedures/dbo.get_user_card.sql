SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_user_card]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	--select cc.user_code
	--	,cc.[user_name]
	--	,ca.application_tag_id
	--	,at.application_tag_name
	--	,count(1) as [count]
	--from dbo.master_customer_company cc
	--left join dbo.master_customer_application_tag ca on cc.customer_code = ca.customer_code and ca.[status] = 'ACTIVE' 
	--left join dbo.master_application_tag at on ca.application_tag_id = at.application_tag_id and at.[status] = 'ACTIVE'
	--where cc.[status] = 'ACTIVE' 
	--Group by cc.user_code
	--	,cc.[user_name]
	--	,ca.application_tag_id
	--	,at.application_tag_name

	select cc.[user_name]
		,cc.[user_display_name]
		,cus.application_tag_id
		,at.application_tag_name
		,count(1) as [count]
	into #tmp
	from dbo.master_customer_company cc
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1 and isnull(cus.is_ignore,0) = 0
	
	left join dbo.master_application_tag at on cus.application_tag_id = at.application_tag_id and at.[is_active] = 1
	
	where cc.[is_active] = 1
	and cc.[user_display_name] is not null
	Group by cc.[user_name]
		,cc.[user_display_name]
		,cus.application_tag_id
		,at.application_tag_name
	union
	select cc.[user_name]
		,cc.[user_display_name]
		,cus.application_tag_id
		,at.application_tag_name
		,count(1) as [count]
	from dbo.master_customer_company cc
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1 and isnull(cus.is_ignore,0) = 0
	inner join (select distinct customer_code, company_code from invoice_header union select distinct customer_code, company_code from receipt) ih 
		on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
	left join dbo.master_application_tag at on cus.application_tag_id = at.application_tag_id and at.[is_active] = 1
	
	where cc.[is_active] = 1
	and cc.[user_display_name] is null
	Group by cc.[user_name]
		,cc.[user_display_name]
		,cus.application_tag_id
		,at.application_tag_name

	order by cc.[user_name],count(1) desc
	
	select *
	from
	(
		select [user_name],[user_display_name],application_tag_id,isnull(application_tag_name,'other') as application_tag_name,[count]
		from
		(
			select row_number() over(partition by [user_name]  order by case when application_tag_id is null then 0 else [count] end desc) as rn 
				,[user_name],[user_display_name],application_tag_id,application_tag_name,[count]
	
			from #tmp 
		)tmp
		where tmp.rn <=3
		union
		select [user_name],[user_display_name],NULL as application_tag_id,'other' as application_tag_name,sum([count]) as [count]
		from (
			select row_number() over(partition by [user_name]  order by case when application_tag_id is null then 0 else [count] end desc ) as rn 
			,[user_name],[user_display_name],application_tag_id,application_tag_name,[count]	
			from #tmp 
		) tmp2
		where tmp2.rn > 3
		group by  [user_name],[user_display_name]
	) list
	order by case when [user_name] is null then 'zzzzzzzzzzz' else [user_name] end ,[count] desc


	drop table #tmp

END
GO
