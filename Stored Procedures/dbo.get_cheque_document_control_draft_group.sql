SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_cheque_document_control_draft_group]
	-- Add the parameters for the stored procedure here
	@receipt_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	;with docList as (
		select substring(item,0,charindex('|', item)) as customer_company_id
					,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as receipt_no 
					,right(item,charindex('|', reverse(item)) -1 ) as [year]
					from dbo.SplitString(@receipt_list,',')
	),
	draft as 
	(
		select distinct cc.customer_company_id
				,r.company_code
				,com.company_name
				,r.customer_code
				,cus.customer_name
				, isnull(cbb.receipt_method ,'PYC') as receipt_method
				--, r.payment_term
				, r.currency
				--, i.collection_date
				, r.reference_document_no
				, receiptSum.amount as document_amount
				,null as document_date
				,0 as document_year --r.[year]  as document_year
		from dbo.receipt r
		inner join dbo.master_customer_company cc on r.company_code = cc.company_code and r.customer_code = cc.customer_code
		inner join dbo.master_customer cus on r.customer_code = cus.customer_code
		inner join dbo.master_company com on r.company_code = com.company_code
		left join dbo.invoice_header i on r.assignment_no = i.invoice_no
		left join dbo.master_customer_collection_behavior cbb 
				on cc.customer_company_id = cbb.customer_company_id
						and i.collection_behavior_id = cbb.collection_behavior_id
		inner join (
			
			select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,year([document_date]) as [year]
			from dbo.receipt
			where receipt_type = 'advance'
			group by reference_document_no,customer_code,company_code,year([document_date])
		) receiptSum on r.reference_document_no = receiptSum.reference_document_no 
					and year(r.[document_date]) = receiptSum.[year]
					and r.company_code = receiptSum.company_code
					and r.customer_code = receiptSum.customer_code
		inner join docList d on cc.customer_company_id = d.customer_company_id 
					and r.reference_document_no = d.receipt_no
					
		--where r.reference_document_no in (select item from dbo.SplitString(@receipt_list,',')) 
		--		and  r.receipt_type = 'advance'
		where  r.receipt_type = 'advance' and year(r.[document_date])  =  d.[year]
	),
	groupDraft as 
	(
		select customer_company_id, receipt_method , currency,sum(document_amount) as document_amount
		from draft 
		group by customer_company_id, receipt_method , currency
	) 
	select gd.customer_company_id
			, subdraft.company_code
			, subdraft.company_name
			, subdraft.customer_code
			, subdraft.customer_name
			, gd.receipt_method as receipt_method
			--, gd.payment_term
			, gd.currency
			, gd.document_amount
			--, isnull(subdraft.collection_date, dateadd(dd,1,getdate())) as collection_date
			, subdraft.receipt as receipt_list
			,document_date
			, document_year
	from groupDraft gd
	left join (
					select distinct customer_company_id
								,company_code
								,customer_code
								,customer_name
								, receipt_method 
								--, payment_term
								, currency
								,document_year
								,document_date
								--, collection_date
								,company_name
								,	stuff(
								(
									select ',' + sub.reference_document_no 
										from draft sub
										where sub.customer_company_id = main.customer_company_id
												and sub.receipt_method = main.receipt_method
												--and sub.payment_term = main.payment_term
												and sub.currency = main.currency
												--and sub.collection_date = main.collection_date
										for xml path('')
								) ,1,1,'') as receipt
					from draft main
				) subdraft on gd.customer_company_id = subdraft.customer_company_id
									and gd.receipt_method = subdraft.receipt_method
									--and gd.payment_term = subdraft.payment_term
									and gd.currency = subdraft.currency

END
GO
