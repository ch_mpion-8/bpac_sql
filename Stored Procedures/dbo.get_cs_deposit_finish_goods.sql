SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cs_deposit_finish_goods]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = 'All',
	@customer_name as varchar(120)=null,
	@order_no as varchar(10) = null,
	@status as varchar(10)='All',
	@other_doc_no as varchar(20)= null,
	@other_doc_date_start as date =null,
	@other_doc_date_end as date =null,
	@invoice_date_start as date=null,
	@invoice_date_end as date = null,
	@customer_code as varchar(20) =null
AS
BEGIN
	if @company_code ='All'
		set @company_code =null
	if @status = 'All'
		set @status = null

	;WITH
	cs_deposit 
		as
	(
		select 
			distinct dep.invoice_date,
			dep.company_code,
			dep.customer_code,
			c.customer_name,
			dep.order_no,
			o.document_no,
			o.document_date,
			cus.user_display_name,
			o.created_by,
			o.[status],
			CONVERT(varchar(20),o.document_id) as document_id,
			com.company_name,
			o.reference_guid
		from
			[dbo].[deposit] as dep
			inner join [dbo].[master_customer_company] cus
			on dep.company_code = cus.company_code
			AND dep.customer_code = cus.customer_code
			inner join dbo.master_customer c
			on c.customer_code = cus.customer_code
			inner join dbo.master_company com
			on com.company_code = dep.company_code 
			left join 
				(select ao.document_no,ao.document_id,ao.document_type,ai.reference_no,ao.document_date,ao.created_by,ao.[status]
				,ao.reference_guid,ao.method from dbo.other_control_document ao inner join dbo.other_control_document_item ai
				on ao.document_id = ai.document_id) as o
			on o.reference_no = dep.order_no
			and o.document_type = 5
		where
			(@company_code is null or dep.company_code in ( select item from dbo.SplitString(@company_code,','))) and
			-- dep.company_code = isnull(@company_code,dep.company_code) and
			(@customer_code is not null AND dep.customer_code = @customer_code OR @customer_code is null AND c.customer_name LIKE '%'+isnull(@customer_name,'')+'%') AND
			dep.order_no LIKE '%'+isnull(@order_no,'')+'%' AND
			((dep.invoice_date Between @invoice_date_start AND @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
			and isnull(o.document_no,'') LIKE '%'+isnull(@other_doc_no,'')+'%'
			AND isnull(convert(date,o.document_date),'') >= isnull(@other_doc_date_start,isnull(convert(date,o.document_date),'')) 
			AND isnull(convert(date,o.document_date),'') <= isnull(@other_doc_date_end,isnull(convert(date,o.document_date),'')) 
			and isnull(o.[status],'WC') = isnull(@status,isnull(o.[status],'WC'))
	),
	re_create
		as
	(
		select 
		distinct 
			dep.invoice_date,
			dep.company_code,
			dep.customer_code,
			cus.customer_name,
			dep.order_no,
			NULL as document_no,
			NULL as document_date,
			mcc.user_display_name,
			NULL as created_by,
			NULL as [status],
			NULL as document_id,
			com.company_name,
			NULL as reference_guid
		from dbo.other_control_document_item i
		inner join dbo.other_control_document o
			on o.document_id = i.document_id
		inner join dbo.deposit dep
			on dep.order_no = i.reference_no
		inner join dbo.master_company com
			on com.company_code = dep.company_code
		inner join dbo.master_customer_company mcc
			on mcc.company_code = dep.company_code
			and mcc.customer_code = dep.customer_code
		inner join dbo.master_customer cus
			on cus.customer_code = dep.customer_code
		where
			o.document_type = 5
			and o.[status] = 'CANCEL'
			and (select TOP 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ao.document_id = ai.document_id
			where ai.reference_no = i.reference_no
			and ao.[status] <> 'CANCEL') is null
			and (@company_code is null or dep.company_code in ( select item from dbo.SplitString(@company_code,','))) and
			-- and dep.company_code = isnull(@company_code,dep.company_code) and
			(@customer_code is not null AND dep.customer_code = @customer_code OR @customer_code is null AND cus.customer_name LIKE '%'+isnull(@customer_name,'')+'%') AND
			dep.order_no LIKE '%'+isnull(@order_no,'')+'%' AND
			((dep.invoice_date Between @invoice_date_start AND @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
			and 'WC' = isnull(@status,'WC')
			and isnull(@other_doc_no,'') = ''
	)

	select * from cs_deposit
	union all
	select * from re_create
END

GO
