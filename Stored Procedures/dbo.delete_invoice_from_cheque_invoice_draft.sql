SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_invoice_from_cheque_invoice_draft]
	-- Add the parameters for the stored procedure here
	@invoice_no as varchar(16) 
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update ct
		set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	from dbo.cheque_invoice_item_temp ct
	where invoice_no = @invoice_no and is_active = 1


END


GO
