SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[unfinish_mismatch]
	-- Add the parameters for the stored procedure here
	@mismatch_id int,
	@updated_by varchar(30) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   UPDATE mismatch set mismatch_status = 1 , updated_by = @updated_by where  mismatch_id = @mismatch_id
END
GO
