SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[get_cheque_document_control_list_v2] 
	-- Add the parameters for the stored procedure here
	@company_code as varchar(4) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@user_name as nvarchar(50) = null
		,@collection_date_from as date = null
		,@collection_date_to as date = null
		,@advance_receipt_no as nvarchar(50) = null
		,@collection_no as nvarchar(50) = null
		,@receive_method as nvarchar(50) = null
		,@status as nvarchar(50) = null
		,@is_billing_with_bill as bit = null
		,@is_billing_with_cheque as bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   /*
		ดึงข้อมูลใบคุมรับเช็คตาม status ที่ user เลือก ซึ่งถ้า user เลือก status เป็น waiting query นี้จะไม่มี result data เพราะ status ของใบคุมเมื่อมีการสร้างแล้วจะเป็น 'COMPLETE' , 'WAITINGRECEIVED' หรือ 'CANCEL'
		
		เอามาใช้ left join ใน query หลัก
   */
		select ccd.cheque_control_id
					,ccd.cheque_control_no
					,ccd.customer_company_id
					,ccd.method
					,ccd.job_date
					,ccd.remark
					,ccd.cheque_control_status
					,ccd.collection_date
					,ccd.created_date
					,ccd.created_by
					,ccd.updated_date
					,ccd.updated_by
					,reference_guid
					,is_manual
					,cheque_control_detail_id
					,cci.reference_document_no
					,cci.tracking_date
					,cci.cheque_no
					,cci.cheque_amount
					,cci.cheque_date
					,cci.cheque_currency
					,cci.cheque_bank
					,cci.cheque_remark
					,cci.is_active
					,cci.document_year
			into #cheque_control
			from dbo.cheque_control_document ccd 
			inner join  dbo.cheque_control_document_item cci on cci.cheque_control_id = ccd.cheque_control_id
			where (isnull(cci.is_active,1) = 1 or ccd.cheque_control_status = 'CANCEL' ) -- if cheque control status is active or canceled cheque control
					and (@status is null or isnull(ccd.cheque_control_status,'WAITING') = @status) 
	
	/*
		ดึงข้อมูล AA ที่ยัง active อยู่ ตาม criteria ที่ user เลือก
	*/
	select cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date as receipt_date
		,sum(document_amount) as receipt_amount
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date as collection_date
		,cci.method  as receipt_method
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') as status
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5)) as [year]
		,isnull(cci.is_active,1) as is_active
		,isnull(re.is_active,1) as receipt_is_active
	into #main
	from dbo.receipt re
	inner join  dbo.master_customer_company cc on re.customer_code = cc.customer_code and re.company_code = cc.company_code
	inner join dbo.master_customer cus on re.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on re.company_code = com.company_code
	inner join dbo.invoice_header h on re.assignment_no = h.invoice_no
	left join #cheque_control cci on re.reference_document_no = cci.reference_document_no and  year(re.[document_date])  = cci.document_year
	 and cc.customer_company_id = cci.customer_company_id
	left join dbo.master_customer_collection_behavior cbb 
			on h.collection_behavior_id = cbb.collection_behavior_id
	left join dbo.master_customer_bill_presentment_behavior cbbb
			on h.bill_presentment_behavior_id = cbbb.bill_presentment_behavior_id
	left join (
				select customer_company_id,ci.reference_document_no,ct.temp_id
				from dbo.cheque_temp ct 
				inner join dbo.cheque_item_temp ci on  ct.temp_id = ci.temp_id and ci.is_active = 1
	)ctemp on re.reference_document_no = ctemp.reference_document_no  and cc.customer_company_id = ctemp.customer_company_id
	where (@company_code is null or re.company_code = @company_code)
			and (@customer is null or (re.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
			and (@customer_code is null or re.customer_code = @customer_code)
			and (@user_name is null or cc.user_name = @user_name)
			and (
					@collection_date_from is null 
					or  
					cci.collection_date is null
					or
					(
						@collection_date_from is not null 
						and @collection_date_to is not null 
						and cci.collection_date   between @collection_date_from and @collection_date_to
					)
				)
			and (@advance_receipt_no is null or re.reference_document_no like '%' + @advance_receipt_no + '%')
			and (@collection_no is null or cci.cheque_control_no like '%' + @collection_no + '%')
			and (@receive_method is null or isnull(cci.method,'') in ( select item from dbo.SplitString(@receive_method,',')))
			and (@is_billing_with_bill is null or isnull(cbbb.is_billing_with_bill,0) = @is_billing_with_bill)
			and ((@is_billing_with_cheque is not null or 
								( 
									isnull(@is_billing_with_cheque,0) = 1 
									and cbb.payment_method like '%cheque%'
								)
						)
				)
			and  re.receipt_type = 'advance'
			and (@status is null or isnull(cci.cheque_control_status,'WAITING') = @status)
			and isnull(re.[is_active],1) = 1 
		group by 
		cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date 
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date 
		,cci.method 
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') 
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end 
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5))
		,isnull(cci.is_active,1)
		,isnull(re.is_active,1)

		
		/*
			ดึงข้อมูล AA ที่เคยสร้างใบคุมแล้วถูก cancel ไป เอามา default status เป็น waiting ใหม่อีกครั้ง
		*/
		if(@status is null or @status = 'WAITING') 
		begin
			insert into #main
			select  m.customer_company_id
				, m.company_code
				, m.company_name
				, m.customer_code
				, m.customer_name
				, m.reference_document_no
				, m.receipt_date
				, m.receipt_amount
				, m.currency
				, null as cheque_control_id
				, null as cheque_control_detail_id
				, null as cheque_control_no
				, null as collection_date
				, null as receipt_method
				, null as cheque_date
				, null as cheque_no
				, null as cheque_amount
				, null as [cheque_currency]
				, null as cheque_bank
				, null as cheque_remark
				, null as collection_behavior_id
				, null as bpac_date
				, null as bank_name
				, m.user_display_name
				, null as created_by
				, 'WAITING' as status
				, m.temp_id
				, case when m.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
				, null as reference_guid
				, m.[year]
				, m.is_active
				, m.receipt_is_active
			from #main m
			left join (select * from #main where status in ( 'COMPLETE' ,'WAITINGRECEIVED','WAITING')) s 
				on m.reference_document_no = s.reference_document_no and   year(m.[year])  = s.[year] and m.customer_company_id = s.customer_company_id
			where m.status = 'CANCEL' and s.reference_document_no is null
		end
		
		

		select * from #main
		union
		/*
			ดึงข้อมูล AA ที่ไม่ active แล้ว แต่เคยมีการสร้างใบคุมรับเช็ค 
		*/
		select cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date as receipt_date
		,sum(document_amount) as receipt_amount
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date as collection_date
		,cci.method  as receipt_method
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') as status
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5)) as [year]
		,isnull(cci.is_active,1) as is_active
		,isnull(re.is_active,1) as receipt_is_active
	from dbo.receipt re
	/*
		มีข้อมูลที่เลข AA , document_date และ ปีเดียวกัน มี status ทั้ง active และ inactive จึงต้อง left join ตัวมันเองเพื่อ filter AA ที่มี record ที่ active ออก เพราะ AA ที่ active อยู่ จะถูก select มาตั้งแต่ query main
		เช่นเลข AA0001050103
	*/
	left join (select reference_document_no,[document_date], customer_code , company_code from  dbo.receipt where isnull(is_active,1) = 1) retemp
			on re.reference_document_no = retemp.reference_document_no 
			and  re.[document_date] = retemp.[document_date]
			and re.customer_code = retemp.customer_code
			and re.company_code = retemp.company_code
	inner join  dbo.master_customer_company cc on re.customer_code = cc.customer_code and re.company_code = cc.company_code
	inner join dbo.master_customer cus on re.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on re.company_code = com.company_code
	inner join dbo.invoice_header h on re.assignment_no = h.invoice_no
	left join #cheque_control cci on re.reference_document_no = cci.reference_document_no and  year(re.[document_date])  = cci.document_year
	 and cc.customer_company_id = cci.customer_company_id
	left join dbo.master_customer_collection_behavior cbb on h.collection_behavior_id = cbb.collection_behavior_id
	left join dbo.master_customer_bill_presentment_behavior cbbb on h.bill_presentment_behavior_id = cbbb.bill_presentment_behavior_id
	left join (
				select customer_company_id,ci.reference_document_no,ct.temp_id
				from dbo.cheque_temp ct 
				inner join dbo.cheque_item_temp ci on  ct.temp_id = ci.temp_id and ci.is_active = 1
	)ctemp on re.reference_document_no = ctemp.reference_document_no  and cc.customer_company_id = ctemp.customer_company_id
	where (@company_code is null or re.company_code = @company_code)
			and (@customer is null or (re.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
			and (@customer_code is null or re.customer_code = @customer_code)
			and (@user_name is null or cc.user_name = @user_name)
			and (
					@collection_date_from is null 
					or  
					cci.collection_date is null
					or
					(
						@collection_date_from is not null 
						and @collection_date_to is not null 
						and cci.collection_date   between @collection_date_from and @collection_date_to
					)
				)
			and (@advance_receipt_no is null or re.reference_document_no like '%' + @advance_receipt_no + '%')
			and (@collection_no is null or cci.cheque_control_no like '%' + @collection_no + '%')
			and (@receive_method is null or isnull(cci.method,'') in ( select item from dbo.SplitString(@receive_method,',')))
			and (@is_billing_with_bill is null or isnull(cbbb.is_billing_with_bill,0) = @is_billing_with_bill)
			and ((@is_billing_with_cheque is not null or 
								( 
									isnull(@is_billing_with_cheque,0) = 1 
									and cbb.payment_method like '%cheque%'
								)
						)
				)
			and  re.receipt_type = 'advance'
			and isnull(re.[is_active],1) = 0 
			and cci.reference_document_no is not null
			and (@status is null or isnull(cci.cheque_control_status,'WAITING') = @status)
			and retemp.reference_document_no is null
		group by 
		cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date 
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date 
		,cci.method 
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') 
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end 
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5))
		,isnull(cci.is_active,1)
		,isnull(re.is_active,1)
		order by reference_document_no

		drop table #cheque_control
		drop table #main
END
GO
