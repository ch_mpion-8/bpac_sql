SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[save_customer_collection_behavior]
	-- Add the parameters for the stored procedure here
		@collection_behavior_id as int = null
		,@customer_company_id as int = null
		,@valid_date_from as date = null
		,@payment_term_code as nvarchar(2000) = null
		,@address_id as int = null
		,@time as nvarchar(100) = null
		,@collection_condition_date as nvarchar(200) = null
		,@period_from as int = null
		,@period_to as int = null
		,@description as nvarchar(2000) = null
		,@remark as nvarchar(2000) = null
		,@condition_by as nvarchar(20) = null
		,@payment_day as int = null
		,@is_business_day as bit = null
		,@date_from as int = null
		,@date_to as int = null
		,@last_x_date as int = null
		,@last_x_business_day as int = null
		,@custom_date as nvarchar(100) = null
		,@custom_day as nvarchar(100) = null
		,@condition_week_by as nvarchar(20) = null
		,@custom_week as nvarchar(100) = null
		,@next_x_week as int = null
		,@condition_month_by as nvarchar(20) = null
		,@custom_month as nvarchar(100) = null
		,@next_x_month as int = null
		,@is_next_collection_date as bit = null
		,@payment_method as nvarchar(50) = null
		,@bank as nvarchar(100) = null
		,@notify_day as smallint = null
		,@is_skip_customer_holiday as bit = null
		,@is_skip_scg_holiday as bit = null
		,@receipt_method as nvarchar(20) = null
		,@document as nvarchar(200) = null
		,@custom_document as nvarchar(2000) = null
		,@custom_date_list as [dbo].[date_table_type] readonly
		,@status as bit = null
		,@updated_by as nvarchar(50) = null
		,@custom_date_next_x_month as int = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if exists( select 1 from [dbo].[master_customer_collection_behavior] 
				where payment_term_code in (select item from dbo.SplitString(@payment_term_code,','))
				and is_active = 1
					and customer_company_id = @customer_company_id
					and valid_date_from = @valid_date_from
					and (
							@period_from between period_from and period_to
							or @period_to  between period_from and period_to
							or period_from between @period_from and @period_to
							or period_to between @period_from and @period_to
						) 
					and (@collection_behavior_id <> group_id)
					--and period_to <> @period_from
					--and period_from <> @period_to
			 )
	begin
		set @error_message ='Dupplicate payment term or invalid period !'
		return;
	end

	-- Check Create Group
	if @collection_behavior_id is null or @collection_behavior_id = 0
	begin
		-- Get Last Group ID  
		declare @from_group_id as int
		select top 1 @from_group_id = group_id
		from [dbo].[master_customer_collection_behavior] 
		where payment_term_code in (select item from dbo.SplitString(@payment_term_code,','))
		and customer_company_id = @customer_company_id
		and is_active = 1
		order by created_date desc

		if exists(
			select 1
			from [dbo].[master_customer_collection_behavior] a 
				full join (select item from dbo.SplitString(@payment_term_code,',')) b on a.payment_term_code = b.item
			where group_id = @from_group_id
			and a.is_active = 1
			and (a.payment_term_code is null or b.item is null)
			)
		begin
			set @error_message = 'กรุณาเลือก Payment Term ที่ยังไม่เคย Maintain !'
			return;
		end
	end

	-- Get Group ID
	declare @group_id as int
	
	if exists( select 1 from [dbo].[master_customer_collection_behavior] 
				where group_id = @collection_behavior_id
			 )
	begin
		set @group_id = @collection_behavior_id
	end
	else
	begin
		select @group_id = isnull(max(group_id),0)+1
		from [dbo].[master_customer_collection_behavior] 
	end

	
	update  dbo.[master_customer_collection_behavior]
	set is_active = 0
		, updated_date = getdate()
		, updated_by = @updated_by	
	from dbo.[master_customer_collection_behavior]
	where group_id = @group_id 
		and payment_term_code not in (select item 
							from dbo.SplitString(@payment_term_code,','))

	declare @term nvarchar(4)
	declare table_cursor cursor for 

	select item 
	from dbo.SplitString(@payment_term_code,',')

	open table_cursor;
	fetch next from table_cursor into @term
	while @@fetch_status = 0
	   begin
	   declare @collection_id as int 

   if exists (
				select 1 
				from [dbo].[master_customer_collection_behavior] 
				where payment_term_code = @term
				and group_id = @collection_behavior_id
			)
		 begin
			-- Get bill_presentment_behavior_id
			select @collection_id = collection_behavior_id
			from [dbo].[master_customer_collection_behavior] 
			where payment_term_code = @term
			and group_id = @collection_behavior_id


			update dbo.[master_customer_collection_behavior]
			set valid_date_from= @valid_date_from
				,payment_term_code= @term
				,address_id= @address_id
				,[time]= @time
				,collection_condition_date= @collection_condition_date
				,period_from= @period_from
				,period_to= @period_to
				,[description]= @description
				,remark= @remark
				,condition_by= @condition_by
				,payment_day= case when @payment_day > 0 then @payment_day * -1 else @payment_day end
				,is_business_day= @is_business_day
				,date_from= @date_from
				,date_to= @date_to
				,last_x_date= @last_x_date
				,last_x_business_day = @last_x_business_day
				,custom_date= @custom_date
				,custom_day= @custom_day
				,condition_week_by= @condition_week_by
				,custom_week= @custom_week
				,next_x_week = @next_x_week
				,condition_month_by= @condition_month_by
				,custom_month= @custom_month
				,next_x_month= @next_x_month
				,is_next_collection_date= @is_next_collection_date
				,payment_method= @payment_method
				,bank= @bank
				,notify_day= @notify_day
				,is_skip_customer_holiday= @is_skip_customer_holiday
				,is_skip_scg_holiday= @is_skip_scg_holiday
				,receipt_method= @receipt_method
				,document= @document
				,custom_document= @custom_document
				,[is_active] = @status
				,updated_date = getdate()
				,updated_by = @updated_by			
				,custom_date_next_x_month = @custom_date_next_x_month	
			from dbo.[master_customer_collection_behavior]
			where collection_behavior_id = @collection_id

		 end
		 else
		 begin
			declare @tmp table(new_id int)
			insert into dbo.[master_customer_collection_behavior]
			( customer_company_id
				, valid_date_from
				, payment_term_code
				, address_id
				, [time]
				, collection_condition_date
				, period_from
				, period_to
				, [description]
				, remark
				, condition_by
				, payment_day
				, is_business_day
				, date_from
				, date_to
				, last_x_date
				,last_x_business_day 
				, custom_date
				, custom_day
				, condition_week_by
				, custom_week
				, next_x_week
				, condition_month_by
				, custom_month
				, next_x_month
				, is_next_collection_date
				, payment_method
				, bank
				, notify_day
				, is_skip_customer_holiday
				, is_skip_scg_holiday
				, receipt_method
				, document
				, custom_document
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by]
			   ,[group_id]
			   ,custom_date_next_x_month)
			     output inserted.collection_behavior_id into @tmp
			select 
				 @customer_company_id
				, @valid_date_from
				, @term
				, @address_id
				, @time
				, @collection_condition_date
				, @period_from
				, @period_to
				, @description
				, @remark
				, @condition_by
				, case when @payment_day > 0 then @payment_day * -1 else @payment_day end
				, @is_business_day
				, @date_from
				, @date_to
				, @last_x_date
				, @last_x_business_day
				, @custom_date
				, @custom_day
				, @condition_week_by
				, @custom_week
				, @next_x_week
				, @condition_month_by
				, @custom_month
				, @next_x_month
				, @is_next_collection_date
				, @payment_method
				, @bank
				, @notify_day
				, @is_skip_customer_holiday
				, @is_skip_scg_holiday
				, @receipt_method
				, @document
				, @custom_document
				,@status
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
				,@group_id
				,@custom_date_next_x_month
				select @collection_id = new_id 
				from @tmp
		 end

		 
		update dbo.master_customer_collection_custom_date
		set [is_active] = 1
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select	*
		from @custom_date_list custom 
		left join dbo.master_customer_collection_custom_date bd on custom.date = bd.date 
			and bd.collection_behavior_id = @collection_id
		where  bd.collection_custom_date_id is not null

		update dbo.master_customer_collection_custom_date
		set [is_active] = 0
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select *
		from  dbo.master_customer_collection_custom_date bd  
		left join @custom_date_list custom on  bd.date  = custom.date 	
		where  bd.collection_behavior_id = @collection_id
				and custom.date is null

		insert into dbo.master_customer_collection_custom_date
		(
			collection_behavior_id
			,date
			,[is_active]
			,[created_date]
			,[created_by]
			,[updated_date]
			,[updated_by]
		)
		select @collection_id
				,custom.date
				,1 as [is_active]
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
		--select *
		from @custom_date_list custom 
		left join dbo.master_customer_collection_custom_date bd on custom.date = bd.date 
			and bd.collection_behavior_id = @collection_id
		where  bd.collection_custom_date_id is null


		--update dbo.[master_customer_collection_behavior]
		--set valid_date_to = dateadd(day,-1,@valid_date_from)
		--from [dbo].[master_customer_collection_behavior] 
		--where (@collection_id is null or collection_behavior_id <> @collection_id)
		--and payment_term_code = @term
		--and customer_company_id = @customer_company_id
		--and period_from = @period_from
		--and period_to = @period_to
		--and valid_date_from < @valid_date_from

		;with sub as(
				select b.collection_behavior_id
						,customer_company_id
						,payment_term_code
						,period_from
						,period_to
						,valid_date_from
						,dateadd(day,-1,valid_date_from)as valid_date_to
						,row_num = ROW_NUMBER() OVER (ORDER BY valid_date_from)
				from [dbo].[master_customer_collection_behavior] b
				where   b.is_active = 1 
						and b.payment_term_code = @term
						and b.customer_company_id = @customer_company_id
						and b.period_from = @period_from
						and b.period_to = @period_to

		) 
		, data as 
		(
			select sub1.collection_behavior_id 
					,sub1.valid_date_from 
					, sub2.valid_date_to
			from sub sub1
			left join sub sub2 on sub1.row_num = sub2.row_num - 1
		) --select * from data
		merge into [dbo].[master_customer_collection_behavior]  b
		using data as d on b.collection_behavior_id = d.collection_behavior_id
		when matched then
			update 
			set b.valid_date_to = d.valid_date_to;


	fetch next from table_cursor into  @term
	end;
	close table_cursor;
	deallocate table_cursor;
END
GO
