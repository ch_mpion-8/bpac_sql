SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_receipt_for_manual_cheque]
	-- Add the parameters for the stored procedure here
	 @cheque_detail_id as nvarchar(2000) = null -- 1016
		,@receipt_list as nvarchar(2000) =  null --'157|AA0001060028|2017,121|AA0001060029|2017'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @cheque_detail_id is not null and @cheque_detail_id <> ''
	begin
    	select distinct cdi.reference_document_no
			,c.company_code + ' - ' + com.company_name as company_code
			,c.customer_code
			,cus.customer_name
			,c.customer_company_id
			,cd.cheque_control_no
			,cdi.cheque_no
			,cdi.cheque_amount
			,cdi.cheque_date
			,cdi.cheque_bank
			,cdi.cheque_currency
			,cdi.cheque_remark
			,cdi.cheque_control_detail_id
			,receiptSum.amount
			,cdi.document_year
		from dbo.cheque_control_document_item cdi
		inner join dbo.cheque_control_document cd on cdi.cheque_control_id = cd.cheque_control_id
		inner join dbo.master_customer_company c on cd.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		inner join dbo.master_company com on c.company_code = com.company_code
		inner join (
						select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,year([document_date]) as [year]
						from dbo.receipt
						where receipt_type = 'advance'
						group by reference_document_no,customer_code,company_code,year([document_date]) 
				) receiptSum on cdi.reference_document_no = receiptSum.reference_document_no 
					and cdi.document_year = receiptSum.[year]
					and c.company_code = receiptSum.company_code
					and c.customer_code = receiptSum.customer_code
		where cdi.is_active = 1 and cdi.cheque_control_detail_id in (select item from dbo.SplitString(@cheque_detail_id,','))
	end
	else
	begin

		;with a as(
					select substring(item,0,charindex('|', item)) as customer_company_id
					,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as receipt_no 
					,right(item,charindex('|', reverse(item)) -1 ) as [year]
					from dbo.SplitString(@receipt_list,',')
		)
		select distinct a.receipt_no as reference_document_no
			,c.company_code + ' - ' + com.company_name as company_code
			,c.customer_code
			,cus.customer_name
			,cast(a.customer_company_id as int) customer_company_id
			/*,cd.cheque_control_no
			,cdi.cheque_no
			,cdi.cheque_amount
			,cdi.cheque_date
			,cdi.cheque_bank
			,cdi.cheque_currency
			,cdi.cheque_remark
			,cdi.cheque_control_detail_id*/
			,null as 'cheque_control_no'
			,null as 'cheque_amount'
			,null as 'cheque_date'
			,null as 'cheque_bank'
			,null as 'cheque_currency'
			,null as 'cheque_remark'
			,null as 'cheque_control_detail_id'
			,receiptSum.amount
			,case when a.[year] is not null then cast(a.[year] as int) else 0 end as document_year
		from a
		inner join dbo.master_customer_company c on a.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		inner join dbo.master_company com on c.company_code = com.company_code
		/*left join dbo.cheque_control_document_item cdi  
				on cdi.is_active = 1
					and a.receipt_no = cdi.reference_document_no
		left join dbo.cheque_control_document cd 
				on a.customer_company_id = cd.customer_company_id 
					and cdi.cheque_control_id = cd.cheque_control_id
					and cd.cheque_control_status = 'WAITINGRECEIVED'*/
		inner join (
						select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,year([document_date]) as [year]
						from dbo.receipt
						where receipt_type = 'advance'
						group by reference_document_no,customer_code,company_code,year([document_date])
				) receiptSum on a.receipt_no = receiptSum.reference_document_no 
					and a.[year] = receiptSum.[year]
					and c.company_code = receiptSum.company_code
					and c.customer_code = receiptSum.customer_code
	end
END
GO
