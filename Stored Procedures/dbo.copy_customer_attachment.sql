SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[copy_customer_attachment]
	-- Add the parameters for the stored procedure here
	@from_customer_company_id as int = null--4 -- '2011271' --null
		,@to_customer_company_id as int = null--18 -- '0230' --null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into dbo.master_customer_attachment
			([customer_company_id]
			  ,title
			   ,remark
			   ,[reference_guid]
			   ,[copy_from_id]
			   ,[file_name]
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
	select @to_customer_company_id	
			,a.title
			,a.remark
			,a.reference_guid
			,a.attachment_id
			,a.[file_name]
			,a.[is_active]
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from dbo.master_customer_attachment a
	where customer_company_id = @from_customer_company_id
			and [is_active] = 1

END



GO
