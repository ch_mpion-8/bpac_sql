SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_cheque_document_control_list] 
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@user_name as nvarchar(50) = null
		,@collection_date_from as date = null
		,@collection_date_to as date = null
		,@advance_receipt_no as nvarchar(50) = null
		,@collection_no as nvarchar(50) = null
		,@receive_method as nvarchar(50) = null
		,@status as nvarchar(50) = null
		,@is_billing_with_bill as bit = null
		,@is_billing_with_cheque as bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	;with main as (
	select cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date as receipt_date
		,sum(document_amount) as receipt_amount
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date as collection_date
		,isnull(conf.[configuration_value],cci.method )  as receipt_method
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') as status
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5)) as [year]
		,isnull(cci.is_active,1) as is_active
		,isnull(re.is_active,1) as receipt_is_active
	from dbo.receipt re
	inner join  dbo.master_customer_company cc on re.customer_code = cc.customer_code and re.company_code = cc.company_code
	inner join dbo.master_customer cus on re.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on re.company_code = com.company_code
	inner join dbo.invoice_header h on re.assignment_no = h.invoice_no
	left join (
			select ccd.cheque_control_id
					,ccd.cheque_control_no
					,ccd.customer_company_id
					,ccd.method
					,ccd.job_date
					,ccd.remark
					,ccd.cheque_control_status
					,ccd.collection_date
					,ccd.created_date
					,ccd.created_by
					,ccd.updated_date
					,ccd.updated_by
					,reference_guid
					,is_manual
					,cheque_control_detail_id
					,cci.reference_document_no
					,cci.tracking_date
					,cci.cheque_no
					,cci.cheque_amount
					,cci.cheque_date
					,cci.cheque_currency
					,cci.cheque_bank
					,cci.cheque_remark
					,cci.is_active
					,cci.document_year
			from dbo.cheque_control_document ccd 
			inner join  dbo.cheque_control_document_item cci on cci.cheque_control_id = ccd.cheque_control_id
			left join dbo.configuration conf on ccd.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
			where (isnull(cci.is_active,1) = 1 or ccd.cheque_control_status = 'CANCEL' )
			) cci
	on re.reference_document_no = cci.reference_document_no
			and  year(re.[document_date])  = cci.document_year
	 --and cci.is_active = 1 
	 and cc.customer_company_id = cci.customer_company_id
	left join dbo.master_customer_collection_behavior cbb 
			on h.collection_behavior_id = cbb.collection_behavior_id
	left join dbo.master_customer_bill_presentment_behavior cbbb
			on h.bill_presentment_behavior_id = cbbb.bill_presentment_behavior_id
	left join (
				select customer_company_id,ci.reference_document_no,ct.temp_id
				from dbo.cheque_temp ct 
				inner join dbo.cheque_item_temp ci on  ct.temp_id = ci.temp_id and ci.is_active = 1
	)ctemp on re.reference_document_no = ctemp.reference_document_no  and cc.customer_company_id = ctemp.customer_company_id
			left join dbo.configuration conf on cci.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where (@company_code is null or re.company_code  in ( select item from dbo.SplitString(@company_code,',')))
			and (@customer is null or (re.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
			and re.customer_code = isnull(@customer_code,re.customer_code)
			and (@user_name is null or cc.user_name = @user_name)
			and (@collection_date_from is null or  cci.collection_date is null
						or
						(
							@collection_date_from is not null 
							and @collection_date_to is not null 
							and cci.collection_date   between @collection_date_from and @collection_date_to
						)
					)
			and (@advance_receipt_no is null or re.reference_document_no like '%' + @advance_receipt_no + '%')
			and (@collection_no is null or cci.cheque_control_no like '%' + @collection_no + '%')
			and (@receive_method is null or isnull(cci.method,'') in ( select item from dbo.SplitString(@receive_method,',')))
			/*and (@is_billing_with_bill is null or isnull(cbbb.is_billing_with_bill,0) = @is_billing_with_bill)
			and ((@is_billing_with_cheque is not null or 
								( 
									isnull(@is_billing_with_cheque,0) = 1 
									and cbb.payment_method like '%Cheque%'
								)
						)
				)*/
			and (@is_billing_with_bill is null  or isnull(cbbb.is_billing_with_bill,0) = @is_billing_with_bill)
			and  re.receipt_type = 'advance'
			--and re.year = year(getdate())
			and (isnull(re.[is_active],1) = 1 or (cci.reference_document_no is not null and isnull(cci.cheque_control_status,'WAITING') = 'CANCEL'))
		group by 
		cc.customer_company_id
		,re.company_code
		,com.company_name
		,re.customer_code
		,cus.customer_name
		,re.reference_document_no
		,re.document_date 
		,re.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date 
		,isnull(conf.[configuration_value],cci.method ) 
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') 
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end 
		, cci.reference_guid
		,cast(year(re.[document_date]) as nvarchar(5))
		,isnull(cci.is_active,1)
		,isnull(re.is_active,1)
	),sub as (
	/* select all AA join with document control */
	select *
	from main
	union
	/* select all AA has been cancel and default status to WAITING */
	select  main.customer_company_id
		,main.company_code
		,main.company_name
		,main.customer_code
		,main.customer_name
		,main.reference_document_no
		,main.receipt_date
		,main.receipt_amount
		,main.currency
		,ccd.cheque_control_id
		,cci.cheque_control_detail_id
		,ccd.cheque_control_no
		,ccd.collection_date as collection_date
		,isnull(conf.[configuration_value],ccd.method)   as receipt_method
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, main.user_display_name
		, cci.created_by
		, isnull(ccd.cheque_control_status,'WAITING') as status
		, main.temp_id
		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, ccd.reference_guid
		,main.[year]
		,main.is_active
		,main.receipt_is_active
	from main
	left join (
			dbo.cheque_control_document_item cci 
				JOIN dbo.cheque_control_document ccd on cci.cheque_control_id = ccd.cheque_control_id
			) on main.reference_document_no = cci.reference_document_no
				 and cci.is_active = 1 
				 and main.customer_company_id = ccd.customer_company_id
				 and main.[year] = cast(year(cci.document_year) as nvarchar(5))
	outer apply(
		select top 1 reference_document_no
		from main  a
		where [status] in( 'COMPLETE' ,'WAITINGRECEIVED')
		and a.reference_document_no = main.reference_document_no
		and a.[year] = main.[year]
		and a.company_code = main.company_code
		and a.customer_code = main.customer_code
		) as main_not_cancel
	left join dbo.configuration conf on ccd.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	 /*where  main.reference_document_no not in (select reference_document_no from main where [status] in( 'COMPLETE' ,'WAITINGRECEIVED') )
		and main.receipt_is_active = 1*/
	where  main.receipt_is_active = 1
	 --main.reference_document_no not in (select reference_document_no from main where [status] in( 'COMPLETE' ,'WAITINGRECEIVED') )
	 and main_not_cancel.reference_document_no is null
	 )
	 select * 
	--into #temp
	from sub
	where 	 (@status is null 
					--or (@status = 'WAITING' and replace(isnull(h.collection_status,'WAITING'),'MANUAL','WAITING') = @status and ccd.cheque_control_status is null)
					or (isnull(sub.[status],'WAITING')  = @status)
				)
				
				and (@collection_no is null or cheque_control_no like '%' + @collection_no + '%')
	order by reference_document_no
END
GO
