SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Suteekom K.>
-- Create date: <06/08/2017>
-- Description:	<for manage log interface>
-- =============================================
CREATE PROCEDURE [dbo].[log_manage_interface_header]
	@web_key nvarchar(40),
	@request_data nvarchar(MAX) = '',
	@response_data nvarchar(MAX) = '',
	@status nvarchar(10) = '',
	@request_by nvarchar(10) = '',
	@response_by nvarchar(10) = '',
	@log_request nvarchar(MAX) = '',
	@log_response nvarchar(MAX) = '',
	@INF_ID nvarchar(2) = '',
	@CPUDT_FROM nvarchar(10) = '',
	@CPUDT_TO nvarchar(10) = '',
	@CPUTM_FROM nvarchar(8) = '',
	@CPUTM_TO nvarchar(8) = '',
	@BUKRS nvarchar(MAX) = '',
	@BLART nvarchar(MAX) = '',
	@PI_MSG_ID_RES nvarchar(40) = '',
	@PI_MSG_ID_REQ nvarchar(40) = '',
	@STATUS_TYPE nvarchar(1) = '',
	@STATUS_ID nvarchar(20) = '',
	@STATUS_NUMBER nvarchar(3) = '',
	@STATUS_MESSAGE nvarchar(220) = '',
	@BKPF_COUNT nvarchar(6) = '',
	@BSEG_COUNT nvarchar(6) = '',
	@BSID_COUNT nvarchar(6) = '',
	@BSAD_COUNT nvarchar(6) = '',
	@VBRK_COUNT nvarchar(6) = '',
	@VBRP_COUNT nvarchar(6) = '',
	@VBAK_COUNT nvarchar(6) = '',
	@BIDNO_COUNT nvarchar(6) = '',
	@DNLOAD_COUNT nvarchar(6) = '',
	@ZFI_0090_ADVREC_COUNT nvarchar(6) = ''
	
AS
BEGIN
	SET NOCOUNT ON;
	declare @id int;

    if exists (select 1 from log_interface_header where  web_key = @web_key)
        begin
			update log_interface_header
			set response_data = @response_data,
				status = @status,
				response_date_time = getdate(),
				response_by = @response_by,
				log_response = @log_response,
				PI_MSG_ID_RES = @PI_MSG_ID_RES,
				PI_MSG_ID_REQ = @PI_MSG_ID_REQ,
				STATUS_TYPE = @STATUS_TYPE,
				STATUS_ID = @STATUS_ID,
				STATUS_NUMBER = @STATUS_NUMBER,
				STATUS_MESSAGE = @STATUS_MESSAGE,
				BKPF_COUNT = @BKPF_COUNT,
				BSEG_COUNT = @BSEG_COUNT,
				BSID_COUNT = @BSID_COUNT,
				BSAD_COUNT = @BSAD_COUNT,
				VBRK_COUNT = @VBRK_COUNT,
				VBRP_COUNT = @VBRP_COUNT,
				VBAK_COUNT = @VBAK_COUNT,
				BIDNO_COUNT = @BIDNO_COUNT,
				DNLOAD_COUNT = @DNLOAD_COUNT,
				ZFI_0090_ADVREC_COUNT = @ZFI_0090_ADVREC_COUNT
			where web_key = @web_key;
        end
    else
        begin
            INSERT INTO [dbo].[log_interface_header]
           ([web_key]
           ,[request_data]
           ,[response_data]
           ,[status]
           ,[request_date_time]
           ,[request_by]
           ,[response_date_time]
           ,[response_by]
           ,[log_request]
           ,[log_response]
           ,[INF_ID]
           ,[CPUDT_FROM]
           ,[CPUDT_TO]
           ,[CPUTM_FROM]
           ,[CPUTM_TO]
           ,[BUKRS]
           ,[BLART]
           ,[PI_MSG_ID_RES]
           ,[PI_MSG_ID_REQ]
           ,[STATUS_TYPE]
           ,[STATUS_ID]
           ,[STATUS_NUMBER]
           ,[STATUS_MESSAGE]
           ,[BKPF_COUNT]
           ,[BSEG_COUNT]
           ,[BSID_COUNT]
           ,[BSAD_COUNT]
           ,[VBRK_COUNT]
           ,[VBRP_COUNT]
           ,[VBAK_COUNT]
           ,[BIDNO_COUNT]
           ,[DNLOAD_COUNT]
           ,[ZFI_0090_ADVREC_COUNT])
     VALUES
		(
			@web_key,
			@request_data,
			@response_data,
			@status,
			getdate(),
			@request_by,
			getdate(),
			@response_by,
			@log_request,
			@log_response,
			@INF_ID,
			@CPUDT_FROM,
			@CPUDT_TO,
			@CPUTM_FROM,
			@CPUTM_TO,
			@BUKRS,
			@BLART,
			@PI_MSG_ID_RES,
			@PI_MSG_ID_REQ,
			@STATUS_TYPE,
			@STATUS_ID,
			@STATUS_NUMBER,
			@STATUS_MESSAGE,
			@BKPF_COUNT,
			@BSEG_COUNT,
			@BSID_COUNT,
			@BSAD_COUNT,
			@VBRK_COUNT,
			@VBRP_COUNT,
			@VBAK_COUNT,
			@BIDNO_COUNT,
			@DNLOAD_COUNT,
			@ZFI_0090_ADVREC_COUNT
			)
        end
END

GO
