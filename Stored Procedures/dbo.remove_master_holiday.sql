SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[remove_master_holiday]
	-- Add the parameters for the stored procedure here
   @date_list as varchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @date_list is not null
	begin
    Delete from dbo.master_holiday   where convert(date,CONVERT(varchar(4),[holiday_year])+RIGHT('00' +  CONVERT(varchar(2),[holiday_month]), 2)
	+RIGHT('00' +  CONVERT(varchar(2),[holiday_day]), 2),112 ) in (select convert(date,Item ,103) from  dbo.splitString(@date_list,','))
	end

END
GO
