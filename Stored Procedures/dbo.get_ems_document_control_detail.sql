SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_ems_document_control_detail]
	-- Add the parameters for the stored procedure here
	@ems_control_id as int
		--,@ems_control_no as varchar(9) output
		--,@ems_company_name as varchar(100)  output
		--,@ems_send_date as date output
		--,@ems_status as varchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	select ems_control_document_id	
			,ems_control_document_no	
			,e.company_code	 as company_code
			,isnull(c.company_name,j.company_full_name)	 as company_name
			,department	
			,cost_center	
			,cost_code	
			,send_date	
			,e.[status]
	from dbo.ems_control_document e
	left join  dbo.master_company c on e.company_code = c.company_code 
	left join  dbo.jv_company j on e.company_code = j.company_code 
	where e.ems_control_document_id = @ems_control_id

    -- Insert statements for procedure here
	--select @ems_control_no = e.ems_control_document_no
	--		,@ems_company_name = e.company_code + ' : ' + c.company_name
	--		,@ems_send_date = send_date
	--		,@ems_status = e.status
	--from dbo.ems_control_document e
	--inner join  dbo.master_company c on e.company_code = c.company_code
	--where e.ems_control_document_id = @ems_control_id

	select e.ems_control_document_id
			,ei.ems_detail_id
			,e.ems_control_document_no
			,ei.customer_code  as customer_code
			,ei.remark
			,isnull(c.customer_name, ei.customer_code) as customer_name
			,'' as [address]
			/*,case when a.[address] is null then '' else a.[address] + ' ' end
							 + case when a.sub_district is null then '' else  a.sub_district + ' ' end
							 + case when a.district is null then '' else a.district + ' ' end
							 + case when a.province is null then '' else a.province + ' ' end
							 + case when a.post_code is null then '' else a.post_code + ' ' end
					as [address]*/
	from dbo.ems_control_document e
	inner join dbo.ems_control_document_item ei on e.ems_control_document_id = ei.ems_control_document_id -- and ei.is_active = 1
	left join dbo.master_customer c on ei.customer_code = c.customer_code
	inner join dbo.ems_sub_item esi on  ei.ems_detail_id = esi.ems_detail_id and esi.reference_type = 'B' --and esi.is_active = 1
	inner join dbo.bill_presentment_control_document bd on esi.reference_id = bd.bill_presentment_id
	inner join dbo.master_customer_company cc on bd.customer_company_id = cc.customer_company_id
	left join dbo.master_customer_address a on cc.customer_company_id = a.customer_company_id and a.is_ems = 1
	where e.ems_control_document_id = @ems_control_id
			and	(ei.is_active = 1 or (e.[status] = 'CANCEL' and ei.is_active = 0))
	union 
	select e.ems_control_document_id
			,ei.ems_detail_id
			,e.ems_control_document_no
			,case when c.customer_name is null then '' else ei.customer_code end as customer_code
			,ei.remark
			,isnull(isnull(receiver_name, c.customer_name),ei.customer_code) as customer_name
			,'' as [address]
			/*,case when cc.customer_company_id is not null 
					then (
							case when a.[address] is null then '' else a.[address] + ' ' end
							 + case when a.sub_district is null then '' else  a.sub_district + ' ' end
							 + case when a.district is null then '' else a.district + ' ' end
							 + case when a.province is null then '' else a.province + ' ' end
							 + case when a.post_code is null then '' else a.post_code + ' ' end
						)
					else '' end as [address]*/
	from dbo.ems_control_document e
	inner join dbo.ems_control_document_item ei on e.ems_control_document_id = ei.ems_control_document_id -- and ei.is_active = 1
	left join dbo.master_customer c on ei.customer_code = c.customer_code
	inner join dbo.ems_sub_item esi on  ei.ems_detail_id = esi.ems_detail_id and esi.reference_type = 'O'  --and esi.is_active = 1
	inner join dbo.other_control_document ot on esi.reference_id = ot.document_id
	left join dbo.master_customer_company cc on ot.company_code = cc.company_code and ot.receiver = cc.customer_code
	left join dbo.master_customer_address a on cc.customer_company_id = a.customer_company_id and a.is_ems = 1
	where e.ems_control_document_id = @ems_control_id
			and	(ei.is_active = 1 or (e.[status] = 'CANCEL' and ei.is_active = 0))
	union 
	--ซ้ำกับอันบน นะพี่แนน 07/05/2018
	--select e.ems_control_document_id
	--		,ei.ems_detail_id
	--		,e.ems_control_document_no
	--		,case when c.customer_name is null then '' else ei.customer_code end as customer_code
	--		,ei.remark
	--		,isnull(c.customer_name, ei.customer_code) as customer_name
	--		,'' as [address]
	--		/*,case when cc.customer_company_id is not null 
	--				then (
	--						case when a.[address] is null then '' else a.[address] + ' ' end
	--						 + case when a.sub_district is null then '' else  a.sub_district + ' ' end
	--						 + case when a.district is null then '' else a.district + ' ' end
	--						 + case when a.province is null then '' else a.province + ' ' end
	--						 + case when a.post_code is null then '' else a.post_code + ' ' end
	--					)
	--				else '' end as [address]*/
	--from dbo.ems_control_document e
	--inner join dbo.ems_control_document_item ei on e.ems_control_document_id = ei.ems_control_document_id --and ei.is_active = 1
	--left join dbo.master_customer c on ei.customer_code = c.customer_code
	--inner join dbo.ems_sub_item esi on  ei.ems_detail_id = esi.ems_detail_id and esi.reference_type = 'O'  --and esi.is_active = 1
	--inner join dbo.other_control_document ot on esi.reference_id = ot.document_id
	--left join dbo.master_customer_company cc on ot.company_code = cc.company_code and ot.receiver = cc.customer_code
	--left join dbo.master_customer_address a on cc.customer_company_id = a.customer_company_id and a.is_ems = 1
	--where e.ems_control_document_id = @ems_control_id
	--		and	(ei.is_active = 1 or (e.[status] = 'CANCEL' and ei.is_active = 0))
	--union
	select e.ems_control_document_id
			,i.ems_detail_id
			,e.ems_control_document_no 
			,i.customer_code
			,i.remark
			,o.contact_name as customer_name
			,'' as [address]
			/*,case when o.[address] is null then '' else ' ' + o.[address] end
							 + case when o.sub_district is null then '' else ' ' + o.sub_district end
							 + case when o.district is null then '' else ' ' + o.district end
							 + case when o.province is null then '' else ' ' + o.province end
							 + case when o.post_code is null then '' else ' ' + o.post_code end
						as [address]*/
	from dbo.ems_control_document e
	inner join dbo.ems_control_document_item i on e.ems_control_document_id = i.ems_control_document_id
	inner join dbo.master_other_contact o on i.customer_code = cast(o.contact_id as nvarchar(10)) 
	where e.ems_control_document_id = @ems_control_id and e.is_manual = 1


	select i.ems_detail_id,i.ems_sub_detail_id,i.reference_id as document_id,i.reference_type
			, case when reference_type = 'B' then b.bill_presentment_no
					when reference_type = 'O' then o.document_no
					end as document_no
			, case when reference_type = 'B'  then 'ใบคุมวางบิล'
					when reference_type = 'O' then 
						case when o.document_type = 1 then 'ใบนำส่งใบเสร็จรับเงิน' 
							when o.document_type = 2 then 'ใบนำส่งเอกสารเซ็นบิล'
							when o.document_type = 3 then 'ใบนำส่งใบส่งคืนสินค้าฝาก'
							when o.document_type = 4 then 'ใบนำส่งเอกสารอื่นๆ'
							else 'ใบคุมใบฝากสินค้า'
						end
					end as document_type
	from dbo.ems_sub_item i
	left join dbo.bill_presentment_control_document b on i.reference_type = 'B' and i.reference_id = b.bill_presentment_id
	left join dbo.other_control_document o on i.reference_type = 'O' and i.reference_id = o.document_id
	where ems_detail_id in 
	(select ems_detail_id from dbo.ems_control_document_item where ems_control_document_id = @ems_control_id)

	select [action]
			,[description]
			,convert(varchar, [created_date] , 103) + ' '  + convert(varchar(8), [created_date], 14) as [created_date]
			,[created_by]
	from dbo.ems_control_document_history
	where ems_control_document_id = @ems_control_id

END

GO
