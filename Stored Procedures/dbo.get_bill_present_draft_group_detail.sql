SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE  [dbo].[get_bill_present_draft_group_detail]
	-- Add the parameters for the stored procedure here
	@invoice_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		declare @invoice table (item varchar(max) null,com varchar(max) null)

		insert into @invoice (item)
		select  item from dbo.SplitString(@invoice_list,',')

		update @invoice
		set item = case charIndex('|',item) when 0 then item else left(item,charindex('|',item)-1) end
		,com = case charIndex('|',item) when 0 then item else right(item,charindex('|', reverse(item)) -1 ) end

		select cc.customer_company_id
				,ih.company_code
				,ih.customer_code
				,cus.customer_name
				, isnull(cbb.billing_method ,'PYC') as billing_method
				, ih.payment_term
				, ih.currency
				--,ih.bill_presentment_date
				,ih.invoice_no
				,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
				,ih.invoice_date
				,ih.due_date
		from dbo.invoice_header ih
		inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
		inner join dbo.master_customer cus on ih.customer_code = cus.customer_code
		left join dbo.master_customer_bill_presentment_behavior cbb 
				on cbb.bill_presentment_behavior_id  = ih.bill_presentment_behavior_id 
					/* cc.customer_company_id = cbb.customer_company_id
					and cbb.payment_term_code = ih.payment_term
					and ih.invoice_date between cbb.valid_date_from and isnull(cbb.valid_date_to,'9999-12-31')	*/
		inner join @invoice si on ih.company_code = si.com and ih.invoice_no = si.item
		--where invoice_no in (select item from @invoice)
		union

		select b.customer_company_id
				,cc.company_code
				,cc.customer_code
				,cus.customer_name
				,b.method as billing_method
				,b.payment_term
				,b.currency
				,bi.invoice_no
				,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
				,ih.invoice_date
				,ih.due_date
		from dbo.bill_presentment_item_temp bi
		inner join dbo.bill_presentment_temp b on bi.temp_id = b.temp_id
		inner join dbo.master_customer_company cc on b.customer_company_id = cc.customer_company_id
		inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
		inner join dbo.invoice_header ih on bi.invoice_no = ih.invoice_no and cc.company_code = ih.company_code
		where bi.temp_id in (
			select t.temp_id
				from dbo.bill_presentment_temp t
				inner join dbo.bill_presentment_item_temp bi on t.temp_id = bi.temp_id
				inner join dbo.master_customer_company cc2 on t.customer_company_id = cc2.customer_company_id
				inner join @invoice si on cc2.company_code = si.com and bi.invoice_no = si.item
			--	where bi.invoice_no in (select item from @invoice)
		) and bi.is_active = 1


 --   -- Insert statements for procedure here
	--select cc.customer_company_id
	--			,ih.company_code
	--			,ih.customer_code
	--			,cus.customer_name
	--			, isnull(cbb.billing_method ,'PYC') as billing_method
	--			, ih.payment_term
	--			, ih.currency
	--			--,ih.bill_presentment_date
	--			,ih.invoice_no
	--			,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
	--			,ih.invoice_date
	--			,ih.due_date
	--	from dbo.invoice_header ih
	--	inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
	--	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code
	--	left join dbo.master_customer_bill_presentment_behavior cbb 
	--			on cbb.bill_presentment_behavior_id  = ih.bill_presentment_behavior_id 
	--				/* cc.customer_company_id = cbb.customer_company_id
	--				and cbb.payment_term_code = ih.payment_term
	--				and ih.invoice_date between cbb.valid_date_from and isnull(cbb.valid_date_to,'9999-12-31')	*/
	--	where invoice_no in (select item from dbo.SplitString(@invoice_list,','))

END

GO
