SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_email_document_control]
    -- Add the parameters for the stored procedure here
    @id UNIQUEIDENTIFIER = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    SELECT id,
           document_id,
           document_type,
           email_type,
           email_to,
           email_cc,
           created_date,
           created_by,
           used,
           used_by,
           used_date,
		   is_active
    FROM dbo.email_document_control
    WHERE id = @id;
END;
GO
