SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[create_cheque_invoice_document_control_from_draft]
	-- Add the parameters for the stored procedure here
	 @temp_id as int = null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @collection_id as int
			, @new_status as nvarchar(20) = 'WAITINGRECEIVED'
	insert into [dbo].cheque_invoice_control_document
		([customer_company_id]
		,[method]
		,[remark]
		,collection_date
		,job_date
		,cheque_control_status
		,[created_date]
		,[created_by]
		,[updated_date]
		,[updated_by])
	select customer_company_id
			,method
			,remark
			,dateadd(dd,1,getdate())
			,getdate()
			,@new_status
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from dbo.cheque_invoice_temp
	where temp_id = @temp_id 

	select @collection_id = SCOPE_IDENTITY()


	insert into [dbo].cheque_invoice_control_document_item
		(cheque_control_id
		,invoice_no
		,is_active
		,[created_date]
		,[created_by]
		,[updated_date]
		,[updated_by])
	select @collection_id
		,invoice_no
		,1
		,getdate()
		,@updated_by
		,getdate()
		,@updated_by
	from dbo.cheque_invoice_item_temp
	where temp_id = @temp_id and is_active = 1



	exec [dbo].[delete_cheque_invoice_document_control_draft] 
		@temp_id = @temp_id



	exec [dbo].[insert_cheque_invoice_control_history]
			@cheque_control_id = @collection_id
			,@description = 'Creat Cheque Document Control.'
			,@action  = 'Creat'
			,@updated_by  = @updated_by
END




GO
