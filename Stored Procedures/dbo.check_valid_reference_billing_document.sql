SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[check_valid_reference_billing_document]
	@reference_no varchar(50)=null,
	@document_id int=null,
	@error_message nvarchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    if @reference_no is null or @reference_no =''
	begin 
		set @error_message = 'โปรดกรอก Reference No.'
		return
	end

	Declare @d_customer_code varchar(20),@d_company_code varchar(10),@d_currency varchar(10)
	Declare @customer_code varchar(20),@company_code varchar(10),@currency varchar(10),@customer_name nvarchar(50),@invoice_date datetime,@invoice_no varchar(20)=null
    Declare @is_active bit
	Select @is_active = case when i.bill_presentment_status = 'CANCEL' then 0 else 1 end,@company_code = i.company_code,@customer_code = i.customer_code,@currency = i.currency,@invoice_date = i.invoice_date,@invoice_no = i.invoice_no,@customer_name = c.user_display_name
	from dbo.invoice_header i
	inner join dbo.master_customer_company c
	on c.company_code = i.company_code
	and c.customer_code = i.customer_code
	inner join dbo.master_customer_bill_presentment_behavior b
		on b.bill_presentment_behavior_id = i.bill_presentment_behavior_id
		--on c.customer_company_id = b.customer_company_id
		--and i.payment_term = b.payment_term_code
		--and i.invoice_date Between b.valid_date_from and ISNULL(b.valid_date_to,'9999-12-31')
		--and b.is_signed_document = 1
	where i.invoice_no = @reference_no
	and b.is_signed_document = 1
	and  isnull(i.bill_presentment_status,'') <> 'CANCEL'

	if @invoice_no is null OR @invoice_no =''
	begin
		set @error_message = 'ไม่พบ Invoice No ไม่ระบุ'
		return
	end

	----check dupicate referecnce in otherdoc.document_type = 1
	--if exists(select 1 from dbo.other_control_document o 
	--	inner join dbo.other_control_document_item i 
	--	on i.document_id=o.document_id
	--	and i.reference_no = @reference_no
	--	where o.document_type = 2)
	--begin
	--	set @error_message ='Reference No: '+@reference_no+' มีอยู่แล้วใน OtherDocument นี้'
	--	return
	--end

	Select TOP 1 
		@d_company_code = inv.company_code,
		@d_customer_code = inv.customer_code,
		@d_currency = inv.currency
	from dbo.other_control_document o
	inner join dbo.other_control_document_item i
	on i.document_id = o.document_id
	inner join dbo.invoice_header inv
	on inv.invoice_no = i.reference_no
	where o.document_id = @document_id


	

	if @d_company_code =@company_code AND @customer_code=@d_customer_code AND @d_currency = @currency
	begin
		Declare @temp Table(
			customer_code varchar(20),
			customer_name nvarchar(50),
			company_code varchar(10),
			invoice_no varchar(20),
			invoice_date datetime,
			reference_no varchar(50),
			is_active bit
		)

		select @customer_name = customer_name from dbo.master_customer where customer_code = @customer_code

		insert into @temp values(
		@customer_code,
		@customer_name,
		@company_code,
		@invoice_no,
		@invoice_date,
		@reference_no,
		@is_active)

		select * from @temp
	end
	ELSE
	begin
		set @error_message ='CompanyCode CustomerCode Current ของ Reference No : '+@reference_no+' ไม่ตรงกัน'
	end	
END
GO
