SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_record_count]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	select count(1) as [count],'BIDNO' as tableName
	from dbo.BIDNO
	union
	select count(1) as [count],'BKPF' as tableName
	from dbo.BKPF
	union
	select count(1) as [count],'BSAD' as tableName
	from dbo.BSAD
	union
	select count(1) as [count],'BSEG' as tableName
	from dbo.BSEG
	union
	select count(1) as [count],'BSID' as tableName
	from dbo.BSID
	union
	select count(1) as [count],'DNLOAD' as tableName
	from dbo.DNLOAD
	union
	select count(1) as [count],'VBAK' as tableName
	from dbo.VBAK
	union
	select count(1) as [count],'VBRK' as tableName
	from dbo.VBRK
	union
	select count(1) as [count],'VBRP' as tableName
	from dbo.VBRP
	union
	select count(1) as [count],'ZFI_0090_ADVREC' as tableName
	from dbo.ZFI_0090_ADVREC

END


GO
