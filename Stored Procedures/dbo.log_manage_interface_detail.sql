SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Suteekom K.>
-- Create date: <06/08/2017>
-- Description:	<insert interface log detail>
-- =============================================
CREATE PROCEDURE [dbo].[log_manage_interface_detail]
	@log_interface_header_id int = 0,
	@web_key nvarchar(40) = '',
	@step_name nvarchar(50) = '',
	@remark nvarchar(max) = ''


AS
BEGIN
	SET NOCOUNT ON;

	select @log_interface_header_id = id 
	from log_interface_header where web_key = @web_key;

	print @log_interface_header_id

	if @log_interface_header_id <> '' or @log_interface_header_id is not null
	begin 
		INSERT INTO [dbo].[log_interface_detail]
			   ([log_interface_header_id]
			   ,[web_key]
			   ,[step_name]
			   ,[start_date_time]
			   ,[remark])
		 VALUES
			   (
					@log_interface_header_id,
					@web_key,
					@step_name,
					getdate(),
					@remark
			   )
	end
END

GO
