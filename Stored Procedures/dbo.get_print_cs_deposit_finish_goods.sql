SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_print_cs_deposit_finish_goods]
	@document_id varchar(20)= null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select 
	distinct d.order_no,
	o.document_date,
	o.document_no,
	cus.customer_name,
	o.remark,
	com.company_name
	from dbo.other_control_document o
	inner join dbo.other_control_document_item i
	on i.document_id = o.document_id
	inner join dbo.deposit d
	on d.order_no = i.reference_no
	inner join dbo.master_customer cus
	on cus.customer_code = d.customer_code
	inner join dbo.master_company com
	on com.company_code = o.company_code
	where o.document_id = @document_id
END
GO
