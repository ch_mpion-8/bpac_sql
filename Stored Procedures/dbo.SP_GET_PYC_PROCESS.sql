SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SP_GET_PYC_PROCESS]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200 ) = null,
	@customer_code as varchar(10) = null,
	@document_no as varchar(20) = null,
	@date_from as varchar(20) = null,
	@date_to as varchar(20) = null,
	@is_skip as varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	;with other_doc as 
	(
		select o.document_id as [document_id]
				,o.document_no
				,mcom.company_code
				,mcus.customer_code
				,o.document_date
				,document_date as trip_date 
				,1 as reference_type 
		from [dbo].[other_control_document] o 
		left join master_company mcom on o.company_code = mcom.company_code
		left join master_customer mcus on mcus.customer_code = o.receiver
		where o.method = 'PYC' 
	), cheque as 
	(
		select o.cheque_control_id as [document_id]
				,o.[cheque_control_no] as document_no
				,mcc.company_code
				,mcc.customer_code
				,o.collection_date as document_date
				,o.collection_date as trip_date
				,2 as reference_type 
		from cheque_control_document o 
		left join master_customer_company mcc on o.customer_company_id = mcc.customer_company_id 
		where o.method = 'PYC'
	), bill as 
	(
		select o.bill_presentment_id as [document_id]
				,o.bill_presentment_no as document_no
				,mcc.company_code
				,mcc.customer_code
				,bill_presentment_date  as document_date
				,o.bill_presentment_date as trip_date
				,3 as reference_type 
		from bill_presentment_control_document o 
		left join master_customer_company mcc on o.customer_company_id = mcc.customer_company_id 
		where o.method = 'PYC'
	), chequeInvoice as 
	(
		select o.cheque_control_id as [document_id]
				,o.[cheque_control_no] as document_no
				,mcc.company_code
				,mcc.customer_code
				,o.collection_date as document_date
				,o.collection_date as trip_date
				,4 as reference_type 
		from cheque_invoice_control_document o 
		left join master_customer_company mcc 	on o.customer_company_id = mcc.customer_company_id 
		where o.method = 'PYC' 
	),all_doc as 
	(
		select * from other_doc 
		union all
		select * from cheque
		union all
		select * from bill
		union all
		select * from chequeInvoice
	)
		select [document_id] 
				,document_no
				,a.company_code
				,a.customer_code
				,document_date
				,pyc.trip_date
				,count_trip
				,pyc.reference_type
				,convert(bit,is_skip) as is_skip
				,mcom.customer_name
				,pyc.remark
				,pyc.process_id
				, pcg.*
				,(select top 1 [address] from dbo.master_customer_address addr where addr.customer_company_id = mcc.customer_company_id) as [address]
		from all_doc a 
		right join scg_pyc_process pyc on pyc.reference_no_id = a.document_id and pyc.reference_type = a.reference_type
		left join master_customer mcom on a.customer_code = mcom.customer_code
		left join pyc_customer_group pcg on  pcg.customer_code = a.customer_code
		left join master_customer_company mcc on mcc.company_code = a.company_code and mcc.customer_code = a.customer_code 
 		where 
		( @company_code is null or a.company_code  in ( select item from dbo.SplitString(@company_code,',')))
		and 
		( @customer_code is null or a.customer_code like '%' + @customer_code + '%' or mcom.customer_name like '%' + @customer_code +'%')
		and ( @document_no is null or document_no like '%' + @document_no + '%')
		--and (  is_skip =  @is_skip or @is_skip is null)
		and
		(is_skip = 
		  CASE @is_skip
			  WHEN 'SKIP' THEN 1
			  WHEN 'COUNT' THEN 0
		  END
		  or @is_skip is null
		  )
		and ( @date_from is null or @date_to is null or (document_date between convert(datetime, LEFT(@date_from,10), 103)  and convert(datetime, LEFT(@date_to,10), 103)))
		and isnull(pyc.remark,'') <>'-.. . .-.. . - . -..'

END
GO
