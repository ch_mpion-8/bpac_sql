SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[update_attachment_otherdoc]
	@document_id int =null,
	@guid uniqueidentifier null,
	@update_by varchar(50) =null,
	@update_date datetime = null,
	@description as nvarchar(200) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @description is null or @description = '' 
		set @description = 'Upload Attachment.'

   insert into dbo.other_control_document_history(document_id,created_by,created_date,[description],[action])
   values(@document_id,@update_by,@update_date,@description,'Upload')

   Declare @s varchar(20)

   select @s =[status] from dbo.other_control_document where document_id = @document_id

   if @s <> 'CANCEL'
   begin
      update dbo.other_control_document
	   set reference_guid = @guid,
	   updated_by = @update_by,
	   update_date = @update_date,
	   [status] = 'C'
	   where document_id= @document_id
   end
   else
   begin
	   update dbo.other_control_document
	   set reference_guid = @guid,
	   updated_by = @update_by,
	   update_date = @update_date
	   where document_id= @document_id
   end



   
END
GO
