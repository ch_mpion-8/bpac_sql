SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[duplicate_holiday]
	-- Add the parameters for the stored procedure here
	@holiday_type nvarchar(20) = null
		,@holiday_year int = null
		,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   

	declare @status bit = 1
			,@type nvarchar(20)
			,@holiday_day smallint
			,@holiday_month smallint 
			,@holiday_name as nvarchar(200) 

	select item 
	into #tmptype
	from dbo.splitstring(@holiday_type,',')


	--select * 
	--from dbo.master_holiday 
	--where holiday_year = @holiday_year
	--		and holiday_type in (select item from #tmptype)
	--order by holiday_type,holiday_month,holiday_day


	declare table_cursor cursor for 

		
	select  holiday_day
			,holiday_month
			,holiday_name
			,item as holiday_type
	from dbo.master_holiday,#tmptype
	where holiday_type = 'DEFAULT'
			and [is_active] = @status


	open table_cursor;
	fetch next from table_cursor into @holiday_day,@holiday_month,@holiday_name,@type
	while @@fetch_status = 0
	   begin

			declare @date date = datefromparts ( @holiday_year, @holiday_month, @holiday_day )  
				,@new_date date

			print 'date' + cast(@date as varchar(10))
			set @new_date = (select [dbo].get_next_holiday(@date,@type,null) )
			print 'new date' + cast(@new_date as varchar(10))
		

		   if exists (select 1 from [dbo].[master_holiday] 
						where holiday_year = year(@new_date)
						and holiday_month = month(@new_date)
						and holiday_day = day(@new_date)
						and holiday_type = @type)
				begin
					update [dbo].[master_holiday] 
					set [is_active] = @status
						,[updated_date] = getdate()
						,[updated_by] = @updated_by
					where holiday_year = year(@new_date)
						and holiday_month = month(@new_date)
						and holiday_day = day(@new_date)
						and holiday_type = @type
				
					print 'update'
				end
			else
				begin
				
				declare @compensate_date as date = null
				if(DATEDIFF(day,@date,@new_date) >0)
				begin

					if exists (select 1 from [dbo].[master_holiday] 
						where holiday_year = year(@date)
						and holiday_month = month(@date)
						and holiday_day = day(@date)
						and holiday_type = @type)
					begin
						update [dbo].[master_holiday] 
						set [is_active] = @status
							,[updated_date] = getdate()
							,[updated_by] = @updated_by
						where holiday_year = year(@date)
							and holiday_month = month(@date)
							and holiday_day = day(@date)
							and holiday_type = @type

					end
					else
					begin
						insert into [dbo].[master_holiday]
						(holiday_year
						,holiday_month
						,holiday_day
						,holiday_type
						,holiday_name
						,[is_active]
						,[created_date]
						,[created_by]
						,[updated_date]
						,[updated_by])

						values(  
							@holiday_year
							,@holiday_month
							,@holiday_day
							,@type
							,@holiday_name
							,@status
							,getdate()
							,@updated_by
							,getdate()
							,@updated_by
						)
					end
					set @holiday_name = 'ชดเชย' + @holiday_name
					set @compensate_date = @date
					end 

					insert into [dbo].[master_holiday]
						(holiday_year
						,holiday_month
						,holiday_day
						,holiday_type
						,holiday_name
						,[is_active]
						,[created_date]
						,[created_by]
						,[updated_date]
						,[updated_by]
						,[compensate_date])

					values(  
						year(@new_date)
						,month(@new_date)
						,day(@new_date)
						,@type
						,@holiday_name
						,@status
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
						,@compensate_date
					)

				
					print 'insert' 
				end	

	   
		  fetch next from table_cursor into @holiday_day,@holiday_month,@holiday_name,@type
	   end;

	close table_cursor;
	deallocate table_cursor;


	--select * 
	--from dbo.master_holiday 
	--where holiday_year = @holiday_year
	--		and holiday_type in (select item from #tmptype)
	--order by holiday_type,holiday_month,holiday_day
	
	drop table #tmptype

END




GO
