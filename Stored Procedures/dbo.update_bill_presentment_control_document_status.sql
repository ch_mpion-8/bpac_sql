SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[update_bill_presentment_control_document_status]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int
	,@status as nvarchar(20) 
	,@updated_by as nvarchar(50) = NULL
    ,@is_from_email AS BIT = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update dbo.bill_presentment_control_document
	set bill_presentment_status = @status
		,updated_by = @updated_by
		,updated_date = getdate()
	where bill_presentment_id = @bill_presentment_id
		
	IF @is_from_email = 1 AND @status = 'COMPLETE'
    BEGIN
    
		update ih 
		SET email_status = @status
			, email_receive_date = GETDATE()
		FROM dbo.invoice_header ih
		INNER JOIN dbo.bill_presentment_control_document_item bi ON bi.invoice_no = bi.invoice_no AND bi.is_active = 1
		WHERE bi.bill_presentment_id = @bill_presentment_id

	END 

	declare @change_status_name as nvarchar(200) = null
	select @change_status_name = case @status when 'WAITINGRECEIVED' then 'Reverse status to Waiting for receive control doc.'
														  when 'COMPLETE' then 'Complete'
														  when 'CANCEL' then 'Cancel'
														  else @status
								 end
		

	exec dbo.insert_bill_presentment_history
			@bill_presentment_id = @bill_presentment_id
			,@description = @change_status_name
			,@action  = 'Update status'
			,@updated_by  = @updated_by
END
GO
