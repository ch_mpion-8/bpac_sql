SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_deposit_finish_goods]
	@company_code as varchar(200) = 'All',
	@customer_name as varchar(120)=null,
	@dp_no as varchar(10) = null,
	@status as varchar(10)='All',
	@other_doc_no as varchar(20)= null,
	@other_doc_date_start as date=null,
	@other_doc_date_end as date =null,
	@invoice_no as varchar(20) = null,
	@invoice_date_start as date =null,
	@invoice_date_end as date =null,
	@collection_user as varchar(100) =null,
	@customer_code as varchar(20) =null,
	@method as varchar(20) =null
AS
BEGIN

	if @status = 'All'
		set @status = NULL
	if @company_code = 'All'
		set @company_code = NULL

	;WITH
		deposit		
	as 
		(
			select 
				distinct dep.dp_no,
				isnull(conf.[configuration_value], o.method) as method ,
				dep.company_code,
				dep.customer_code,
				cus.user_display_name,
				o.document_no,
				o.document_date,
				c.customer_name,
				o.created_by,
				o.[status],
				CONVERT(varchar(20),o.document_id) as document_id,
				com.company_name,
				o.reference_guid,
				inv.invoice_date,
				STUFF(
					(
					  SELECT ',' + invoice_no
					  from (SELECT distinct invoice_no from dbo.deposit where dp_no = dep.dp_no) t order by invoice_no asc
					  FOR XML PATH(''), TYPE
					 ).value('.', 'NVARCHAR(MAX)')
					,1
					,1
					,''
				) as invoice_no,
				bh.billing_method
			from
				[dbo].[deposit] as dep
				inner join [dbo].[master_customer_company] cus
					on cus.customer_code = dep.customer_code 
					AND cus.company_code = dep.company_code
				inner join dbo.master_customer c
				on c.customer_code = cus.customer_code
				inner join dbo.master_company com
				on com.company_code = dep.company_code
				inner join dbo.invoice_header inv
				on inv.invoice_no = dep.invoice_no
				/*left join [dbo].[other_control_document_item] item
					on item.reference_no = dep.dp_no
				left join [dbo].[other_control_document] o
					on o.document_id = item.document_id
					and o.document_type=3*/
				left join dbo.master_customer_bill_presentment_behavior bh
					on bh.customer_company_id = cus.customer_company_id
					and bh.payment_term_code = inv.payment_term
				left join (select o.document_no,
					o.method,
					o.document_date,
					o.created_by,
					o.[status], 
					o.document_id,
					o.reference_guid,
					item.reference_no 
					from [dbo].[other_control_document_item] item
						inner join [dbo].[other_control_document] o
							on o.document_id = item.document_id
							and o.document_type=3) as o
				on o.reference_no = dep.dp_no
				
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
			where
				(@company_code is null or dep.company_code in ( select item from dbo.SplitString(@company_code,','))) and
			--	dep.company_code = isnull(@company_code,dep.company_code) AND
				(@customer_code is not null AND dep.customer_code = @customer_code OR @customer_code is null AND c.customer_name LIKE '%'+isnull(@customer_name,'')+'%') AND
				dep.dp_no LIKE '%'+isnull(@dp_no,'')+'%' AND
				dep.invoice_no LIKE '%'+ISNULL(@invoice_no,'') + '%'
				AND ((inv.invoice_date between @invoice_date_start and @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
				AND isnull(convert(date,o.document_date),'') >= isnull(@other_doc_date_start,isnull(convert(date,o.document_date),'')) 
				AND isnull(convert(date,o.document_date),'') <= isnull(@other_doc_date_end,isnull(convert(date,o.document_date),'')) 
				AND isnull(o.[status],'WC') = ISNULL(@status,isnull(o.[status],'WC'))
				and isnull(o.document_no,'') LIKE '%'+@other_doc_no+'%' 
				and isnull(cus.user_display_name,'') like '%'+isnull(@collection_user,'')+'%'
				and (@method is null or o.method = @method)
		),
		re_create
	as
		(
			select 
			distinct 
				dep.dp_no,
				NULL as method,
				dep.company_code,
				dep.customer_code,
				mcc.user_display_name,
				NULL as document_no,
				NULL as document_date,
				cus.customer_name,
				NULL as created_by,
				NULL as [status],
				NULL as  document_id,
				com.company_name,
				NULL as reference_guid,
				inv.invoice_date,
				STUFF(
					(
					SELECT ',' + invoice_no
					  from (SELECT distinct invoice_no from dbo.deposit where dp_no = dep.dp_no) t order by invoice_no asc
					  FOR XML PATH(''), TYPE
					 ).value('.', 'NVARCHAR(MAX)')
					,1
					,1
					,''
				) as invoice_no,
				bh.billing_method
			from dbo.other_control_document_item i 
			inner join dbo.other_control_document o 
				on o.document_id = i.document_id
			inner join dbo.deposit dep
				on dep.dp_no = i.reference_no
			inner join dbo.master_company com
				on com.company_code = dep.company_code
			inner join dbo.master_customer_company mcc
				on mcc.company_code = dep.company_code
				and mcc.customer_code = dep.customer_code
			inner join dbo.master_customer cus
				on cus.customer_code = dep.customer_code
			inner join dbo.invoice_header inv
				on inv.invoice_no = dep.invoice_no
			left join dbo.master_customer_bill_presentment_behavior bh
				on bh.customer_company_id = mcc.customer_company_id
				and bh.payment_term_code = inv.payment_term
			where
				o.[status] = 'CANCEL'
				and o.document_type = 3
				and (select top 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ai.document_id = ao.document_id
				where ai.reference_no = i.reference_no
				and ao.[status] <> 'CANCEL') is null
				and (@company_code is null or dep.company_code in ( select item from dbo.SplitString(@company_code,','))) and
			--	and dep.company_code = isnull(@company_code,dep.company_code) AND
				(@customer_code is not null AND dep.customer_code = @customer_code OR @customer_code is null AND cus.customer_name LIKE '%'+isnull(@customer_name,'')+'%') AND
				dep.dp_no LIKE '%'+isnull(@dp_no,'')+'%' AND
				dep.invoice_no like '%'+isnull(@invoice_no,'') +'%'
				AND ((inv.invoice_date between @invoice_date_start and @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
				AND 'WC' = ISNULL(@status,'WC')
				and isnull(mcc.user_display_name,'') like '%'+isnull(@collection_user,'')+'%'
				and isnull(@other_doc_no,'') = ''
				and (@method is null or o.method = @method)
		)

	select * from deposit
	union all
	select * from re_create

END
GO
