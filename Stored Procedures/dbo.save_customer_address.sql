SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[save_customer_address]
	-- Add the parameters for the stored procedure here
		@address_id as int = null -- 4
		,@customer_company_id as int = null -- '2220'
		,@title as nvarchar(100) = null -- 'ที่อยู่วางบิล 2'
		,@address as nvarchar(500) = null -- '12/789'
		,@sub_district as nvarchar(50) = null -- 'ตำบล'
		,@district as nvarchar(50) = null -- 'อำเภอ'
		,@province as nvarchar(50) = null -- 'กทม'
		,@post_code as nvarchar(10) = null -- '12345'
		,@is_ems as bit = null
		,@status as bit = null -- 'CANCEL'
		,@updated_by as nvarchar(50) = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if ( @is_ems = 1 and exists(select 1 from [dbo].master_customer_address 
					where customer_company_id = @customer_company_id and isnull(is_ems,0) = 1 
					and (@address_id is null or address_id <> @address_id)
					)
		)
	begin
		if exists(select 1 from [dbo].master_customer_address 
					where customer_company_id = @customer_company_id and isnull(is_ems,0) = 1 
					and (@address_id is null or address_id <> @address_id)
					and title = 'ภพ.20'
					)
			begin
				update dbo.master_customer_address
					set is_ems = 0
						,updated_date = getdate()
						,updated_by = @updated_by
				from dbo.master_customer_address
				where customer_company_id = @customer_company_id
				and title = 'ภพ.20'
			end 
		else
			begin
				set @error_message = 'ที่อยู่สำหรับ ems มีได้เพียงที่อยู่เดียวเท่านั้น'
				return;
			end
	end
	else if ( @is_ems = 0 and not exists(select 1 from [dbo].master_customer_address 
					where customer_company_id = @customer_company_id and isnull(is_ems,0) = 1
					and (@address_id is null or address_id <> @address_id)
					)
		)
	begin
		update dbo.master_customer_address
					set is_ems = 1
						,updated_date = getdate()
						,updated_by = @updated_by
				from dbo.master_customer_address
				where customer_company_id = @customer_company_id
				and title = 'ภพ.20'
	end 

	 if exists (select 1 from [dbo].master_customer_address where address_id = @address_id)
		 begin
			
			update dbo.master_customer_address
			set title = @title
				,[address] = @address
				,sub_district = @sub_district
				,district = @district
				,province = @province
				,post_code = @post_code
				,is_ems = @is_ems
				,[is_active] = @status
				,updated_date = getdate()
				,updated_by = @updated_by
			--select 
			--	@title
			--	,@address
			--	,@sub_district
			--	,@district
			--	,@province
			--	,@post_code
			--	,@is_ems
			--	,@status
			--	,getdate()
			--	,@updated_by
			from dbo.master_customer_address
			where address_id = @address_id

			if exists(select 1 from dbo.master_customer_address where copy_from_id = @address_id and is_active = 1)
			begin
				update dbo.master_customer_address
				set title = @title
					,[address] = @address
					,sub_district = @sub_district
					,district = @district
					,province = @province
					,post_code = @post_code
					,is_ems = @is_ems
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_address
				where copy_from_id = @address_id and is_active = 1
			end

			declare @copy_from_id int
			select @copy_from_id = copy_from_id
			from dbo.master_customer_address
			where address_id = @address_id

			if exists(select 1 from dbo.master_customer_address where address_id = @copy_from_id and is_active = 1)
			begin
				update dbo.master_customer_address
				set title = @title
					,[address] = @address
					,sub_district = @sub_district
					,district = @district
					,province = @province
					,post_code = @post_code
					,is_ems = @is_ems
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_address
				where address_id = @copy_from_id and is_active = 1
			end


		 end
		 else
		 begin
			
			insert into dbo.master_customer_address
			([customer_company_id]
			   ,[title]
			   ,[address]
			   ,[sub_district]
			   ,[district]
			   ,[province]
			   ,[post_code]
			   ,[is_ems]
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
			select 
				@customer_company_id
				,@title
				,@address
				,@sub_district
				,@district
				,@province
				,@post_code
				,@is_ems
				,@status
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by

		 end

END
GO
