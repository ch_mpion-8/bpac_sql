SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_bill_presentment_id_by_document_no]
	-- Add the parameters for the stored procedure here
	@document_no varchar(30) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select bill_presentment_id as document_id from dbo.bill_presentment_control_document 
	where bill_presentment_no = @document_no 
			and bill_presentment_status <> 'CANCEL'
END

GO
