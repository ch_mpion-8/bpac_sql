SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_print_deposit_finish_goods]
	@document_id varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
			select 
				distinct 
				d.dp_no,
				isnull(conf.[configuration_value], o.method) as method ,
				o.document_no,
				o.document_date,
				cus.customer_name,
				c.customer_code,
				o.remark,
				STUFF(
					(
					  SELECT ',' + invoice_no
					  from dbo.deposit where dp_no = i.reference_no
					  FOR XML PATH(''), TYPE
					 ).value('.', 'NVARCHAR(MAX)')
					,1
					,1
					,''
				) as invoice_no,
				c.user_display_name,
				com.company_name
				from dbo.other_control_document o 
				inner join dbo.other_control_document_item i on i.document_id = o.document_id
				inner join dbo.deposit d on d.dp_no = i.reference_no
				inner join dbo.master_customer_company c on c.customer_code = d.customer_code and c.company_code = d.company_code
				inner join dbo.master_customer cus on c.customer_code = cus.customer_code
				inner join dbo.master_company com on com.company_code = c.company_code
				left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
				where o.document_id = @document_id
		

END
GO
