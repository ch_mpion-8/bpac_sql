SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_BULK_SAVE_INVOICE_WITH_BILLING]
	-- Add the parameters for the stored procedure here
	@invoice_list as [dbo].[InvoiceCalculateNew_With_Status] readonly
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--	update dbo.invoice_header
--set bill_presentment_date = convert(datetime, LEFT(tmp.bill_prement_date,10), 103) ,
--bill_presentment_notify_date = convert(datetime, LEFT(tmp.bill_prement_notify_date,10), 103) ,
--bill_presentment_calculation_log = tmp.bill_prement_log
----select tmp.*
--from @invoice_list tmp
--left join dbo.invoice_header i on tmp.invoice_no = i.invoice_no
--where i.invoice_no is not null
DECLARE @row_count int = 0;
	DECLARE @invoice_no nvarchar(100)
	DECLARE @bill_prement_date nvarchar(100)
	DECLARE @bill_prement_notify_date nvarchar(100)
	DECLARE @bill_prement_log nvarchar(100)
	DECLARE @collection_prement_date nvarchar(100)
	DECLARE @collection_prement_notify_date nvarchar(100)
	DECLARE @collection_prement_log nvarchar(100)
	DECLARE @billing_behavior_id int
	DECLARE @collection_behavior_id int
	DECLARE @updated_by as nvarchar(50) = 'SYSTEM'

	DECLARE @billing_status as nvarchar(max)

--	DECLARE invoice_cursor CURSOR FOR 
--	SELECT invoice_no,bill_prement_date,bill_prement_notify_date,
--	bill_prement_log,collection_prement_date,collection_prement_notify_date,collection_prement_log,
--	billing_behavior_id,collection_behavior_id
--	FROM @invoice_list
	
--	declare @billcount int =  (SELECT COUNT(*) 
--	FROM @invoice_list);
--	insert into [SCGBPAC].[dbo].[service_log] (webkey,[message],[date]) values ('TEST COUNT BEFORE','BEFORE BILL COUNT' + convert(varchar,(SELECT COUNT(*) 
--	FROM @invoice_list)),GETDATE())
--	-- Open Cursor
--	OPEN invoice_cursor 
--	FETCH NEXT FROM invoice_cursor 
--	INTO @invoice_no, @bill_prement_date,@bill_prement_notify_date,@bill_prement_log,
--	@collection_prement_date,@collection_prement_notify_date,@collection_prement_log
--	,@billing_behavior_id,@collection_behavior_id
   select * into #Temp from @invoice_list	
	-- Loop From Cursor
	Declare @Id int
Declare @ZeroId int

While (Select Count(*) From #Temp) > 0
Begin

    Select Top 1 @invoice_no = invoice_no,@bill_prement_date = bill_prement_date,@bill_prement_notify_date = bill_prement_notify_date,
	@bill_prement_log = bill_prement_log,@collection_prement_date = collection_prement_date,@collection_prement_notify_date = collection_prement_notify_date,
	@collection_prement_log = collection_prement_log,
	@billing_behavior_id = billing_behavior_id,@collection_behavior_id = collection_behavior_id,
	@billing_status = billing_status
	From #Temp

		--declare @isManual as bit

		--select @isManual = case when isnull(bill_presentment_status,'') = 'MANUAL' then 1 else 0 end
		--from dbo.invoice_header
		--where invoice_no = @invoice_no
		
		
		-- /*add condition if status id manual then update only behavior by nan 20180703*/
		-- IF (@isManual = 1)
		--BEGIN
		--	update dbo.invoice_header
		--		set bill_presentment_calculation_log = 'success calculate behavior. update only bill_presentment_behavior_id'
		--		,bill_presentment_behavior_id = @billing_behavior_id
		--		,updated_date_time = GETDATE()
		--		,updated_by = @updated_by
		--		where invoice_no = @invoice_no

		--		set @row_count = @row_count +1;
		--END
		--ELSE
		IF (@billing_status = 'MANUAL' and @billing_behavior_id != 0)
		BEGIN
			update dbo.invoice_header
					set 
					 bill_presentment_behavior_id = @billing_behavior_id
					,bill_presentment_calculation_log = null
						,updated_date_time = GETDATE()
				,updated_by = @updated_by
		
					where invoice_no = @invoice_no

				set @row_count = @row_count +1;
		END
		ELSE IF (@bill_prement_date is null ) or (@bill_prement_notify_date is null) 
		BEGIN 
				if(@billing_behavior_id != 0)begin
				update dbo.invoice_header
					set 
					bill_presentment_date = null
					,bill_presentment_notify_date = null
					,bill_presentment_behavior_id = @billing_behavior_id
					,bill_presentment_calculation_log = null
					,bill_presentment_status = null
						,updated_date_time = GETDATE()
				,updated_by = @updated_by
		
					where invoice_no = @invoice_no
					and isnull(bill_presentment_status,'') not in ('COMPLETE','CANCEL')

				set @row_count = @row_count +1;
				end
				else
				begin
				update dbo.invoice_header
				set [bill_presentment_calculation_log] = @bill_prement_log
					,updated_date_time = GETDATE()
					,bill_presentment_date = null
					,bill_presentment_notify_date = null
					,bill_presentment_behavior_id = @billing_behavior_id
					,bill_presentment_status = 'ERROR'
				,updated_by = @updated_by
					where invoice_no = @invoice_no
					and isnull(bill_presentment_status,'') not in ('COMPLETE','CANCEL')

				set @row_count = @row_count +1;
				end
		END
		ELSE BEGIN
			update dbo.invoice_header
				set bill_presentment_date = convert(datetime, LEFT(@bill_prement_date,10), 103)
				,bill_presentment_notify_date = convert(datetime, LEFT(@bill_prement_notify_date,10), 103) 
				,bill_presentment_behavior_id = @billing_behavior_id
				,bill_presentment_calculation_log = 'success calculate'
				,bill_presentment_status = null
					,updated_date_time = GETDATE()
				,updated_by = @updated_by
		
				where invoice_no = @invoice_no
				and isnull(bill_presentment_status,'') not in ('COMPLETE','CANCEL')

				set @row_count = @row_count +1;
		 END

		Delete #Temp Where invoice_no = @invoice_no 

End
END
--	WHILE (@@FETCH_STATUS = 0) 
--	BEGIN 

----update dbo.invoice_header
----set bill_presentment_date = tmp.bill_prement_date
------select tmp.*
----from @invoice_list tmp
----left join dbo.invoice_header i on tmp.invoice_no = i.invoice_no
----where i.invoice_no is not null




--		IF (@bill_prement_date is null) or (@bill_prement_notify_date is null) 
--		BEGIN 
--			update dbo.invoice_header
--				set [bill_presentment_calculation_log] = @bill_prement_log
--					,updated_date_time = GETDATE()
--		,bill_presentment_date = null
--				,bill_presentment_notify_date = null
--				,bill_presentment_behavior_id = @billing_behavior_id
--				where invoice_no = @invoice_no

--				set @row_count = @row_count +1;
--		END
--		ELSE BEGIN
--			update dbo.invoice_header
--				set bill_presentment_date = convert(datetime, LEFT(@bill_prement_date,10), 103)
--				,bill_presentment_notify_date = convert(datetime, LEFT(@bill_prement_notify_date,10), 103) 
--				,bill_presentment_behavior_id = @billing_behavior_id
--				,bill_presentment_calculation_log = 'success calculate'
--					,updated_date_time = GETDATE()
		
--				where invoice_no = @invoice_no

--				set @row_count = @row_count +1;
--		 END

--		FETCH NEXT FROM invoice_cursor -- Fetch next cursor
--		INTO @invoice_no, @bill_prement_date,@bill_prement_notify_date,@bill_prement_log,
--		@collection_prement_date,@collection_prement_notify_date,@collection_prement_log
--		,@billing_behavior_id,@collection_behavior_id

--	END
	
--	CLOSE invoice_cursor; 
--	DEALLOCATE invoice_cursor; 


--	insert into [SCGBPAC].[dbo].[service_log] (webkey,[message],[date]) values ('TEST COUNT ' + convert(varchar(10),getdate(),120),'AFTER BILLING COUNT' + convert(varchar,@row_count),GETDATE())

--END

--declare @date nvarchar(25) 
--set @date = '31/07/2017 00:00:00' 

---- To datetime datatype
--SELECT convert(datetime, LEFT(@date,10), 103)

 --SELECT convert(datetime, '23/07/2009', 103)

GO
