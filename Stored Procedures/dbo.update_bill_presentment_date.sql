SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[update_bill_presentment_date]
	@invoice_list as [dbo].[InvoiceCalculate] readonly
		,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.invoice_header
	set bill_presentment_date = convert(date, LEFT(tmp.bill_prement_date,10), 103)
		,bill_presentment_notify_date = convert(date, LEFT(tmp.bill_prement_notify_date,10), 103)
		,bill_presentment_remark =  tmp.bill_prement_log 
		,updated_by = @updated_by
		,updated_date_time = getdate()
		,bill_presentment_status = 'MANUAL'
	from @invoice_list tmp
	inner join dbo.invoice_header i on tmp.invoice_no = i.invoice_no

END
GO
