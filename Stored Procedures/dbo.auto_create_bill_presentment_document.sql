SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[auto_create_bill_presentment_document] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE  @document_control_list  as [dbo].[document_control_table_type] 
			,@document_control_id as nvarchar(2000)
			,@invoice_list AS NVARCHAR(2000) = NULL 
	BEGIN TRY
	 ;WITH draft as 
		(
			select  cc.customer_company_id
					,ih.company_code
					, isnull(cbb.billing_method ,'PYC') as billing_method
					, ih.payment_term
					, ih.currency
					,ih.invoice_no
					,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
					, ih.bill_presentment_date
			from dbo.invoice_header ih
			inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
			left join dbo.master_customer_bill_presentment_behavior cbb 
					on cbb.bill_presentment_behavior_id  = ih.bill_presentment_behavior_id 
			LEFT JOIN dbo.invoice_document_detail id 
				ON ih.invoice_no = id.invoice_no
					AND ih.company_code = id.company_code
					AND YEAR(ih.invoice_date) = id.[year]
					AND id.document_type = 'INV'
					AND id.is_active = 1
			WHERE   (ISNULL(ih.require_document_status ,'WAITING') = 'COMPLETE' OR (ih.invoice_type = 'RA' AND id.invoice_no IS NOT NULL))
					AND ISNULL(ih.bill_presentment_status, 'WAITING') NOT IN ('COMPLETE', 'CANCEL')
					AND ih.bill_presentment_notify_date = CAST( GETDATE() AS DATE )

		),
		groupDraft as 
		(
			select customer_company_id, billing_method , payment_term, currency,sum(invoice_amount) as invoice_amount ,bill_presentment_date
			from draft 
			group by customer_company_id, billing_method , payment_term, currency ,bill_presentment_date
		)
		INSERT INTO @document_control_list ([customer_company_id] ,
		[date],
		[method],
		[payment_term],
		[currency],
		[remark] ,
		[reference_no_list] )
		select gd.customer_company_id
				,gd.bill_presentment_date
				, gd.billing_method as bill_presentment_method
				, gd.payment_term
				, gd.currency
				, NULL AS remark
				-- , gd.invoice_amount
				--, isnull(subdraft.bill_presentment_date, dateadd(dd,1,getdate())) as bill_presentment_date
				, subdraft.invoice as invoice_list
		from groupDraft gd
		left join (
						select distinct main.customer_company_id
									, main.billing_method 
									, main.payment_term
									, main.currency
									, main.bill_presentment_date
									,left(cs.Codes,len(cs.Codes)-1) as invoice
									-----------------------------------------------------------------
						from draft main
						--boy update 
						cross apply (
									select CONCAT(invoice_no,'|',sq.company_code) + ','
										from draft sq
										where sq.customer_company_id = main.customer_company_id
												and sq.billing_method = main.billing_method
												and sq.payment_term = main.payment_term
												and sq.currency = main.currency
										for xml path('')
									) cs (Codes)
						-------------------------------
					) subdraft on gd.customer_company_id = subdraft.customer_company_id
										and gd.billing_method = subdraft.billing_method
										and gd.payment_term = subdraft.payment_term
										and gd.currency = subdraft.currency
										and gd.bill_presentment_date = subdraft.bill_presentment_date

	
	EXEC  [dbo].[create_bill_presentment_control_document] @document_control_list,'SYSTEM',@document_control_id
	

	SELECT @invoice_list =  COALESCE(@invoice_list + ', ' + [reference_no_list], [reference_no_list]) 
	FROM @document_control_list

	INSERT INTO [dbo].[log_auto_create_document_control]
           ([log_date]
           ,[log_status]
           ,[log_message]
		   ,[data])
		SELECT
			GETDATE()
			,'COMPLETE'
			,NULL
			,@invoice_list

	END TRY
	BEGIN CATCH
		

	SELECT @invoice_list =  COALESCE(@invoice_list + ', ' + [reference_no_list], [reference_no_list]) 
	FROM @document_control_list

		INSERT INTO [dbo].[log_auto_create_document_control]
           ([log_date]
           ,[log_status]
           ,[log_message]
		   ,[data])
		SELECT
			GETDATE()
			,'ERROR'
			,ERROR_MESSAGE()
			,@invoice_list
		
	END CATCH
END
GO
