SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[validate_invoice]
	-- Add the parameters for the stored procedure here
	@invoice_list as nvarchar(max)
	,@error_invoice as nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	select stuff(
		(
			select ',' + sub.invoice_no 
				from dbo.bill_presentment_control_document_item sub with(nolock)
				inner join dbo.bill_presentment_control_document main with(nolock) on sub.bill_presentment_id = main.bill_presentment_id
				inner join dbo.master_customer_company cc with(nolock) on main.customer_company_id = cc.customer_company_id
				inner join (
					select  
					substring(item,0,charindex('|', item)) as invoice
					,right(item,charindex('|', reverse(item)) -1 ) as com
					from dbo.SplitString(@invoice_list,',')
				) ref on sub.invoice_no = ref.invoice 
												and cc.company_code = ref.com
					where sub.is_active = 1 and main.bill_presentment_status <> 'CANCEL'
				for xml path('')
		) ,1,1,'') 


	 --select @error_invoice = stuff(
		--			(
		--				select ',' + sub.invoice_no 
		--					from dbo.bill_presentment_control_document_item sub with(nolock)
		--					inner join dbo.bill_presentment_control_document main with(nolock) on sub.bill_presentment_id = main.bill_presentment_id
		--						where sub.invoice_no in (select item from dbo.SplitString(@invoice_list,','))
		--								and sub.is_active = 1 and main.bill_presentment_status <> 'CANCEL'
		--					for xml path('')
		--			) ,1,1,'') 
	 ----select @error_invoice
END

	

GO
