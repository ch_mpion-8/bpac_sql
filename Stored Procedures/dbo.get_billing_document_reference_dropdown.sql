SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_billing_document_reference_dropdown] 
	-- Add the parameters for the stored procedure here
	@company_code varchar(4) =null,
	@customer_code varchar(10)=null,
	@currency varchar(10)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		--select 
		--	distinct i.invoice_no as reference_no
		--from dbo.invoice_header i
		--inner join dbo.master_customer_company m
		--	on m.company_code = i.company_code
		--	and m.customer_code = i.customer_code
		--inner join dbo.master_customer_bill_presentment_behavior b
		--	on b.bill_presentment_behavior_id = i.bill_presentment_behavior_id
		--left join (select o.document_id,i.reference_no,o.company_code,o.receiver,o.[status] from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id where o.document_type=2) as o
		--	on o.company_code = i.company_code
		--	and o.receiver = i.customer_code
		--	and o.reference_no = i.invoice_no
		--where i.company_code = @company_code
		--and i.customer_code = @customer_code
		--and i.currency = @currency
		--and ( o.document_id is null or (o.document_id is not null AND o.[status] = 'CANCEL'))
		--and b.is_signed_document =1
		--and  isnull(i.bill_presentment_status,'') <> 'CANCEL'

			select  distinct
			inv.invoice_no as reference_no from dbo.invoice_header inv
			left join dbo.other_control_document_item i 
				on i.reference_no = inv.invoice_no
			left join dbo.other_control_document o
				on o.document_id = i.document_id
			inner join dbo.master_customer_company m
				on m.company_code = inv.company_code
				and m.customer_code = inv.customer_code
			inner join dbo.master_customer_bill_presentment_behavior b
				on b.bill_presentment_behavior_id = inv.bill_presentment_behavior_id
			where 
			inv.company_code = @company_code
			and inv.customer_code =@customer_code
			and inv.currency =  @currency
			and  isnull(o.[status],'CANCEL') = 'CANCEL' 
			and (select TOP 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ai.document_id= ao.document_id
			where ai.reference_no = inv.invoice_no
			and ao.document_type = 2
			and ao.[status] <>'CANCEL') is null
			and isnull(b.is_signed_document,1) = 1
			and case isnull(inv.bill_presentment_status,'') when 'CANCEL' then 0 else 1 end = 1
END
GO
