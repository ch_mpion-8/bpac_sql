SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_bill_presentment_following_email]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@bill_presentment_no as nvarchar(50) = null
		,@invoice_no as nvarchar(50) = null
		,@upload_date as DATETIME = null
		,@user as nvarchar(50) = NULL
        ,@output_type AS NVARCHAR(50) = NULL
        ,@document_status AS NVARCHAR(50) = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @output_type = CASE WHEN @output_type = '' THEN NULL ELSE @output_type end
	SET @document_status = CASE WHEN @document_status = '' THEN NULL ELSE @document_status end
   

	SELECT ih.invoice_no
			, ih.invoice_date
			,ih.bill_presentment_behavior_id
			,ih.company_code
			,ih.customer_code
			,cc.customer_company_id
			,cus.customer_name
			,c.is_require_document
			,c.is_waiting_all_dp
			,bdc.bill_presentment_id
			,bdc.bill_presentment_no
			,ec.output_type_code
			,CASE WHEN ISNULL(ih.require_document_status,'WAITING') = 'COMPLETE' AND ISNULL(ih.print_document_status,'WAITING') = 'COMPLETE' 
					THEN 'COMPLETE' 
					ELSE 'WAITING' END AS require_document_status
	INTO #temp1
	FROM dbo.invoice_header ih
	INNER JOIN dbo.master_company c ON ih.company_code = c.company_code
	INNER JOIN dbo.master_customer cus ON ih.customer_code = cus.customer_code
	INNER JOIN dbo.master_customer_company cc ON ih.company_code = cc.company_code AND ih.customer_code = cc.customer_code
	LEFT JOIN (
			select bdc.bill_presentment_id,bdc.bill_presentment_no,bdc.bill_presentment_date,bdc.bill_presentment_status
					, bdc.method
					,bdc.reference_guid,bdc.remark,cc.company_code
					,bdci.bill_presentment_detail_id,bdci.invoice_no,bdci.is_active
			from dbo.bill_presentment_control_document bdc 
			left join dbo.bill_presentment_control_document_item bdci on  bdc.bill_presentment_id = bdci.bill_presentment_id
			inner join  dbo.master_customer_company cc on bdc.customer_company_id = cc.customer_company_id 
			where (isnull(bdci.is_active,1) = 1 AND  bdc.bill_presentment_status <> 'CANCEL' )
		) bdc on ih.invoice_no = bdc.invoice_no and ih.company_code = bdc.company_code
	LEFT JOIN dbo.master_email_config ec ON cc.customer_company_id = ec.customer_company_id
	WHERE ih.bill_presentment_behavior_id IS NOT NULL
	--	AND ISNULL(ih.bill_presentment_status,'WAITING') NOT IN ('COMPLETE','CANCEL')
	--	AND ISNULL(ih.require_document_status,'') <> 'COMPLETE'
		AND c.is_require_document = 1
		-- AND ec.output_type_code LIKE '%Print%'
		AND ih.bill_presentment_behavior_id IN (
				SELECT DISTINCT bill_presentment_behavior_id			
				FROM dbo.master_customer_bill_presentment_document bd
				--INNER JOIN dbo.[configuration] con ON bd.document_code = con.configuration_code AND con.configuration_key = 'BillingDocument'
				WHERE  bd.document_code in  ('1','2','3','4','8','10','11') AND bd.is_active = 1
				)
		 AND ( @invoice_no IS NULL OR ih.invoice_no LIKE '%' + @invoice_no + '%')
		 AND (@company_code is null or ih.company_code  in ( select item from dbo.SplitString(@company_code,',')))
		 AND (@customer is null or ih.customer_code +' : '+ cus.customer_name like '%' + @customer + '%')
		 AND (@customer_code IS NULL OR cc.customer_code  LIKE '%' + @customer_code + '%')
		 AND (@bill_presentment_no IS NULL OR bdc.bill_presentment_no LIKE '%' + @bill_presentment_no +'%')
		 AND (@upload_date IS NULL OR ih.invoice_no IN (SELECT invoice_no FROM dbo.invoice_document_detail WHERE CAST(updated_date AS DATE) = CAST(@upload_date AS DATE)))
		 AND (@user IS NULL OR cc.user_name = @user)
		 AND (@output_type IS NULL OR ec.output_type_code = @output_type)
		 AND (@document_status IS NULL OR 
				(CASE WHEN ISNULL(ih.require_document_status,'WAITING') = 'COMPLETE' AND ISNULL(ih.print_document_status,'WAITING') = 'COMPLETE' 
						THEN 'COMPLETE' 
						ELSE 'WAITING' END  = @document_status))

	SELECT *
	FROM #temp1

	SELECT invoice_no
			,company_code
			,[year]
			,document_type
			,document_name
			,document_id
			,print_count
			,is_active
			,created_date
			,created_by
			,updated_date
			,updated_by
	FROM dbo.invoice_document_detail id
	WHERE id.invoice_no IN (SELECT invoice_no FROM #temp1)
		AND id.is_active = 1

	SELECT DISTINCT  invoice_no, dp_no
	FROM dbo.invoice_detail
	WHERE invoice_no IN (SELECT invoice_no FROM #temp1)

	SELECT bill_presentment_document_id
			,bill_presentment_behavior_id
			,document_code
			,custom_document
	FROM dbo.master_customer_bill_presentment_document
	WHERE bill_presentment_behavior_id IN (SELECT bill_presentment_behavior_id FROM #temp1)
			AND document_code in  ('1','2','3','4','8','10','11')
			AND is_active = 1

	DROP TABLE #temp1
END
GO
