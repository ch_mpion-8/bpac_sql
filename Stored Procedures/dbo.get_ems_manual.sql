SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_ems_manual] 
	-- Add the parameters for the stored procedure here
	@company_code as varchar(4) = null
		,@receiver as nvarchar(50) = null
		,@running_no as nvarchar(50) = null
		,@send_date as date = null
		,@status as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --    select '' as ems_control_document_id
	--		,'' as running_no
	--		,''  as company_code
	--		,''  as department
	--		,''  as cost_center
	--		,''  as cost_code
	--		,''  as send_date
	--		,null  as reference_guid
	--		,''  as [status]
	--		,''  as ems_detail_id
	--		,cast (o.contact_id as varchar(1))  as customer_code
	--		,o.contact_name
	--		,''  as remark
	--		,'' as [address]
	--from dbo.master_other_contact o 
	--union
	select e.ems_control_document_id
			,e.ems_control_document_no  as running_no
			,e.company_code
			,e.department
			,e.cost_center
			,e.cost_code
			,e.send_date
			,e.reference_guid
			,e.[status]
			,i.ems_detail_id
			,i.customer_code
			,o.contact_name
			,i.remark
			,case when o.[address] is null then '' else ' ' + o.[address] end
							 + case when o.sub_district is null then '' else ' ' + o.sub_district end
							 + case when o.district is null then '' else ' ' + o.district end
							 + case when o.province is null then '' else ' ' + o.province end
							 + case when o.post_code is null then '' else ' ' + o.post_code end
						as [address]
	from dbo.ems_control_document e
	inner join dbo.ems_control_document_item i on e.ems_control_document_id = i.ems_control_document_id
	inner join dbo.master_other_contact o on i.customer_code = cast(o.contact_id as nvarchar(10)) 
	where (@company_code is null or e.company_code = @company_code)
			and (@receiver is null or o.contact_name  like '%' + @receiver + '%')
			and (@running_no is null or e.ems_control_document_no  like '%' + @running_no + '%' )				
			and (@send_date is null or e.send_date  = @send_date )		
			and (@status is null or isnull(e.status,'WAITINGCREATE')  = @status )
			and e.is_manual = 1

END

GO
