SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[update_bpac_date]
	-- Add the parameters for the stored procedure here
	@invoice_list as [dbo].[InvoiceCalculate] readonly
		,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	/*update dbo.invoice_header
	set collection_date =  convert(datetime, LEFT(tmp.collection_prement_date,10), 103)
		,collection_notify_date =  convert(datetime, LEFT(tmp.collection_prement_notify_date,10), 103)
		,[collection_remark] =   tmp.collection_prement_log 
		,updated_by = @updated_by
		,updated_date_time = getdate()
		,collection_status = 'MANUAL'
	from @invoice_list tmp
	left join dbo.invoice_header i on tmp.invoice_no = i.invoice_no
	where i.invoice_no is not null*/
	

	update i
	set i.collection_date =  convert(datetime, LEFT(tmp.collection_prement_date,10), 103)
		,i.collection_notify_date =  convert(datetime, LEFT(tmp.collection_prement_notify_date,10), 103)
		,i.[collection_remark] =   tmp.collection_prement_log
		,i.updated_by = @updated_by
		,i.updated_date_time = getdate()
		,i.collection_status = 'MANUAL'
	from @invoice_list tmp
	inner join dbo.invoice_header i on tmp.invoice_no = i.invoice_no
	where isnull(i.collection_status,'') <> 'COMPLETE'
	and tmp.invoice_no is not null

END
GO
