SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[validate_remaining_invoice]
	-- Add the parameters for the stored procedure here
	@invoice_list as nvarchar(max)
	,@error_message as nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 select @error_message = stuff(
					(
						select ',' + ih.invoice_no 
							from dbo.invoice_header ih with(nolock)
							inner join (
										select  
												substring(item,0,charindex('|', item)) as invoice_no
												,case when right(item,charindex('|', reverse(item)) -1 ) is not null 
														and right(item,charindex('|', reverse(item)) -1 ) <> ''
													then CAST(right(item,charindex('|', reverse(item)) -1 ) AS DECIMAL(15, 2))
													else 0 end as amount
												from dbo.SplitString(@invoice_list,',')
										) ref on ih.invoice_no =  ref.invoice_no 
								where ih.remaining_amount - ref.amount < 0
							for xml path('')
					) ,1,1,'') 
	 --select @error_invoice
END
GO
