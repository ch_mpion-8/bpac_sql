SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[delete_cheque_invoice_document]
	-- Add the parameters for the stored procedure here
	 @cheque_control_id as int = null--1
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update c
	set cheque_control_status = 'CANCEL'
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 'CANCEL',*
	from dbo.cheque_invoice_control_document c
	where cheque_control_id = @cheque_control_id

	update ci
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 0,*
	from dbo.cheque_invoice_control_document_item ci
	where cheque_control_id = @cheque_control_id

	update ih
	set remaining_amount = remaining_amount + ci.amount
	from invoice_header ih
	inner join  dbo.cheque_invoice_control_document_item ci on ih.invoice_no = ci.invoice_no
	where cheque_control_id = @cheque_control_id

		
		if exists(select 1 from dbo.cheque_invoice_control_document where cheque_control_id = @cheque_control_id and convert(date,getdate())<convert(date,collection_date) and method = 'PYC')
			begin
				if exists (select 1 from dbo.scg_pyc_process where reference_no_id = @cheque_control_id and reference_type = 2 )
				begin
					update dbo.scg_pyc_process
					set remark = '-.. . .-.. . - . -..',
					updated_by = @updated_by,
					updated_date = getdate(),
					is_skip = 1
					where reference_no_id = @cheque_control_id
					and reference_type = 2
				end
				else
				begin
					
					declare @trip_date as date = null
					select @trip_date = collection_date
					from dbo.cheque_invoice_control_document where cheque_control_id = @cheque_control_id 

						insert into scg_pyc_process(
							reference_no_id,
							reference_type,
							remark,
							created_by,
							created_date,
							count_trip,
							is_skip,
							trip_date
						)
						values(
							@cheque_control_id,
							2,
							'-.. . .-.. . - . -..',
							@updated_by,
							getdate(),
							1,
							1,
							@trip_date)
				end

			end

		exec dbo.insert_cheque_invoice_control_history 
			@cheque_control_id = @cheque_control_id
			,@description = 'Cancel Document Control'
			,@action  = 'Cancel'
			,@updated_by  = @updated_by

END
GO
