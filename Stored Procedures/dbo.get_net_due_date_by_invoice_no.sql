SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_net_due_date_by_invoice_no]
	-- Add the parameters for the stored procedure here
	@invoice_no varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select i.net_due_date from dbo.invoice_header i where invoice_no = @invoice_no
END

GO
