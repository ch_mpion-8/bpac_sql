SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[validate_document_for_ems]
	-- Add the parameters for the stored procedure here
	 @document_list as nvarchar(max) = null --'B17-0230-00008,O17-0230-000006,O17-2220-000003,B17-0230-00002'
	,@error_message as nvarchar(max) = '' output --
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @id as nvarchar(max)  

	;with doc_list as (
		select item 
		from dbo.SplitString(@document_list,',')
	), doc as (
	select item
		,e.ems_sub_detail_id as id
	from doc_list d
	left join dbo.bill_presentment_control_document bd on d.Item = bd.bill_presentment_no 
	left join [dbo].[ems_sub_item] e on bd.bill_presentment_id = e.reference_id
							and e.reference_type = left(d.item,1)
								and e.is_active = 1
	left join dbo.ems_control_document_item i on e.ems_detail_id = i.ems_detail_id and i.is_active = 1
	where e.reference_type = 'B' and e.ems_sub_detail_id is not null and i.ems_detail_id is not null
	union
	select item
			,e2.ems_sub_detail_id as id
	from doc_list d
	left join dbo.other_control_document o on d.Item = o.document_no 
	left join [dbo].[ems_sub_item] e2 on o.document_id = e2.reference_id 
								and e2.reference_type = left(d.item,1)
								and e2.is_active = 1
	left join dbo.ems_control_document_item i on e2.ems_detail_id = i.ems_detail_id and i.is_active = 1
	where e2.reference_type = 'O' and e2.ems_sub_detail_id is not null and i.ems_detail_id is not null
	)
	select @id = stuff(
					(
						select distinct ',' + item
							from doc
							for xml path('')
					) ,1,1,'')

	if(@id <> '' or @id is not null)
	begin
		set @error_message = 'ไม่สามารถเลือกเอกสารที่เคยสร้างใบคุม (' + @id + ')'
	end

END
GO
