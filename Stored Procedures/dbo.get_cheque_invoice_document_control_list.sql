SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cheque_invoice_document_control_list]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@user_name as nvarchar(50) = null
		,@collection_date_from as date = null
		,@collection_date_to as date = null
		,@invoice_no as nvarchar(50) = null
		,@collection_no as nvarchar(50) = null
		,@receive_method as nvarchar(50) = null
		,@status as nvarchar(50) = null
		,@is_billing_with_bill as bit = null
		,@is_billing_with_cheque as bit = null
		,@invoice_date_from as date = null
		,@invoice_date_to as date = null
		,@net_due_date_from as date = null
		,@net_due_date_to as date = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 ;with main as
(	select cc.customer_company_id
		,ih.company_code
		,com.company_name
		,ih.customer_code
		,cus.customer_name
		,ih.invoice_no
		,ih.invoice_date as invoice_date
		,ih.net_due_date as net_due_date
		,ih.remaining_amount as remaining_amount
		,ih.currency
		,cci.cheque_control_id
		,cci.cheque_control_detail_id
		,cci.cheque_control_no
		,cci.collection_date as collection_date
		,isnull(conf.[configuration_value],cci.method )  as receipt_method
		,cci.amount
		,cci.cheque_date
		,cci.cheque_no
		,cci.cheque_amount
		,cci.[cheque_currency]
		, cci.cheque_bank
		, cci.cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, cc.user_display_name
		, cci.created_by
		, isnull(cci.cheque_control_status,'WAITING') as status
		, ctemp.temp_id
		, case when ctemp.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, cci.reference_guid
		,cast(year(ih.invoice_date) as nvarchar(5)) as [year]
		,isnull(cci.is_active,1) as is_active
		, ih.invoice_amount + isnull(ih.tax_amount,0) as invoice_amount
	from dbo.invoice_header ih
	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on ih.company_code = com.company_code
	left join (
			select ccd.cheque_control_id
					,ccd.cheque_control_no
					,ccd.customer_company_id
					,ccd.method
					,ccd.job_date
					,ccd.remark
					,ccd.cheque_control_status
					,ccd.collection_date
					,ccd.created_date
					,ccd.created_by
					,ccd.updated_date
					,ccd.updated_by
					,reference_guid
					,is_manual
					,cheque_control_detail_id
					,cci.invoice_no
					,cci.tracking_date
					,cci.amount
					,cci.cheque_no
					,cci.cheque_amount
					,cci.cheque_date
					,cci.cheque_currency
					,cci.cheque_bank
					,cci.cheque_remark
					,cci.is_active
					,cci.document_year
			from dbo.cheque_invoice_control_document ccd 
			inner join  dbo.cheque_invoice_control_document_item cci on cci.cheque_control_id = ccd.cheque_control_id
			where (isnull(cci.is_active,1) = 1 or ccd.cheque_control_status = 'CANCEL' )
			) cci
	on ih.invoice_no = cci.invoice_no
	 and cc.customer_company_id = cci.customer_company_id
	left join dbo.master_customer_collection_behavior cbb 
			on ih.collection_behavior_id = cbb.collection_behavior_id
	left join dbo.master_customer_bill_presentment_behavior cbbb
			on ih.bill_presentment_behavior_id = cbbb.bill_presentment_behavior_id
	left join (
				select customer_company_id,ci.[invoice_no],ct.temp_id
				from dbo.cheque_invoice_temp ct 
				inner join dbo.cheque_invoice_item_temp ci on  ct.temp_id = ci.temp_id and ci.is_active = 1
	)ctemp on ih.[invoice_no] = ctemp.[invoice_no]  and cc.customer_company_id = ctemp.customer_company_id
			left join dbo.configuration conf on cci.method  = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where (@company_code is null or ih.company_code  in ( select item from dbo.SplitString(@company_code,',')))
			and (@customer is null or (ih.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%'))
			and ih.customer_code = isnull(@customer_code,ih.customer_code)
			and (@user_name is null or cc.user_name = @user_name)
			and (@collection_date_from is null or  cci.collection_date is null
						or
						(
							@collection_date_from is not null 
							and @collection_date_to is not null 
							and cci.collection_date   between @collection_date_from and @collection_date_to
						)
					)
			and (@invoice_no is null or ih.invoice_no like '%' + @invoice_no + '%')
			and (@collection_no is null or cci.cheque_control_no like '%' + @collection_no + '%')
			and (@receive_method is null or isnull(cci.method,'') in ( select item from dbo.SplitString(@receive_method,',')))
			and (@is_billing_with_bill is null  or isnull(cbbb.is_billing_with_bill,0) = @is_billing_with_bill)
			--and (@status is null 
			--		or (isnull(cci.cheque_control_status,'WAITING')  = @status)
			--	)
			-- and year(ih.invoice_date) = 2019
			and isnull(ih.collection_status,'') <> 'COMPLETE'
			and (@invoice_date_from is null or  ih.invoice_date is null
						or
						(
							@invoice_date_from is not null 
							and @invoice_date_to is not null 
							and ih.invoice_date   between @invoice_date_from and @invoice_date_to
						)
					)
			and (@net_due_date_from is null or  ih.net_due_date  is null
						or
						(
							@net_due_date_from is not null 
							and @net_due_date_to is not null 
							and ih.net_due_date    between @net_due_date_from and @net_due_date_to
						)
					)
),sub as (
	select *
	from main
	union
	select  main.customer_company_id
		,main.company_code
		,main.company_name
		,main.customer_code
		,main.customer_name
		,main.invoice_no
		,main.invoice_date
		,main.net_due_date
		,main.remaining_amount
		,main.currency
		,null as cheque_control_id
		,null as cheque_control_detail_id
		,null as cheque_control_no
		,null as collection_date
		,null as receipt_method
		,null as amount
		,null as cheque_date
		,null as cheque_no
		,null as cheque_amount
		,null as [cheque_currency]
		,null as cheque_bank
		,null as cheque_remark
		, null as collection_behavior_id
		, null as bpac_date
		, null as bank_name
		, main.user_display_name
		, null as created_by
		, 'WAITING' as status
		, main.temp_id
		, case when main.temp_id is null then cast(0 as bit) else cast(1 as bit) end as is_in_draft
		, null as reference_guid
		,main.[year]
		, 1 as is_active
		, main.invoice_amount
	from main
	inner join invoice_header ih on main.invoice_no = ih.invoice_no
	 where  ih.remaining_amount > 0
	 ) 
	 select * 
	--into #temp
	from sub
	where 	 (@status is null or (isnull(sub.[status],'WAITING')  = @status))
				and (@collection_no is null or cheque_control_no like '%' + @collection_no + '%')
	order by invoice_no
END
GO
