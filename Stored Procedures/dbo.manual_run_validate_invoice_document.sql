SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manual_run_validate_invoice_document] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
DECLARE @invoice_no as nvarchar(20) = NULL -- '1580373596' -- 1580369810
	,@company_code as varchar(4) = NULL -- '0230'
	,@year as int = NULL -- 2019

	DECLARE inv_cur CURSOR FOR
	SELECT invoice_no,company_code,YEAR(invoice_date) AS [year]
	FROM dbo.invoice_header
	WHERE bill_presentment_behavior_id IS NOT NULL
    
	OPEN inv_cur;  
	FETCH NEXT FROM inv_cur INTO @invoice_no, @company_code, @year;  
  
	-- Check @@FETCH_STATUS to see if there are any more rows to fetch.  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		EXECUTE dbo.validate_invoice_require_document @invoice_no ,@company_code,@year
		

	 FETCH NEXT FROM inv_cur  INTO  @invoice_no, @company_code, @year;  
	END  
	CLOSE inv_cur;  
	DEALLOCATE inv_cur;  
END
GO
