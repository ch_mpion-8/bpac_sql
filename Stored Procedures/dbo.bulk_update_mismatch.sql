SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[bulk_update_mismatch]
	-- Add the parameters for the stored procedure here
	@mismatch_id as varchar(100) = null,
	@status as int,
	@user as varchar(30),
	@remark as varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update mismatch set mismatch_status = @status,remark = @remark where mismatch_id in (select * from dbo.SplitString(@mismatch_id,','));
END
GO
