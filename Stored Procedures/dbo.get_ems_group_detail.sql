SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_ems_group_detail]
	-- Add the parameters for the stored procedure here
	 @document_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    ;with doc_list as (
		select item 
		from dbo.SplitString(@document_list,',')
	), list as (
		select c.customer_code
				,bd.bill_presentment_id as document_id
				,bd.bill_presentment_no as document_no
				,bd.bill_presentment_date as document_date
				,'ใบคุมวางบิล' as document_type
		from dbo.bill_presentment_control_document bd
		inner join dbo.master_customer_company c on bd.customer_company_id = c.customer_company_id
		left join dbo.master_customer_address a on c.customer_company_id = a.customer_company_id and a.is_ems = 1
		where bd.bill_presentment_no in (select item from doc_list)
		union
		select case when isnull(ot.contact_person_type,1) = 1 then cus.customer_code else cast(ot.contact_person_id as nvarchar(50)) end   as customer_code
				--,case when cc.customer_company_id is null then ot.receiver else cus.customer_name end as customer_name 
				,ot.document_id as document_id
				,ot.document_no as document_no
				,ot.document_date
				,case when ot.document_type = 1 then 'ใบนำส่งใบเสร็จรับเงิน' 
					when ot.document_type = 2 then 'ใบนำส่งเอกสารเซ็นบิล'
					when ot.document_type = 3 then 'ใบนำส่งใบส่งคืนสินค้าฝาก'
					when ot.document_type = 4 then 'ใบนำส่งเอกสารอื่นๆ'
					else 'ใบคุมใบฝากสินค้า'
					end as document_type
		from dbo.other_control_document ot
		left join dbo.master_customer_company cc on ot.company_code = cc.company_code and ot.receiver = cc.customer_code
		left join dbo.master_customer cus on ot.receiver = cus.customer_code
		left join dbo.master_other_contact_person con on ot.contact_person_id = con.other_contact_id
		left join dbo.master_customer_address a on cc.customer_company_id = a.customer_company_id and a.is_ems = 1
		where ot.document_no in (select item from doc_list)
	)
	select *
	from list

END

GO
