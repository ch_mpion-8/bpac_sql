SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[get_bill_presentment_following]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@bill_presentment_no as nvarchar(50) = null
		,@bill_presentment_date_from as date = null
		,@bill_presentment_date_to as date = null
		,@invoice_no as nvarchar(50) = null
		,@status as nvarchar(50) = null
		,@notify_date_from as date = null
		,@notify_date_to as date = null
		,@need_po as bit = null
		,@bill_presentment_type as nvarchar(50) = null
		,@bill_presentment_method as nvarchar(50) = null
		,@application_tag as nvarchar(50) = null
		,@user_name as nvarchar(50) = null
		,@is_no_bill_presentment as bit = 0
		,@is_billing_every_day as bit = 1
		,@is_billing_with_condition as bit = 1
		,@query_string        nvarchar(max) = null
		,@parameter_list  nvarchar(4000) = null
		,@invoice_date_from as date = null
		,@invoice_date_to as date = null
		,@is_signed_document as bit = NULL
        ,@net_due_date_from AS DATE = null
		,@net_due_date_to AS DATE = null
AS
BEGIN
	SET NOCOUNT ON;
		set @query_string = ''

		set @query_string = @query_string + N' select * from (
  select  distinct  cc.customer_company_id
		,ih.company_code
		,com.company_name
		,ih.customer_code
		,cus.customer_name
		, cbb.description as bill_presentment_description
		, isnull(conf.[configuration_value] ,cbb.billing_method) as bill_presentment_method
		, cbb.document_type as bill_presentment_type
		, subdoc.doc  as bill_presentment_document
		,ih.invoice_no
		,(select stuff((
					select distinct '', '' + bid_no 
						from dbo.invoice_detail sub
						where sub.invoice_no = ih.invoice_no
						for xml path('''')
				) ,1,1,'''') ) as bid_no
		,(select stuff(
					(
						select distinct '', '' + po_no 
							from dbo.invoice_detail sub
							where sub.invoice_no = ih.invoice_no
							for xml path('''')
					) ,1,1,'''') ) as po_no
		,ih.invoice_date
		,bdc.bill_presentment_no 
		,case when cc.company_code in  (''7850'',''0470'') then '''' else ih.payment_term end as payment_term_name
		--,term.payment_term_name
		,case when cc.company_code in  (''7850'',''0470'') then ih.net_due_date else ih.due_date end as due_date
		,ih.net_due_date
		,bdc.bill_presentment_date 
		,ih.bill_presentment_date as bpac_date
		,ih.bill_presentment_notify_date
		,replace(isnull(ih.bill_presentment_status,''WAITING''),''MANUAL'',''WAITING'') as bill_presentment_status
		,cc.user_display_name
		, cbb.notify_day as notify_day
		,cbb.remark as bill_presentment_remark
		,bdc.bill_presentment_detail_id
		,bdc.bill_presentment_id 
		,ih.currency
		,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount 
		,cbb.bill_presentment_behavior_id 
		,ih.bill_presentment_remark as invoice_remark
		,cbb.is_bill_presentment
		,re.doc as receiptNo
		,app.application_tag_name
		,(select stuff(
					(
						select distinct '', '' + dp_no 
							from dbo.invoice_detail sub
							where sub.invoice_no = ih.invoice_no
							for xml path('''')
					) ,1,1,'''') ) as dp_no
		,ih.invoice_qty as invoice_qty
		,ih.qty_unit as qty_unit

	from dbo.invoice_header ih
	inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
	inner join dbo.master_customer cus on ih.customer_code = cus.customer_code	and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on ih.company_code = com.company_code		
	left join dbo.master_application_tag app on cus.application_tag_id = app.application_tag_id	
	left join dbo.master_customer_bill_presentment_behavior cbb 
			on cbb.bill_presentment_behavior_id = ih.bill_presentment_behavior_id					
	left join (
		select bdc.bill_presentment_id,bdc.bill_presentment_no,bdc.bill_presentment_date,bdc.bill_presentment_status
				,bdc.method,bdc.reference_guid,bdc.remark,cc.company_code
				,bdci.bill_presentment_detail_id,bdci.invoice_no,bdci.is_active
		from dbo.bill_presentment_control_document bdc 
		left join dbo.bill_presentment_control_document_item bdci on  bdc.bill_presentment_id = bdci.bill_presentment_id
		inner join  dbo.master_customer_company cc on bdc.customer_company_id = cc.customer_company_id 
	
	) bdc on ih.invoice_no = bdc.invoice_no and ih.company_code = bdc.company_code  and bdc.is_active = 1
	left join (
					select  main.bill_presentment_behavior_id,	stuff(
					(
						select '', '' + configuration_value + case when sub.custom_document is null then '''' else '' '' + sub.custom_document end
							from master_customer_bill_presentment_document sub
							inner join dbo.configuration con on sub.document_code = con.configuration_code and con.configuration_key = ''BillingDocument''
							where sub.bill_presentment_behavior_id = main.bill_presentment_behavior_id and sub.is_active = 1
							for xml path('''')
					) ,1,1,'''') as doc
					from dbo.master_customer_bill_presentment_document main
				) subdoc on cbb.bill_presentment_behavior_id = subdoc.bill_presentment_behavior_id		
	left join (
	
		select  distinct main.assignment_no,main.company_code ,	stuff(
			(
				select '', '' + sub.reference_document_no 
					from receipt sub 
					where sub.assignment_no = main.assignment_no 
							and main.company_code = sub.company_code
							and main.[year] = sub.[year]
					for xml path('''')
			) ,1,1,'''') as doc
			from dbo.receipt main
	) re on ih.invoice_no = re.assignment_no and ih.company_code = re.company_code
	left join dbo.configuration conf on cbb.billing_method = conf.[configuration_code] and conf.[configuration_key] = ''Method''
	WHERE (
					(ih.invoice_type = ''RA'' and (ih.payment_term not in (''TS00'',''NT00'') or ih.company_code in (''0470'',''7850'')))
					or ih.invoice_type in (''RC'',''RD'',''DA'',''DC'')
				)'

		IF @invoice_date_from IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND ih.invoice_date between @usp_invoice_date_from and @usp_invoice_date_to' 
		END

		IF @is_signed_document IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND isnull(cbb.is_signed_document,0) = @usp_is_signed_document' 
		END

		IF @company_code IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND ih.company_code  in ( select item from dbo.SplitString(@usp_company_code,'',''))' 
		END

		IF @customer IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (ih.customer_code like ''%'' + @usp_customer + ''%'' or cus.customer_name  like ''%'' + @usp_customer + ''%'')' 
		END

		IF @customer_code IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (ih.customer_code =  @usp_customer_code )' 
		END

		IF @bill_presentment_no IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND bdc.bill_presentment_no  like ''%'' + @usp_bill_presentment_no + ''%'''
		END

		IF @bill_presentment_date_from IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (
					(
						bdc.bill_presentment_date between @usp_bill_presentment_date_from and @usp_bill_presentment_date_to
					))'
		END
		
		/* Update query by veeraya
		IF @notify_date_from IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (ih.bill_presentment_notify_date is null or
					(
						@usp_notify_date_from is not null 
						and @usp_notify_date_to is not null 
						and ih.bill_presentment_notify_date between @usp_notify_date_from and @usp_notify_date_to
					))'
		END*/

		IF @notify_date_from IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (ih.bill_presentment_notify_date is null or
					(
						 ih.bill_presentment_notify_date = @usp_notify_date_from 
						 or ((@usp_status is null or @usp_status like ''%WAITING%'')  and ih.bill_presentment_notify_date <= @usp_notify_date_from )
					))'
		END
		
	if @net_due_date_from is not null   
		begin
			set @query_string = @query_string + ' and ih.net_due_date between @usp_net_due_date_from and @usp_net_due_date_to '
		end

		IF @invoice_no IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND ih.invoice_no like ''%'' + @usp_invoice_no + ''%'''
		END

		IF @status IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND replace(isnull(ih.bill_presentment_status,''WAITING''),''MANUAL'',''WAITING'')  in (select item from dbo.SplitString(@usp_status,'','')) '
		END

		IF @bill_presentment_method IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND cbb.billing_method in ( select item from dbo.SplitString(@usp_bill_presentment_method,'',''))'
		END

		IF @application_tag IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND isnull(cus.application_tag_id,999) in ( select item from dbo.SplitString(@usp_application_tag,'',''))'
		END

		IF @user_name IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND (isnull(cc.user_name,''Customer Need User'') = @usp_user_name ) '
		END

		IF @bill_presentment_type IS NOT NULL   
		BEGIN
			set @query_string = @query_string + ' AND cbb.document_type = @usp_bill_presentment_type'
		END

		IF @need_po = 1
		BEGIN
			set @query_string = @query_string + ' AND subdoc.doc like ''%PO%'' '
		END
		
		IF @is_no_bill_presentment = 0 or @is_billing_every_day = 1 or @is_billing_with_condition = 1
			BEGIN
				set @query_string = @query_string + ' and (
					cbb.bill_presentment_behavior_id is  null '
				if @is_no_bill_presentment = 0
				begin
					set @query_string = @query_string + ' or (isnull(cbb.is_bill_presentment,1) =  0) '
				end

				if @is_billing_every_day = 1
				begin
					set @query_string = @query_string + ' or (isnull(cbb.condition_date_by,'''') like ''%EVERYDATE%'' and isnull(cbb.is_bill_presentment,1) =  1) '
				end

				if @is_billing_with_condition = 1
				begin
					set @query_string = @query_string + ' or (isnull(cbb.condition_date_by,'''') not like ''%EVERYDATE%'' and isnull(cbb.is_bill_presentment,1) =  1) '
				end

				set @query_string = @query_string + ' )'
			END
			
		--set @query_string = @query_string + ' order by ih.bill_presentment_date,cus.customer_name'
		--PRINT @query_string

		set @query_string = @query_string + ' ) a order by case bill_presentment_status 
		when ''ERROR'' then  1
		when ''WAITING'' then  2
		when ''WAITINGRECEIVED'' then  3
		when ''COMPLETE'' then  4
		else 5 end
		,bill_presentment_method 
		,company_code
		,customer_code
		,invoice_date
		,invoice_no
	'

		select @parameter_list = '@usp_company_code as varchar(200) = null
		,@usp_customer as nvarchar(200) = null
		,@usp_customer_code as nvarchar(50) = null
		,@usp_bill_presentment_no as nvarchar(50) = null
		,@usp_bill_presentment_date_from as date = null
		,@usp_bill_presentment_date_to as date = null
		,@usp_invoice_no as nvarchar(50) = null
		,@usp_status as nvarchar(50) = null
		,@usp_notify_date_from as date = null
		,@usp_notify_date_to as date = null
		,@usp_need_po as bit = null
		,@usp_bill_presentment_type as nvarchar(50) = null
		,@usp_bill_presentment_method as nvarchar(50) = null
		,@usp_application_tag as nvarchar(50) = null
		,@usp_user_name as nvarchar(50) = null
		,@usp_is_no_bill_presentment as bit = null
		,@usp_is_billing_every_day as bit = null
		,@usp_is_billing_with_condition as bit = null
		,@usp_invoice_date_from as date = null
		,@usp_invoice_date_to as date = null
		,@usp_is_signed_document as bit = null
		,@usp_net_due_date_from as date =null
		,@usp_net_due_date_to as date = null'

		EXEC sp_executesql @query_string, @parameter_list, @company_code
		,@customer 
		,@customer_code 
		,@bill_presentment_no 
		,@bill_presentment_date_from 
		,@bill_presentment_date_to 
		,@invoice_no 
		,@status
		,@notify_date_from 
		,@notify_date_to 
		,@need_po
		,@bill_presentment_type 
		,@bill_presentment_method 
		,@application_tag 
		,@user_name 
		,@is_no_bill_presentment 
		,@is_billing_every_day 
		,@is_billing_with_condition 
		,@invoice_date_from
		,@invoice_date_to
		,@is_signed_document
		,@net_due_date_from
		,@net_due_date_to
		SET NOCOUNT OFF;
END
GO
