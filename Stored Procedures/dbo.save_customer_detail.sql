SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[save_customer_detail]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null--'2007211'
		,@remark as nvarchar(2000) = null--'tesrrr'
		,@application_tag_id as int = null--2
		,@updated_by as nvarchar(50) = null
		,@is_ignore as bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     update [dbo].[master_customer]
			set remark = @remark
				,application_tag_id = @application_tag_id
				,[updated_date] = getdate()
				,[updated_by] = @updated_by
				,is_ignore = @is_ignore
			where customer_code = @customer_code;
		
		
		
END
GO
