SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_incomplete_customer]
	-- Add the parameters for the stored procedure here
	@user_name as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	--select distinct cc.customer_company_id
	--		,cc.company_code
	--		,com.company_name
	--		,cc.customer_code
	--		,cus.customer_name
	--		,cc.[user_name]
	--		,cc.[user_display_name]
	--from dbo.master_customer_company cc
	--left join dbo.master_customer_bill_presentment_behavior bill 
	--	on cc.customer_company_id = bill.customer_company_id
	--		and bill.[is_active] = 1
	--left join dbo.master_customer_collection_behavior coll 
	--	on cc.customer_company_id = coll.customer_company_id
	--		and coll.[is_active] = 1
	--inner join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	--inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1
	--where (bill.customer_company_id is null
	--		or coll.customer_company_id is null) and cc.[is_active] = 1

	
	select  distinct cc.customer_company_id
			,i.company_code
			,com.company_name
			,i.customer_code
			,cus.customer_name
			,cc.[user_name]
			,cc.[user_display_name]
	from dbo.invoice_header i
	inner join dbo.master_customer_company cc on i.customer_code = cc.customer_code and i.company_code = cc.company_code
	inner join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1
		and isnull(cus.is_ignore,0) =0
	where (((i.bill_presentment_calculation_log = 'No Matched Billing Behavior' 
				and isnull(i.bill_presentment_status,'') <> 'COMPLETE' 
				/*
					comment by Nan 
					and (isnull(i.bill_presentment_status,'') <> 'COMPLETE' or isnull(i.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%')
				*/
				)
			or ( i.collection_calculation_log = 'No Matched Collection Behavior' and isnull(i.collection_status,'') <> 'COMPLETE' ))
			)
			and (@user_name is null or cc.user_name = @user_name or cc.user_name is null)
			and isnull(i.bill_presentment_status,'') <> 'CANCEL'
		

end


GO
