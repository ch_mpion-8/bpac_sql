SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_customer_holiday]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null
	,@year as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select h.customer_holiday_id as holiday_id 
			,h.customer_code
			,h.holiday_year
			,h.holiday_month
			,h.holiday_day
			,h.holiday_name
			,'CUSTOMER' as holiday_type
			,h.[is_active]
			,h.created_date
			,h.created_by
			,h.updated_date
			,h.updated_by
	from dbo.master_customer_holiday h
	where (@year is null or h.holiday_year = @year)
			and customer_code = @customer_code
			and [is_active] = 1
END




GO
