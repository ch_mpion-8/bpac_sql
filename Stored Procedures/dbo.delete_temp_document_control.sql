SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_temp_document_control]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   

	delete
	from dbo.cheque_temp
	where CAST(updated_date as date) <> CAST(getdate() as date)



	delete
	from dbo.cheque_item_temp
	where CAST(updated_date as date) <> CAST(getdate() as date)


	delete
	from dbo.bill_presentment_temp
	where CAST(updated_date as date) <> CAST(getdate() as date)

	delete
	from dbo.bill_presentment_item_temp
	where CAST(updated_date as date) <> CAST(getdate() as date)

	delete
	from dbo.zero_loan_item
	where zero_loan_id in ( select a.zero_loan_id from zero_loan a, zero_loan_item b, cheque_control_document c, cheque_control_document_item d
		where a.zero_loan_id = b.zero_loan_id
		and b.cheque_control_id = d.cheque_control_detail_id
		and c.cheque_control_id = d.cheque_control_id
		and b.zero_loan_status = 2
		and collection_date <= cast(DATEADD(day, DATEDIFF(day, 0, GETDATE()), -1) as date) ) 

	delete
	from dbo.zero_loan
	where zero_loan_id in ( select a.zero_loan_id from zero_loan a, zero_loan_item b, cheque_control_document c, cheque_control_document_item d
		where a.zero_loan_id = b.zero_loan_id
		and b.cheque_control_id = d.cheque_control_detail_id
		and c.cheque_control_id = d.cheque_control_id
		and b.zero_loan_status = 2
		and collection_date <= cast(DATEADD(day, DATEDIFF(day, 0, GETDATE()), -1) as date) )
END

GO
