SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[get_bill_present_draft_group]
	-- Add the parameters for the stored procedure here
	@invoice_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   ;with invoice_list as (
		select   substring(item,0,charindex('|', item)) as ref_no
					,right(item,charindex('|', reverse(item)) -1 ) as com
				from dbo.SplitString(@invoice_list,',')
	),draft as 
	(
		select  cc.customer_company_id
				,ih.company_code
				,com.company_name
				,ih.customer_code
				,cus.customer_name
				, isnull(cbb.billing_method ,'PYC') as billing_method
				, ih.payment_term
				, ih.currency
				--,ih.bill_presentment_date
				,ih.invoice_no
				,isnull(ih.invoice_amount,0.00) + isnull(ih.tax_amount,0.00) as invoice_amount
		from dbo.invoice_header ih
		inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
		inner join dbo.master_customer cus on ih.customer_code = cus.customer_code
		inner join dbo.master_company com on ih.company_code = com.company_code
		left join dbo.master_customer_bill_presentment_behavior cbb 
				on cbb.bill_presentment_behavior_id  = ih.bill_presentment_behavior_id 
					/* cc.customer_company_id = cbb.customer_company_id
					and cbb.payment_term_code = ih.payment_term
					and ih.invoice_date between cbb.valid_date_from and isnull(cbb.valid_date_to,'9999-12-31')	*/
			inner join invoice_list si on ih.company_code = si.com and ih.invoice_no = si.ref_no
		--where invoice_no in (select item from dbo.SplitString(@invoice_list,','))
	),
	groupDraft as 
	(
		select customer_company_id, billing_method , payment_term, currency,sum(invoice_amount) as invoice_amount--,bill_presentment_date
		from draft 
		group by customer_company_id, billing_method , payment_term, currency--,bill_presentment_date
	)

	select gd.customer_company_id
			, subdraft.company_code
			, subdraft.company_name
			, subdraft.customer_code
			, isnull(subdraft.customer_name,'') as customer_name
			, gd.billing_method as bill_presentment_method
			, gd.payment_term
			, gd.currency
			, gd.invoice_amount
			--, isnull(subdraft.bill_presentment_date, dateadd(dd,1,getdate())) as bill_presentment_date
			, subdraft.invoice as invoice_list
	from groupDraft gd
	left join (
					select distinct customer_company_id
								,company_code
								,company_name
								,customer_code
								,customer_name
								, billing_method 
								, payment_term
								, currency
								--,bill_presentment_date

								-- very slow but BOY had update new query ,it's too fastttttttttttttttttttt
								--,	stuff(
								--(
								--	select ',' + sub.invoice_no 
								--		from draft sub
								--		where sub.customer_company_id = main.customer_company_id
								--				and sub.billing_method = main.billing_method
								--				and sub.payment_term = main.payment_term
								--				and sub.currency = main.currency
								--			--	and sub.bill_presentment_date = main.bill_presentment_date
								--		for xml path('')
								--) ,1,1,'') as invoice
								,left(cs.Codes,len(cs.Codes)-1) as invoice
								-----------------------------------------------------------------
					from draft main
					--boy update 
					cross apply (
									select invoice_no + ','
									from draft sq
									where sq.customer_company_id = main.customer_company_id
											and sq.billing_method = main.billing_method
											and sq.payment_term = main.payment_term
											and sq.currency = main.currency
									for xml path('')
								) cs (Codes)
					-------------------------------
				) subdraft on gd.customer_company_id = subdraft.customer_company_id
									and gd.billing_method = subdraft.billing_method
									and gd.payment_term = subdraft.payment_term
									and gd.currency = subdraft.currency
								--	and gd.bill_presentment_date = subdraft.bill_presentment_date
			
	order by customer_name
END


GO
