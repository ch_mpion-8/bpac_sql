SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_invoice_document_detail] 
	@invoice_no as varchar(18) = null
	,@company_code as varchar(4) = null
	,@year as int = null
	,@document_type as nvarchar(20) = null
	,@document_name as nvarchar(100) = null
	,@document_id as uniqueidentifier = null
	,@is_active as bit = null
	,@updated_by as nvarchar(200) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF EXISTS(SELECT 1 FROM invoice_document_detail 
						WHERE invoice_no = @invoice_no AND company_code = @company_code 
								AND [year] = @year AND document_type = @document_type AND document_name = @document_name)
	BEGIN
		UPDATE invoice_document_detail
		SET document_name = @document_name
			,document_id = @document_id
			,is_active = @is_active
			,updated_by = @updated_by
			,updated_date = GETDATE()
		WHERE invoice_no = @invoice_no AND company_code = @company_code 
				AND [year] = @year AND document_type = @document_type AND document_name = @document_name
    END
	ELSE
    BEGIN
		INSERT INTO invoice_document_detail
		(
			invoice_no
			, company_code
			, year
			, document_type
			, document_name
			, document_id
			, is_active
			, created_date
			, created_by
			, updated_date
			, updated_by
		)
		VALUES(
			@invoice_no
			, @company_code
			, @year
			, @document_type
			, @document_name
			, @document_id
			, @is_active
			, GETDATE()
			, @updated_by
			, GETDATE()
			, @updated_by
		)
	END 
        
	exec dbo.validate_invoice_require_document @invoice_no,@company_code,@year
END
GO
