SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_billing_document]
	@company_code as varchar(200)='All',
	@customer_name as varchar(120)=null,
	@invoice_no as varchar(20) = null,
	@invoice_date_start as date =null,
	@invoice_date_end as date =null,
	@status as varchar(20)=null,
	@otherdoc_no as varchar(20)=null,
	@otherdoc_date_start as date =null,
	@otherdoc_date_end as date =null,
	@collectionuser varchar(100)=null,
	@customer_code as varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @company_code = 'All'
		set @company_code = null
	if @status = 'All'
		set @status = null

	;with
	invoice
		as
		(select 
			distinct
			o.created_by,
			isnull(conf.[configuration_value], o.method) as method,
			o.document_date,
			o.document_id,
			o.reference_guid,
			o.[status],
			o.document_no,
			case when o.action_type = 0 then 'SR' when o.action_type = 1 then 'S' when o.action_type = 2 then 'R' else NULL end as action_type,
			i.company_code,
			i.customer_code,
			cus.customer_name,
			i.invoice_no,
			i.invoice_date,
			i.invoice_amount,
			i.currency,
			c.user_display_name,
			case isnull(i.bill_presentment_status,'') when 'CANCEL' then 0 else 1 end as is_active,
			com.company_name
		from dbo.invoice_header i
		inner join dbo.master_customer_company c
			on i.customer_code = c.customer_code 
			and i.company_code = c.company_code
		inner join dbo.master_customer cus
		on cus.customer_code = c.customer_code
		inner join dbo.master_company com
			on com.company_code = i.company_code
		inner join dbo.master_customer_bill_presentment_behavior b
			on b.customer_company_id = c.customer_company_id
		left join (select o.created_by,o.method,o.document_date,o.document_id,o.reference_guid,i.action_type,o.document_no,o.[status],i.reference_no,o.company_code,o.receiver from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id
		) as o
			on o.reference_no = i.invoice_no
			and o.company_code = i.company_code
			and o.receiver = i.customer_code
		
		left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
		where 
		(@company_code is null or i.company_code in ( select item from dbo.SplitString(@company_code,',')))   and
		(@customer_code is not null and i.customer_code = @customer_code OR @customer_code is null and cus.customer_name LIKE '%'+isnull(@customer_name,'')+'%') and
		i.invoice_no LIKE '%'+isnull(@invoice_no,'')+'%' and
		((i.invoice_date between @invoice_date_start and @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
		and isnull(b.is_signed_document,1) =1
		and i.invoice_date >= cast('2017-12-06' as date) --go live date: 6 december 2017
		and isnull(c.user_display_name,'') like '%'+isnull(@collectionuser,'')+'%'
		and (
				o.document_id is null AND isnull(i.bill_presentment_status,'') <> 'CANCEL'
			OR
				o.document_id is not null
			)
		),
	invoice_send
		as
		(
			select * from invoice where action_type in ('S','SR')
		),
	invoice_receive
		as
		(
			select * from invoice where action_type in ('R','SR')
		),
	invoice_wait_create
		as
		(
			select * from invoice where action_type is null
		),
	combind_all
		as
		(
			select distinct * from (
				select 
					s.user_display_name,
					s.company_code,
					s.company_name,
					s.currency,
					s.customer_code,
					s.customer_name,
					s.invoice_amount,
					s.invoice_date,
					s.invoice_no,
					s.method as send_method,
					r.method as receive_method,
					s.document_no as send_document_no,
					r.document_no as receive_document_no,
					s.[status] as send_status,
					isnull(r.[status],'WC') as receive_status,
					s.document_id as send_document_id,
					r.document_id as receive_document_id,
					s.reference_guid as send_reference_guid,
					r.reference_guid as receive_reference_guid,
					s.document_date as send_document_date,
					r.document_date as receive_document_date,
					case 
						when s.[status] = 'CANCEL' then 'CANCEL'
						when isnull(r.[status],'WC') = 'WC' then 'WRINV'
						when s.[status] <> 'WC' and isnull(r.[status],'WC') <> 'WC' and (s.[status]<>'C' or isnull(r.[status],'WC') <> 'C') then 'WR'
						else 'C'
						end as total_status,
					s.created_by
					from invoice_send s
					left join invoice_receive r
					on s.invoice_no = r.invoice_no
					and s.customer_code = r.customer_code
					and s.company_code = r.company_code
					and s.currency = r.currency
					and (r.[status] <> 'CANCEL'
					and s.[status] <> 'CANCEL'
					or s.document_id = r.document_id)
					union all
				select 
					r.user_display_name,
					r.company_code,
					r.company_name,
					r.currency,
					r.customer_code,
					r.customer_name,
					r.invoice_amount,
					r.invoice_date,
					r.invoice_no,
					NULL as send_method,
					r.method as receive_method,
					NULL as send_document_no,
					r.document_no as receive_document_no,
					'WC' as send_status,
					isnull(r.[status],'WC') as receive_status,
					NULL as send_document_id,
					r.document_id as receive_document_id,
					NULL as send_reference_guid,
					r.reference_guid as receive_reference_guid,
					NULL as send_document_date,
					r.document_date as receive_document_date,
					'CANCEL' as toal_status,
					r.created_by
					from invoice_receive r
					left join invoice_send s
					on s.document_id = r.document_id
					where r.[status] = 'CANCEL'
					and s.document_id is null
						union all
				select
					user_display_name,
					company_code,
					company_name,
					currency,
					customer_code,
					customer_name,
					invoice_amount,
					invoice_date,
					invoice_no,
					NULL as send_method,
					NULL as receive_method,
					NULL as send_document_no,
					NULL as receive_document_no,
					'WC' as send_status,
					'WC' as receive_status ,
					NULL as send_document_id,
					NULL as receive_document_id,
					NULL as send_reference_guid,
					NULL as receive_reference_guid,
					NULL as send_document_date,
					NULL as receive_document_date,
					'WSINV' as total_status,
					NULL as created_by
					from invoice_wait_create
					) as t
		),
	re_create
		as
		(
			select distinct
			mcc.user_display_name,
			com.company_code,
			com.company_name,
			currency,
			inv.customer_code,
			customer_name,
			invoice_amount,
			invoice_date,
			invoice_no,
			NULL as send_method,
			NULL as receive_method,
			NULL as send_document_no,
			NULL as receive_document_no,
			'WC' as send_status,
			'WC' as receive_status ,
			NULL as send_document_id,
			NULL as receive_document_id,
			NULL as send_reference_guid,
			NULL as receive_reference_guid,
			NULL as send_document_date,
			NULL as receive_document_date,
			'WSINV' as total_status,
			NULL as created_by
			from dbo.other_control_document_item i
			inner join dbo.other_control_document o
			on o.document_id = i.document_id
			inner join dbo.invoice_header inv
			on i.reference_no = inv.invoice_no
			inner join dbo.master_customer_company mcc
			on mcc.company_code = inv.company_code
			and mcc.customer_code = inv.customer_code
			inner join dbo.master_customer cus
			on cus.customer_code = inv.customer_code
			inner join dbo.master_company com
			on com.company_code = inv.company_code
			where

				o.document_type =2
				and  inv.invoice_date >= cast('2017-12-06' as date) --go live date: 6 december 2017
				and o.[status] = 'CANCEL'
				and (select TOP 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ao.document_id = ai.document_id
				where ao.document_type = 2
				and ao.[status] <>'CANCEL'
				and ai.reference_no = inv.invoice_no
				) is null
				and (@company_code is null or inv.company_code in ( select item from dbo.SplitString(@company_code,','))) and
				-- inv.company_code = isnull(@company_code,inv.company_code)   and
				(@customer_code is not null AND  inv.customer_code =@customer_code OR @customer_code is null AND cus.customer_name LIKE '%'+isnull(@customer_name,'')+'%') and
				inv.invoice_no LIKE '%'+isnull(@invoice_no,'')+'%' and
				((inv.invoice_date between @invoice_date_start and @invoice_date_end) OR @invoice_date_end is null OR @invoice_date_start is null)
				and 'WSINV' = isnull(@status,'WSINV')
				and isnull(mcc.user_display_name,'') like '%'+isnull(@collectionuser,'')+'%'
				and isnull(@otherdoc_no,'') = ''
		)


	
	select *
	from combind_all
	where 
	( 
			(
				isnull(convert(date,send_document_date),'') >= isnull(@otherdoc_date_start,isnull(convert(date,send_document_date),'')) 
			AND isnull(convert(date,receive_document_date),'') <= isnull(@otherdoc_date_end,isnull(convert(date,send_document_date),'')) 
			) 
		OR 
			(
				isnull(convert(date,receive_document_date),'') >= isnull(@otherdoc_date_start,isnull(convert(date,receive_document_date),'')) 
			AND isnull(convert(date,receive_document_date),'') <= isnull(@otherdoc_date_end,isnull(convert(date,receive_document_date),'')) 
			)
	)
	and 
	(
		total_status = @status or @status is null
	)
	and (isnull(send_document_no,'') LIKE '%'+isnull(@otherdoc_no,'')+'%' OR isnull(receive_document_no,'') LIKE '%'+isnull(@otherdoc_no,'')+'%')
	union all
	select *
	from re_create
	
			
	
END

GO
