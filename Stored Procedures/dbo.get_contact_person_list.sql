SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_contact_person_list]
	-- Add the parameters for the stored procedure here
	@contact_person_name as nvarchar(100) = null -- 'saw'--
	,@contact_group_id as nvarchar(100) = null
	,@contact_person_phone as nvarchar(100) = null --'0'--
	,@contact_person_email as nvarchar(50) = null --'a' --
	,@status as bit = null --'a' --
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	select contact_person_id
		  ,cp.contact_person_name
		  ,cp.contact_group_id
		  ,cg.contact_group_value
		  ,cp.contact_person_phone
		  ,cp.contact_person_email
		  ,cp.contact_person_remark
		  ,cp.[is_active]
		  ,cp.created_date
		  ,cp.created_by
		  ,cp.updated_date
		  ,cp.updated_by
	from [dbo].[master_contact_person] cp
	left join dbo.master_contact_group cg on cp.contact_group_id = cg.contact_group_id
	where (@contact_person_name is null or cp.contact_person_name like '%' + @contact_person_name + '%')
			and (@contact_group_id is null or cp.contact_group_id in (SELECT Item FROM dbo.SplitString(@contact_group_id, ',')))
			and (@contact_person_phone is null or cp.contact_person_phone like '%' + @contact_person_phone + '%')
			and (@contact_person_email is null or cp.contact_person_email like '%' + @contact_person_email + '%')
			and (@status is null or cp.[is_active] = @status)
	order by  cp.contact_person_name
END




GO
