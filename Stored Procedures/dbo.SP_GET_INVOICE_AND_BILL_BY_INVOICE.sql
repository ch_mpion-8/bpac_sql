SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE [dbo].[SP_GET_INVOICE_AND_BILL_BY_INVOICE]
--@company_code as varchar(10) = null,
@invoice_no as nvarchar(max)  = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @bill TABLE(
    bill_id INT
    );

    select 
	ih.invoice_no,
	ih.invoice_type,
	ih.invoice_date,
	(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)
	as load_date,
	ih.bill_presentment_date,
	ih.due_date,
	ih.net_due_date,
	ih.collection_date,
	ih.company_code,
	ih.customer_code,
	ih.payment_term,
	ih.invoice_amount,
	ih.tax_amount,
	ih.currency,
	ih.referenece_document_no,
	--	ih.bill_presentment_behavior_id,
	--ih.collection_behavior_id,
	ih.assignment_no,
	mcb.bill_presentment_behavior_id,
	mcb.customer_company_id,
	mcb.valid_date_from,
	mcb.valid_date_to,
	mcb.payment_term_code,
	mcb.invoice_period_from,
	mcb.invoice_period_to,
	mcb.condition_date_by,
	mcb.date_from,
	mcb.date_to,
	mcb.last_x_date,
	mcb.custom_date,
	mcb.custom_day,
	mcb.condition_week_by,
	mcb.custom_week,
	mcb.condition_month_by,
	mcb.custom_month,
	mcb.next_x_month,
	mcb.is_next_bill_presentment_date,
	mcb.notify_day,
	mcb.is_skip_customer_holiday,
	mcb.is_skip_scg_holiday,
	mcb.next_x_week,
	mcb.last_x_business_day

		into #temp
		from invoice_header ih 
		left join master_customer_company mcc 
		on ih.company_code = mcc.company_code 
		and ih.customer_code =  mcc.customer_code
		left join master_customer_bill_presentment_behavior mcb
		on mcc.customer_company_id = mcb.customer_company_id
		and ih.payment_term = mcb.payment_term_code
		and (DATEDIFF(DAY,  mcb.valid_date_from,ih.invoice_date) >= 0)
		and (mcb.valid_date_to is null or DATEDIFF(DAY,  mcb.valid_date_to,ih.invoice_date) >= 0)
		and DAY(ih.invoice_date) between mcb.invoice_period_from and mcb.invoice_period_to
		and mcb.is_bill_presentment =1 and mcb.is_active = 1
		where ih.invoice_no in (select item from dbo.SplitString(@invoice_no,','))
	
	select * from #temp 
	--select * from master_customer_bill_presentment_custom_date
	--where bill_presentment_behavior_id in ( select bill_presentment_behavior_id  from #temp) 
	--and is_active =1

	select h.customer_holiday_id as holiday_id 
			,h.customer_code
			,h.holiday_year
			,h.holiday_month
			,h.holiday_day
			,h.holiday_name
			,'CUSTOMER' as holiday_type
			,h.is_active
			,h.created_date
			,h.created_by
			,h.updated_date
			,h.updated_by
	from dbo.master_customer_holiday h
	where   customer_code in (select distinct customer_code from #temp where is_skip_customer_holiday is null or is_skip_customer_holiday = 0)
			and is_active = 1

	drop table #temp
END
GO
