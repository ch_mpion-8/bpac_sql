SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_receipt_date]
	-- Add the parameters for the stored procedure here
	@invoice_no varchar(20) =null,
	@referece_no varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @is_invoice bit
	
	select  @is_invoice = case when @referece_no like 'AA%' then 0 else 1 end
	if @is_invoice = 0
	begin
		select r.document_date as receipt_date
		from dbo.cheque_control_document_item i
		inner join dbo.cheque_control_document h
			on h.cheque_control_id = i.cheque_control_id
		inner join dbo.receipt r
			on r.reference_document_no = i.reference_document_no
			and r.[year] = i.document_year
		inner join dbo.invoice_header inv
			on inv.assignment_no = r.assignment_no
		inner join dbo.master_customer_company Invmcc
			on Invmcc.company_code = inv.company_code
		and Invmcc.customer_code = inv.customer_code
		where  i.reference_document_no = @referece_no
			and r.assignment_no = @invoice_no
			and Invmcc.customer_company_id = h.customer_company_id
			and isnull(h.cheque_control_status,'') <> 'CANCEL'
	end
	else
	begin
		select h.created_date as receipt_date
		from dbo.cheque_invoice_control_document_item i
		inner join dbo.cheque_invoice_control_document h
			on h.cheque_control_id = i.cheque_control_id
		inner join dbo.invoice_header inv
			on inv.invoice_no = i.invoice_no
		inner join dbo.master_customer_company Invmcc
			on Invmcc.company_code = inv.company_code
		and Invmcc.customer_code = inv.customer_code
		where 
			i.is_active = 1
			and i.invoice_no = @invoice_no
			and h.cheque_control_no = @referece_no
			and Invmcc.customer_company_id = h.customer_company_id
			and isnull(h.cheque_control_status,'') <> 'CANCEL'
	end
END

GO
