SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[add_receipt_to_cheque_document_control]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
	,@receipt_no as varchar(16)
	,@updated_by as nvarchar(50)
	,@year as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists(select 1 from dbo.cheque_control_document_item 
					where cheque_control_id = @cheque_control_id 
							and reference_document_no = @receipt_no)
	begin
		update  dbo.cheque_control_document_item
		set is_active = 1
			,updated_by = @updated_by
			,updated_date = getdate()
		where cheque_control_id = @cheque_control_id 
			and reference_document_no = @receipt_no
	end
	else
	begin
		insert into dbo.cheque_control_document_item
		(
			cheque_control_id
			,reference_document_no
			,is_active
			,created_date
			,created_by
			,updated_date
			,updated_by
			,document_year
		)
		values
		(
			@cheque_control_id
			,@receipt_no
			,1
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
			,@year
		)
	end

	
	--declare @description as nvarchar(100)
	--set @description = 'Add receipt ' + @receipt_no

	exec dbo.insert_cheque_control_history 
			@cheque_control_id = @cheque_control_id
			,@description = @receipt_no
			,@action  = 'Add receipt'
			,@updated_by  = @updated_by
END

GO
