SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_JV_PYC]
	-- Add the parameters for the stored procedure here
	@date varchar(30) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set LANGUAGE us_english
    -- Insert statements for procedure here
	select * from (
		select j.document_date, c.company_name,j.count_trip from dbo.jv_pyc_process  j
		inner join dbo.jv_company c
		on c.company_id = j.company_id
		where @date = DATENAME(MONTH,document_date) +' '+DATENAME(YEAR,document_date)
	) as t
	pivot(
		sum (count_trip) for  company_name in (tprc,smpc,roc,moc)
	) as PV
END

GO
