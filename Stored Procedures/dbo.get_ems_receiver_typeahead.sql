SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_ems_receiver_typeahead]
	-- Add the parameters for the stored procedure here
	@search_word varchar(100) =null,
	@has_customer bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@search_word is null)
		set @search_word = ''

	if @has_customer = 1
	begin
		select cast(contact_id as nvarchar(20)) + '|R' as contact_code
				,contact_name
		from dbo.master_other_contact
		where is_active = 1 
				and contact_name like '%'+@search_word+'%'
		union
		select c.customer_code+ '|C' as contact_code
				,c.customer_code + ' : ' + c.customer_name as contact_name
		from  dbo.master_customer c 
		where (c.customer_code like '%'+@search_word+'%' or c.customer_name like '%'+@search_word+'%')
				and isnull(c.is_active,1) = 1
				and isnull(c.is_ignore,0) = 0

	end
	else
	begin
		select cast(contact_id as nvarchar(10)) as contact_code
				,contact_name
		from dbo.master_other_contact
		where is_active = 1 
				and contact_name like '%'+@search_word+'%'
	end
END
GO
