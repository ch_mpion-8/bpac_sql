SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[validate_receipt]
	-- Add the parameters for the stored procedure here
	@receipt_list as nvarchar(max)
	,@error_receipt as nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 select @error_receipt = stuff(
					(
						select ',' + sub.reference_document_no 
							from dbo.cheque_control_document_item sub with(nolock)
							inner join dbo.cheque_control_document main with(nolock) on sub.cheque_control_id = main.cheque_control_id
							inner join (
										select  
												substring(item,0,charindex('|', item)) as customer_company_id
												,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) 
														- charindex('|', reverse(item))) as receipt_no 
												,right(item,charindex('|', reverse(item)) -1 ) as [year]
												from dbo.SplitString(@receipt_list,',')
										) ref on sub.reference_document_no = ref.receipt_no 
												and main.customer_company_id = ref.customer_company_id
												and sub.document_year = ref.year
								where sub.is_active = 1 and main.cheque_control_status <> 'CANCEL'
							for xml path('')
					) ,1,1,'') 
	 --select @error_invoice
END
GO
