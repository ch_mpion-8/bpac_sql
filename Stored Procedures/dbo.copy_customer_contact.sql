SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[copy_customer_contact]
	-- Add the parameters for the stored procedure here
	@from_customer_company_id as int = null--4 -- '2011271' --null
		,@to_customer_company_id as int = null--18 -- '0230' --null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into dbo.master_customer_contact
			([customer_company_id]
			   ,contact_name
			   ,position
			   ,email
			   ,telephone
			   ,fax
			   ,remark
			   ,is_show_in_ems
			   ,is_show_in_other_document
			   ,[copy_from_id]
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
	select @to_customer_company_id	
			,c.contact_name	
			,c.position
			,c.email
			,c.telephone
			,c.fax
			,c.remark
			,c.is_show_in_ems
			,c.is_show_in_other_document
			,c.contact_id
			,c.[is_active]
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from dbo.master_customer_contact c
	where customer_company_id = @from_customer_company_id
			and [is_active] = 1

END



GO
