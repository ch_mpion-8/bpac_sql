SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_advance_receipt_reference_dropdown] 
	-- Add the parameters for the stored procedure here
	@company_code varchar(4) =null,
	@customer_code varchar(10)=null,
	@currency varchar(10)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	;with
		receipt_select as(
			select 
				r.reference_document_no
				,r.[year]
			from dbo.receipt r
			where r.company_code = @company_code
			and r.customer_code =@customer_code
			and r.currency =  @currency
			and isnull(r.is_active,1) =1 
			and r.receipt_type = 'normal'
		)
		,have_otherdoc as (
			select i.reference_no
			from dbo.other_control_document o
			inner join dbo.other_control_document_item i
			on i.document_id = o.document_id
			inner join receipt_select r
			on r.reference_document_no = i.reference_no
			and r.[year] = i.[year]
			where o.[status] <> 'CANCEL'
			and o.document_type = 1
		)

	select 
	a.reference_document_no,
	a.[year]
	from receipt_select  a
	left join have_otherdoc b
	on a.reference_document_no = b.reference_no 
	where b.reference_no is null

	--select  distinct
	--r.reference_document_no as reference_no from dbo.receipt r
	--left join dbo.other_control_document_item i 
	--on i.reference_no = r.reference_document_no
	--left join dbo.other_control_document o
	--on o.document_id = i.document_id
	--where 
	--r.company_code = @company_code
	--and r.customer_code =@customer_code
	--and r.currency =  @currency
	--and  isnull(o.[status],'CANCEL') = 'CANCEL' 
	--and (select TOP 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ai.document_id= ao.document_id
	--where ai.reference_no = r.reference_document_no
	--and ao.document_type = 1
	--and ao.[status] <>'CANCEL') is null
	--and isnull(r.is_active,1) =1 
	--and r.receipt_type = 'normal'

END

SET ANSI_NULLS ON
GO
