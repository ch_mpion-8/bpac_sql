SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_COLLECTION_USER]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(100),
	@custome_code as varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  distinct top(1) user_display_name from master_customer_company
	where customer_code = @custome_code and company_code = @company_code
END
GO
