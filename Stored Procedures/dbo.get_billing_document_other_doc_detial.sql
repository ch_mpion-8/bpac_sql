SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_billing_document_other_doc_detial]
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select 
	o.document_id,
	o.remark,
	o.created_by,
	o.method,
	c.company_code,
	c.customer_code,
	cus.customer_name,
	h.invoice_no,
	h.invoice_date,
	i.reference_no,
	o.document_no,
	i.action_type,
	o.document_date as other_doc_date,
	case isnull(h.bill_presentment_status,'') when 'CANCEL' then convert(bit,0) else convert(bit,1) end as is_active,
	com.company_name
	from dbo.other_control_document o
	left join dbo.other_control_document_item i
	on o.document_id=i.document_id
	inner join dbo.invoice_header h
	on h.invoice_no = i.reference_no
	inner join dbo.master_customer_company c
	on c.customer_code = h.customer_code
	and c.company_code = h.company_code
	inner join dbo.master_customer cus
	on cus.customer_code = c.customer_code
	inner join dbo.master_company com
	on com.company_code = h.company_code
	where o.document_id = @document_id


END

GO
