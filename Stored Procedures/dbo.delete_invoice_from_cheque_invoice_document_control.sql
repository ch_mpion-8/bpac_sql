SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[delete_invoice_from_cheque_invoice_document_control]
	-- Add the parameters for the stored procedure here
	@cheque_control_detail_id as int
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update ci
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	from dbo.cheque_invoice_control_document_item ci
	where cheque_control_detail_id = @cheque_control_detail_id 

	declare @invoice_no as varchar(16) 
			,@cheque_control_id as int
			,@amount as decimal(15,2)

	select @invoice_no = invoice_no ,@cheque_control_id = cheque_control_id, @amount = amount
	from dbo.cheque_invoice_control_document_item
	where cheque_control_detail_id = @cheque_control_detail_id 

	update invoice_header
	set remaining_amount = remaining_amount + @amount
	where invoice_no = @invoice_no



	--declare @description as nvarchar(100)
	--set @description = 'Remove receipt ' + @receipt_no
	exec dbo.insert_cheque_invoice_control_history
			@cheque_control_id= @cheque_control_id
			,@description = @invoice_no
			,@action  = 'Remove'
			,@updated_by  = @updated_by

	if not exists ( select 1 from dbo.cheque_invoice_control_document_item where cheque_control_id = @cheque_control_id and cheque_no is null)
	begin
		exec dbo.update_cheque_invoice_control_document_status
				@cheque_control_id = @cheque_control_id
				,@status = 'COMPLETE'
				,@updated_by  = @updated_by
	end

END
GO
