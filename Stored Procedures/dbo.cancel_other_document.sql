SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[cancel_other_document]
	-- Add the parameters for the stored procedure here
	@document_id int = null,
	@submited_by varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		    update dbo.other_control_document
			set [status] = 'CANCEL',
			update_date = GETDATE(),
			updated_by = @submited_by
			where document_id = @document_id


			Declare @no varchar(20),@date datetime

			select @no = o.document_no,@date = document_date from dbo.other_control_document o where document_id = @document_id

			insert into dbo.other_control_document_history ([action],[description],[created_by],[created_date],[document_id])
			values(
			'Cancel',
			'Cancel Otherdocument No: '+@no,
			@submited_by,
			getdate(),
			@document_id)

			if exists(select 1 from dbo.other_control_document where document_id = @document_id and convert(date,getdate())<convert(date,document_date) and method = 'PYC')
			begin
				if exists (select 1 from dbo.scg_pyc_process where reference_no_id = @document_id and reference_type =1 )
				begin
					update dbo.scg_pyc_process
					set remark = '-.. . .-.. . - . -..',
					updated_by = @submited_by,
					updated_date = getdate(),
					is_skip = 1
					where reference_no_id = @document_id
					and reference_type = 1
				end
				else
				begin
						insert into scg_pyc_process(
						reference_no_id,
						reference_type,
						remark,
						created_by,
						created_date,
						count_trip,
						is_skip,
						trip_date
						)values(
						@document_id,
						1,
						'-.. . .-.. . - . -..',
						@submited_by,
						getdate(),
						1,
						1,
						@date)
				end

			end

	

END
GO
