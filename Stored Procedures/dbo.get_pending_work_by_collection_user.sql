SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_pending_work_by_collection_user]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Declare @output Table(
	collection_user varchar(50),
	overdue_from_net_due int,
	bill_presentment int,
	[collection] int,
	advance_receipt int,
	advance_receipt_wr int,
	incomplete_customer int,
	bpac_over_net_due int)

	

	--receipt 
	insert into @output (advance_receipt,collection_user)
	select count(*) as num , t.user_name 
	from(
		select
		distinct isnull(c.user_name,'Customer Need User') as user_name ,
		c.customer_company_id
		from dbo.receipt r
		inner join dbo.master_customer_company c
			on r.customer_code = c.customer_code
			and r.company_code = c.company_code
		inner join dbo.master_customer cus
			on cus.customer_code = c.customer_code
		left join (select o.document_id,o.document_no,o.document_date,o.created_by,o.created_date,o.method,o.[status],i.reference_no,o.receiver,o.company_code from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id where o.document_type=1) o
			on o.company_code = r.company_code
			and o.receiver = r.customer_code
			and o.reference_no = r.reference_document_no
		left join dbo.master_customer_bill_presentment_behavior bev
			on bev.customer_company_id = c.customer_company_id
		where
			r.receipt_type = 'normal'
			and (o.[status] is null and r.is_active =1 
				OR o.[status] is not null
				) 
			and isnull(o.[status],'WC') = 'WC'
			--and isnull(bev.is_billing_with_bill,0) <> 1
			group by c.user_name,c.customer_company_id
		union
			select
			distinct isnull(mcc.user_name,'Customer Need User') as user_name ,
			mcc.customer_company_id
			from dbo.other_control_document o
			inner join  dbo.other_control_document_item i
			on i.document_id  = o.document_id
			inner join dbo.receipt r
			on r.reference_document_no = i.reference_no
			inner join dbo.master_customer_company mcc
			on mcc.company_code = r.company_code
			and mcc.customer_code = r.customer_code
			inner join dbo.master_customer cus
			on cus.customer_code = r.customer_code
			inner join dbo.master_company com
			on com.company_code = r.company_code
			left join dbo.master_customer_bill_presentment_behavior bev
				on bev.customer_company_id = mcc.customer_company_id
			where 
			o.document_type = 1
			and r.company_code = o.company_code
			and r.customer_code = o.receiver
			and o.[status] ='CANCEL'
			and isnull(r.is_active,1) = 1
			and r.receipt_type = 'normal'
			and (select top 1 1 from dbo.other_control_document ao inner join dbo.other_control_document_item ai on ao.document_id = ai.document_id
				where ai.reference_no = r.reference_document_no
				and ao.company_code = r.company_code
				and ao.receiver = r.customer_code
				and ao.[status] <>'CANCEL') is null
			--and isnull(bev.is_billing_with_bill,0) <> 1
			) as t
			
		group by t.user_name


	--receipt wr
	insert into @output (advance_receipt_wr,collection_user)
	select count(*) as num , t.user_name 
	from(
	select
		distinct isnull(c.user_name,'Customer Need User') as user_name ,
		c.customer_company_id
		from dbo.receipt r
		inner join dbo.master_customer_company c
			on r.customer_code = c.customer_code
			and r.company_code = c.company_code
		inner join dbo.master_customer cus
			on cus.customer_code = c.customer_code
		left join (select o.document_id,o.document_no,o.document_date,o.created_by,o.created_date,o.method,o.[status],i.reference_no,o.receiver,o.company_code from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id where o.document_type=1) o
			on o.company_code = r.company_code
			and o.receiver = r.customer_code
			and o.reference_no = r.reference_document_no
		where
			r.receipt_type = 'normal'
			and (o.[status] is null and r.is_active =1 
				OR o.[status] is not null
				) 
			and isnull(o.[status],'WC') = 'WR'
			group by c.user_name,c.customer_company_id
			) as t
		group by t.user_name
	--Declare @temp Table(num int,customer_company_id int)

	--bill presentment
	insert into @output (bill_presentment,collection_user)
	select count(*) as num , isnull(t.user_name,'Customer Need User') as user_name from (
		select mc.user_name,mc.customer_company_id from dbo.invoice_header i
		inner join dbo.master_customer_company mc
		on mc.company_code = i.company_code
		and mc.customer_code = i.customer_code
		inner join dbo.master_customer cc
		on mc.customer_code = cc.customer_code
		left join dbo.master_customer_bill_presentment_behavior bh 
		on bh.bill_presentment_behavior_id = i.bill_presentment_behavior_id
		where  isnull(i.bill_presentment_status,'WAITING') in ('WAITING','ERROR')
		and (i.bill_presentment_notify_date <= convert(date,GETDATE()) or i.bill_presentment_notify_date is null)
		and isnull(bh.is_bill_presentment,1) = 1
			and (
					(i.invoice_type = 'RA' and i.payment_term not in ('TS00','NT00'))
					or i.invoice_type in ('RC','RD','DA')
				)
		and isnull(cc.is_ignore,0) <>1
		group by mc.user_name,mc.customer_company_id
	) as t
	group by t.user_name

	--collection
	insert into @output([collection],collection_user)
	select count(*) as num , isnull(t.user_name,'Customer Need User') as user_name from(
		select mc.user_name,mc.customer_company_id from dbo.invoice_header i
		inner join dbo.master_customer_company mc
		on mc.company_code = i.company_code
		and mc.customer_code = i.customer_code
		inner join dbo.master_customer cc
		on mc.customer_code = cc.customer_code
		where  isnull(i.collection_status,'WAITING') in ('WAITING','ERROR')
		and (i.collection_notify_date <= convert(date,GETDATE()) or i.collection_notify_date is null)
			/*and (
					(i.invoice_type = 'RA' and i.payment_term not in ('TS00','NT00'))
					or i.invoice_type in ('RC','RD','DA')
				)*/
		and isnull(cc.is_ignore,0) <>1
		group by mc.user_name,mc.customer_company_id
	) as t
	group by t.user_name

	--overdue_from_net_due
	insert into @output(overdue_from_net_due,collection_user)
	select count(*) as num , isnull(t.user_name,'Customer Need User') as user_name from(
		select mc.user_name,mc.customer_company_id from dbo.invoice_header i
		inner join dbo.master_customer_company mc
		on mc.company_code = i.company_code
		and mc.customer_code = i.customer_code
		inner join dbo.master_customer cc
		on mc.customer_code = cc.customer_code
		where  replace(isnull(i.collection_status,'WAITING'),'MANUAL','WAITING') in ('WAITING','ERROR')
		and (i.collection_notify_date <= convert(date,GETDATE()) or i.collection_notify_date is null)
		and DATEDIFF ( dd , i.net_due_date , getdate() )  > 0
		and isnull(cc.is_ignore,0) = 0
		group by mc.user_name,mc.customer_company_id
	) as t
	group by t.user_name


	--incomplete_customer
	insert into @output(incomplete_customer,collection_user)
	select count(*) as num , isnull(t.user_name,'Customer Need User') as user_name from 
	(select  cc.user_name,cc.customer_company_id
	 from dbo.invoice_header i
	 inner join dbo.master_customer_company cc on i.customer_code = cc.customer_code and i.company_code = cc.company_code
	 inner join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	 inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1 
		and isnull(cus.is_ignore,0) =0
	where (((i.bill_presentment_calculation_log = 'No Matched Billing Behavior' 
					and isnull(i.bill_presentment_status,'') <> 'COMPLETE' 
				/*
					comment by Nan 
					and (isnull(i.bill_presentment_status,'') <> 'COMPLETE' or isnull(i.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%')
				*/
				)
			or ( i.collection_calculation_log = 'No Matched Collection Behavior' and isnull(i.collection_status,'') <> 'COMPLETE' ))
			)
			and isnull(i.bill_presentment_status,'') <> 'CANCEL'
	group by cc.user_name,cc.customer_company_id) as t
	group by t.user_name


	--select o.collection_user,
	--		sum(o.advance_receipt) as advance_receipt,
	--		sum(o.advance_receipt_wr) as advance_receipt_wr,
	--		sum(o.bill_presentment) as bill_presentment,
	--		sum(o.[collection]) as [collection],
	--		sum(o.overdue_from_net_due) as overdue_from_net_due,
	--		sum(o.incomplete_customer) as incomplete_customer
	--from @output o
	--group by o.collection_user
	--order by case o.collection_user when 'Customer Need User' then 1 else 0 end, o.collection_user

	
	insert into @output(bpac_over_net_due,collection_user)
	select count(*) as num , isnull(t.user_name,'Customer Need User') as user_name from(
		select mc.user_name,mc.customer_company_id from dbo.invoice_header i
		inner join dbo.master_customer_company mc
		on mc.company_code = i.company_code
		and mc.customer_code = i.customer_code
		inner join dbo.master_customer cc
		on mc.customer_code = cc.customer_code
		where  replace(isnull(i.collection_status,'WAITING'),'MANUAL','WAITING') in ('WAITING','ERROR')
		and (i.collection_notify_date <= convert(date,GETDATE()) or i.collection_notify_date is null)
		and isnull(i.collection_date,'1900-01-01') > i.net_due_date
		and isnull(cc.is_ignore,0) = 0
		group by mc.user_name,mc.customer_company_id
	) as t
	group by t.user_name

	
	;with a as (select o.collection_user,
			sum(o.advance_receipt) as advance_receipt,
			sum(o.advance_receipt_wr) as advance_receipt_wr,
			sum(o.bill_presentment) as bill_presentment,
			sum(o.[collection]) as [collection],
			sum(o.overdue_from_net_due) as overdue_from_net_due,
			sum(o.incomplete_customer) as incomplete_customer,
			sum(o.bpac_over_net_due) as bpac_over_net_due
	from @output o
	group by o.collection_user),b as (
	select *
	from a
	union
	select 'Customer Need User' as collection_user,
			0 as advance_receipt,
			0 as advance_receipt_wr,
			0 as bill_presentment,
			0 as [collection],
			0 as overdue_from_net_due,
			0 as incomplete_customer,
			0 as bpac_over_net_due
	from a
	where not exists (select 1 from a where collection_user = 'Customer Need User'))
	select *
	from b
	order by case b.collection_user when 'Customer Need User' then 1 else 0 end, b.collection_user
END
GO
