SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_bill_presentment]
 -- Add the parameters for the stored procedure here
 @customer_code as varchar(10) = null -- '2011271' --
	,@customer_company_id as int = null
AS
BEGIN
	 -- SET NOCOUNT ON added to prevent extra result sets from
	 -- interfering with SELECT statements.
	 SET NOCOUNT ON;
 
	 select distinct cb.[group_id] as bill_presentment_behavior_id
			, cc.customer_company_id
			, cc.[company_code]
			, com.company_name
			, cc.[customer_code]
			, cb.[valid_date_from] as valid_from
			, cb.[valid_date_to] as valid_to
			--, pt.[payment_term_code]+' : '+ pt.[payment_term_name] as payment_term
			--, pt.[payment_term_code]
			--, pt.[payment_term_name]
			,(
				select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_bill_presentment_behavior] b
						where b.customer_company_id = cb.customer_company_id and b.group_id = cb.group_id
									and b.is_active = cb.is_active
								for xml path('')) ,1,1,'') as txt
				) as payment_term
			
			,(
				select stuff(( select ',' + [payment_term_code]  from dbo.[master_customer_bill_presentment_behavior] b
						where b.customer_company_id = cb.customer_company_id and b.group_id = cb.group_id
									and b.is_active = cb.is_active
								for xml path('')) ,1,1,'') as txt
				) as [payment_term_code]
			,''as [payment_term_name]

			,cb.invoice_period_from
			,cb.invoice_period_to
			, cb.[time] 
			,cb.is_bill_presentment 
			, cb.[condition_date_by] 
			, case
			   when cb.is_bill_presentment = 0 then 'ไม่ต้องวางบิล'
			   when cb.[condition_date_by]  = 'CUSTOMDATE' then 
					case when cb.custom_date like '%-1%' 
						then case when cb.last_x_date is not null and cb.last_x_date = 1 then replace(cb.custom_date,'-1','Last day')
								 when cb.last_x_date is not null and cb.last_x_date = 2 then replace(cb.custom_date,'-1','Last 2 days')
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then replace(cb.custom_date,'-1','Last business day')
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then replace(cb.custom_date,'-1','Last 2 business days')
								 else '' end
					else 'วันที่ ' + cb.custom_date end
			   when  cb.[condition_date_by]  = 'PERIOD' then 'Period ' 
						+ case when cb.date_from = -1 
								then case when cb.last_x_date is not null and cb.last_x_date = 1 then 'Last day'
								 when cb.last_x_date is not null and cb.last_x_date = 2 then 'Last 2 days'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then 'Last business day'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then 'Last 2 business days'
								 else '' end
							else cast(cb.date_from as nvarchar(10)) 
							end
						+ ' - ' 
						+ case when cb.date_to =-1 
						then case when cb.last_x_date is not null and cb.last_x_date = 1 then 'Last day'
								 when cb.last_x_date is not null and cb.last_x_date = 2 then 'Last 2 days'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 1 then 'Last business day'
								 when cb.last_x_business_day is not null and cb.last_x_business_day = 2 then 'Last 2 business days'
								 else '' end
						else cast(cb.date_to as nvarchar(10)) 
						end

			   when  cb.[condition_date_by]  = 'CUSTOMDAY' then  (
										select stuff(
												(
													select ',' + dayName 
														from (
														select dayname = configuration_value
														from dbo.SplitString(cb.[custom_day],',') s
														inner join  dbo.configuration  c 
															on s.item = c.configuration_code and c.configuration_key = 'Day'
													) a
														for xml path('')
												) ,1,1,'') as txt
										)
			   when  cb.[condition_date_by]  = 'EVERYDATE' then  'Every day'
			   when  cb.[condition_date_by]  = 'LASTDAY' then 'Last ' + cast(cb.last_x_date as nvarchar(10)) + ' day(s)'
			   when  cb.[condition_date_by]  = 'LASTBUSINESSDAY' then 'Last ' + cast(cb.last_x_business_day as nvarchar(10)) + ' business day(s)'
			   else 'Error'
			  end as [date]
			, cb.[is_active]
			, isnull(conf.[configuration_value],cb.billing_method ) as billing_method
			, cb.address_id
			, ca.[title]
			, case when ca.[address] is null then '' else  ca.[address] +' ' end
				+ case when ca.sub_district is null then '' else  ca.sub_district +' ' end
				+ case when ca.district is null then '' else  ca.district +' ' end
				+ case when ca.province is null then '' else  ca.province +' ' end
				+ case when ca.post_code is null then '' else  ca.post_code end as [address]
			, cb.[description]
			, case when cb.condition_week_by = 'EVERYWEEK' then 'Every week' else 
			 (
										select stuff(
												(
													select ',' + mon 
														from (
														select mon = 'week ' + item
														from dbo.SplitString(cb.custom_week ,',') s
													) a
														for xml path('')
												) ,1,1,'') as txt
										)
					
					end as [week]
			, case when cb.condition_week_by = 'EVERYMONTH' then 'Every month' else cb.custom_month end as [month]
			,case when cb.is_signed_document is null or cb.is_signed_document = 0 then 'No' else 'YES' end as bill_is_signed_document
			,case when cb.is_billing_wiith_truck is null or cb.is_billing_wiith_truck = 0 then 'No' else 'YES' end as bill_is_billing_wiith_truck
			,cb.notify_day as bill_notify_day
			,cb.document_type as bill_document_type
			,subdoc.doc as bill_document
			,cb.remark as bill_remark
	 from [dbo].[master_customer_bill_presentment_behavior]  cb
	  inner join [dbo].[master_customer_company] cc on cb.[customer_company_id] = cc.[customer_company_id]
	  inner join dbo.master_company com on cc.company_code = com.company_code
	  --inner join [dbo].[master_payment_term] pt on cb.[payment_term_code] = pt.[payment_term_code]
	  left join [dbo].[master_customer_address] ca on cb.[address_id] = ca.[address_id]
	  left join (
					select  main.bill_presentment_behavior_id,	stuff(
					(
						select ', ' + configuration_value  + case when sub.custom_document is null then '' else ' ' + sub.custom_document end
							from master_customer_bill_presentment_document sub
							inner join dbo.configuration con on sub.document_code = con.configuration_code and con.configuration_key = 'BillingDocument'
							where sub.bill_presentment_behavior_id = main.bill_presentment_behavior_id and sub.is_active =1 
							for xml path('')
					) ,1,1,'') as doc
					from dbo.master_customer_bill_presentment_document main
				) subdoc on cb.bill_presentment_behavior_id = subdoc.bill_presentment_behavior_id
		left join dbo.configuration conf on cb.billing_method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where (@customer_code is null or customer_code = @customer_code )--and ad.[status] = 'ACTIVE'
		and (@customer_company_id is null or cb.customer_company_id = @customer_company_id)
		and cb.is_active = 1 
		--and (cb.is_active = 1 
		--			or ( cb.is_active = 0 
		--					and not exists ( select 1 from  dbo.[master_customer_bill_presentment_behavior] 
		--										where group_id = cb.group_id and is_active = 1)))
END
GO
