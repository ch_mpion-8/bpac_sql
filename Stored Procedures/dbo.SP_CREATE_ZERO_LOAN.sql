SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_CREATE_ZERO_LOAN]
	-- Add the parameters for the stored procedure here
	@date datetime null
	,@cheque_list [dbo].[ZeroloanList_New] READONLY
	,@submit_by nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @zero_loan_detail_id as int
	declare @zero_loan_id as int
	declare @reference_document_no as nvarchar(50) 
	declare @cheque_control_id as int
	declare @zero_loan_status as int
	declare @created_by as datetime
	declare @updated_by as datetime
	declare @company_code as nvarchar(4)
	declare @amount as decimal

	DECLARE zero_loan_cursor CURSOR FOR 
	SELECT zero_loan_detail_id,
	zero_loan_id ,
	reference_document_no,
	cheque_control_id,
	zero_loan_status,
	created_by,
	updated_by,
	company_code,
	amount
	FROM @cheque_list

		-- Open Cursor
	OPEN zero_loan_cursor 
	FETCH NEXT FROM zero_loan_cursor 
	INTO @zero_loan_detail_id, @zero_loan_id,@reference_document_no,@cheque_control_id,
	@zero_loan_status,@created_by,@updated_by
	,@company_code,@amount

	-- Loop From Cursor
	WHILE (@@FETCH_STATUS = 0) 
	BEGIN 

	declare @lastestId int;	
	if exists(select 1 from zero_loan where  DATEDIFF(DAY,zero_loan_date, @date) = 0 and company_code = @company_code ) 
	begin 
		select @lastestId=[zero_loan_id] from zero_loan where  DATEDIFF(DAY,zero_loan_date, @date) = 0 and company_code = @company_code
	end
	else
	begin
		insert into zero_loan(zero_loan_date,company_code,submit_by,created_by,created_date,summary)values(@date,@company_code,null,@submit_by,GETDATE(),0);
		SELECT @lastestId = SCOPE_IDENTITY() 
	end


	insert into zero_loan_item([zero_loan_id]
      ,[reference_document_no]
      ,[cheque_control_id]
      ,[zero_loan_status]
	  ,create_by
	  ,create_date)
	  values (@lastestId,@reference_document_no,@cheque_control_id,@zero_loan_status,@created_by,getdate())
	 

	UPDATE zero_loan set summary = ISNULL(summary, 0) + ISNULL(@amount, 0) where zero_loan_id = @lastestId


	FETCH NEXT FROM zero_loan_cursor -- Fetch next cursor
	INTO @zero_loan_detail_id, @zero_loan_id,@reference_document_no,@cheque_control_id,
	@zero_loan_status,@created_by,@updated_by
	,@company_code,@amount

	set @lastestId = null

	END
	
	CLOSE zero_loan_cursor; 
	DEALLOCATE zero_loan_cursor; 

END
GO
