SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[SP_INSERT_CUSTOMER_TO_MASTER_CUSTOMER_COMPANY]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into master_customer_company (customer_code,company_code,is_active,created_by,created_date)
	SELECT  ih.customer_code,ih.company_code,1,'system',GETDATE() FROM invoice_header ih
	left join master_customer_company mcc 
	on ih.company_code = mcc.company_code 
	and ih.customer_code =  mcc.customer_code
	where mcc.customer_company_id is null
	GROUP BY ih.company_code,ih.customer_code
	--order by ih.company_code,ih.customer_code
	union 
	select rp.customer_code,rp.company_code,1,'system',GETDATE() 
	FROM receipt rp
	left join master_customer_company mcc 
	on rp.company_code = mcc.company_code 
	and rp.customer_code =  mcc.customer_code
	where mcc.customer_company_id is null
	GROUP BY rp.company_code,rp.customer_code
	order by company_code,customer_code
	/****Aof add union for customer open item****/

	DECLARE @customer_company_id as int =null
		,@address as nvarchar(200) = null

	DECLARE customer_cursor CURSOR FAST_FORWARD FOR
		SELECT  customer_company_id, 
					inserted.street+' ' +inserted.district +' ' +inserted.city +' ' +inserted.post_code  as 'address'
		from master_customer_company a, inserted
		where a.customer_code = inserted.customer_code
	OPEN customer_cursor;

	FETCH NEXT from customer_cursor 
		INTO @customer_company_id, @address

	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (EXISTS(select 1 from master_customer_address b 
						where b.customer_company_id = @customer_company_id
						and b.is_allowed_edit = 0))
				BEGIN
					update b
					set b.address = @address
					from master_customer_address b
						where b.customer_company_id = @customer_company_id
						and b.is_allowed_edit = 0
				END
			ELSE
				BEGIN
					insert into master_customer_address(
						customer_company_id, 
						title,
						address,
						is_allowed_edit,
						is_active,
						created_by,
						created_date)
					values(
						@customer_company_id,
						'ภพ.20',
						@address,
						0,
						1,
						'SYSTEM',
						getdate()
					)
				END

				 if not exists(select 1 from [dbo].master_customer_address 
						where customer_company_id = @customer_company_id and isnull(is_ems,0) = 1)
					begin
						update dbo.master_customer_address
						set is_ems = 1
							,updated_date = getdate()
							,updated_by = 'SYSTEM'
						from dbo.master_customer_address
						where customer_company_id = @customer_company_id
						and title = 'ภพ.20'
					end 

			FETCH NEXT from customer_cursor 
				INTO @customer_company_id, @address
		END

	CLOSE customer_cursor;
	DEALLOCATE customer_cursor;

	
END
GO
