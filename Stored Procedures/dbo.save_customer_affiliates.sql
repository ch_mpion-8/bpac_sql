SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_customer_affiliates]
	-- Add the parameters for the stored procedure here
	@source_customer_code as varchar(10) = null --'2019181'
		,@destination_customer_code as varchar(10) = null --'2010550'
		,@updated_by as nvarchar(20) = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @source_aff_id as int
			,@destination_aff_id as int

	select  @source_aff_id = affiliate_id
	from dbo.master_customer_affiliate_list
	where customer_code = @source_customer_code
			and [is_active] = 1

	select  @destination_aff_id = affiliate_id
	from dbo.master_customer_affiliate_list 
	where customer_code = @destination_customer_code
			and [is_active] = 1

	--select @source_aff_id, @destination_aff_id

	if(@source_aff_id is not null and @destination_aff_id is not null and @source_aff_id <> @destination_aff_id)
	begin 
		set @error_message = @destination_customer_code + ' อยู่บริษัทเครืออื่นแล้ว'
	end
	else
	begin
		create table #tmp
		  ( 
			customer varchar(10) collate database_default
		  ) 

		insert into #tmp( customer)
		values (@source_customer_code)
			,(@destination_customer_code)

		declare @aff_id as int 
		select @aff_id = case when @source_aff_id is not null then @source_aff_id 
							  else @destination_aff_id end
		
		
		if(@aff_id is not null)
		begin
			update [dbo].master_customer_affiliate_list 
				set [is_active] = 1
					,updated_by = @updated_by
					,updated_date = getdate()
				--select *
				from  #tmp tmp 
				left join [dbo].master_customer_affiliate_list aff on  tmp.customer = aff.customer_code
				where affiliate_id = @aff_id
					and aff.customer_code is not null
		end
		else
		begin

			insert into [dbo].master_customer_affiliate_group
						(
							[is_active]
							,[created_date]
							,[created_by]
							,[updated_date]
							,[updated_by]
						)			
			select 1
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by

			select @aff_id = SCOPE_IDENTITY()
		end			
		

			insert into [dbo].master_customer_affiliate_list 
						(
							affiliate_id
							,customer_code
							,[is_active]
							,[created_date]
							,[created_by]
							,[updated_date]
							,[updated_by]
						)			
			select 
				@aff_id 
				,tmp.customer
				,1
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
			from  #tmp tmp 
			left join [dbo].master_customer_affiliate_list aff on  tmp.customer = aff.customer_code and affiliate_id = @aff_id
			where aff.customer_code is null

        
		drop table #tmp
	end
END




GO
