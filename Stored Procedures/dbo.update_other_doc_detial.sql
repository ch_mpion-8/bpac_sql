SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_other_doc_detial]
	@document_id int=null,
	@other_doc_date date = null,
	@create_by varchar(50)=null,
	@create_date datetime = null,
	@method varchar(20)=null,
	@remark nvarchar(2000)=null,
	@remove_item nvarchar(1000)=null,
	@add_item nvarchar(1000)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set @create_date = getdate()

	Declare @_method varchar(20) ,@_remark nvarchar(2000),@_doc_id int = null,@_date date = null
	select @_date = document_date,@_method=method,@_remark = remark ,@_doc_id=document_id from dbo.other_control_document where document_id=@document_id
    -- Insert statements for procedure here
	if @_doc_id is not null
	begin
		update dbo.other_control_document 
		set updated_by = @create_by,
		update_date = @create_date
		where document_id=@document_id

		if @other_doc_date != @_date
			exec [dbo].[update_other_document_date] @document_id,@other_doc_date,@create_by
		if @method !=@_method
			exec [dbo].[update_other_document_method] @document_id,@create_by,@create_date,@method
		if @remark <>@_remark OR (@remark is null AND @_remark is not null) OR (@remark is not null AND @_remark is null)
			exec [dbo].[update_other_document_remark] @remark,@document_id,@create_by,@create_date
		if @remove_item is not null
		begin
			Declare @ref_remove varchar(50)
			Declare cur Cursor For
			Select * from [dbo].SplitString (@remove_item,',')

			Open cur
			Fetch Next From cur 
			into @ref_remove
			while (@@FETCH_STATUS=0)
			begin
				exec [dbo].[update_other_doc_item_remove] @document_id,@ref_remove,@create_by,@create_date
				Fetch Next From cur 
				into @ref_remove
			end
			close cur
			Deallocate cur

			
		end


		if @add_item is not null
		begin
			Declare @ref_add varchar(50)
			Declare cur Cursor For
			Select * from [dbo].SplitString (@add_item,',')

			Open cur
			Fetch Next From cur 
			into @ref_add
			while (@@FETCH_STATUS=0)
			begin
				exec [dbo].[update_other_doc_item_add] @document_id,@ref_add,@create_by,@create_date,1
				Fetch Next From cur 
				into @ref_add
			end
			close cur
			Deallocate cur			
		end

	end
END
GO
