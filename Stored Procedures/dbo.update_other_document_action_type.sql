SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[update_other_document_action_type]
	-- Add the parameters for the stored procedure here
	@document_id int=null,
	@create_by varchar(50)=null,
	@create_date datetime = null,
	@action_type int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update dbo.other_control_document_item
		set action_type = @action_type
		, updated_by  =@create_by
		, updated_date = GETDATE()
		where document_id = @document_id

	update dbo.other_control_document 
		set update_date = GETDATE()
		, updated_by = @create_by
		where document_id = @document_id

		insert into dbo.other_control_document_history(document_id,
		[action],
		created_by,
		created_date,
		[description])
		values(
		@document_id,
		'Update',
		@create_by,
		@create_date,
		'Action type : '+case when @action_type = 0 then 'S/R' when @action_type =1 then 'Send' else 'Receive' end)
END
GO
