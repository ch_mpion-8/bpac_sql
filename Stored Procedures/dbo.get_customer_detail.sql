SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_customer_detail]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) = null --'2007211'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  select	cc.customer_company_id
			,cc.company_code
			,com.company_name
			,cc.customer_code
			,cus.customer_name
			,cc.[user_name]
			,cc.[user_display_name]
			,cus.remark
			,cus.application_tag_id
			,tag.application_tag_name
			,cus.is_ignore

			--,*
	from dbo.master_customer_company cc
	inner join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1
	left join dbo.master_application_tag tag on cus.application_tag_id = tag.application_tag_id
	where cc.customer_code = @customer_code
			and cc.[is_active] = 1

	declare @aff_id as int
	select  @aff_id = affiliate_id
	from dbo.master_customer_affiliate_list
	where customer_code = @customer_code
			and [is_active] = 1

	select aff.affiliate_id
		,aff.affiliate_detail_id
		,aff.customer_code
		,cus.customer_name
		,aff.[is_active]
		,cast(0 as bit) as is_has_com
		,cus2.customer_code as search_customer_code
		,cus2.customer_name as search_customer_name 
	from dbo.master_customer_affiliate_list aff 
	inner join dbo.master_customer cus on aff.customer_code = cus.customer_code
	inner join dbo.master_customer cus2 on cus2.customer_code= @customer_code
	where aff.affiliate_id = @aff_id
			and aff.[is_active] = 1
			and aff.customer_code <> @customer_code


END
GO
