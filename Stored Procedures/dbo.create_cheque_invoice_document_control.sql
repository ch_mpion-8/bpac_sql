SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_cheque_invoice_document_control]
	 @document_control_list  as [dbo].[document_control_table_type] readonly
	 ,@updated_by as nvarchar(50) = null
	 ,@is_manual as bit = null
	 ,@cheque_no as varchar(50) = null 
	,@cheque_amount as decimal(15,2) = null 
	,@cheque_date as date = null 
	,@cheque_currency as varchar(5) = null 
	,@cheque_bank as nvarchar(2000) = null 
	,@cheque_remark as nvarchar(2000) = null 
	,@document_control_id as nvarchar(2000) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
   declare @customer_company_id [int]  = NULL,
		@collection_date [date] =  NULL,
		@method [nvarchar](20) = NULL,
		@payment_term [varchar](4) = NULL,
		@currency [varchar](5) = NULL,
		@remark [nvarchar](2000) = NULL,
		@reference_no_list [nvarchar](max) = NULL,
		@new_status as nvarchar(20) = 'WAITINGRECEIVED'

		declare table_cursor cursor for 

	select [customer_company_id]
			,[date]
			,[method]
			,[payment_term]
			,[currency]
			,[remark]
			,[reference_no_list]
	from @document_control_list

	open table_cursor;
	fetch next from table_cursor into @customer_company_id,@collection_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	while @@fetch_status = 0
	   begin
      
		  declare @cheque_id as int = null
				, @cheque_detail_id as int = null

		
			declare @cheque_id_new table(new_id int)
			
				insert into [dbo].cheque_invoice_control_document
					([customer_company_id]
					,[method]
					,[remark]
					,collection_date
					,job_date
					,cheque_control_status
					,[created_date]
					,[created_by]
					,[updated_date]
					,[updated_by]
					,is_manual
					)
				output inserted.cheque_control_id into @cheque_id_new
				select @customer_company_id
						,@method
						,@remark
						,@collection_date
						,getdate()
						,@new_status
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
						,@is_manual


				--select @cheque_id = ident_current('cheque_control_document')				
				select @cheque_id = new_id
				from @cheque_id_new

				set @document_control_id = case when @document_control_id is null or @document_control_id = '' 
							then cast(@cheque_id as nvarchar(200)) else @document_control_id + ',' + cast(@cheque_id as nvarchar(200)) end
				
				print @document_control_id
			
			select *
			into #tmp
			from (
					select -- item as ref_no
					 rtrim(ltrim(substring(item,0,charindex('|', item)))) as ref_no
					, case when right(item,charindex('|', reverse(item)) -1 ) is not null 
								and right(item,charindex('|', reverse(item)) -1 ) <> ''
							then CAST(right(item,charindex('|', reverse(item)) -1 ) AS DECIMAL(15, 2))
							else 0 end as amount
					--,right(item,charindex('|', reverse(item)) -1 ) as [year]
					from dbo.SplitString(@reference_no_list,',') ss

				--select item as ref_no
				--from dbo.SplitString(@reference_no_list,',') ss
				union
				select i.invoice_no as ref_no -- , document_year as [year]
						, i.amount as amount
				from dbo.cheque_invoice_temp c
				inner join dbo.cheque_invoice_item_temp i on c.temp_id = i.temp_id and i.is_active = 1
				where c.customer_company_id = @customer_company_id 
					--and method = @method 
					and collection_date = @collection_date 
					and currency = @currency
			) a

			--select * from #tmp


			declare @detail_tmp table(new_id int)
			insert into [dbo].cheque_invoice_control_document_item
				(cheque_control_id
					,invoice_no
					,amount
					,is_active
					,[created_date]
					,[created_by]
					,[updated_date]
					,[updated_by]
				--	,[document_year]
					)
			output inserted.cheque_control_detail_id into @detail_tmp
			select @cheque_id
					,rtrim(ltrim(ref_no)) as ref_no
					,amount
					,1
					,getdate()
					,@updated_by
					,getdate()
					,@updated_by
				--	,[year]
			from #tmp

			
			declare @cheque_id_str as nvarchar(200) = null
			select @cheque_id_str = stuff(
						(
							select ',' + cast(new_ids as nvarchar(200))
								from (
								select new_ids = new_id
								from @detail_tmp
							) a
								for xml path('')
						) ,1,1,'') 

			 

			update dbo.cheque_invoice_item_temp
			set is_active = 0
				,updated_by = @updated_by
				,updated_date = getdate()	
			where invoice_no in (select rtrim(ltrim(ref_no)) as ref_no from #tmp)

		exec [dbo].insert_cheque_invoice_control_history
			@cheque_control_id = @cheque_id
			,@description = 'Create Cheque Document Control.'
			,@action  = 'Create'
			,@updated_by  = @updated_by

			
			if @is_manual = 1
			begin
					exec [dbo].[save_invoice_receive_cheque]
						 @cheque_control_detail_id  = @cheque_id_str --'AA0001030160,AI0001041407,AA0001030094,AA0001040020'
						,@cheque_no  = @cheque_no 
						,@cheque_amount = @cheque_amount 
						,@cheque_date  = @cheque_date 
						,@cheque_currency  = @cheque_currency 
						,@cheque_bank  = @cheque_bank 
						,@remark  = @cheque_remark 
						,@updated_by  = @updated_by

			end


			-- update invoice remaining amount
			update ih
			set remaining_amount = remaining_amount - t.amount
			from invoice_header ih
			inner join #tmp t on ih.invoice_no = rtrim(ltrim(t.ref_no))
			


			drop table #tmp
	   
		  fetch next from table_cursor into   @customer_company_id,@collection_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	   end;

	close table_cursor;
	deallocate table_cursor;

END
GO
