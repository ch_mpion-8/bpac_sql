SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_other_document_method]
	-- Add the parameters for the stored procedure here
	@document_id int=null,
	@create_by varchar(50)=null,
	@create_date datetime = null,
	@method varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


			update dbo.other_control_document
			set method = @method
			where document_id =@document_id

			insert into dbo.other_control_document_history(document_id,
			[action],
			created_by,
			created_date,
			[description])
			values(
			@document_id,
			'Update',
			@create_by,
			@create_date,
			'Update Method to '+@method)


		
END
GO
