SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[save_customer_bill_presentment_behavior]
	-- Add the parameters for the stored procedure here
	@bill_presentment_behavior_id as int = null
		,@customer_company_id as int = null
		,@valid_date_from as date = null
		,@payment_term_code as nvarchar(2000)  = null
		,@is_bill_presentment as bit = null
		,@invoice_period_from as int = null
		,@invoice_period_to as int = null
		,@address_id as int = null
		,@time as nvarchar(100) = null
		,@description as nvarchar(2000) = null
		,@remark as nvarchar(2000) = null
		,@condition_date_by as nvarchar(20) = null
		,@date_from as int = null
		,@date_to as int = null
		,@last_x_date as int = null
		,@last_x_business_day as int = null
		,@custom_date as nvarchar(100) = null
		,@custom_day as nvarchar(10) = null
		,@condition_week_by as nvarchar(20) = null
		,@custom_week as nvarchar(100) = null
		,@next_x_week as int = null
		,@condition_month_by as nvarchar(20) = null
		,@custom_month as nvarchar(100) = null
		,@next_x_month as int = null
		,@is_next_bill_presentment_date as bit = null
		,@billing_method as nvarchar(20) = null
		,@is_signed_document as bit = null
		,@is_billing_wiith_truck as bit = null
		,@notify_day as smallint = null
		,@is_skip_customer_holiday as bit = null
		,@is_skip_scg_holiday as bit = null
		,@is_billing_with_bill as bit = null
		,@document_type as nvarchar(20) = null
		,@document_list as [dbo].[document_table_type] readonly
		,@custom_date_list as [dbo].[date_table_type] readonly
		,@status as bit = null
		,@updated_by as nvarchar(50) = null
		,@custom_date_next_x_month as int = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if exists( select 1 from [dbo].master_customer_bill_presentment_behavior 
				where payment_term_code in (select item from dbo.SplitString(@payment_term_code,','))
				and is_active = 1
					and customer_company_id = @customer_company_id
					and valid_date_from = @valid_date_from
					and (
							@invoice_period_from between invoice_period_from and invoice_period_to
							or @invoice_period_to  between invoice_period_from and invoice_period_to
							or invoice_period_from between @invoice_period_from and @invoice_period_to
							or invoice_period_to between @invoice_period_from and @invoice_period_to
						) 
					and (@bill_presentment_behavior_id <> group_id)
					--and invoice_period_from <> @invoice_period_from
					--and invoice_period_to <> @invoice_period_to
			 )
	begin
		set @error_message = 'Duplicate payment term or invalid invoice period !'
		return;
	end

	-- Check Create Group
	if @bill_presentment_behavior_id is null or @bill_presentment_behavior_id = 0
	begin
		-- Get Last Group ID  
		declare @from_group_id as int
		select top 1 @from_group_id = group_id
		from [dbo].[master_customer_bill_presentment_behavior] 
		where payment_term_code in (select item from dbo.SplitString(@payment_term_code,','))
		and customer_company_id = @customer_company_id
		and is_active = 1
		order by created_date desc

		if exists(
			select 1
			from [dbo].[master_customer_bill_presentment_behavior] a 
				full join (select item from dbo.SplitString(@payment_term_code,',')) b on a.payment_term_code = b.item
			where group_id = @from_group_id
			and a.is_active = 1
			and (a.payment_term_code is null or b.item is null)
			)
		begin
			set @error_message = 'กรุณาเลือก Payment Term ที่ยังไม่เคย Maintain !'
			return;
		end
	end

	-- Get Group ID
	declare @group_id as int
	
	if exists( select 1 from [dbo].master_customer_bill_presentment_behavior 
				where group_id = @bill_presentment_behavior_id
			 )
	begin
		set @group_id = @bill_presentment_behavior_id
	end
	else
	begin
		select @group_id = isnull(max(group_id),0)+1
		from [dbo].master_customer_bill_presentment_behavior 
	end

	
	update  dbo.master_customer_bill_presentment_behavior
	set is_active = 0
		, updated_date = getdate()
		, updated_by = @updated_by	
	from dbo.master_customer_bill_presentment_behavior
	where group_id = @group_id 
		and payment_term_code not in (select item 
							from dbo.SplitString(@payment_term_code,','))

	declare @term nvarchar(4)
	declare table_cursor cursor for 

	select item 
	from dbo.SplitString(@payment_term_code,',')

	open table_cursor;
	fetch next from table_cursor into @term
	while @@fetch_status = 0
	   begin

	declare @bill_id as int 

    if exists (
				select 1 
				from [dbo].master_customer_bill_presentment_behavior 
				where payment_term_code = @term
				and group_id = @bill_presentment_behavior_id
			)
		 begin
			-- Get bill_presentment_behavior_id
			select @bill_id = bill_presentment_behavior_id
			from [dbo].master_customer_bill_presentment_behavior 
			where payment_term_code = @term
			and group_id = @bill_presentment_behavior_id

			-- Update 
			update dbo.master_customer_bill_presentment_behavior
			set valid_date_from = @valid_date_from
				,payment_term_code = @term
				,is_bill_presentment = @is_bill_presentment
				,invoice_period_from = @invoice_period_from
				,invoice_period_to = @invoice_period_to
				,address_id = @address_id
				,time = @time
				,description = @description
				,remark = @remark
				,condition_date_by = @condition_date_by
				,date_from = @date_from
				,date_to = @date_to
				,last_x_date = @last_x_date
				,last_x_business_day = @last_x_business_day
				,custom_date = @custom_date
				,custom_day = @custom_day
				,condition_week_by = @condition_week_by
				,custom_week = @custom_week
				,next_x_week = @next_x_week
				,condition_month_by = @condition_month_by
				,custom_month = @custom_month
				,next_x_month = @next_x_month
				,is_next_bill_presentment_date = @is_next_bill_presentment_date
				,billing_method = @billing_method
				,is_signed_document = @is_signed_document
				,is_billing_wiith_truck = @is_billing_wiith_truck
				,notify_day = @notify_day
				,is_skip_customer_holiday = @is_skip_customer_holiday
				,is_skip_scg_holiday = @is_skip_scg_holiday
				,is_billing_with_bill = @is_billing_with_bill
				,document_type = @document_type
				,[is_active] = @status
				,updated_date = getdate()
				,updated_by = @updated_by		
				,custom_date_next_x_month = @custom_date_next_x_month	
			from dbo.master_customer_bill_presentment_behavior
			where bill_presentment_behavior_id = @bill_id

		 end
	else
		 begin
			declare @tmp table(new_id int)
			insert into dbo.master_customer_bill_presentment_behavior
			( customer_company_id
				, valid_date_from
				, payment_term_code
				, is_bill_presentment
				, invoice_period_from
				, invoice_period_to
				, address_id
				, [time]
				, [description]
				, remark
				, condition_date_by
				, date_from
				, date_to
				, last_x_date
				,last_x_business_day
				, custom_date
				, custom_day
				, condition_week_by
				, custom_week
				, next_x_week
				, condition_month_by
				, custom_month
				, next_x_month
				, is_next_bill_presentment_date
				, billing_method
				, is_signed_document
				, is_billing_wiith_truck
				, notify_day
				, is_skip_customer_holiday
				, is_skip_scg_holiday
				, is_billing_with_bill
				, document_type
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by]
			   ,[group_id]
			   ,custom_date_next_x_month)
			   output inserted.bill_presentment_behavior_id into @tmp
			select 
				 @customer_company_id
				, @valid_date_from
				, @term
				, @is_bill_presentment
				, @invoice_period_from
				, @invoice_period_to
				, @address_id
				, @time
				, @description
				, @remark
				, @condition_date_by
				, @date_from
				, @date_to
				, @last_x_date
				, @last_x_business_day
				, @custom_date
				, @custom_day
				, @condition_week_by
				, @custom_week
				, @next_x_week
				, @condition_month_by
				, @custom_month
				, @next_x_month
				, @is_next_bill_presentment_date
				, @billing_method
				, @is_signed_document
				, @is_billing_wiith_truck
				, @notify_day
				, @is_skip_customer_holiday
				, @is_skip_scg_holiday
				, @is_billing_with_bill
				, @document_type
				,@status
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
				,@group_id
				,@custom_date_next_x_month

				select @bill_id = new_id 
				from @tmp
		 end

		 
		update dbo.master_customer_bill_presentment_document
		set custom_document = doc.custom_document
			, [is_active] = 1
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select	doc.custom_document
		--		,'ACTIVE' as [status]
		--		,*
		from @document_list doc 
		left join dbo.master_customer_bill_presentment_document bd on doc.document_code = bd.document_code 
			and bd.bill_presentment_behavior_id = @bill_id
		where  bd.bill_presentment_document_id is not null

		update dbo.master_customer_bill_presentment_document
		set [is_active] = 0
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select 'CANCEL' as [status]
		--		,*
		from  dbo.master_customer_bill_presentment_document bd  
		left join @document_list doc on  bd.document_code  = doc.document_code 	
		where  bd.bill_presentment_behavior_id = @bill_id
				and doc.document_code is null

		insert into dbo.master_customer_bill_presentment_document
		(
			bill_presentment_behavior_id
			,document_code
			,custom_document
			,[is_active]
			,[created_date]
			,[created_by]
			,[updated_date]
			,[updated_by]
		)
		select @bill_id
				,doc.document_code
				,doc.custom_document
				, 1 as [is_active]
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
			--,*
		from @document_list doc 
		left join dbo.master_customer_bill_presentment_document bd on doc.document_code = bd.document_code 
			and bd.bill_presentment_behavior_id = @bill_id
		where  bd.bill_presentment_document_id is null


		
		update dbo.master_customer_bill_presentment_custom_date
		set [is_active] = 1
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select	*
		from @custom_date_list custom 
		left join dbo.master_customer_bill_presentment_custom_date bd on custom.date = bd.date 
			and bd.bill_presentment_behavior_id = @bill_id
		where  bd.bill_presentment_custom_date_id is not null

		update dbo.master_customer_bill_presentment_custom_date
		set [is_active] = 0
			, updated_date = getdate()
			, updated_by = @updated_by	
		--select *
		from  dbo.master_customer_bill_presentment_custom_date bd  
		left join @custom_date_list custom on  bd.date  = custom.date 	
		where  bd.bill_presentment_behavior_id = @bill_id
				and custom.date is null

		insert into dbo.master_customer_bill_presentment_custom_date
		(
			bill_presentment_behavior_id
			,date
			,[is_active]
			,[created_date]
			,[created_by]
			,[updated_date]
			,[updated_by]
		)
		select @bill_id
				,custom.date
				,1 as [is_active]
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by
		--select *
		from @custom_date_list custom 
		left join dbo.master_customer_bill_presentment_custom_date bd on custom.date = bd.date 
			and bd.bill_presentment_behavior_id = @bill_id
		where  bd.bill_presentment_custom_date_id is null



		
		;with sub as(
				select b.bill_presentment_behavior_id
						,customer_company_id
						,payment_term_code
						,invoice_period_from
						,invoice_period_to
						,valid_date_from
						,dateadd(day,-1,valid_date_from)as valid_date_to
						,row_num = ROW_NUMBER() OVER (ORDER BY valid_date_from)
				from [dbo].master_customer_bill_presentment_behavior b
				where   b.is_active = 1 
						and b.payment_term_code = @term
						and b.customer_company_id = @customer_company_id
						and b.invoice_period_from = @invoice_period_from
						and b.invoice_period_to = @invoice_period_to

		) 
		, data as 
		(
			select sub1.bill_presentment_behavior_id 
					,sub1.valid_date_from 
					, sub2.valid_date_to
			from sub sub1
			left join sub sub2 on sub1.row_num = sub2.row_num - 1
		) --select * from data
		merge into [dbo].master_customer_bill_presentment_behavior  b
		using data as d on b.bill_presentment_behavior_id = d.bill_presentment_behavior_id
		when matched then
			update 
			set b.valid_date_to = d.valid_date_to;

		fetch next from table_cursor into  @term
	end;
	close table_cursor;
	deallocate table_cursor;
END
GO
