SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[update_status_other_contact]
	-- Add the parameters for the stored procedure here
	@contact_id as int = null 
	,@is_active as bit  = null 
	,@updated_by as nvarchar(20) = null--'ACTIVE'
AS
BEGIN
	SET NOCOUNT ON;

    update [dbo].[master_other_contact]
	set [is_active] = @is_active
		,updated_by = @updated_by
		,updated_date = getdate()
	--select *
	from [dbo].[master_other_contact]
	where contact_id = @contact_id

END



GO
