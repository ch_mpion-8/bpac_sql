SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_customer_list]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null--'0480'
		,@user_name as nvarchar(100) =  null--'10002' -- null
		,@customer_code as nvarchar(20) =null
		,@customer as nvarchar(100) =  null--'ha' --'b'
		,@bill_presentment_method as nvarchar(100) = null--'EMS,Messenger'
		,@bill_presentment_type as nvarchar(100) =  null--'Special'
		,@sign_document as bit = null
		,@billing_with_truck as bit = null
		,@notify_day as int = null
		,@payment_method as nvarchar(100) = null--''Cheque,E-Payment'
		,@receipt_method as nvarchar(100) = null--'EMS,Messenger'
		,@application_tag as nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
	select distinct cc.customer_company_id
				,cc.company_code
				,com.company_name
				,cc.customer_code
				,cus.customer_name
				,cc.[user_name]
				,cc.[user_display_name]
				,cus.remark as customer_remark
				,cc.is_active
				,isnull(ad.[count],0) as address_count
				,isnull(con.[count],0)  as contact_count
				,isnull(cb.[count],0)  as bill_count
				,isnull(coll.[count],0)  as collection_count
				,isnull(aff.[count],0)  as affiliates_count
	from dbo.master_customer_company cc 
	inner join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1 
	left join dbo.master_customer_bill_presentment_behavior bill on cc.customer_company_id = bill.customer_company_id and bill.[is_active] = 1
	left join dbo.master_customer_collection_behavior col on cc.customer_company_id = col.customer_company_id and col.[is_active] = 1
	left join dbo.master_customer_affiliate_list af on cc.customer_code = af.customer_code and af.is_active = 1
	left join (
			select customer_company_id,count(1) as [count]
			from master_customer_address 
			where is_active = 1
			group by customer_company_id
		) ad on cc.customer_company_id = ad.customer_company_id
	left join (
			select customer_company_id,count(1) as [count]
			from master_customer_contact 
			where is_active = 1
			group by customer_company_id
		) con on cc.customer_company_id = con.customer_company_id
	left join (
		select customer_company_id,count(1) as [count]
		from (
			select group_id,customer_company_id
			from master_customer_bill_presentment_behavior 
			where is_active = 1
			group by group_id,customer_company_id
		)a
		group by customer_company_id
		) cb on cc.customer_company_id = cb.customer_company_id
	left join (
	
		select customer_company_id,count(1) as [count]
		from (
			select group_id,customer_company_id
			from master_customer_collection_behavior 
			where is_active = 1
			group by group_id,customer_company_id
		)a
		group by customer_company_id
		) coll on cc.customer_company_id = coll.customer_company_id
	left join (

			select aff.affiliate_id ,case when count(1) = 0 then 0 else count(1) - 1 end  as [count]
			from dbo.master_customer_affiliate_list aff 
			where aff.is_active = 1
			group by aff.affiliate_id
		) aff on af.affiliate_id = aff.affiliate_id
	where ( @company_code is null or cc.company_code in ( select item from dbo.SplitString(@company_code,',')))
			and ( @user_name is null or cc.user_name in (select item from dbo.SplitString(@user_name,',')))
			--and (@customer is null or (cc.customer_code like '%' + @customer + '%' or cus.customer_name like '%' + @customer + '%'))
			and (@customer is null or cc.customer_code +' : '+ cus.customer_name like '%' + @customer + '%')
			and cc.customer_code  = isnull(@customer_code,cc.customer_code)
			and ( @bill_presentment_method is null or bill.billing_method  in (select item from dbo.SplitString(@bill_presentment_method,',')))
			and ( @bill_presentment_type is null or bill.document_type = @bill_presentment_type)
			and ( @sign_document is null or bill.is_signed_document = @sign_document)
			and ( @billing_with_truck is null or bill.is_billing_wiith_truck = @billing_with_truck)
			and ( @notify_day is null or bill.notify_day = @notify_day)
			and ( @payment_method is null or exists (
							select charindex( item, col.payment_method, 1)
							from dbo.SplitString(@payment_method,',')
								 where charindex( item, col.payment_method, 1)  > 0))
			--and ( @payment_method is null or ( @payment_method_query = '"1"' or contains(col.payment_method ,@payment_method_query)))
			and ( @receipt_method is null or col.receipt_method in (select item from dbo.SplitString(@receipt_method,',')))
			and ( @application_tag is null or isnull(cus.application_tag_id,999) in (select item from dbo.SplitString(@application_tag,',')))

	--select distinct cc.customer_company_id
	--		,cc.company_code
	--		,com.company_name
	--		,cc.customer_code
	--		,cus.customer_name
	--		,cc.[user_name]
	--		,cc.[user_display_name]
	--		,cus.remark as customer_remark
	--		,ca.address_id
	--		,ca.title as address_title
	--		,ca.[address]
	--		,ca.sub_district
	--		,ca.district
	--		,ca.province
	--		,ca.post_code
	--		,ct.contact_id
	--		,ct.contact_name
	--		,ct.position
	--		,ct.telephone
	--		,ct.email
	--		,ct.fax
	--		,ct.remark as contact_remark
	--		,bill.billing_method 
	--		,bill.payment_term_code as bill_payment_term_code
	--		,term.payment_term_name as bill_payment_term_name
	--		,ca2.address_id as bill_address_id
	--		,ca2.[address] as bill_address
	--		,bill.[time] as bill_time
	--		,bill.[description] as bill_description
	--		,bill.custom_date as bill_date
	--		,bill.custom_week as bill_week
	--		,bill.custom_month as bill_month
	--		,case when bill.is_signed_document is null or bill.is_signed_document = 0 then 'No' else 'YES' end as bill_is_signed_document
	--		,case when bill.is_billing_wiith_truck is null or bill.is_billing_wiith_truck = 0 then 'No' else 'YES' end as bill_is_billing_wiith_truck
	--		,bill.notify_day as bill_notify_day
	--		,bill.document_type as bill_document_type
	--		,subdoc.doc as bill_document
	--		,bill.remark as bill_remark
	--		,col.payment_term_code as collection_payment_term_code
	--		,term2.payment_term_name as collection_payment_term_name
	--		,ca3.address_id as collection_address_id
	--		,ca3.[address] as collection_address
	--		,col.[time] as collection_time
	--		,col.period_from as collection_period_from
	--		,col.period_to as collection_period_to
	--		,col.collection_condition_date as collection_condition_date
	--		,col.[description] as collection_description
	--		,col.custom_date as collection_date
	--		,col.custom_week as collection_week
	--		,col.custom_month as collection_month
	--		,col.payment_method as collection_payment_method
	--		,col.receipt_method as collection_receipt_method
	--		,col.notify_day as collection_notify_day
	--		,col.bank as collection_bank_id
	--		,bank.bank_name as collection_bank_name
	--		,col.document as collection_document
	--		,col.remark as collection_remark
	--		,cc.[is_active] as [is_active]
	--from dbo.master_customer_company cc 
	--left join dbo.master_company com on cc.company_code = com.company_code and com.[is_active] = 1
	--left join dbo.master_customer cus on cc.customer_code = cus.customer_code and cus.[is_active] = 1
	--left join dbo.master_customer_address ca on cc.customer_company_id = ca.customer_company_id  and ca.[is_active] = 1
	--left join dbo.master_customer_contact ct on cc.customer_company_id = ct.customer_company_id and ct.[is_active] = 1
	--left join dbo.master_customer_bill_presentment_behavior bill on cc.customer_company_id = bill.customer_company_id and bill.[is_active] = 1
	--left join dbo.master_payment_term term on bill.payment_term_code = term.payment_term_code and term.[is_active] = 1
	--left join dbo.master_customer_address ca2 on bill.address_id = ca2.address_id and ca2.[is_active] = 1
	--left join dbo.master_customer_collection_behavior col on cc.customer_company_id = col.customer_company_id and col.[is_active] = 1
	--left join dbo.master_payment_term term2 on col.payment_term_code = term2.payment_term_code and term2.[is_active] = 1
	--left join dbo.master_customer_address ca3 on col.address_id = ca3.address_id and ca3.[is_active] = 1
	--left join (
	--				select  main.collection_behavior_id, stuff(
	--				(
	--					select ',' + bank_name 
	--						from dbo.SplitString(main.bank,',') s
	--						inner join dbo.master_bank b on s.Item = b.bank_id
	--						for xml path('')
	--				) ,1,1,'') as bank_name
	--				from dbo.master_customer_collection_behavior main
	--			) bank on col.collection_behavior_id = bank.collection_behavior_id	
	----left join dbo.master_bank bank on col.bank_id = bank.bank_id and bank.[is_active] = 1
	--left join (
	--				select  main.bill_presentment_behavior_id,	stuff(
	--				(
	--					select ', ' + configuration_value 
	--						from master_customer_bill_presentment_document sub
	--						inner join dbo.configuration con on sub.document_code = con.configuration_code and con.configuration_key = 'BillingDocument'
	--						where sub.bill_presentment_behavior_id = main.bill_presentment_behavior_id
	--						for xml path('')
	--				) ,1,1,'') as doc
	--				from dbo.master_customer_bill_presentment_document main
	--			) subdoc on bill.bill_presentment_behavior_id = subdoc.bill_presentment_behavior_id
	--where bill.customer_company_id is not  null and col.customer_company_id is not null
	--	and ( @company_code is null or cc.company_code = @company_code)
	--	and ( @user_name is null or cc.user_name in (select item from dbo.SplitString(@user_name,',')))
	--	and (@customer is null or (cc.customer_code like '%' + @customer + '%' or cus.customer_name like '%' + @customer + '%'))
		
	--	and ( @bill_presentment_method is null or bill.billing_method  in (select item from dbo.SplitString(@bill_presentment_method,',')))
	--	and ( @bill_presentment_type is null or bill.document_type = @bill_presentment_type)
	--	and ( @sign_document is null or bill.is_signed_document = @sign_document)
	--	and ( @billing_with_truck is null or bill.is_billing_wiith_truck = @billing_with_truck)
	--	and ( @notify_day is null or bill.notify_day = @notify_day)
	--	and @payment_method is null or exists (
	--					select charindex( item, col.payment_method, 1)
	--					from dbo.SplitString(@payment_method,',')
	--						 where charindex( item, col.payment_method, 1)  > 0)
	--	and ( @receipt_method is null or col.receipt_method in (select item from dbo.SplitString(@receipt_method,',')))
	--	and ( @application_tag is null or cus.application_tag_id in (select item from dbo.SplitString(@application_tag,',')))


END
GO
