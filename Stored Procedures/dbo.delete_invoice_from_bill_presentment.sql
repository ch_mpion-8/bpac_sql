SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[delete_invoice_from_bill_presentment]
	-- Add the parameters for the stored procedure here
	@bill_presentment_detail_id as int
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--update dbo.bill_presentment_control_document_item
	--set is_active = 0
	--	,updated_by = @updated_by
	--	,updated_date = getdate()
	--from dbo.bill_presentment_control_document_item
	--where bill_presentment_detail_id = @bill_presentment_detail_id 

	declare @invoice_no as varchar(18) 
			,@bill_presentment_id as int
	select @invoice_no = invoice_no ,@bill_presentment_id = bill_presentment_id
	from dbo.bill_presentment_control_document_item
	where bill_presentment_detail_id = @bill_presentment_detail_id 
	
	delete dbo.bill_presentment_control_document_item
	where bill_presentment_detail_id = @bill_presentment_detail_id 



	exec dbo.update_invoice_bill_presentment_status
		@invoice_no = @invoice_no
		,@bill_presentment_id = null
		,@status  = 'WAITING'
		,@updated_by  = @updated_by

	--declare @description as nvarchar(100)
	--set @description = 'Remove invoice ' + @invoice_no
	exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_presentment_id
			,@description = @invoice_no
			,@action  = 'Remove invoice'
			,@updated_by  = @updated_by

END
GO
