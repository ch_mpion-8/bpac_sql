SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SP_SKIP_PYC]
	-- Add the parameters for the stored procedure here
	@pyc_list dbo.PycTable readonly ,
	@submitby varchar(50) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @document_no varchar(50),@document_date datetime,@company_code varchar(4),@customer_code varchar(10),@count_trip int
	,@trip_date date,@is_skip bit,@document_id int,@reference_type int,@process_id int,@remark varchar(max)

	declare cur Cursor For
	select * from @pyc_list

	open cur
	fetch next from cur into @document_no,@document_date,@company_code,@customer_code,@count_trip,@trip_date,@is_skip,@document_id,@reference_type,@process_id,@remark

	while @@FETCH_STATUS=0
	begin
		if @process_id=0 or @process_id is null
		begin
			exec dbo.SP_Create_PYC @document_id,@reference_type,@trip_date,@count_trip,1,@remark,@submitby
		end
		else
		begin
			update dbo.scg_pyc_process
			set  is_skip = 1
				,updated_date = GETDATE()
				,updated_by = @submitby
				,remark = @remark
			where process_id = @process_id
		end
		
		fetch next from cur into @document_no,@document_date,@company_code,@customer_code,@count_trip,@trip_date,@is_skip,@document_id,@reference_type,@process_id,@remark
	end

	close cur
	deallocate cur
END
GO
