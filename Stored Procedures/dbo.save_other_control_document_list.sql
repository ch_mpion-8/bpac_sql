SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_other_control_document_list]
	@other_doc_list [dbo].[OtherDocumentList] READONLY,
	@error_message nvarchar(200) output,
	@document_ids nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @document_no varchar(20) 
	Declare @document_type int 
	Declare @action_type int
	Declare @company_code varchar(4)
	Declare @receiver varchar(10)
	Declare @method varchar(20)
	Declare @document_date datetime
	Declare @user_code varchar(50)
	Declare @remark nvarchar(2000)
	Declare @status nvarchar(20)
	Declare @create_date datetime
	Declare @create_by nvarchar(50)
	Declare @update_date datetime
	Declare @update_by nvarchar(50)
	Declare @document_receive nvarchar(max)
	Declare @document_send nvarchar(max)
	Declare @document_sendreceive  nvarchar(max)
	Declare @total_document nvarchar(max) = '';
	Declare _cur Cursor FOR
	Select * FROM @other_doc_list

	Open _cur
	Fetch Next FROM _cur INTO 
	@document_no,
	@document_type,
	@action_type,
	@company_code,
	@receiver,
	@method,
	@document_date,
	@user_code,
	@remark,
	@status,
	@create_date,
	@create_by,
	@update_date,
	@update_by,
	@document_receive,
	@document_send,
	@document_sendreceive

	while(@@FETCH_STATUS=0)
	begin
		Declare @t_id int =null 

		exec [dbo].[save_other_control_document]
		@document_no ,
		@document_type ,
		@action_type,
		@company_code ,
		@receiver ,
		@method,
		@document_date ,
		@user_code,
		@remark ,
		@status,
		@create_date ,
		@create_by ,
		@update_date ,
		@update_by ,
		@document_receive ,
		@document_send,
		@document_sendreceive,
		@error_message output,
		@t_id output

		if @t_id is not null
		begin
			set @total_document += '_'+convert(nvarchar(10),@t_id)
		end
		Fetch Next FROM _cur INTO 
		@document_no,
		@document_type,
		@action_type,
		@company_code,
		@receiver,
		@method,
		@document_date,
		@user_code,
		@remark,
		@status,
		@create_date,
		@create_by,
		@update_date,
		@update_by,
		@document_receive,
		@document_send,
		@document_sendreceive
	end
	Close _cur
	Deallocate _cur
	set @document_ids = @total_document

	IF RTRIM(LTRIM(ISNULL(@error_message,''))) <> ''
		SET @error_message = '[CRL]:' +ISNULL(CONVERT(varchar(10),(SELECT COUNT(*) FROM @other_doc_list)),0)+'>'+@error_message
END

GO
