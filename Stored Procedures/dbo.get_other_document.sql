SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_other_document]
	@company_code as varchar(200)=null,
	@receiver as	varchar(200)=null,
	@otherdoc_no as varchar(20)=null,
	@otherdoc_date_start as date =null,
	@otherdoc_date_end as date =null,
	@status as nvarchar(20)=null,
	@customer_code as varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @temp table(
	document_no varchar(20),
	document_date datetime,
	document_type varchar(2),
	company_code varchar(4),
	receiver varchar(200),
	receiver_no varchar(20),
	created_by varchar(50),
	created_date datetime,
	updated_by varchar(50),
	update_date datetime,
	method varchar(20),
	user_code varchar(50),
	[status] varchar(20),
	document_id int,
	action_type varchar(2),
	receiver_type int,
	reference_guid uniqueidentifier
	)

	insert into @temp
	select
	o.document_no,
	o.document_date,
	CONVERT(varchar(4),o.document_type) as document_type,
	o.company_code,
	case when o.contact_person_type = 0 then o.receiver_name else cus.customer_name end,
	o.receiver ,
	o.created_by,
	o.created_date,
	o.updated_by,
	o.update_date,
	isnull(conf.[configuration_value], o.method) as method,
	o.user_code,
	o.[status],
	o.document_id,
	NULL,
	o.contact_person_type,
	o.reference_guid
	from dbo.other_control_document o
	left join dbo.master_customer_company c on c.company_code = o.company_code and c.customer_code = o.receiver
	left join dbo.master_customer cus on o.receiver = cus.customer_code
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where
		 (@company_code is null or o.company_code in ( select item from dbo.SplitString(@company_code,',')))
		 --o.company_code = case @company_code when  'All' then  o.company_code else @company_code end AND
		 AND
		(	@customer_code is null
			AND
			(
				cus.customer_name LIKE '%'+isnull(@receiver,'')+'%'
				OR
				 o.receiver_name like '%'+isnull(@receiver,'')+'%' 
			 )
			 OR
			 @customer_code is not null
			 AND
			 o.receiver = @customer_code
		)
		AND
		o.document_no LIKE '%'+isnull(@otherdoc_no,'')+'%' AND
		((convert(date,o.document_date) Between @otherdoc_date_start AND @otherdoc_date_end) OR @otherdoc_date_end is null OR @otherdoc_date_start is null )AND
		o.[status] = case @status when 'All' then o.[status] else @status end AND
		o.document_type=4
	
	Declare @doc_id int,@c_type int
	Declare cur Cursor For
	
	select document_id,receiver_type from @temp

	Open cur
	Fetch Next From cur
	Into @doc_id,@c_type

	while(@@FETCH_STATUS=0)
	begin
			--search contact name




			Declare @has_send int =0,@has_receive int =0,@type int =0

			If Exists(Select 1 from dbo.other_control_document_item where document_id=@doc_id AND action_type=1)
				set @has_send=1
			If Exists(Select 1 from dbo.other_control_document_item where document_id = @doc_id AND action_type = 2)
				set @has_receive =1

			if @has_receive=1 AND @has_send=1
				set @type=0
			else if @has_receive=1 AND @has_send=0
				set @type =2
			else if @has_receive=0 AND @has_send=1
				set @type= 1

			update @temp
			set action_type = Convert(varchar(2),@type)
			where document_id = @doc_id

			Fetch Next From cur
			Into @doc_id,@c_type
	end

	Close cur
	Deallocate cur

	select * from @temp

	delete from @temp
END
GO
