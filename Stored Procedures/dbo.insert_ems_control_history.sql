SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_ems_control_history]
	-- Add the parameters for the stored procedure here
	@ems_control_document_id as int
	,@description as nvarchar(2000) 
	,@action as nvarchar(2000) 
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	insert into [dbo].[ems_control_document_history]
	(
		ems_control_document_id
		,[action]
		,[description]
		,created_by
		,created_date
	)
	select @ems_control_document_id
			,@action
			,@description
			,@updated_by
			,getdate()


END


GO
