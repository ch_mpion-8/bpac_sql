SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_latest_sync_data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select top 1 request_date_time,response_date_time,*
	from dbo.log_interface_header h
	inner join dbo.log_interface_detail d on h.id = d.log_interface_header_id
	where d.remark is not null and lower(ltrim(rtrim(d.remark))) = 'success'
	and d.step_name = 'MASTERDATA' 
	order by h.request_date_time desc

END
GO
