SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_contact_person]
	-- Add the parameters for the stored procedure here
	@contact_person_id as int = null
	,@contact_person_name as nvarchar(100) = null
	,@contact_group_id as int = null
	,@contact_person_phone as nvarchar(200) = null
	,@contact_person_email as nvarchar(200) = null
	,@contact_person_remark as nvarchar(2000) = null
	,@status as bit = null
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

      if exists (select * from [dbo].[master_contact_person] where contact_person_id = @contact_person_id)
        begin
			if(@status = 0)
			begin
			update [dbo].[master_contact_person]
			set [is_active] = @status
				,[updated_date] = getdate()
				,[updated_by] = @updated_by
			where contact_person_id = @contact_person_id;
			end
			else
			begin
            update [dbo].[master_contact_person]
			set contact_person_name = @contact_person_name
				,contact_group_id = @contact_group_id				
				,[contact_person_phone] = @contact_person_phone
				,[contact_person_email] = @contact_person_email
				,[contact_person_remark] = @contact_person_remark
				,[is_active] = @status
				,[updated_date] = getdate()
				,[updated_by] = @updated_by
			where contact_person_id = @contact_person_id;
			end
        end
    else
        begin
            insert into [dbo].[master_contact_person]
			   ([contact_person_name]
			   ,[contact_group_id]
			   ,[contact_person_phone]
			   ,[contact_person_email]
			   ,[contact_person_remark]
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
			 values
				(
				   @contact_person_name
				   ,@contact_group_id
				   ,@contact_person_phone
				   ,@contact_person_email
				   ,@contact_person_remark
				   ,@status
				   ,getdate()
				   ,@updated_by
				   ,getdate()
				   ,@updated_by
				 )
        end
END




GO
