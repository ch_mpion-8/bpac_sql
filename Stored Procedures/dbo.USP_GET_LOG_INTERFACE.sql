SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GET_LOG_INTERFACE]
	-- Add the parameters for the stored procedure here
	@webkey varchar(20) = null,
	@syncby varchar(20) = null,
	@date_from date = null,
	@date_to date = null,
	@status varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 50 *,CASE WHEN EXISTS (SELECT 1 FROM log_interface_detail WHERE log_interface_header_id = log_interface_header.id and step_name = 'calculate' and remark = 'Success')
       THEN 'SUCCESS' 
       ELSE 'FAIL'
	END AS [Status]   FROM log_interface_header
	where 
	(@webkey is null or web_key like '%'+ @webkey +'%')
	and (@syncby is null or request_by=@syncby)
	and (@status is null or [Status]=@status)
	and ( @date_from is null or @date_to is null or (request_date_time between @date_from and @date_to))
	order by request_date_time desc
END

GO
