SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[delete_ems_document]
	-- Add the parameters for the stored procedure here
	 @ems_doucment_id as int = null--1
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update dbo.ems_control_document
	set [status] = 'CANCEL'
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 'CANCEL',*
	from dbo.ems_control_document
	where ems_control_document_id = @ems_doucment_id

	update dbo.ems_control_document_item
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	--select 0,*
	from dbo.ems_control_document_item
	where ems_control_document_id = @ems_doucment_id

	
	update dbo.ems_sub_item
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	--select i.*
	from dbo.ems_sub_item i
	inner join dbo.ems_control_document_item s on i.ems_detail_id = s.ems_detail_id
	where s.is_active  = 0


			
	exec dbo.insert_ems_control_history
			@ems_control_document_id = @ems_doucment_id
			,@description =  'Cancel Document Control'
			,@action  = 'Update status'
			,@updated_by  = @updated_by

END
GO
