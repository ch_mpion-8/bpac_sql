SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_SAVE_INVOICE_WITH_COLLECTION]
	-- Add the parameters for the stored procedure here
		@invoice_code as  nvarchar(20) = null 

		,@collection_prement_date as date 
		,@collection_prement_notify_date as date 
		,@collection_prement_log as nvarchar(100) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		--update dbo.invoice_header
		--	set bill_presentment_date = @bill_prement_date
		--		,bill_presentment_notify_date = @bill_prement_notify_date
		--		,[bill_presentment_calculation_log] = @bill_prement_log
		--		--,collection_date = @collection_prement_date
		--		--,collection_notify_date = @collection_prement_notify_date
		--		--,collection_calculation_log = @collection_prement_log
		--		,updated_date_time = GETDATE()
		
		--	where invoice_no = @invoice_code
				
				
				IF (@collection_prement_date is null) or (@collection_prement_notify_date is null) BEGIN 
			update dbo.invoice_header
				set collection_calculation_log = @collection_prement_log
					,updated_date_time = GETDATE()
		
				where invoice_no = @invoice_code
		END
		ELSE BEGIN
			update dbo.invoice_header
				set collection_date = @collection_prement_date
				,collection_notify_date = @collection_prement_notify_date
				,collection_calculation_log = ''
					,updated_date_time = GETDATE()
		
				where invoice_no = @invoice_code
		 END
END


GO
