SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_other_contact]
	@contact_id as int = null
	,@contact_name as nvarchar(100) = null
	,@address as nvarchar(200) = null
	,@sub_district as nvarchar(50) = null
	,@district as nvarchar(50) = null
	,@province as nvarchar(50) = null
	,@post_code as nvarchar(10) = null
	,@remark as nvarchar(2000) = null
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists(select 1 from dbo.master_other_contact where contact_id = @contact_id)
	begin
		update dbo.master_other_contact
		set contact_name = @contact_name
			,[address] = @address
			,sub_district = @sub_district
			,district = @district
			,province = @province
			,post_code = @post_code
			,remark = @remark
			,updated_date = getdate()
			,updated_by = @updated_by
		 where contact_id = @contact_id
	end
	else
	begin
		insert into dbo.master_other_contact
		(
			 contact_name
			, address
			, sub_district
			, district
			, province
			, post_code
			, remark
			, is_active
			, created_date
			, created_by
			, updated_date
			, updated_by
		)
		values
		(
			 @contact_name
			, @address
			, @sub_district
			, @district
			, @province
			, @post_code
			, @remark
			, 1
			, getdate()
			, @updated_by
			, getdate()
			, @updated_by
		)
	end

END

GO
