SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_bill_presentment_history]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int
	,@description as nvarchar(2000) 
	,@action as nvarchar(2000) 
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	insert into dbo.bill_presentment_control_document_history
	(
		bill_presentment_id
		,[action]
		,bpac_due_date
		,[description]
		,created_by
		,created_date
	)
	select @bill_presentment_id
			,@action
			,bill_presentment_date
			,@description
			,@updated_by
			,getdate()
	from dbo.bill_presentment_control_document 
	where bill_presentment_id = @bill_presentment_id


END


GO
