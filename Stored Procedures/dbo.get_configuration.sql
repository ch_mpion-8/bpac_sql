SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_configuration]
	-- Add the parameters for the stored procedure here
	@config_key as nvarchar(100) = null --'Document,Method'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select configuration_key
			,configuration_code
			,configuration_value
			,[sequence_no]
	from dbo.configuration
	where @config_key is null or configuration_key in (select item from dbo.SplitString(@config_key,','))
	AND ISNULL(is_active,0) = 1
END




GO
