SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[uspWriteAuditLog]
	-- Add the parameters for the stored procedure here
	@EventName	VARCHAR(MAX),
	@EventDetail	NVARCHAR(MAX) = NULL,
	@ActionUser	NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[AuditLog]
           ([Id]
           ,[Timestamp]
           ,[EventName]
           ,[EventDetail]
           ,[ActionUser])
     VALUES
           (NEWID()
           ,SYSUTCDATETIME()
           ,@EventName
           ,@EventDetail
           ,@ActionUser)
END


GO
