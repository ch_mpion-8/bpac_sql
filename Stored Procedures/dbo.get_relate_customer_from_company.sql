SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_relate_customer_from_company]
	-- Add the parameters for the stored procedure here
	@company_code varchar(4)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct master_customer.* from master_customer_company inner join master_customer 
	on master_customer_company.customer_code = master_customer.customer_code
	where master_customer_company.company_code = @company_code

END
GO
