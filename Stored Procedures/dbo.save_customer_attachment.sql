SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_customer_attachment]
	-- Add the parameters for the stored procedure here
	 @attachment_id as int = null 
		,@customer_company_id as int = null
		,@title as nvarchar(100) = null
		,@remark as nvarchar(2000) = null
		,@reference_guid as uniqueidentifier = null
		,@file_name as nvarchar(100) = null
		,@status as bit = null
		,@updated_by as nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	if exists (select 1 from [dbo].master_customer_attachment where attachment_id = @attachment_id)
		 begin
			
			update dbo.master_customer_attachment
			set title = @title
				,remark = @remark
				,[file_name] = @file_name
				,[reference_guid] = @reference_guid
				,[is_active] = @status
				,updated_date = getdate()
				,updated_by = @updated_by
			from dbo.master_customer_attachment
			where attachment_id = @attachment_id


			if exists(select 1 from dbo.master_customer_attachment where copy_from_id = @attachment_id and is_active = 1)
			begin
				update dbo.master_customer_attachment
				set title = @title
					,remark = @remark
					,[file_name] = @file_name
					,[reference_guid] = @reference_guid
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_attachment
				where copy_from_id = @attachment_id and is_active = 1
			end

			declare @copy_from_id int
			select @copy_from_id = copy_from_id
			from dbo.master_customer_attachment
			where attachment_id = @attachment_id

			if exists(select 1 from dbo.master_customer_attachment where attachment_id = @copy_from_id and is_active = 1)
			begin
				update dbo.master_customer_attachment
				set title = @title
					,remark = @remark
					,[file_name] = @file_name
					,[reference_guid] = @reference_guid
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_attachment
				where attachment_id = @copy_from_id and is_active = 1
			end




		 end
		 else
		 begin
			
			insert into dbo.master_customer_attachment
			([customer_company_id]
			   ,title
			   ,remark
			   ,[reference_guid]
			   ,[file_name]
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
			select 
				@customer_company_id
				,@title
				,@remark
				,@reference_guid
				,@file_name
				,@status
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by

		 end

END



GO
