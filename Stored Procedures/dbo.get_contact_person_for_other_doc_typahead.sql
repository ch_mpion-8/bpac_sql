SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_contact_person_for_other_doc_typahead]
	@customer_company_id int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select cp.contact_id,cp.contact_name,cp.telephone from dbo.master_customer_contact cp
	where cp.customer_company_id = @customer_company_id
	and is_show_in_other_document = 1
		
END

GO
