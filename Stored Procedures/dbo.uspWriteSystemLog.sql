SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[uspWriteSystemLog]
	-- Add the parameters for the stored procedure here
	@LogLevel	VARCHAR(10),
	@LogMessage	NVARCHAR(MAX),
	@LogDetail	NVARCHAR(MAX) = NULL,
	@ActionUser	NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[SystemLog]
           ([Id]
           ,[Timestamp]
           ,[LogLevel]
           ,[LogMessage]
           ,[LogDetail]
           ,[ActionUser])
     VALUES
           (NEWID()
           ,SYSUTCDATETIME()
           ,@LogLevel
           ,@LogMessage
           ,@LogDetail
           ,@ActionUser)
END


GO
