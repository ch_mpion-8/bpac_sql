SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_collection_user_typeahead]
	-- Add the parameters for the stored procedure here
	@search_word varchar(200) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select distinct user_display_name from dbo.master_customer_company
	where user_display_name like '%'+isnull(@search_word,'')+'%'
END
GO
