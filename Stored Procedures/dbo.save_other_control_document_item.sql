SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[save_other_control_document_item]
	-- Add the parameters for the stored procedure here
	@document_id as int =null,
	@reference_no as varchar(500)=null,
	@action_type as int =null,
	@document_type as int =null,
	@status as nvarchar(20) =null,
	@created_date as datetime =null,
	@created_by as nvarchar(50) =null,
	@updated_date as datetime =null,
	@updated_by as nvarchar(50)=null,
	@sequence_no as int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS(select 1 from dbo.other_control_document where document_id = @document_id)
	begin
		if @document_type = 1
		begin
					--find year of reference no
					--only send reference [advance receipt]
				declare @year varchar(4) =null
				declare @split_idx int = charindex(',',@reference_no)
				if @split_idx is not null and @split_idx > 0
				begin
					set @year = SUBSTRING(@reference_no,@split_idx+1,4)
					set @reference_no = SUBSTRING(@reference_no,1,@split_idx-1)
				end

				if not exists(select 1 from dbo.other_control_document_item i  where i.document_id = @document_id and i.reference_no = @reference_no)
				begin
						insert into [dbo].[other_control_document_item]
							(document_id,
							reference_no,
							[year],
							action_type,
							--[status],
							sequence_no,
							created_date,
							created_by,
							updated_date,
							updated_by)	
						values
							(@document_id,
							@reference_no,
							@year,
							@action_type,
							--@status,
							@sequence_no,
							@created_date,
							@created_by,
							@updated_date,
							@updated_by)
				end
		end
		else if @document_type = 4
		begin
			select @reference_no
				insert into [dbo].[other_control_document_item]
					(document_id,
					reference_no,
					action_type,
					--[status],
					sequence_no,
					created_date,
					created_by,
					updated_date,
					updated_by)	
				values
					(@document_id,
					@reference_no,
					@action_type,
					--@status,
					@sequence_no,
					@created_date,
					@created_by,
					@updated_date,
					@updated_by)
		end
		else
		begin
				if not exists(select 1 from dbo.other_control_document_item i  where i.document_id = @document_id and i.reference_no = @reference_no)
				begin
						insert into [dbo].[other_control_document_item]
							(document_id,
							reference_no,
							action_type,
							--[status],
							sequence_no,
							created_date,
							created_by,
							updated_date,
							updated_by)	
						values
							(@document_id,
							@reference_no,
							@action_type,
							--@status,
							@sequence_no,
							@created_date,
							@created_by,
							@updated_date,
							@updated_by)
				end
		end



	end
END
GO
