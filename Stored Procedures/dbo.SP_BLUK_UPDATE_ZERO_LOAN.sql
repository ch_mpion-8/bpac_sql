SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_BLUK_UPDATE_ZERO_LOAN]
	-- Add the parameters for the stored procedure here
	@cheque_list [dbo].[ZeroloanList_New] READONLY,
	@submit_by nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select * into #Temp from @cheque_list


Declare @Id int
Declare @ZeroId int
Declare @ZeroloanHeaderId int 

While (Select Count(*) From #Temp) > 0
Begin

    Select Top 1 @Id = cheque_control_id,@ZeroId = zero_loan_detail_id,@ZeroloanHeaderId = zero_loan_id From #Temp

    update zero_loan_item set zero_loan_status = 3,updated_by=@submit_by,updated_date=GETDATE()
	 where cheque_control_id = @Id and zero_loan_detail_id = @ZeroId

	 update zero_loan set submit_by = @submit_by where zero_loan_id = @ZeroloanHeaderId

    Delete #Temp Where cheque_control_id = @Id and  zero_loan_detail_id = @ZeroId

End
END


GO
