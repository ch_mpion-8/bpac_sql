SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[manage_data_from_sap_to_bpac_2]
AS
BEGIN
	SET NOCOUNT ON;
	/*invoice header field */
	DECLARE @invoice_no as varchar(18) = null
		,@invoice_type as varchar(4) = null
		,@invoice_date as date = null
		,@company_code as varchar(4) = null
		,@customer_code as varchar(10) = null
		,@payment_term as varchar(4) = null
		,@invoice_amount as decimal(15,2) = null
		,@tax_amount as decimal(13,2) = null
		,@currency as varchar(5) = null
		,@referenece_document_no as varchar(16) = null
		,@assignment_no as varchar(18) = null
		,@due_date as date = null
		,@net_due_date as date = null
		,@created_by as varchar(10) = 'INTERFACE'
		,@vat_type AS NVARCHAR(2)=null

	/*invoice detail field*/
	DECLARE @invoice_item as varchar(6) = null
		,@dp_no as varchar(10) = null
		,@load_date as date = null
		,@order_no as varchar(10) = null
		,@bid_no as varchar(10) = null
		,@po_no as varchar(20) = null
		,@material_no as varchar(20) = null
		,@created_date as date = null
		,@created_time as time = null
		,@cost as decimal = null

	/*receipt field*/
	DECLARE @clearing_document_no as varchar(10) = null
		,@document_type as varchar(2) = null
		,@year as varchar(4) = null
		,@clearing_date as date = null
		,@posting_date as date = null
		,@document_date as date = null
		,@reference_document_no as varchar(16) = null
		,@reference_clearing_document as varchar(10) = null
		,@indicator as varchar(10) = null
		,@document_amount as decimal(13,2) = null
		,@base_line_due_date as date = null
		,@day_term as decimal = null
		,@bsad_move as varchar(10) = null
		,@cancel_by as nvarchar(12) =null
		,@cancel_date as date = null
		,@cancel_reason as nvarchar(100) = null

	DECLARE @accounting_document_no as varchar(10) = null
	 , @line_no as varchar(3) =null
	 , @period as varchar(2) =null
	 , @local_amount as decimal = null
	 , @gl_account as varchar(10) = null
	 , @payment_reference as varchar(30) = null

	/*------------------1. Add Invoice header-------------------*/
	insert into dbo.invoice_header 
				 (invoice_no, invoice_type, invoice_date, company_code
					, customer_code
					, payment_term
					, invoice_amount
					, tax_amount
					, currency, referenece_document_no, assignment_no
					, created_by, vat_type)
	select a.vbeln as invoice_no, c.blart as invoice_type, cast(a.fkdat AS date) as invoice_date, a.bukrs as company_code
					, CASE WHEN try_cast(a.KUNRG AS int) IS NULL
						THEN a.KUNRG
						ELSE cast(cast(a.KUNRG AS int)as varchar(10))  end as [customer_code]
					, c.zterm as payment_term
					, CONVERT (decimal(15,2),a.NETWR)  as net_amount
					, CONVERT (decimal(13,2),a.MWSBK) as tax_amount
					, c.WAERS as CCY, c.XBLNR as [Reference Document Number], c.ZUONR as [Assignment number]
					, 'INTERFACE', c.mwskz AS [vat_type]
	FROM dbo.VBRK_Temp a
		INNER JOIN dbo.master_customer b
			ON a.KUNRG = right(RTRIM(REPLICATE('0',10) + b.customer_code),10) --b.customer_code
		INNER JOIN dbo.BSID_Temp c
			ON a.vbeln = c.vbeln
		LEFT JOIN dbo.invoice_header ih
			ON a.VBELN = ih.invoice_no
				AND a.BUKRS = ih.company_code
	where b.country = 'TH'
		and c.BLART = 'RA'
		and a.fkart not in ('ZF07','ZF08','ZS07','ZS08') -- FOC
		and isnull(b.is_ignore,0) <> 1
		AND (ih.invoice_no IS NULL AND ih.company_code IS NULL); 

	UPDATE ih
	set ih.invoice_date = cast(a.fkdat AS date)
					, ih.customer_code =  CASE WHEN try_cast(a.KUNRG AS int) IS NULL
						THEN a.KUNRG ELSE cast(cast(a.KUNRG AS int)as varchar(10))  end
					, ih.payment_term = c.zterm
					, ih.invoice_amount = CONVERT(decimal(15,2),a.NETWR)
					, ih.tax_amount = CONVERT(decimal(13,2),a.MWSBK)
					, ih.currency = c.WAERS
					, ih.referenece_document_no = c.XBLNR
					, ih.assignment_no = c.ZUONR
					, ih.vat_type = c.mwskz
					, ih.due_date = DATEADD(day,cast(c.ZBD1T as int),ih.invoice_date)
					, ih.net_due_date = DATEADD(day,cast(c.ZBD1T as int),cast(c.ZFBDT AS date))
		FROM dbo.VBRK_Temp a
			INNER JOIN dbo.master_customer b
				ON a.KUNRG = right(RTRIM(REPLICATE('0',10) + b.customer_code),10) --b.customer_code
			INNER JOIN dbo.BSID_Temp c
				ON a.vbeln = c.vbeln
			INNER JOIN dbo.invoice_header ih
				ON a.VBELN = ih.invoice_no
				AND a.BUKRS = ih.company_code
		where b.country = 'TH'
			and c.BLART = 'RA'
			and a.fkart not in ('ZF07','ZF08','ZS07','ZS08') -- FOC
			and isnull(b.is_ignore,0) <> 1;

	/*update a
	set a.due_date = DATEADD(day,cast(ZBD1T as int),a.invoice_date)
		, a.net_due_date = DATEADD(day,cast(ZBD1T as int),cast(ZFBDT AS date))
	from invoice_header a, BSID_Temp b
	where a.invoice_no = b.vbeln
	and a.company_code = b.BUKRS
	and b.BLART = 'RA'*/

	/* indicator 1 = reversed document (document ที่ถูก Reversed)
		indicator 2 = reversal Document (document ที่สร้างเพื่อไปล้างตัวอื่น)
	*/
	update a
	set a.bill_presentment_status = 'CANCEL'
	, a.collection_status = 'CANCEL'
	, collection_amount = 0
	from invoice_header a, BKPF_Temp b
	where a.invoice_no = b.xBLNR
	and a.company_code = b.BUKRS
	and b.XREVERSAL = '1'
	/*------------------end 1. invoice header-------------------*/

	/*------------------2. invoice detail-----------------------*/
	insert into dbo.invoice_detail
				 (invoice_no, invoice_item, dp_no, load_date
				   , order_no, bid_no, po_no
				   , material_no, net_value, tax_value
				   , created_date, created_time, cost)
     select a.vbeln as invoice_no, a.POSNR as invoice_item,  a.vgbel as dp_no, null as load_date
				, a.AUBEL as order_no, '' as bid_no, '' as po_no
				, a.MATNR as material_no
				, CONVERT (decimal(15,2),a.NETWR)  as net_amount
				, CONVERT (decimal(13,2),a.MWSBP) as tax_amount
				, cast(a.ERDAT AS date) as created_date, CAST(STUFF(STUFF(a.ERZET,5,0,':'),3,0,':') AS time) as create_time
				, CONVERT (decimal(13,2),a.WAVWR) as Cost_in_document_currency
	from dbo.vbrp_temp a 
		INNER JOIN dbo.invoice_header b
			ON a.vbeln = b.invoice_no
		LEFT JOIN dbo.invoice_detail id
			ON a.VBELN = id.invoice_no
				AND a.POSNR = id.invoice_item
	WHERE (id.invoice_no IS NULL AND id.invoice_item IS NULL ) 

		update id
                set dp_no = a.vgbel
					,order_no = a.AUBEL
					,material_no = a.MATNR
					,net_value = CONVERT (decimal(15,2),a.NETWR)
					,tax_value = CONVERT (decimal(13,2),a.MWSBP)
					,created_date = cast(a.ERDAT AS date)
					,created_time = CAST(STUFF(STUFF(a.ERZET,5,0,':'),3,0,':') AS time)
					,cost = CONVERT (decimal(13,2),a.WAVWR)
		FROM dbo.vbrp_temp a 
			INNER JOIN dbo.invoice_detail id
				ON a.VBELN = id.invoice_no
					AND a.POSNR = id.invoice_item

	update a
	set a.load_date = b.lfdat
	from dbo.invoice_detail a 
		INNER JOIN  dbo.dnload_temp b
		ON a.dp_no = b.vbeln

	update a
	set a.bid_no = b.BIDNO
	from dbo.invoice_detail a
		INNER JOIN  dbo.BIDNO b
		ON a.order_no = b.vbeln

	update a
	set a.po_no = b.BSTNK
	from dbo.invoice_detail a
		INNER JOIN dbo.VBAK_Temp b
		ON a.order_no = b.vbeln
	/*----------------------end 2. invoice detail-------------------*/

	/*------------------3. receipt----AI-and-AG------------------*/
	
	insert INTO dbo.receipt
					(clearing_document_no, document_type, receipt_type, year, company_code
					, customer_code, assignment_no, posting_date, document_date, created_date
					, reference_document_no, document_amount
					, currency, base_line_due_date
					, payment_term, day_term)
		SELECT c.accounting_document_no,  c.doc_type, 'normal' AS [receipt_type], c.year, c.company_code
						, c.customer_code, c.assignment_no, c.posting_date, c.document_date, c.created_date
						, c.reference_document, c.document_amount
						, c.ccy, c.base_line_due_date
						, c.payment_term, c.day_term
		FROM (SELECT a.accounting_document_no,  a.doc_type, a.year, a.company_code
						, a.customer_code, a.assignment_no, a.posting_date, a.document_date, a.created_date
						, a.reference_document, sum(a.document_amount) as document_amount
						, a.ccy, a.base_line_due_date
						, a.payment_term, a.day_term
					FROM (select  BELNR as accounting_document_no, BLART as doc_type
								, GJAHR as year, BUKRS as company_code
								--, kunnr as customer_code
								, CASE WHEN try_cast(kunnr AS int) IS NULL
									THEN kunnr
									ELSE cast(cast(kunnr AS int)as varchar(10))  end as customer_code
								,'' as assignment_no --, ZUONR as assignment_no
								, cast(BUDAT AS date) as posting_date, cast(bldat AS date) as document_date, cast(CPUDT AS date) as created_date
								, xblnr as reference_document
								, abs(sum(case SHKZG when 'H' then -1*(CONVERT (decimal(13,2),WRBTR)+CONVERT (decimal(13,2),WMWST))
												else (CONVERT (decimal(13,2),WRBTR)+CONVERT (decimal(13,2),WMWST))
												end)) as document_amount
								, WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
								, zterm as payment_term, CONVERT (decimal(3,0),ZBD1T) as day_term
						from dbo.bsid_temp
						where (blart in ('RB') AND substring(XBLNR,1,2) in ('AI')
							OR blart in ('DE') AND substring(XBLNR,1,2) in ('AG'))
						group by BELNR, BLART
								, GJAHR, BUKRS , kunnr
								, cast(BUDAT AS date), cast(bldat AS date), cast(CPUDT AS date)
								, xblnr, WAERS, zterm, CONVERT (decimal(3,0),ZBD1T)
					UNION 
						select  a.BELNR as accounting_document_no, a.BLART as doc_type
									, a.GJAHR as year, a.BUKRS as company_code
									--, a.kunnr as customer_code
									, CASE WHEN try_cast(a.kunnr AS int) IS NULL
										THEN a.kunnr
										ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
									,'' as assignment_no --, ZUONR as assignment_no
									, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date, cast(a.CPUDT AS date) as created_date
									, a.xblnr as reference_document
									, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),a.WMWST))
													else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),a.WMWST))
													end)) as document_amount
									, a.WAERS as ccy
									, null as base_line_due_date, a.zterm as payment_term
									, CONVERT (decimal(3,0),a.ZBD1T) as day_term
							from dbo.bsad_temp a
								INNER JOIN dbo.bkpf_temp b
									ON a.belnr =b.belnr
							and a.BUKRS =b.BUKRS
							AND a.gjahr = b.gjahr
							where (a.blart in ('RB') and substring(a.XBLNR,1,2) in ('AI')
								OR a.blart in ('DE') AND substring(a.XBLNR,1,2) in ('AG'))
								AND isnull(b.xreversal,'') = ''
							group by a.BELNR, a.BLART
									, a.GJAHR, a.BUKRS , a.kunnr
									, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
									, a.xblnr, a.WAERS, a.zterm
									, CONVERT (decimal(3,0),a.ZBD1T) ) a
				group by a.accounting_document_no,  a.doc_type
									, a.year, a.company_code, a.customer_code
									, a.assignment_no, a.posting_date, a.document_date, a.created_date
									, a.reference_document, a.ccy, a.base_line_due_date --, ZFBDT as base_line_due_date
									, a.payment_term, a.day_term) c
			LEFT JOIN dbo.receipt r
					ON r.clearing_document_no = c.accounting_document_no
						AND r.year = c.year
						AND r.company_code =c.company_code
				WHERE ( r.clearing_document_no IS NULL  AND r.company_code IS NULL AND r.year IS NULL)

		UPDATE r
		set document_type = c.doc_type
					,receipt_type = 'normal'
					,customer_code = c.customer_code
					,assignment_no = c.assignment_no
					,posting_date = c.posting_date
					,document_date = c.document_date
					,created_date = c.created_date
					,reference_document_no = c.reference_document
					--,document_amount = @document_amount /*comment with condition*/
					,currency = c.ccy
					,base_line_due_date = c.base_line_due_date
					,payment_term = c.payment_term
					,day_term = c.day_term
		FROM dbo.receipt r 
				INNER JOIN (SELECT a.accounting_document_no,  a.doc_type, a.year, a.company_code
						, a.customer_code, a.assignment_no, a.posting_date, a.document_date, a.created_date
						, a.reference_document, sum(a.document_amount) as document_amount
						, a.ccy, a.base_line_due_date
						, a.payment_term, a.day_term
					FROM (select  BELNR as accounting_document_no, BLART as doc_type
								, GJAHR as year, BUKRS as company_code
								--, kunnr as customer_code
								, CASE WHEN try_cast(kunnr AS int) IS NULL
									THEN kunnr
									ELSE cast(cast(kunnr AS int)as varchar(10))  end as customer_code
								,'' as assignment_no --, ZUONR as assignment_no
								, cast(BUDAT AS date) as posting_date, cast(bldat AS date) as document_date, cast(CPUDT AS date) as created_date
								, xblnr as reference_document
								, abs(sum(case SHKZG when 'H' then -1*(CONVERT (decimal(13,2),WRBTR)+CONVERT (decimal(13,2),WMWST))
												else (CONVERT (decimal(13,2),WRBTR)+CONVERT (decimal(13,2),WMWST))
												end)) as document_amount
								, WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
								, zterm as payment_term, CONVERT (decimal(3,0),ZBD1T) as day_term
						from dbo.bsid_temp
						where (blart in ('RB') AND substring(XBLNR,1,2) in ('AI')
							OR blart in ('DE') AND substring(XBLNR,1,2) in ('AG'))
						group by BELNR, BLART
								, GJAHR, BUKRS , kunnr
								, cast(BUDAT AS date), cast(bldat AS date), cast(CPUDT AS date)
								, xblnr, WAERS, zterm, CONVERT (decimal(3,0),ZBD1T)
					UNION 
						select  a.BELNR as accounting_document_no, a.BLART as doc_type
									, a.GJAHR as year, a.BUKRS as company_code
									--, a.kunnr as customer_code
									, CASE WHEN try_cast(a.kunnr AS int) IS NULL
										THEN a.kunnr
										ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
									,'' as assignment_no --, ZUONR as assignment_no
									, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date, cast(a.CPUDT AS date) as created_date
									, a.xblnr as reference_document
									, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),a.WMWST))
													else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),a.WMWST))
													end)) as document_amount
									, a.WAERS as ccy
									, null as base_line_due_date, a.zterm as payment_term
									, CONVERT (decimal(3,0),a.ZBD1T) as day_term
							from dbo.bsad_temp a
								INNER JOIN dbo.bkpf_temp b
									ON a.belnr =b.belnr
							and a.BUKRS =b.BUKRS
							AND a.gjahr = b.gjahr
							where (a.blart in ('RB') and substring(a.XBLNR,1,2) in ('AI')
								OR a.blart in ('DE') AND substring(a.XBLNR,1,2) in ('AG'))
								AND isnull(b.xreversal,'') = ''
							group by a.BELNR, a.BLART
									, a.GJAHR, a.BUKRS , a.kunnr
									, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
									, a.xblnr, a.WAERS, a.zterm
									, CONVERT (decimal(3,0),a.ZBD1T) ) a
				group by a.accounting_document_no,  a.doc_type
									, a.year, a.company_code, a.customer_code
									, a.assignment_no, a.posting_date, a.document_date, a.created_date
									, a.reference_document, a.ccy, a.base_line_due_date --, ZFBDT as base_line_due_date
									, a.payment_term, a.day_term) c 
					ON r.clearing_document_no = c.accounting_document_no
						AND r.year = c.year
						AND r.company_code =c.company_code
	/*----------------------end 3. receipt----AI-AG--------------*/

	/*------------------4. receipt----BJ-------------------*/
	insert into dbo.receipt
					(clearing_document_no, document_type, receipt_type
					, year, company_code, customer_code, assignment_no
					, posting_date, document_date, created_date, reference_document_no
					, document_amount, currency, base_line_due_date, payment_term, day_term)
	select  a.BELNR as accounting_document_no, a.BLART as doc_type, 'normal' AS [receipt_type]
				, a.GJAHR as year, a.BUKRS as company_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				,'' as assignment_no --, ZUONR as assignment_no
				, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date, a.xblnr as reference_document
				, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),b.WRBTR))
								else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),b.WRBTR))
								end)) as document_amount
				, a.WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
				, a.zterm as payment_term, CONVERT (decimal(3,0),a.ZBD1T) as day_term
		from dbo.bsid_temp a
			INNER JOIN dbo.bseg_temp b
				ON a.belnr = b.belnr
					AND a.BUKRS =b.BUKRS
					AND a.gjahr = b.gjahr
			LEFT JOIN dbo.receipt r
				ON a.BELNR = r.clearing_document_no
					AND a.BUKRS = r.company_code
					AND a.GJAHR = r.year
		where a.rstgr <>'B01'
			and b.mwart<>''
			and a.blart in ('RE')
			and substring(a.XBLNR,1,2) in ('BJ')
			AND( r.clearing_document_no IS NULL  AND r.company_code IS NULL AND r.year IS NULL)
		group by a.BELNR, a.BLART
				, a.GJAHR, a.BUKRS , a.kunnr
				--, ZUONR
				, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
				, a.xblnr, a.WAERS, a.zterm, CONVERT (decimal(3,0),a.ZBD1T)


	UPDATE r 
		set document_type = a.doc_type
					,receipt_type = 'normal'
					,customer_code = a.customer_code
					,assignment_no = a.assignment_no
					,posting_date = a.posting_date
					,document_date = a.document_date
					,created_date = a.created_date
					,reference_document_no = a.reference_document
					,document_amount = a.document_amount
					,currency = a.ccy
					,base_line_due_date = a.base_line_due_date
					,payment_term = a.payment_term
					,day_term = a.day_term
		from dbo.receipt r
			INNER JOIN (select  a.BELNR as accounting_document_no, a.BLART as doc_type
				, a.GJAHR as year, a.BUKRS as company_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				,'' as assignment_no --, ZUONR as assignment_no
				, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date, a.xblnr as reference_document
				, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),b.WRBTR))
								else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),b.WRBTR))
								end)) as document_amount
				, a.WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
				, a.zterm as payment_term, CONVERT (decimal(3,0),a.ZBD1T) as day_term
		from dbo.bsid_temp a
			INNER JOIN dbo.bseg_temp b
				ON a.belnr = b.belnr
					AND a.BUKRS =b.BUKRS
					AND a.gjahr = b.gjahr
		where a.rstgr <>'B01'
			and b.mwart<>''
			and a.blart in ('RE')
			and substring(a.XBLNR,1,2) in ('BJ')
		group by a.BELNR, a.BLART
				, a.GJAHR, a.BUKRS , a.kunnr
				, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
				, a.xblnr, a.WAERS, a.zterm, CONVERT (decimal(3,0),a.ZBD1T)) a
			ON a.accounting_document_no = r.clearing_document_no
					AND a.company_code = r.company_code
					AND a.year = r.year
	/*----------------------4. end receipt- BJ------------------*/

	
	/*------------------5. receipt BSAD-----BJ------------------*/
	insert into dbo.receipt
					(clearing_document_no, document_type, receipt_type
					, year, company_code, customer_code, assignment_no
					, posting_date, document_date, created_date, reference_document_no
					, document_amount, currency, base_line_due_date, payment_term, day_term)
	select  a.BELNR as accounting_document_no, a.BLART as doc_type, 'normal' AS [receipt_type]
				, a.GJAHR as year, a.BUKRS as company_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				,'' as assignment_no --, ZUONR as assignment_no
				, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date, a.xblnr as reference_document
				, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),c.WRBTR))
								else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),c.WRBTR))
								end)) as document_amount
				, a.WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
				, a.zterm as payment_term, CONVERT (decimal(3,0),a.ZBD1T) as day_term
		from dbo.BSAD_Temp a
			INNER JOIN dbo.bkpf_temp b
				ON a.belnr = b.belnr
					AND a.BUKRS =b.BUKRS
					AND a.gjahr = b.gjahr
			INNER JOIN dbo.BSEG_Temp c
				ON a.belnr = c.belnr
				AND a.BUKRS =c.BUKRS
				AND a.gjahr = c.gjahr
			LEFT JOIN dbo.receipt r
				ON a.BELNR = r.clearing_document_no
					AND a.BUKRS = r.company_code
					AND a.GJAHR = r.year
		where a.rstgr <>'B01'
			and c.mwart<>''
			and a.blart in ('RE')
			and substring(a.XBLNR,1,2) in ('BJ')
			and isnull(b.xreversal,'') = ''
			AND( r.clearing_document_no IS NULL  AND r.company_code IS NULL AND r.year IS NULL)
		group by a.BELNR, a.BLART
				, a.GJAHR, a.BUKRS , a.kunnr
				--, ZUONR
				, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
				, a.xblnr, a.WAERS, a.zterm, CONVERT (decimal(3,0),a.ZBD1T)


	UPDATE r 
		set document_type = a.doc_type
					,receipt_type = 'normal'
					,customer_code = a.customer_code
					,assignment_no = a.assignment_no
					,posting_date = a.posting_date
					,document_date = a.document_date
					,created_date = a.created_date
					,reference_document_no = a.reference_document
					,document_amount = a.document_amount
					,currency = a.ccy
					,base_line_due_date = a.base_line_due_date
					,payment_term = a.payment_term
					,day_term = a.day_term
		from dbo.receipt r
			INNER JOIN (select  a.BELNR as accounting_document_no, a.BLART as doc_type
				, a.GJAHR as year, a.BUKRS as company_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				,'' as assignment_no --, ZUONR as assignment_no
				, cast(a.BUDAT AS date) as posting_date, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date, a.xblnr as reference_document
				, abs(sum(case a.SHKZG when 'H' then -1*(CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),c.WRBTR))
								else (CONVERT (decimal(13,2),a.WRBTR)+CONVERT (decimal(13,2),c.WRBTR))
								end)) as document_amount
				, a.WAERS as ccy, null as base_line_due_date --, ZFBDT as base_line_due_date
				, a.zterm as payment_term, CONVERT (decimal(3,0),a.ZBD1T) as day_term
		from dbo.bsid_temp a
			INNER JOIN dbo.bkpf_temp b
				ON a.belnr = b.belnr
					AND a.BUKRS =b.BUKRS
					AND a.gjahr = b.gjahr
			INNER JOIN dbo.bseg_temp c
				ON a.belnr = c.belnr
					AND a.BUKRS =c.BUKRS
					AND a.gjahr = c.gjahr
		where a.rstgr <>'B01'
			and c.mwart<>''
			and a.blart in ('RE')
			and substring(a.XBLNR,1,2) in ('BJ')
			and isnull(b.xreversal,'') = ''
		group by a.BELNR, a.BLART
				, a.GJAHR, a.BUKRS , a.kunnr
				, cast(a.BUDAT AS date), cast(a.bldat AS date), cast(a.CPUDT AS date)
				, a.xblnr, a.WAERS, a.zterm, CONVERT (decimal(3,0),a.ZBD1T)) a
			ON a.accounting_document_no = r.clearing_document_no
					AND a.company_code = r.company_code
					AND a.year = r.year
	/*----------------------5. end bsad receipt----BJ---------------*/


	/*------------------6. advance receipt-----------------------*/
	DECLARE advance_receipt_cursor CURSOR FAST_FORWARD FOR
		select distinct FI_DOCUMENT as accounting_document_no, b.BLART as doc_type
				, FI_GJAHR as year, COM_CODE as company_code
				--, CUSTOMER as customer_code
				, CASE WHEN try_cast(CUSTOMER AS int) IS NULL
					THEN CUSTOMER
					ELSE cast(cast(CUSTOMER AS int)as varchar(10))  end as customer_code
				, b.XBLNR as assignment_no
				, null as posting_date
				, cast(ADV_REC_DATE AS date) as document_date
				, cast(CREATE_DATE AS date) as created_date
				, ADV_REC_NO as reference_document
				, CASE WHEN CHARINDEX('-',amount) > 0
					THEN (-1)*CAST(SUBSTRING(amount,1,CHARINDEX('-',amount)-1) AS numeric(13,2))
					ELSE CAST(amount AS numeric(13,2))
				  end as document_amount
				, CURRENCY as ccy
				, null as base_line_due_date, '' as payment_term
				, null as day_term
				, cancel_by
				--, null as cancel_date
				,  case rtrim(ltrim(isnull(cancel_reason,''))) when '' then null
					else cast(cancel_date as date)
					end  as cancel_date
				, cancel_reason
		from ZFI_0090_ADVREC_Temp a, BKPF_Temp b
		where a.FI_DOCUMENT = b.BELNR
		and a.COM_CODE = b.bukrs
		and cast(a.CREATE_DATE AS date) >= cast('2017-12-06' as date)

	OPEN advance_receipt_cursor;

	FETCH NEXT from advance_receipt_cursor
		INTO	@clearing_document_no
				, @document_type
				, @year
				, @company_code
				, @customer_code
				, @assignment_no
				, @posting_date
				, @document_date
				, @created_date
				, @reference_document_no
				, @document_amount
				, @currency
				, @base_line_due_date
				, @payment_term
				, @day_term
				, @cancel_by, @cancel_date, @cancel_reason

	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (EXISTS(select 1 from receipt b
						where b.clearing_document_no = @clearing_document_no
						and b.year = @year
						and b.company_code = @company_code
						and b.reference_document_no = @reference_document_no))
				begin
					update receipt
					set document_type = @document_type
						,receipt_type = 'advance'
						,customer_code = @customer_code
						,assignment_no = @assignment_no
						,posting_date = @posting_date
						,document_date = @document_date
						,created_date = @created_date
						,document_amount = @document_amount
						,currency = @currency
						,base_line_due_date = @base_line_due_date
						,payment_term = @payment_term
						,day_term = @day_term
						,cancel_by = @cancel_by
						,cancel_date = @cancel_date
						,cancel_reason = @cancel_reason
					where clearing_document_no = @clearing_document_no
							and year = @year
							and company_code = @company_code
							and reference_document_no = @reference_document_no
				end
			ELSE
				begin
					insert into receipt
						(clearing_document_no
						, document_type
						, receipt_type
						, year
						, company_code
						, customer_code
						, assignment_no
						, posting_date
						, document_date
						, created_date
						, reference_document_no
						, document_amount
						, currency
						, base_line_due_date
						, payment_term
						, day_term
						, cancel_by
						, cancel_date
						, cancel_reason)
					values
						(@clearing_document_no
						, @document_type
						, 'advance'
						, @year
						, @company_code
						, @customer_code
						, @assignment_no
						, @posting_date
						, @document_date
						, @created_date
						, @reference_document_no
						, @document_amount
						, @currency
						, @base_line_due_date
						, @payment_term
						, @day_term
						, @cancel_by
						, @cancel_date
						, @cancel_reason)
					end

				if isnull(@cancel_reason,'') <> ''
				begin
					update receipt
					set is_active = 0
					where clearing_document_no = @clearing_document_no
							and year = @year
							and company_code = @company_code
							and reference_document_no = @reference_document_no


					/*if not exists ( select 1 from dbo.cheque_control_document_item cdi
											inner join dbo.cheque_control_document cd
												on cd.cheque_control_id = cdi.cheque_control_id
										where cheque_control_id = @cheque_control_id
										and cheque_no is null)
						begin
							exec dbo.update_cheque_control_document_status
								@cheque_control_id = @cheque_control_id
								,@status = 'COMPLETE'
								,@updated_by  = @updated_by
						end*/
				end

				FETCH NEXT from advance_receipt_cursor
				INTO @clearing_document_no
				, @document_type
				, @year
				, @company_code
				, @customer_code
				, @assignment_no
				, @posting_date
				, @document_date
				, @created_date
				, @reference_document_no
				, @document_amount
				, @currency
				, @base_line_due_date
				, @payment_term
				, @day_term
				, @cancel_by, @cancel_date, @cancel_reason
			END

	CLOSE advance_receipt_cursor;

	DEALLOCATE advance_receipt_cursor;
	/*----------------------6. end advance receipt-------------------*/


	/*------------------7. invoice ค่าล่าช้า + เพิ่มหนี้ + BG-------------------*/
	DECLARE debt_cursor CURSOR FAST_FORWARD FOR
		select DISTINCT CASE WHEN a.blart = 'DA' OR a.BLART = 'DC'
				THEN  XBLNR + '-' + substring(GJAHR,3,2)
					ELSE XBLNR end  as invoice_no, BLART as invoice_type/*document_type*/
				, cast(bldat AS date) as invoice_date/*document_date*/, BUKRS as company_code
				--,  kunnr as customer_code
				, CASE WHEN try_cast(kunnr AS int) IS NULL
					THEN kunnr
					ELSE cast(cast(kunnr AS int)as varchar(10))  end as customer_code
				, zterm as payment_term
				--, CONVERT (decimal(13,2),WRBTR) as net_amount /*document_amount*/
				, CASE blart WHEN 'RC'
					THEN (-1)*CONVERT (decimal(13,2),WRBTR)
					ELSE CONVERT (decimal(13,2),WRBTR)  /*document_amount*/
				  end as net_amount
				, null as tax_amount
				, WAERS as CCY, BELNR as 'Reference Document Number' /*swap field*/, ZUONR as 'Assignment number'
				, 'INTERFACE'	, DATEADD(day,cast(ZBD1T as int),cast(bldat AS date)) as due_date
				, CASE blart WHEN 'RC'
					THEN cast(ZFBDT AS date)
					ELSE DATEADD(day,cast(ZBD1T as int),cast(ZFBDT AS date))
				  end as net_due_date
				, a.mwskz AS [vat_type]
		from bsid_temp a , master_customer b
			  where ((a.ZUONR like N'%ค่าล่าช้า%' AND a.BLART = 'DA')
				OR a.BLART in ('RD','RC')
				OR (a.BUKRS = '6140' AND a.BLART = 'DA' AND a.ZUONR NOT LIKE N'%ค่าล่าช้า%')
				OR a.SGTXT like N'%ค่าธรรมเนียมค้ำประกันธนาคาร%' AND a.BLART = 'DC')
			 and  a.kunnr = right(RTRIM(REPLICATE('0',10) + b.customer_code),10)-- b.customer_code
			and b.country = 'TH'
			AND ISNULL(a.xblnr,'') <> '';

	OPEN debt_cursor;

	FETCH NEXT from debt_cursor
		INTO @invoice_no, @invoice_type, @invoice_date, @company_code
					, @customer_code, @payment_term
					, @invoice_amount
					, @tax_amount
					, @currency, @referenece_document_no, @assignment_no
					, @created_by, @due_date, @net_due_date
					, @vat_type

	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (EXISTS(select 1 from invoice_header b
						where b.invoice_no = @invoice_no
						and b.company_code = @company_code))

				update invoice_header
                set invoice_date = @invoice_date
					, company_code = @company_code
					, customer_code = @customer_code
					, payment_term = @payment_term
					, invoice_amount = @invoice_amount
					, tax_amount = @tax_amount
					, currency = @currency
					, referenece_document_no = @referenece_document_no
					, assignment_no = @assignment_no
					, due_date = @due_date
					, net_due_date = @net_due_date
					, vat_type = @vat_type
				where invoice_no = @invoice_no
				and company_code = @company_code
			ELSE
				insert into invoice_header
				 (invoice_no, invoice_type, invoice_date, company_code
					, customer_code, payment_term
					, invoice_amount
					, tax_amount
					, currency, referenece_document_no, assignment_no
					, created_by, due_date, net_due_date
					, vat_type)
				values
				(@invoice_no, @invoice_type, @invoice_date, @company_code
					, @customer_code, @payment_term
					, @invoice_amount
					, @tax_amount
					, @currency, @referenece_document_no, @assignment_no
					, @created_by, @due_date, @net_due_date
					, @vat_type)

				FETCH NEXT from debt_cursor
				INTO @invoice_no, @invoice_type, @invoice_date, @company_code
					, @customer_code, @payment_term
					, @invoice_amount
					, @tax_amount
					, @currency, @referenece_document_no, @assignment_no
					, @created_by, @due_date, @net_due_date
					, @vat_type
			END

	CLOSE debt_cursor;
	DEALLOCATE debt_cursor;

	/*insert into dbo.invoice_header
				 (invoice_no, invoice_type, invoice_date, company_code
					, customer_code, payment_term, invoice_amount, tax_amount
					, currency, referenece_document_no, assignment_no
					, created_by, due_date, net_due_date, vat_type)
	select DISTINCT CASE WHEN a.blart = 'DA' OR a.BLART = 'DC'
				THEN  a.XBLNR + '-' + substring(a.GJAHR,3,2)
					ELSE a.XBLNR end  as invoice_no, a.BLART as invoice_type/*document_type*/
				, cast(a.bldat AS date) as invoice_date/*document_date*/, a.BUKRS as company_code
				--,  kunnr as customer_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				, a.zterm as payment_term
				--, CONVERT (decimal(13,2),WRBTR) as net_amount /*document_amount*/
				, CASE a.blart WHEN 'RC'
					THEN (-1)*CONVERT (decimal(13,2),a.WRBTR)
					ELSE CONVERT (decimal(13,2),a.WRBTR)  /*document_amount*/
				  end as net_amount
				, null as tax_amount
				, a.WAERS as CCY, a.BELNR as 'Reference Document Number' /*swap field*/, a.ZUONR as 'Assignment number'
				, 'INTERFACE'	, DATEADD(day,cast(a.ZBD1T as int),cast(a.bldat AS date)) as due_date
				, CASE a.blart WHEN 'RC'
					THEN cast(a.ZFBDT AS date)
					ELSE DATEADD(day,cast(a.ZBD1T as int),cast(a.ZFBDT AS date))
				  end as net_due_date
				, a.mwskz AS [vat_type]
		from dbo.bsid_temp a 
			INNER JOIN dbo.master_customer b
				ON a.kunnr = right(RTRIM(REPLICATE('0',10) + b.customer_code),10)-- b.customer_code
			LEFT JOIN dbo.invoice_header ih
				ON CASE WHEN a.blart = 'DA' OR a.BLART = 'DC' THEN  a.XBLNR + '-' + substring(a.GJAHR,3,2)
						ELSE a.XBLNR END = ih.invoice_no
					AND a.BUKRS = ih.company_code
			  where ((a.ZUONR like N'%ค่าล่าช้า%' AND a.BLART = 'DA')
				OR a.BLART in ('RD','RC')
				OR (a.BUKRS = '6140' AND a.BLART = 'DA' AND a.ZUONR NOT LIKE N'%ค่าล่าช้า%')
				OR a.SGTXT like N'%ค่าธรรมเนียมค้ำประกันธนาคาร%' AND a.BLART = 'DC')
			and b.country = 'TH'
			AND ISNULL(a.xblnr,'') <> ''
			AND (ih.invoice_no IS NULL AND ih.company_code IS NULL)

	update ih
                set invoice_date = @invoice_date
					, company_code = @company_code
					, customer_code = @customer_code
					, payment_term = @payment_term
					, invoice_amount = @invoice_amount
					, tax_amount = @tax_amount
					, currency = @currency
					, referenece_document_no = @referenece_document_no
					, assignment_no = @assignment_no
					, due_date = @due_date
					, net_due_date = @net_due_date
					, vat_type = @vat_type
	 FROM dbo.invoice_header ih 
		INNER JOIN dbo.bsid_temp a 
				ON CASE WHEN a.blart = 'DA' OR a.BLART = 'DC' THEN  a.XBLNR + '-' + substring(a.GJAHR,3,2)
						ELSE a.XBLNR END = ih.invoice_no
					AND a.BUKRS = ih.company_code 
			INNER JOIN dbo.master_customer b
				ON a.kunnr = right(RTRIM(REPLICATE('0',10) + b.customer_code),10)-- b.customer_code
			  where ((a.ZUONR like N'%ค่าล่าช้า%' AND a.BLART = 'DA')
				OR a.BLART in ('RD','RC')
				OR (a.BUKRS = '6140' AND a.BLART = 'DA' AND a.ZUONR NOT LIKE N'%ค่าล่าช้า%')
				OR a.SGTXT like N'%ค่าธรรมเนียมค้ำประกันธนาคาร%' AND a.BLART = 'DC')
			and b.country = 'TH'
			AND ISNULL(a.xblnr,'') <> ''*/
	/*------------------7. end ค่าล่าช้า+ เพิ่มหนี้ + BG-------------*/

	/*------------------8. clearing-bsid----------------------*/
	DECLARE clearing_cursor CURSOR FAST_FORWARD FOR
		select vbeln as invoice_no, BUKRS as company_code
				--, kunnr as customer_code
				, CASE WHEN try_cast(kunnr AS int) IS NULL
					THEN kunnr
					ELSE cast(cast(kunnr AS int)as varchar(10))  end as customer_code
				, ZUONR as assignment_no, GJAHR as year
				, BELNR as accounting_document_no
				, AUGBL as clearing_document_no
				, buzei as line_no
				, case augdt when  '00000000' then null else cast(AUGDT AS date) end as clearing_date
				, cast(BUDAT AS date) as posting_date
				, cast(bldat AS date) as document_date
				, cast(CPUDT AS date) as created_date
				, WAERS as currency
				, xblnr as reference_document
				, REBZG as reference_clearing_document
				, SHKZG as indicator
				, BLART as document_type, MONAT as period
				, CONVERT (decimal(13,2),DMBTR) as local_amount
				, CONVERT (decimal(13,2),WRBTR) as document_amount
				, SAKNR as gl_account
				, ZFBDT as base_line_due_date
				, KIDNO as payment_reference
				, BSAD_MOVE
		from bsid_temp
		--where blart not in ('RA','RD','RC')

	OPEN clearing_cursor;

	FETCH NEXT from clearing_cursor
		INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference
				, @bsad_move

	WHILE @@FETCH_STATUS = 0
		BEGIN
			if @document_type = 'RA' or @document_type = 'RD' or @document_type = 'RC'  or @document_type = 'DA' or @document_type = 'DC' /*add ค่าธรรมเนียม BG*/
				BEGIN
					/*Condition for DC ค่าธรรมเนียม*/
					if @document_type = 'DA' OR @document_type = 'DC'
						begin
							if @bsad_move = 'X'
								begin
									update invoice_header
									set collection_status = NULL, clearing_document_no = null
									where invoice_no = @reference_document_no + '-' + substring(@year,3,2)
									and company_code = @company_code
								end
							else
								begin
									update invoice_header
									set collection_amount = 0
									where invoice_no = @reference_document_no + '-' + substring(@year,3,2)
									and isnull(collection_status,'') <> 'COMPLETE'
									and company_code = @company_code
								end

							if @payment_reference <> ''
							begin
								update invoice_header
								set bill_presentment_status = 'COMPLETE'
									,bill_presentment_remark = 'สร้างใบคุมบนSAP: ' + @payment_reference
								where invoice_no = @reference_document_no  + '-' + substring(@year,3,2)
								and company_code = @company_code
								and isnull(bill_presentment_status,'') <> 'COMPLETE'
								and company_code not in ('0480','2220','0230')
							end
						end
					else
						begin
							if @bsad_move = 'X'
								begin
									update invoice_header
									set collection_status = null, clearing_document_no = null
									where invoice_no = @reference_document_no
									and company_code = @company_code
								end
							else
								begin
									update invoice_header
									set collection_amount = 0
									where invoice_no = @reference_document_no
									and isnull(collection_status,'') <> 'COMPLETE'
									and company_code = @company_code
								end

							if @payment_reference <> ''
							begin
								update invoice_header
								set bill_presentment_status = 'COMPLETE'
									,bill_presentment_remark = 'สร้างใบคุมบนSAP: ' + @payment_reference
								where invoice_no = @reference_document_no
								and company_code = @company_code
								and isnull(bill_presentment_status,'') <> 'COMPLETE'
								and company_code not in ('0480','2220','0230')
							end
						end
				end

			else
				begin
					IF (EXISTS(select 1 from invoice_clearing_history b
							where b.accounting_document_no = @accounting_document_no
							and b.year = @year
							and b.company_code = @company_code
							and b.line_no = @line_no))
						begin
							update invoice_clearing_history
							set posting_date = @posting_date
								, reference_clearing_document = @reference_clearing_document
								,document_date = @document_date
								,clearing_document_no = @clearing_document_no
								,clearing_date = @clearing_date
								,created_date = @created_date
								,bsad_move = @bsad_move
							where  accounting_document_no = @accounting_document_no
									and year = @year
									and company_code = @company_code
									and line_no = @line_no
						end
					ELSE
						begin
							insert into invoice_clearing_history
								(invoice_no
							, company_code
							, customer_code
							, assignment_no
							, year
							, accounting_document_no
							, clearing_document_no
							, line_no
							, clearing_date
							, posting_date
							, document_date
							, created_date
							, currency
							, reference_document
							, reference_clearing_document
							, indicator
							, document_type
							, period
							, local_amount
							, document_amount
							, gl_account
							, base_line_due_date
							, payment_reference
							, bsad_move)
							values
								(@invoice_no
							, @company_code
							, @customer_code
							, @assignment_no
							, @year
							, @accounting_document_no
							, @clearing_document_no
							, @line_no
							, @clearing_date
							, @posting_date
							, @document_date
							, @created_date
							, @currency
							, @reference_document_no
							, @reference_clearing_document
							, @indicator
							, @document_type
							, @period
							, @local_amount
							, @document_amount
							, @gl_account
							, @base_line_due_date
							, @payment_reference
							, @bsad_move)


							/*update invoice_header
							set collection_amount = case invoice_type
																when 'RC' then isnull(collection_amount,0) - @document_amount
																else isnull(collection_amount,0) + @document_amount
																end
							where invoice_no = @invoice_no*/
						end

						/*update a
						set a.due_date = DATEADD(day,cast(ZBD1T as int),a.invoice_date)
							, a.net_due_date = DATEADD(day,cast(ZBD1T as int),cast(ZFBDT AS date))
							, a.referenece_document_no = b.BELNR
						from invoice_header a, BSID_Temp b
						where a.invoice_no = b.vbeln
						and b.BLART = 'RA'
						and a.company_code = b.bukrs*/
				end


				FETCH NEXT from clearing_cursor
				INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference
				, @bsad_move
			END

	CLOSE clearing_cursor;
	DEALLOCATE clearing_cursor;
	/*----------------------8. end bsid clearing-------------------*/


	/*------------------9. clearing-bsad----------------------*/
	DECLARE clearing_bsad_cursor CURSOR FAST_FORWARD FOR
		select a.vbeln as invoice_no, a.BUKRS as company_code
				--, a.kunnr as customer_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				, a.ZUONR as assignment_no, a.GJAHR as year
				, a.BELNR as accounting_document_no
				, a.AUGBL as clearing_document_no
				, a.buzei as line_no
				, case a.augdt when  '00000000' then null else cast(a.AUGDT AS date) end as clearing_date
				, cast(a.BUDAT AS date) as posting_date
				, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date
				, a.WAERS as currency
				, a.xblnr as reference_document
				, a.REBZG as reference_clearing_document
				, a.SHKZG as indicator
				, a.BLART as document_type, a.MONAT as period
				, CONVERT (decimal(13,2),a.DMBTR) as local_amount
				, CONVERT (decimal(13,2),a.WRBTR) as document_amount
				, a.SAKNR as gl_account
				, a.ZFBDT as base_line_due_date
				, a.KIDNO as payment_reference
		from bsad_temp a, bkpf_temp b
		where a.blart not in ('RA','RD','RC') /*BPACEN1: ADD DC for let DE clear DC*/
		and a.belnr = b.belnr
		and a.bukrs = b.bukrs
		AND a.gjahr = b.gjahr
		and isnull(b.xreversal,'') = ''
		order by a.augbl, a.blart

	OPEN clearing_bsad_cursor;

	FETCH NEXT from clearing_bsad_cursor
		INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference

	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (EXISTS(select 1 from invoice_clearing_history b
						where b.accounting_document_no = @accounting_document_no
						and b.year = @year
						and b.company_code = @company_code
						and b.line_no = @line_no))
				begin
					update invoice_clearing_history
					set posting_date = @posting_date
						, reference_clearing_document = @reference_clearing_document
						,document_date = @document_date
						,clearing_document_no = @clearing_document_no
						,clearing_date = @clearing_date
						,created_date = @created_date
					where  accounting_document_no = @accounting_document_no
							and year = @year
							and company_code = @company_code
							and line_no = @line_no
				end
			ELSE
				begin
					insert into invoice_clearing_history
						(invoice_no
					, company_code
					, customer_code
					, assignment_no
					, year
					, accounting_document_no
					, clearing_document_no
					, line_no
					, clearing_date
					, posting_date
					, document_date
					, created_date
					, currency
					, reference_document
					, reference_clearing_document
					, indicator
					, document_type
					, period
					, local_amount
					, document_amount
					, gl_account
					, base_line_due_date
					, payment_reference)
					values
						(@invoice_no
					, @company_code
					, @customer_code
					, @assignment_no
					, @year
					, @accounting_document_no
					, @clearing_document_no
					, @line_no
					, @clearing_date
					, @posting_date
					, @document_date
					, @created_date
					, @currency
					, @reference_document_no
					, @reference_clearing_document
					, @indicator
					, @document_type
					, @period
					, @local_amount
					, @document_amount
					, @gl_account
					, @base_line_due_date
					, @payment_reference)
				end

				update invoice_header
				set collection_amount = isnull(invoice_amount,0) + isnull(tax_amount,0)
					,collection_status = 'COMPLETE'
					,clearing_document_no = @clearing_document_no
				where invoice_no in (select vbeln FROM bsad_temp where AUGBL = @clearing_document_no)
					AND invoice_type not in  ('DA','DZ','DC')
					AND company_code = @company_code

				update invoice_header
				set collection_amount = isnull(invoice_amount,0) + isnull(tax_amount,0)
					,collection_status = 'COMPLETE'
					,clearing_document_no = @clearing_document_no
				--where invoice_no in (select xblnr from bsad_temp where AUGBL = @clearing_document_no and invoice_type not in  ('DA','DZ'))
				where invoice_no in (select xblnr + '-' + substring(GJAHR,3,2) from bsad_temp where AUGBL = @clearing_document_no and invoice_type  in  ('DA','DC'))
				and invoice_type in ('DA','DC')
				and company_code = @company_code

				update invoice_header
				set collection_amount = 0
					,collection_status = 'CANCEL'
				where invoice_no in (select xblnr + '-' + substring(GJAHR,3,2)  from bsad_temp where AUGBL = @clearing_document_no and invoice_type in  ('DZ'))
				and invoice_type in ('DA','DC')
				and company_code = @company_code


				FETCH NEXT from clearing_bsad_cursor
				INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference
			END

	CLOSE clearing_bsad_cursor;
	DEALLOCATE clearing_bsad_cursor;
	/*----------------------9. end bsad clearing-------------------*/


	/*------------------10. report clearing bsad----------------------*/
	DECLARE report_clearing_bsad_cursor CURSOR FAST_FORWARD FOR
		select a.vbeln as invoice_no, a.BUKRS as company_code
				--, a.kunnr as customer_code
				, CASE WHEN try_cast(a.kunnr AS int) IS NULL
					THEN a.kunnr
					ELSE cast(cast(a.kunnr AS int)as varchar(10))  end as customer_code
				, a.ZUONR as assignment_no, a.GJAHR as year
				, a.BELNR as accounting_document_no
				, a.AUGBL as clearing_document_no
				, a.buzei as line_no
				, case a.augdt when  '00000000' then null else cast(a.AUGDT AS date) end as clearing_date
				, cast(a.BUDAT AS date) as posting_date
				, cast(a.bldat AS date) as document_date
				, cast(a.CPUDT AS date) as created_date
				, a.WAERS as currency
				, a.xblnr as reference_document
				, a.REBZG as reference_clearing_document
				, a.SHKZG as indicator
				, a.BLART as document_type, a.MONAT as period
				, CONVERT (decimal(13,2),a.DMBTR) as local_amount
				, CONVERT (decimal(13,2),a.WRBTR) as document_amount
				, a.SAKNR as gl_account
				, a.ZFBDT as base_line_due_date
				, a.KIDNO as payment_reference
		from bsad_temp a, bkpf_temp b
		where a.blart not in ('RA','RD','RC') /*BPACEN1: ADD DC for let DE clear DC*/
		and a.belnr = b.belnr
		and a.bukrs = b.bukrs
		AND a.gjahr = b.gjahr
		and isnull(b.xreversal,'') = ''
		and a.shkzg = 'H'
		and a.augbl = a.belnr
		order by a.augbl, a.blart

	OPEN report_clearing_bsad_cursor;

	FETCH NEXT from report_clearing_bsad_cursor
		INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference

	WHILE @@FETCH_STATUS = 0
		BEGIN
			/*-------------invoice clearing detail for shown in report---------------*/
				IF (EXISTS(select 1 from invoice_clearing_detail b
						where b.accounting_document_no = @accounting_document_no
						and b.year = @year
						and b.company_code = @company_code
						and b.line_no = @line_no))
				begin
					update invoice_clearing_detail
					set posting_date = @posting_date
						, reference_clearing_document = @reference_clearing_document
						, reference_document = @reference_document_no
						,document_date = @document_date
						,clearing_document_no = @clearing_document_no
						,clearing_date = @clearing_date
						,created_date = @created_date
						,updated_date_time = getdate()
					where  accounting_document_no = @accounting_document_no
							and year = @year
							and company_code = @company_code
							and line_no = @line_no
				end
			ELSE
				begin
					insert into invoice_clearing_detail
						(invoice_no
					, company_code
					, customer_code
					, assignment_no
					, year
					, accounting_document_no
					, clearing_document_no
					, line_no
					, clearing_date
					, posting_date
					, document_date
					, created_date
					, currency
					, reference_document
					, reference_clearing_document
					, indicator
					, document_type
					, period
					, local_amount
					, document_amount
					, gl_account
					, base_line_due_date
					, payment_reference
					, is_actived
					, is_count)
					values
						(@invoice_no
					, @company_code
					, @customer_code
					, @assignment_no
					, @year
					, @accounting_document_no
					, @clearing_document_no
					, @line_no
					, @clearing_date
					, @posting_date
					, @document_date
					, @created_date
					, @currency
					, @reference_document_no
					, @reference_clearing_document
					, @indicator
					, @document_type
					, @period
					, @local_amount
					, @document_amount
					, @gl_account
					, @base_line_due_date
					, @payment_reference
					, 1
					, 1)
				end


				/*--------------end clearing detail for shown in report bsad-----------------*/

				FETCH NEXT from report_clearing_bsad_cursor
				INTO	@invoice_no
				, @company_code
				, @customer_code
				, @assignment_no
				, @year
				, @accounting_document_no
				, @clearing_document_no
				, @line_no
				, @clearing_date
				, @posting_date
				, @document_date
				, @created_date
				, @currency
				, @reference_document_no
				, @reference_clearing_document
				, @indicator
				, @document_type
				, @period
				, @local_amount
				, @document_amount
				, @gl_account
				, @base_line_due_date
				, @payment_reference
			END

	CLOSE report_clearing_bsad_cursor;
	DEALLOCATE report_clearing_bsad_cursor;
	/*----------------------10. end report clearing-------------------*/


	/*------Update Partial Payment-------*/
		UPDATE a
				set a.collection_amount =
					--case invoice_type when 'RC' then -1*(select sum(CONVERT (decimal(13,2),WRBTR))
					CASE WHEN invoice_type = 'DC' OR  invoice_type = 'RC' then -1*(select sum(CONVERT (decimal(13,2),WRBTR))
						FROM bsid_temp c where c.rebzg = b.belnr
						and c.kunnr = b.KUNNR
						and c.BUKRS = b.BUKRS)
					else
						(select sum(CONVERT (decimal(13,2),WRBTR))
						FROM bsid_temp c where c.rebzg = b.belnr
						and c.kunnr = b.KUNNR
						and c.BUKRS = b.BUKRS)
					end
				from invoice_header a, BSID_Temp b
				where a.invoice_no = CASE WHEN a.invoice_type = 'DA' OR a.invoice_type = 'DC'
							then b.xblnr + '-' + SUBSTRING(b.gjahr,3,2) ELSE b.xblnr end
				and a.company_code =b.BUKRS
				--AND a.invoice_type NOT IN ('DA','DC')
				and isnull(a.collection_status,'') <> 'COMPLETE';
	/*----------------------------------------*/

	/*------Update Partial Payment DA DC-------*/
	/*update a
				set a.collection_amount =
					case invoice_type when 'DC' then -1*(select sum(CONVERT (decimal(13,2),WRBTR))
						FROM bsid_temp c where c.rebzg = b.belnr
						and c.kunnr = b.KUNNR
						and c.BUKRS = b.BUKRS)
					else
						(select sum(CONVERT (decimal(13,2),WRBTR))
						FROM bsid_temp c where c.rebzg = b.belnr
						and c.kunnr = b.KUNNR
						and c.BUKRS = b.BUKRS)
					end
				from invoice_header a, BSID_Temp b
				where a.invoice_no = b.xblnr + '-' + SUBSTRING(b.gjahr,3,2)
				and a.company_code =b.BUKRS
				AND a.invoice_type IN ('DA','DC')
					and isnull(a.collection_status,'') <> 'COMPLETE';*/
	/*----------------------------------------*/

	/*-----update cancel receipt*/

	update a
	set a.is_active = 0
	from dbo.receipt a, dbo.BKPF_Temp b
	where a.clearing_document_no = b.belnr
	and a.company_code =b.BUKRS
	AND a.year = b.gjahr
	and b.XREVERSAL = '1'
	/*------------------------------*/

	/*-----------------update reverse clearing---------------*/
	update a
	set a.bsad_move = 'R'
		,updated_date_time = getdate()
		,is_actived = 0
		,is_count = 0
		,is_reverse = 1
	from dbo.invoice_clearing_detail a, dbo.BKPF_Temp b
	where a.accounting_document_no = b.belnr
	and a.company_code =b.BUKRS
	AND a.year = b.gjahr
	and b.XREVERSAL = '1'
	and is_count = 1
	/*-----------------end update reverse-------------------*/


	/*------------Clear Data is_ignore = 1; filter customer not need to do */
	DELETE r
	FROM receipt r
	INNER JOIN master_customer c
		ON r.customer_code = c.customer_code
	WHERE isnull(c.is_ignore,0) = '1'

	/*-------------end delete--------------------------*/

	/*--------update billpresentment behavior for create document control on SAP*/
	update ih
							set ih.bill_presentment_behavior_id = mcb.bill_presentment_behavior_id,
							ih.bill_presentment_calculation_log = null
							from invoice_header ih
							inner join master_customer_company mcc
							on ih.company_code = mcc.company_code
							and ih.customer_code =  mcc.customer_code
							inner join master_customer_bill_presentment_behavior mcb
							on mcc.customer_company_id = mcb.customer_company_id
								and ih.payment_term = mcb.payment_term_code
								and (DATEDIFF(DAY,  mcb.valid_date_from,ih.invoice_date) >= 0)
								and (mcb.valid_date_to is null or DATEDIFF(DAY,  mcb.valid_date_to,ih.invoice_date) >= 0)
								and DAY(ih.invoice_date) between mcb.invoice_period_from and mcb.invoice_period_to
								and mcb.is_active = 1
							where ih.bill_presentment_status = 'COMPLETE'
							--and ih.bill_presentment_remark like '%สร้างใบคุมบนSAP:%'
							and ih.bill_presentment_behavior_id is null
	/*--------end update-------------------------------*/


	/*insert Receipt When update by bkpf*/
	begin
		IF (not EXISTS(select 1 from receipt c, invoice_clearing_detail a, BKPF_Temp b
							where isnull(a.reference_document,'') = ''
							and a.accounting_document_no = b.belnr
							and substring(B.XBLNR,1,2) in ('AI')
							and a.year = b.gjahr
							and a.company_code = b.bukrs
							and c.clearing_document_no = a.clearing_document_no
							and c.year = a.year
							and c.company_code = a.company_code	))
				begin
				/*	update c
					set c.reference_document_no = b.xblnr
					from dbo.receipt c, invoice_clearing_detail a, BKPF_Temp b
							where isnull(a.reference_document,'') = ''
							and a.accounting_document_no = b.belnr
							and substring(B.XBLNR,1,2) in ('AI')
							and a.year = b.gjahr
							and a.company_code = b.bukrs
							and c.clearing_document_no = a.clearing_document_no
							and c.year = a.year
							and c.company_code = a.company_code
				end

			ELSE*/
				insert into receipt
								(clearing_document_no
								, document_type
								, receipt_type
								, year
								, company_code
								, customer_code
								, assignment_no
								, posting_date
								, document_date
								, created_date
								, reference_document_no
								, document_amount
								, currency
								, base_line_due_date
								, payment_term
								, day_term)
						  select a.clearing_document_no
								, a.document_type
								, 'normal' as 'receipt_type'
								, a.year
								, a.company_code
								, a.customer_code
								, a.assignment_no
								, a.posting_date
								, a.document_date
								, a.created_date
								, b.XBLNR
								, a.document_amount
								, a.currency
								, a.base_line_due_date
								, null as 'payment_term'
								, null as 'day_term'
							from invoice_clearing_detail a, BKPF_Temp b
							where isnull(reference_document,'') = ''
							and a.accounting_document_no = b.belnr
							and substring(B.XBLNR,1,2) in ('AI')
							and a.year = b.gjahr
							and a.company_code = b.bukrs
			end
		end
	/*end insert receipt update reference late*/
	SET NOCOUNT OFF;
END
GO
