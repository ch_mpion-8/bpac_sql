SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[copy_customer_collection_behavior]
	-- Add the parameters for the stored procedure here
	 @from_customer_company_id as int = null--4 -- '2011271' --null
		,@to_customer_company_id as int = null--18 -- '0230' --null
		,@updated_by as nvarchar(50) = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if exists( select 1 from dbo.[master_customer_collection_behavior] 
				where customer_company_id = @to_customer_company_id and is_active = 1)
	begin
		set @error_message = 'This company already have collection behavior.'
		return;
	end


	declare @group_id as int
	declare @group_id_ins as int
	declare table_cursor cursor for 
		select distinct group_id
		from dbo.[master_customer_collection_behavior]
		where customer_company_id = @from_customer_company_id
		and [is_active] = 1

	open table_cursor;
	fetch next from table_cursor into @group_id
	while @@fetch_status = 0
		begin
			-- Get Group ID
			begin
				select @group_id_ins = isnull(max(group_id),0)+1
				from [dbo].[master_customer_collection_behavior] 
			end
			
			declare @behavior_new2 table(new_id int)
			insert into dbo.[master_customer_collection_behavior]
					( customer_company_id
						, valid_date_from
						, valid_date_to
						, payment_term_code
						--, address_id
						, [time]
						, collection_condition_date
						, period_from
						, period_to
						, [description]
						, remark
						, condition_by
						, payment_day
						, is_business_day
						, date_from
						, date_to
						, last_x_date
						, custom_date
						, custom_day
						, condition_week_by
						, custom_week
						, condition_month_by
						, custom_month
						, next_x_month
						, is_next_collection_date
						, payment_method
						, bank
						, notify_day
						, is_skip_customer_holiday
						, is_skip_scg_holiday
						, receipt_method
						, document
						, custom_document
						,last_x_business_day
					   ,[is_active]
					   ,[created_date]
					   ,[created_by]
					   ,[updated_date]
					   ,[updated_by]
					   ,[group_id]
					   ,custom_date_next_x_month)
					select 
						 @to_customer_company_id
						, valid_date_from
						, valid_date_to
						, payment_term_code
						--, address_id
						, time
						, collection_condition_date
						, period_from
						, period_to
						, description
						, remark
						, condition_by
						, payment_day
						, is_business_day
						, date_from
						, date_to
						, last_x_date
						, custom_date
						, custom_day
						, condition_week_by
						, custom_week
						, condition_month_by
						, custom_month
						, next_x_month
						, is_next_collection_date
						, payment_method
						, bank
						, notify_day
						, is_skip_customer_holiday
						, is_skip_scg_holiday
						, receipt_method
						, document
						, custom_document
						,last_x_business_day
						,[is_active]
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
						,@group_id_ins
						,custom_date_next_x_month
					from dbo.[master_customer_collection_behavior]
					where customer_company_id = @from_customer_company_id
					and [is_active] = 1
					and group_id = @group_id
		
					insert into [dbo].[master_customer_collection_custom_date]
					   (collection_behavior_id
					   ,[date]
					   ,[is_active]
					   ,[created_date]
					   ,[created_by]
					   ,[updated_date]
					   ,[updated_by])
					select distinct new_id
							,da.[date]
							,1
							,getdate()
							,@updated_by
							,getdate()
							,@updated_by
					from @behavior_new2,dbo.[master_customer_collection_custom_date] da
						inner join dbo.[master_customer_collection_behavior] c 
							on da.collection_behavior_id = c.collection_behavior_id
					where c.group_id = @group_id

					delete @behavior_new2
		fetch next from table_cursor into  @group_id
	end;
	close table_cursor;
	deallocate table_cursor;
END
GO
