SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cheque_invoice_document_control_draft_group]
	@invoice_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	;with docList as (
		select substring(item,0,charindex('|', item)) as customer_company_id
					-- ,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as invoice_no 
				 ,right(item,charindex('|', reverse(item)) -1 ) as invoice_no
					from dbo.SplitString(@invoice_list,',')
	),
	draft as 
	(
		select distinct cc.customer_company_id
				,ih.company_code
				,com.company_name
				,ih.customer_code
				,cus.customer_name
				, isnull(cbb.receipt_method ,'PYC') as receipt_method
				--, r.payment_term
				, ih.currency
				--, i.collection_date
				, ih.invoice_no
				, ih.remaining_amount as remaining_amount
				, ih.invoice_amount + isnull(ih.tax_amount,0)  as original_amount
		from dbo.invoice_header ih
		inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
		inner join dbo.master_customer cus on ih.customer_code = cus.customer_code
		inner join dbo.master_company com on ih.company_code = com.company_code
		left join dbo.master_customer_collection_behavior cbb 
				on cc.customer_company_id = cbb.customer_company_id
						and ih.collection_behavior_id = cbb.collection_behavior_id
		inner join docList d on cc.customer_company_id = d.customer_company_id 
					and ih.invoice_no = d.invoice_no
					
		--where r.reference_document_no in (select item from dbo.SplitString(@receipt_list,',')) 
		--		and  r.receipt_type = 'advance'
	),
	groupDraft as 
	(
		select customer_company_id, receipt_method , currency,sum(remaining_amount) as document_amount,sum(original_amount) as original_amount
		from draft 
		group by customer_company_id, receipt_method , currency
	) 
	select gd.customer_company_id
			, subdraft.company_code
			, subdraft.company_name
			, subdraft.customer_code
			, subdraft.customer_name
			, gd.receipt_method as receipt_method
			--, gd.payment_term
			, gd.currency
			, gd.document_amount
			--, isnull(subdraft.collection_date, dateadd(dd,1,getdate())) as collection_date
			, subdraft.invoice as invoice_list
			,gd.original_amount
	from groupDraft gd
	left join (
					select distinct customer_company_id
								,company_code
								,customer_code
								,customer_name
								, receipt_method 
								--, payment_term
								, currency
								--, collection_date
								,company_name
								,	stuff(
								(
									select ',' + sub.invoice_no 
										from draft sub
										where sub.customer_company_id = main.customer_company_id
												and sub.receipt_method = main.receipt_method
												and sub.currency = main.currency
										for xml path('')
								) ,1,1,'') as invoice
					from draft main
				) subdraft on gd.customer_company_id = subdraft.customer_company_id
									and gd.receipt_method = subdraft.receipt_method
									and gd.currency = subdraft.currency
END
GO
