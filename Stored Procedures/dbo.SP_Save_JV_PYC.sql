SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Save_JV_PYC]
	@data_list dbo.JvPycTable readonly,
	@submit_by nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @document_date date,@tprc int,@smpc int,@roc int,@moc int

    -- Insert statements for procedure here
	declare cur Cursor for
	select * from @data_list 
	open cur
	fetch next from cur 
	into @document_date,@tprc,@smpc,@roc,@moc

	while (@@FETCH_STATUS =0)
	begin
		exec SP_UPDATE_OR_INSERT_JV_PYC 1,@document_date,@tprc,@submit_by
		exec SP_UPDATE_OR_INSERT_JV_PYC 2,@document_date,@smpc,@submit_by
		exec SP_UPDATE_OR_INSERT_JV_PYC 3,@document_date,@roc,@submit_by
		exec SP_UPDATE_OR_INSERT_JV_PYC 4,@document_date,@moc,@submit_by

		fetch next from cur 
		into @document_date,@tprc,@smpc,@roc,@moc
	end

	close cur
	deallocate cur

END
GO
