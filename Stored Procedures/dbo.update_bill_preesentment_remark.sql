SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[update_bill_preesentment_remark]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int
	,@remark as nvarchar(2000) 
	,@bill_presentment_date as date = null
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

	update dbo.bill_presentment_control_document
	set remark = @remark
		,bill_presentment_date = @bill_presentment_date
		,updated_by = @updated_by
		,updated_date = getdate()
	where bill_presentment_id = @bill_presentment_id

	-- Moom Added 20-12-2017
	UPDATE dbo.invoice_header set collection_calculation_log = '',collection_date = null,collection_notify_date = null
	where invoice_no in (
	select ih.invoice_no from dbo.bill_presentment_control_document b
	inner join dbo.bill_presentment_control_document_item bi on
	bi.bill_presentment_id = b.bill_presentment_id
	inner join dbo.invoice_header ih on
	ih.invoice_no = bi.invoice_no
	inner join master_customer_company mcc
	on 
	ih.company_code = mcc.company_code
	and
	ih.customer_code = mcc.customer_code
	inner join master_customer_collection_behavior mcb
	on
	mcb.payment_term_code = ih.payment_term
	and mcb.customer_company_id = mcc.customer_company_id
	where mcb.collection_condition_date = 'Billing Date'
	and b.bill_presentment_id = @bill_presentment_id
	)
	--End Moom Added 

	
	declare @description as nvarchar(2000) 
	select @description = 'Bill Presentment Date : ' + cast(@bill_presentment_date as nvarchar(20)) + '</br>Remark : ' + @remark

	exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_presentment_id
			,@description = @description
			,@action  = 'Update Bill Presentment Date / Remark'
			,@updated_by  = @updated_by


END

GO
