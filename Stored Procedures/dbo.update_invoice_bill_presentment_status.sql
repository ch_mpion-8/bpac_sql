SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[update_invoice_bill_presentment_status]
	-- Add the parameters for the stored procedure here
	@invoice_no as varchar(20) = null
	,@bill_presentment_id as int = null
	,@status as nvarchar(20) = null
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @invoice_no is not null 
	begin
		 update dbo.invoice_header
	set bill_presentment_status = @status 
		,updated_by = @updated_by
		,updated_date_time = getdate()
	where invoice_no = @invoice_no 
			and isnull(bill_presentment_status,'WAITING') <> 'CANCEL'
	end
	else if @bill_presentment_id is not null
	begin
		update dbo.invoice_header
		set bill_presentment_status = @status 
			,updated_by = @updated_by
			,updated_date_time = getdate()
		where  isnull(bill_presentment_status,'WAITING') <> 'CANCEL' 
				and invoice_no in 
						(select invoice_no 
							from dbo.bill_presentment_control_document_item with(nolock)
							where bill_presentment_id = @bill_presentment_id)
	end

END
GO
