SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[update_pyc_document_upload]
	-- Add the parameters for the stored procedure here
	@chqno varchar(20)=null,
	@chqamount decimal(15,2) =null,
	@chqdate date = null,
	@branch varchar(200) =null,
	@invoice_no varchar(20) =null,
	@receipt_no varchar(20)=null,
	@is_pdc bit = null,
	@remark nvarchar(2000) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @header_id int =null
	DECLARE @receipt_currency varchar(5) = null
	declare @year varchar(4) =null
	declare @is_invoice bit
	
	select  @is_invoice = case when @receipt_no like 'AA%' then 0 else 1 end
	
	if (@is_invoice = 0)
	begin
		select @header_id =  h.cheque_control_id
			, @receipt_currency =r.currency
			,@year = i.document_year
		from dbo.cheque_control_document_item i
		inner join dbo.cheque_control_document h
			on h.cheque_control_id = i.cheque_control_id
		inner join dbo.receipt r
			on r.reference_document_no = i.reference_document_no
			and r.[year] = i.document_year
		inner join dbo.invoice_header inv
			on inv.assignment_no = r.assignment_no
		inner join dbo.master_customer_company Invmcc
			on Invmcc.company_code = inv.company_code
		and Invmcc.customer_code = inv.customer_code
		where 
			i.is_active = 1
			and i.reference_document_no = @receipt_no
			and r.assignment_no = @invoice_no
			and Invmcc.customer_company_id = h.customer_company_id
			and isnull(h.cheque_control_status,'') <> 'CANCEL'

		Declare @receiptActive bit = 0;

		select @receiptActive = isnull(r.is_active,1) 
		from dbo.receipt r
		where r.assignment_no = @invoice_no
		and r.reference_document_no = @receipt_no

		if @is_pdc = 1
		begin
			update dbo.receipt
			set is_pdc  =1
			where reference_document_no = @receipt_no
			and assignment_no = @invoice_no
		end


		if @receiptActive =1
		begin
			update dbo.cheque_control_document_item
			set cheque_no = @chqno,
			cheque_bank = @branch,
			cheque_amount = @chqamount,
			cheque_date =@chqdate,
			cheque_currency = @receipt_currency,
			cheque_remark = @remark,
			updated_date = getdate(),
			updated_by = 'SYSTEM'
			where cheque_control_id = @header_id
			and reference_document_no = @receipt_no
			and document_year = @year
		end

		if not exists (select 1 from dbo.cheque_control_document_item i 
						where isnull(i.cheque_no,'') = ''
						and is_active =1
						and i.cheque_control_id = @header_id)
		begin
			update dbo.cheque_control_document 
			set cheque_control_status = 'COMPLETE',
			updated_by = 'System',
			updated_date = getdate()
			where cheque_control_id = @header_id
			and cheque_control_status <>'CANCEL'
		end
	end
	else
	begin

		select @header_id =  h.cheque_control_id
			, @receipt_currency = inv.currency
			,@year = i.document_year
		from dbo.cheque_invoice_control_document_item i
		inner join dbo.cheque_invoice_control_document h
			on h.cheque_control_id = i.cheque_control_id
		inner join dbo.invoice_header inv
			on inv.invoice_no = i.invoice_no
		inner join dbo.master_customer_company Invmcc
			on Invmcc.company_code = inv.company_code
		and Invmcc.customer_code = inv.customer_code
		where 
			i.is_active = 1
			and i.invoice_no = @invoice_no
			and h.cheque_control_no = @receipt_no
			and Invmcc.customer_company_id = h.customer_company_id
			and isnull(h.cheque_control_status,'') <> 'CANCEL'

	

		--if @is_pdc = 1
		--begin
		--	update dbo.invoice_header
		--	set is_pdc  =1
		--	where invoice_no = @invoice_no
		--end


		update dbo.cheque_invoice_control_document_item
			set cheque_no = @chqno,
			cheque_bank = @branch,
			cheque_amount = @chqamount,
			cheque_date =@chqdate,
			cheque_currency = @receipt_currency,
			cheque_remark = @remark,
			updated_date = getdate(),
			updated_by = 'SYSTEM'
			where cheque_control_id = @header_id
			and invoice_no = @invoice_no

		if not exists (select 1 from dbo.cheque_invoice_control_document_item i 
						where isnull(i.cheque_no,'') = ''
						and is_active =1
						and i.cheque_control_id = @header_id)
		begin
			update dbo.cheque_invoice_control_document 
			set cheque_control_status = 'COMPLETE',
			updated_by = 'System',
			updated_date = getdate()
			where cheque_control_id = @header_id
			and cheque_control_status <>'CANCEL'
		end
	end

END
GO
