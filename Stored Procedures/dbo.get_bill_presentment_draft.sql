SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[get_bill_presentment_document_control]    Script Date: 28/11/2560 18:13:04 ******/

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_bill_presentment_draft]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select bt.[temp_id]
      ,bt.[customer_company_id]
	  ,cc.company_code
	  ,cc.customer_code
	  ,cus.customer_name
      ,bt.[bill_presentment_date]
      ,bt.[method] as bill_presentment_method
      ,bt.[payment_term]
      ,bt.[currency]
      ,bt.[remark]
	  ,cc.user_display_name
	  ,subInvoice.invoice as invoice_list
	  ,sumamount.invoice_amount as invoice_amount
      ,bt.[created_date]
      ,bt.[created_by]
      ,bt.[updated_date]
      ,bt.[updated_by]
  from [dbo].[bill_presentment_temp] bt
  inner join dbo.master_customer_company cc on bt.customer_company_id = cc.customer_company_id
  inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
  inner join 
  (
		select  main.temp_id,	stuff(
					(
						select ', ' + invoice_no
							from dbo.[bill_presentment_item_temp] sub
							where sub.temp_id = main.temp_id and sub.[is_active] = 1
							for xml path('')
					) ,1,1,'') as invoice
					from dbo.[bill_presentment_temp] main
  ) subInvoice on bt.temp_id = subInvoice.temp_id
  inner join 
  (
		select temp_id, sum(subih.invoice_amount) as invoice_amount
		from dbo.[bill_presentment_item_temp] sub
		inner join dbo.invoice_header subih on sub.invoice_no = subih.invoice_no
		where sub.[is_active] = 1
		group by temp_id
	) sumamount on bt.temp_id = sumamount.temp_id


END




GO
