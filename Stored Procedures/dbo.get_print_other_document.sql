SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_print_other_document]
	@document_id varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @type int =null

	select @type = contact_person_type from dbo.other_control_document where document_id = @document_id

	if @type =1
	begin
		select 
		isnull(conf.[configuration_value], o.method) as method ,
		o.document_no,
		o.document_date,
		o.receiver,
		c.customer_name,
		c.customer_code,
		i.reference_no,
		i.action_type,
		o.user_code,
		cp.telephone,
		o.contact_person as  contact_name,
		company_name,
		o.remark
		from dbo.other_control_document o
		inner join dbo.other_control_document_item i on i.document_id= o.document_id
		left join dbo.master_customer c on c.customer_code = o.receiver
		left join dbo.master_customer_contact cp on cp.contact_id = o.contact_person_id
		inner join dbo.master_company com on com.company_code = o.company_code
		left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
		where o.document_id = @document_id
		order by i.created_date asc,i.sequence_no asc
	end
	else
	begin
		select 
		isnull(conf.[configuration_value], o.method) as method ,
		o.document_no,
		o.document_date,
		o.receiver,
		o.receiver_name as customer_name,
		i.reference_no,
		i.action_type,
		cp.created_by as user_code,
		cp.telephone,
		o.contact_person as contact_name,
		com.company_name,
		o.remark,
		cp.title
		from dbo.other_control_document o
		inner join dbo.other_control_document_item i on i.document_id= o.document_id
		left join dbo.master_other_contact_person cp on cp.other_contact_id = o.contact_person_id
		inner join dbo.master_company com on com.company_code = o.company_code
		left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
		where o.document_id = @document_id
		order by i.created_date asc,i.sequence_no asc
	end



	
END

GO
