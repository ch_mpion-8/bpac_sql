SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_MISMATCH]
		@company_code as varchar(100) = null-- '0230'
		,@customer_code as nvarchar(100) =null--  '2003137'
		,@start_date as datetime = null--'EMS,Messenger'
		,@end_date as datetime = null-- '0001'
		,@user as nvarchar(100) = null
		,@status as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT mm.mismatch_id,mm.mismatch_date,mm.amount,mm.interest_day,mm.interest_rate,mm.mismatch_status,mm.reason,mm.solution,mm.remark,
	mm.company_code,mm.customer_code,mc.customer_name,mcc.user_display_name as [collection_user],mm.remark
	from mismatch mm 
	left join master_customer_company mcc on mm.customer_code = mcc.customer_code and mm.company_code = mcc.company_code
	left join master_customer mc on mc.customer_code = mcc.customer_code
	where 
	( @company_code is null or mm.company_code  in ( select item from dbo.SplitString(@company_code,',')))
	and 
	( @customer_code is null or mcc.customer_code = @customer_code or mc.customer_name = @customer_code)
	and ( @user is null or mcc.[user_name] = @user)
	and ( @status is null or mismatch_status =  @status)
	and ( @start_date is null or @end_date is null or (mm.mismatch_date between @start_date and @end_date))
	and amount > 0
END
GO
