SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[validate_manual_receive_cheque]
	-- Add the parameters for the stored procedure here
	 @cheque_detail_id as nvarchar(2000) = null--'1016,1015'
		,@receipt_list as nvarchar(2000) = null--'157|AA0001060028|2017,121|AA0001060029|2017'
		,@cheque_date as date = null--'2017-10-01'
		,@cheque_amount as decimal(15,2) = null--1500000--1773200
		,@error_message as nvarchar(2000) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @temp table ( 
				reference_document_no nvarchar(20),
				amount  decimal(15,2),
				company_code  nvarchar(10),
				customer_code  nvarchar(10),
				net_due_date  date,
				customer_company_id  int,
				receive_date date
				) 
		if @cheque_detail_id is not null  and @cheque_detail_id <> ''
		begin

		insert into @temp(reference_document_no ,amount ,company_code,customer_code ,net_due_date ,customer_company_id, receive_date   )
		select distinct cdi.reference_document_no
				,rs.amount
				,c.company_code
				,c.customer_code
				,i.net_due_date
				,c.customer_company_id
				,r.document_date
			
		from dbo.cheque_control_document_item cdi
		inner join dbo.cheque_control_document cd on cdi.cheque_control_id = cd.cheque_control_id
		inner join dbo.master_customer_company c on cd.customer_company_id = c.customer_company_id
		left join (
						select reference_document_no,year([document_date]) as [year],customer_code,company_code,sum(document_amount) as amount
						from dbo.receipt 
						group by  reference_document_no,year([document_date]) ,customer_code,company_code
					) rs on c.company_code = rs.company_code 
						and c.customer_code = rs.customer_code 
						and cdi.reference_document_no = rs.reference_document_no
						and cdi.document_year = rs.[year]
		inner join dbo.receipt r on cdi.reference_document_no = r.reference_document_no 
						and c.company_code = rs.company_code 
						and c.customer_code = rs.customer_code 
						and  cdi.document_year = r.[year]
		inner join dbo.invoice_header i on r.assignment_no = i.invoice_no
		where cdi.cheque_control_detail_id in (select item from dbo.SplitString(@cheque_detail_id,','))

		end
		else
		begin
	
			;with a as ( 
					select substring(item,0,charindex('|', item)) as customer_company_id
						,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as receipt_no 
						,right(item,charindex('|', reverse(item)) -1 ) as [year]
						 --,*
					from dbo.SplitString(@receipt_list,',')
				)
			
			insert into @temp(reference_document_no ,amount ,company_code,customer_code ,net_due_date ,customer_company_id  )
			select a.receipt_no as reference_document_no 
					,rs.amount
					,c.company_code
					,c.customer_code
					,i.net_due_date
					,a.customer_company_id
			
				--into @temp
			from a 
			inner join dbo.master_customer_company c on a.customer_company_id = c.customer_company_id
			inner join dbo.receipt r on c.customer_code = r.customer_code  
										and c.company_code = r.company_code 
										and a.[year] = year(r.[document_date])
										and a.receipt_no = r.reference_document_no
			inner join dbo.invoice_header i on r.assignment_no = i.invoice_no
			left join (
							select reference_document_no,year([document_date]) as [year],customer_code,company_code,sum(document_amount) as amount
							from dbo.receipt 
							--where reference_document_no in ( 'AA0001060028','AA0001060029' ) 
							group by  reference_document_no,year([document_date]),customer_code,company_code
						) rs on c.company_code = rs.company_code 
							and c.customer_code = rs.customer_code 
							and r.reference_document_no = rs.reference_document_no
							and year(r.[document_date]) = rs.[year]
		end

	--if exists (select 1 from @temp where a)

	--select *
	--from @temp

	if exists (select count(1) from (select 1 as  a from @temp group by customer_company_id) as t having count(1) > 1)
	begin
		select @error_message = 'กรุณาเลือกใบเสร็จที่มีลูกค้าคนเดียวกัน'
	end


	if exists (select 1 from @temp	having abs(@cheque_amount - sum(distinct amount) ) > 20)
	begin
		select @error_message = case when @error_message is null or @error_message = '' then 'ยอดเงินเช็คแตกต่างกันเกิน 20 กรุณากรอก remark' else @error_message + ', ยอดเงินเช็คแตกต่างกันเกิน 20 กรุณากรอก remark' end
	end

	if exists (select 1	from @temp 	where datediff(day,@cheque_date,net_due_date) < 0)
	begin
		select @error_message = case when @error_message is null or @error_message = ''  then 'วันที่หน้าเช็คเกินจาก net due date กรุณากรอก remark' else @error_message + ', วันที่หน้าเช็คเกินจาก net due date กรุณากรอก remark' end
	end

	
	if (@cheque_detail_id is not null  and @cheque_detail_id <> '') and exists (select 1 from @temp 	where datediff(day,@cheque_date,receive_date) < 0)
	begin
		select @error_message = case when @error_message is null or @error_message = ''  then 'วันที่หน้าเช็คเกินจากวันที่รับเช็ค กรุณากรอก remark' else @error_message + ', วันที่หน้าเช็คเกินจากวันที่รับเช็ค กรุณากรอก remark' end
	end
	
	--select @error_message = 'TEST'

END

GO
