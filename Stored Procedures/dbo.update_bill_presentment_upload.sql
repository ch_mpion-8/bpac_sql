SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[update_bill_presentment_upload]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as int = null
		,@reference_guid as uniqueidentifier = null
		,@updated_by as nvarchar(50) = null
		,@description as nvarchar(200) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   declare @status as nvarchar(20) = 'COMPLETE'

	update dbo.bill_presentment_control_document
	set bill_presentment_status = @status
		,reference_guid = @reference_guid
		,updated_by = @updated_by
		,updated_date = getdate()
	where bill_presentment_id = @bill_presentment_id

	exec dbo.update_invoice_bill_presentment_status
			@invoice_no = null
			,@bill_presentment_id = @bill_presentment_id
			,@status  = @status
			,@updated_by  = @updated_by

	if @description is null or @description = '' 
		set @description = 'Upload bill presentment document.'

	exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_presentment_id
			,@description = @description
			,@action  = 'Complete'
			,@updated_by  = @updated_by


END
GO
