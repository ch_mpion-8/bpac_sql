SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_bill_presentment_from_draft]
	-- Add the parameters for the stored procedure here
	 @temp_id as int = null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	declare @bill_id as int
			, @new_status as nvarchar(20) = 'WAITINGRECEIVED'
	insert into [dbo].[bill_presentment_control_document]
		([customer_company_id]
		,[method]
		,[remark]
		,[bill_presentment_date]
		,[bill_presentment_status]
		,[created_date]
		,[created_by]
		,[updated_date]
		,[updated_by])
	select customer_company_id
			,method
			,remark
			,bill_presentment_date
			,@new_status
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from dbo.bill_presentment_temp
	where temp_id = @temp_id 

	select @bill_id = ident_current('bill_presentment_control_document')


	insert into [dbo].[bill_presentment_control_document_item]
		([bill_presentment_id]
		,[invoice_no]
		,is_active
		,[created_date]
		,[created_by]
		,[updated_date]
		,[updated_by])
	select @bill_id
		,invoice_no
		,1
		,getdate()
		,@updated_by
		,getdate()
		,@updated_by
	from dbo.bill_presentment_item_temp
	where temp_id = @temp_id and is_active = 1


	exec dbo.update_invoice_bill_presentment_status
		@invoice_no = null
		,@bill_presentment_id = @bill_id
		,@status  = 'COMPLETE'
		,@updated_by  = @updated_by
	--update dbo.invoice_header
	--set bill_presentment_status = 'COMPLETE'
	--where invoice_no in (select invoice_no from dbo.bill_presentment_control_document_item with(nolock)
	--										where bill_presentment_id = @bill_id)

	exec [dbo].[delete_bill_presentment_draft] 
		@temp_id = @temp_id
	--delete from bill_presentment_item_temp
	--where temp_id = @temp_id


	--delete from bill_presentment_temp
	--where temp_id = @temp_id

	exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_id
			,@description = 'Creat bill presentment.'
			,@action  = 'Creat'
			,@updated_by  = @updated_by
END




GO
