SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_client_login]
	-- Add the parameters for the stored procedure here
	@ip varchar(12) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select top 1 * from dbo.client_login where  DATEADD(MINUTE,10,last_date) > getdate() and [ip] = @ip 
	order by last_date desc
END
GO
