SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_status_customer_address]
	-- Add the parameters for the stored procedure here
	@address_id as int = null --'2010550'
	,@status as bit  = null --'2010550'
	,@updated_by as nvarchar(20) = null--'ACTIVE'
	,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @customer_company_id as int = null 

	if ( @status = 0 and exists(select 1 from [dbo].master_customer_bill_presentment_behavior 
					where address_id = @address_id and is_active = 1))
	begin
		set @error_message = 'ไม่สามารถลบที่อยู่นี้ได้ เพราะถูกตั้งค่าเป็นที่อยู่สำหรับวางบิล'
		return;
	end

	if ( @status = 0 and exists(select 1 from [dbo].master_customer_collection_behavior 
					where address_id = @address_id and is_active = 1))
	begin
		set @error_message = 'ไม่สามารถลบที่อยู่นี้ได้ เพราะถูกตั้งค่าเป็นที่อยู่สำหรับชำระเงิน'
		return;
	end
	
	select @customer_company_id = customer_company_id 
	from [dbo].master_customer_address 
	where address_id = @address_id

	if not exists(select 1 from [dbo].master_customer_address 
					where customer_company_id = @customer_company_id 
					and isnull(is_ems,0) = 1
					and address_id <> @address_id
					)
		
	begin
		update dbo.master_customer_address
					set is_ems = 1
						,updated_date = getdate()
						,updated_by = @updated_by
				from dbo.master_customer_address
				where customer_company_id = @customer_company_id
				and title = 'ภพ.20'
	end 

	update [dbo].master_customer_address 
	set [is_active] = @status
		,updated_by = @updated_by
		,updated_date = getdate()
		,is_ems = 0
	from [dbo].master_customer_address  
	where address_id = @address_id
	
END




GO
