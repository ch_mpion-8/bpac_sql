SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[copy_customer_bill_presentment_behavior]
	-- Add the parameters for the stored procedure here
	 @from_customer_company_id as int = null--4 -- '2011271' --null
		,@to_customer_company_id as int = null--18 -- '0230' --null
		,@updated_by as nvarchar(50) = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if exists( select 1 from dbo.master_customer_bill_presentment_behavior 
			where customer_company_id = @to_customer_company_id and is_active = 1)
	begin
		set @error_message = 'This company already have bill presentment behavior.'
		return;
	end

	declare @group_id as int
	declare @group_id_ins as int
	declare table_cursor cursor for 
		select distinct group_id
		from dbo.master_customer_bill_presentment_behavior
		where customer_company_id = @from_customer_company_id
		and [is_active] = 1

	open table_cursor;
	fetch next from table_cursor into @group_id
	while @@fetch_status = 0
		begin
			-- Get Group ID
			begin
				select @group_id_ins = isnull(max(group_id),0)+1
				from [dbo].master_customer_bill_presentment_behavior 
			end

			

			declare @behavior_new table(new_id int)

			insert into dbo.master_customer_bill_presentment_behavior
					( customer_company_id
						, valid_date_from
						, valid_date_to
						, payment_term_code
						, invoice_period_from
						, invoice_period_to
						--, address_id
						, [time]
						, [description]
						, remark
						, condition_date_by
						, date_from
						, date_to
						, last_x_date
						, custom_date
						, custom_day
						, condition_week_by
						, custom_week
						, condition_month_by
						, custom_month
						, next_x_month
						, is_next_bill_presentment_date
						, billing_method
						, is_signed_document
						, is_billing_wiith_truck
						, notify_day
						, is_skip_customer_holiday
						, is_skip_scg_holiday
						, is_billing_with_bill
						, document_type
						,is_bill_presentment
						,last_x_business_day
					   ,[is_active]
					   ,[created_date]
					   ,[created_by]
					   ,[updated_date]
					   ,[updated_by]
					   ,[group_id]
					   ,custom_date_next_x_month
					   )
					output inserted.bill_presentment_behavior_id into @behavior_new
					select 
						 @to_customer_company_id
						, valid_date_from
						, valid_date_to
						, payment_term_code
						, invoice_period_from
						, invoice_period_to
						--, address_id
						, [time]
						, [description]
						, remark
						, condition_date_by
						, date_from
						, date_to
						, last_x_date
						, custom_date
						, custom_day
						, condition_week_by
						, custom_week
						, condition_month_by
						, custom_month
						, next_x_month
						, is_next_bill_presentment_date
						, billing_method
						, is_signed_document
						, is_billing_wiith_truck
						, notify_day
						, is_skip_customer_holiday
						, is_skip_scg_holiday
						, is_billing_with_bill
						, document_type
						,is_bill_presentment
						,last_x_business_day
						,[is_active]
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
						,@group_id_ins
						,custom_date_next_x_month
					from dbo.master_customer_bill_presentment_behavior
					where customer_company_id = @from_customer_company_id
					and [is_active] = 1
					and group_id = @group_id

				insert into [dbo].[master_customer_bill_presentment_document]
					   ([bill_presentment_behavior_id]
					   ,[document_code]
					   ,[custom_document]
					   ,[is_active]
					   ,[created_date]
					   ,[created_by]
					   ,[updated_date]
					   ,[updated_by])
				select distinct new_id
						,doc.document_code
						,doc.custom_document 
						,1
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
				from @behavior_new,dbo.master_customer_bill_presentment_document doc
					inner join dbo.master_customer_bill_presentment_behavior b 
						on doc.bill_presentment_behavior_id = b.bill_presentment_behavior_id
				where b.group_id = @group_id
 
 
				insert into [dbo].[master_customer_bill_presentment_custom_date]
					   ([bill_presentment_behavior_id]
					   ,[date]
					   ,[is_active]
					   ,[created_date]
					   ,[created_by]
					   ,[updated_date]
					   ,[updated_by])
				select distinct new_id
						,da.[date]
						,1
						,getdate()
						,@updated_by
						,getdate()
						,@updated_by
				from @behavior_new,dbo.[master_customer_bill_presentment_custom_date] da
					inner join dbo.master_customer_bill_presentment_behavior b 
						on da.bill_presentment_behavior_id = b.bill_presentment_behavior_id
				where b.group_id = @group_id

				delete  @behavior_new
			fetch next from table_cursor into  @group_id
	end;
	close table_cursor;
	deallocate table_cursor;
END




GO
