SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[get_badge_pending_otherdoc]
	-- Add the parameters for the stored procedure here
	@user_name varchar(100) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--set arithabort off;

	declare @user_name_c varchar(100) = @user_name;
   with
	advance_wc
	as
		(
			select count(*) as advance_wc from (
				select 
				c.customer_company_id  
				from dbo.receipt r
				inner join dbo.master_customer_company c
					on r.customer_code = c.customer_code
					and r.company_code = c.company_code
				left join (select o.[status],i.reference_no,o.receiver,o.company_code from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id where o.document_type=1) o
					on o.company_code = r.company_code
					and o.receiver = r.customer_code
					and o.reference_no = r.reference_document_no
				left join dbo.master_customer_bill_presentment_behavior bev
					on bev.customer_company_id = c.customer_company_id
				where
					r.receipt_type = 'normal'
					and (c.[user_name] = @user_name_c or @user_name_c is null)
					and isnull(r.is_active,1) =1 
					--and isnull(bev.is_billing_with_bill,0) <> 1
					and 
					(
						isnull(o.[status],'WC') = 'WC' 
							or
						(
							o.[status] ='CANCEL'
							and (select top 1 1 from dbo.other_control_document ao inner join dbo.other_control_document_item ai on ao.document_id = ai.document_id
								where ai.reference_no = r.reference_document_no
								and ao.document_type=1
								and ao.[status] <>'CANCEL') is null
						)
					)
					group by c.customer_company_id
			) as t
		),
	advance_wr
	as
	(
		select count(*) as advance_wr from (
			select c.customer_company_id
				from dbo.receipt r
				inner join dbo.master_customer_company c
					on r.customer_code = c.customer_code
					and r.company_code = c.company_code
				left join (select o.[status],i.reference_no,o.receiver,o.company_code from dbo.other_control_document o inner join dbo.other_control_document_item i on i.document_id = o.document_id where o.document_type=1) o
					on o.company_code = r.company_code
					and o.receiver = r.customer_code
					and o.reference_no = r.reference_document_no
				left join dbo.master_customer_bill_presentment_behavior bev
					on bev.customer_company_id = c.customer_company_id
				where
					r.receipt_type = 'normal'
					and o.[status] = 'WR'
					and (c.[user_name] = @user_name_c or @user_name_c is null)
					--and isnull(bev.is_billing_with_bill,0) <>1
					group by c.customer_company_id
			) as t
	),
	deposit_wc
	as
	(
		select count(*) as deposit_wc from (
			select c.customer_company_id
			from dbo.deposit d
			inner join dbo.master_customer_company c
				on d.customer_code = c.customer_code
				and d.company_code = c.company_code
			left join dbo.other_control_document_item i
				on i.reference_no = d.dp_no
			left join dbo.other_control_document o
				on o.document_id = i.document_id
			where
				(
					isnull(o.[status],'WC') = 'WC' 
						or
					(
						o.[status] ='CANCEL'
						and (select top 1 1 from dbo.other_control_document ao inner join dbo.other_control_document_item ai on ao.document_id = ai.document_id
							where ai.reference_no = d.dp_no
							and ao.document_type=3
							and ao.[status] <>'CANCEL') is null
					)
				)
				and (c.[user_name] = @user_name_c or @user_name_c is null)
				and isnull(o.document_type,3) = 3
				group by c.customer_company_id
		) as t 
	),
	deposit_wr 
	as
	(
		select count(*)  as deposit_wr from (
			select c.customer_company_id
			from dbo.deposit d
			inner join dbo.master_customer_company c
				on d.customer_code = c.customer_code
				and d.company_code = c.company_code
			left join dbo.other_control_document_item i
				on i.reference_no = d.dp_no
			left join dbo.other_control_document o
				on o.document_id = i.document_id
			where
				o.[status] = 'WR' 
				and o.document_type = 3
				and (c.[user_name] = @user_name_c or @user_name_c is null)
				group by c.customer_company_id
		) as t 
	),
	cs_deposit_wc
	as
	(
		select count(*) as cs_deposit_wc from (
			select c.customer_company_id
			from dbo.deposit d
			inner join dbo.master_customer_company c
				on d.customer_code = c.customer_code
				and d.company_code = c.company_code
			left join dbo.other_control_document_item i
				on i.reference_no = d.order_no
			left join dbo.other_control_document o
				on o.document_id = i.document_id
			where
				isnull(o.document_type,5) = 5 and 
				(
					isnull(o.[status],'WC') = 'WC' 
						or
					(
						o.[status] ='CANCEL'
						and (select top 1 1 from dbo.other_control_document ao inner join dbo.other_control_document_item ai on ao.document_id = ai.document_id
							where ai.reference_no = d.order_no
							and ao.document_type=5
							and ao.[status] <>'CANCEL') is null
					)
				)
				and (c.[user_name] = @user_name_c or @user_name_c is null)
				group by c.customer_company_id
		) as t 
	),
	cs_deposit_wr
	as
	(
		select count(*) as cs_deposit_wr from (
			select c.customer_company_id
			from dbo.deposit d
			inner join dbo.master_customer_company c
				on d.customer_code = c.customer_code
				and d.company_code = c.company_code
			left join dbo.other_control_document_item i
				on i.reference_no = d.order_no
			left join dbo.other_control_document o
				on o.document_id = i.document_id
			where
				o.[status] = 'WR' 
				and o.document_type =5
				and (c.[user_name] = @user_name_c or @user_name_c is null)
				group by c.customer_company_id
		) as  t
	),
	otherdoc_wr
	as
	(
		select count(*) as otherdoc_wr   
		from dbo.other_control_document 
		where [status] = 'WAITING'
		and document_type = 4
		and (created_by  = @user_name_c or @user_name_c is null)
	),
	billing_WRINV
	as
	(
		select count(*) as billing_WRINV from (
			select mcc.customer_company_id
			from dbo.invoice_header inv
			inner join dbo.master_customer_company mcc
			on mcc.company_code = inv.company_code
			and mcc.customer_code = inv.customer_code
			inner join dbo.master_customer_bill_presentment_behavior b
			on b.customer_company_id = mcc.customer_company_id
			left join dbo.other_control_document_item i
			on i.reference_no = inv.invoice_no
			left join dbo.other_control_document o
			on o.document_id = i.document_id
			and o.company_code = inv.company_code
			and o.receiver = inv.customer_code
			where 
				o.document_type=2
				and isnull(b.is_signed_document,1) = 1
				and i.action_type = 1
				and not exists (select 1 from dbo.other_control_document_item ci inner join dbo.other_control_document co on co.document_id = ci.document_id
					where co.document_type =2
					and ci.reference_no = inv.invoice_no
					and co.company_code = inv.company_code
					and co.receiver = inv.customer_code
					and ci.action_type = 2
				)
				and (mcc.[user_name] = @user_name_c or @user_name_c is null)
				and o.[status] <>'CANCEL'
			group by mcc.customer_company_id
		) as t 
	),
	billing_WR
	as
	(
		select count(*) as billing_WR from (
			select mcc.customer_company_id
			from dbo.invoice_header inv
			inner join dbo.master_customer_company mcc
			on mcc.company_code = inv.company_code
			and mcc.customer_code = inv.customer_code
			inner join dbo.master_customer_bill_presentment_behavior b
			on b.customer_company_id = mcc.customer_company_id
			left join dbo.other_control_document_item i
			on i.reference_no = inv.invoice_no
			left join dbo.other_control_document o
			on o.document_id = i.document_id
			and o.company_code = inv.company_code
			and o.receiver = inv.customer_code
			where 
				o.document_type=2
				and isnull(b.is_signed_document,1) = 1
				and (mcc.[user_name] = @user_name_c or @user_name_c is null)
				and 
				(
					(
						i.action_type = 0
						and 
						o.[status] = 'WR'
					)
					or
					(
						i.action_type = 1 
						and exists (select 1 from dbo.other_control_document_item ci inner join dbo.other_control_document co on co.document_id = ci.document_id
							where co.document_type =2
							and ci.reference_no = inv.invoice_no
							and co.company_code = inv.company_code
							and co.receiver = inv.customer_code
							and ci.action_type = 2
							and o.[status] in ('WR','C')
							and co.[status] in ('WR','C')
							and ( o.[status] != 'C' or co.[status] !='C')
						)
					)
				)
				group by mcc.customer_company_id
		) as t
	),
	bill_following
	as
	(
		select count(*) as bill_following from (
			select cc.customer_company_id
				from dbo.invoice_header ih
				inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
				inner join dbo.master_customer cus on ih.customer_code = cus.customer_code	and isnull(cus.is_ignore,0) = 0
				left join dbo.master_customer_bill_presentment_behavior cbb 
						on cbb.bill_presentment_behavior_id = ih.bill_presentment_behavior_id					
				WHERE (
							(ih.invoice_type = 'RA' and ih.payment_term not in ('TS00','NT00'))
							or ih.invoice_type in ('RC','RD','DA')
						)
						and isnull(cbb.is_bill_presentment,1) = 1
						and (ih.bill_presentment_notify_date is null or ih.bill_presentment_notify_date <= getdate() )
						and isnull(ih.bill_presentment_status,'WAITING') in ('WAITING','ERROR','MANUAL')
						and  (@user_name_c is null or cc.user_name = @user_name_c) 
						and isnull(cus.is_ignore,0) <> 1 
				group by cc.customer_company_id
		) as t
	),
	bill_control
	as
	(
		select count(*) as bill_control from ( select cc.customer_company_id
			from dbo.bill_presentment_control_document b
			inner join dbo.bill_presentment_control_document_item bdci 
				on bdci.bill_presentment_id = b.bill_presentment_id
			inner join  dbo.master_customer_company cc 
				on b.customer_company_id = cc.customer_company_id 
			inner join dbo.invoice_header ih
				on ih.invoice_no = bdci.invoice_no
			where b.bill_presentment_status = 'WAITINGRECEIVED'
			and  (@user_name_c is null or cc.user_name = @user_name_c) 
			and ((isnull(ih.bill_presentment_status,'') <> 'COMPLETE' 
			or isnull(ih.bill_presentment_remark,'') not like '%สร้างใบคุมบนSAP%'))
			group by cc.customer_company_id
			) as t
	),
	collection_following
	as
	(
		select count(*) as collection_following from (select cc.customer_company_id
		from dbo.invoice_header ih
		inner join  dbo.master_customer_company cc on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
		inner join dbo.master_customer cus on ih.customer_code = cus.customer_code	and isnull(cus.is_ignore,0) = 0					
		WHERE  (ih.collection_notify_date is null or ih.collection_notify_date <= getdate() )
				and isnull(ih.collection_status,'WAITING') in ('WAITING','ERROR','MANUAL')
				and  (@user_name_c is null or cc.user_name = @user_name_c) 
				and isnull(cus.is_ignore,0) <> 1 
		group by cc.customer_company_id
				) as t
	),
	chq_control_red
	as
	(
	select count(*) as chq_control_red  from (
		select cc.customer_company_id
			from dbo.receipt re
			inner join  dbo.master_customer_company cc on re.customer_code = cc.customer_code and re.company_code = cc.company_code
			inner join dbo.master_customer cus on re.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
			inner join dbo.invoice_header h on re.assignment_no = h.invoice_no 
				and isnull(h.collection_status,'') <> 'COMPLETE'
			left join (dbo.cheque_control_document_item cci 
				 JOIN dbo.cheque_control_document ccd 
					on cci.cheque_control_id = ccd.cheque_control_id
					and ccd.cheque_control_status <> 'CANCEL')
			on re.reference_document_no = cci.reference_document_no
			 and cci.is_active = 1 
			 and cc.customer_company_id = ccd.customer_company_id
			left join dbo.master_customer_bill_presentment_behavior cbbb
				on h.bill_presentment_behavior_id = cbbb.bill_presentment_behavior_id
			where re.receipt_type = 'advance'
					and  (@user_name_c is null or cc.user_name = @user_name_c)
					and ccd.cheque_control_id is null
					and isnull(cbbb.is_billing_with_bill,0) = 0
					and re.is_active = 1
			group by cc.customer_company_id
			) as t

	),
	chq_control_yellow
	as
	(
		select count(*) as chq_control_yellow from (
			select cc.customer_company_id
			from dbo.cheque_control_document c
			inner join  dbo.master_customer_company cc on c.customer_company_id = cc.customer_company_id 
			where cheque_control_status = 'WAITINGRECEIVED'
						and  (@user_name_c is null or cc.user_name = @user_name_c)
			group by cc.customer_company_id
			) as t
	)

	select *  
	from 
	 advance_wc
	,advance_wr
	,billing_WRINV
	,billing_WR,
	deposit_wc,
	deposit_wr,
	cs_deposit_wc,
	cs_deposit_wr,
	otherdoc_wr,
	bill_following,
	bill_control,
	collection_following,
	chq_control_red,
	chq_control_yellow












END
GO
