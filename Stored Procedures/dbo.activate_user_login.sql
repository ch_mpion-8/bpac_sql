SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[activate_user_login]
	-- Add the parameters for the stored procedure here
	@username as nvarchar(100),
	@is_activate as bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @is_activate = 1 begin
		if exists(select 1 from user_login where username = @username) begin
			update user_login set fail_count = fail_count + 1,reference_datetime = GETDATE() where username = @username
		end
		else
		begin
			insert into user_login (username,fail_count,reference_datetime) values (@username,1,GETDATE())
		end
	end
	else
	begin
		update user_login set fail_count = 0,reference_datetime = GETDATE() where username = @username
	end
END
GO
