SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[check_valid_diff_chq_amount]
	-- Add the parameters for the stored procedure here
	@chq_amount decimal = null,
	@data_list varchar(max) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Declare @total_amount decimal 

	--;with a
	--	 as
	--	 (
	--		select SUBSTRING(item,0,CHARINDEX('_',item)) as invoice_no,
	--		SUBSTRING(item,CHARINDEX('_',item)+1,LEN(item)) as reference_no			 
	--		from dbo.SplitString(@data_list,',')
	--	 )

	--select @total_amount= sum(document_amount) 
	--from a 
	--inner join dbo.receipt r
	--on r.reference_document_no = a.reference_no
	--and r.assignment_no = a.invoice_no

	;with a as
	(
		select SUBSTRING(item,0,CHARINDEX('_',item)) as invoice_no,
		SUBSTRING(item,CHARINDEX('_',item)+1,LEN(item)) as reference_no			 
		from dbo.SplitString(@data_list,',')
	), chq_receipt as 
	(
		select sum(document_amount) as total_amount
		from a 
		inner join dbo.receipt r on r.reference_document_no = a.reference_no and r.assignment_no = a.invoice_no
	),chq_invoice as 
	(
		select sum(ci.amount) as total_amount
		from a
		inner join dbo.cheque_invoice_control_document_item ci on a.invoice_no = ci.invoice_no
		inner join dbo.cheque_invoice_control_document c on ci.cheque_control_id = c.cheque_control_id and  c.cheque_control_no = a.reference_no
		where ci.is_active = 1 and c.cheque_control_status <> 'CANCEL'
	), all_rec as
	(
		select total_amount
		from chq_invoice
		union
		select total_amount
		from chq_receipt
	)
		select @total_amount = sum(isnull(total_amount,0))
		from all_rec

	if abs(@chq_amount - @total_amount ) > 20
		select convert(bit,0) as valid
	else
		select convert(bit,1) as valid
END
GO
