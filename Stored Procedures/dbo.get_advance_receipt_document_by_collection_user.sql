SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_advance_receipt_document_by_collection_user]
	@user_code varchar(50) = null,
	@mode varchar(2) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	select 
		r.customer_code,
		r.company_code,
		cus.customer_name,
		r.clearing_document_no,
		r.document_date,
		c.user_display_name as 'collection_user',
		r.currency,
		r.[year],
		r.document_amount,
		r.reference_document_no,
		o.[status],
		o.method,
		o.created_by,
		o.document_no,
		o.document_date as other_document_date,
	 	CONVERT(varchar(20),o.document_id) as document_id,
		o.reference_no,
		r.is_active,
		com.company_name,
		o.reference_guid
	from dbo.receipt r
	inner join dbo.master_customer_company c
		on r.customer_code = c.customer_code
		and r.company_code = c.company_code
	inner join dbo.master_customer cus
		on cus.customer_code = c.customer_code
	inner join dbo.master_company com
		on com.company_code = r.company_code
	outer apply (
			select 
				o.document_id
				,o.document_no
				,o.document_date
				,o.created_by
				,o.created_date
				,o.method
				,o.[status]
				,o.reference_guid
				,i.reference_no
				,o.receiver
				,o.company_code 
			from dbo.other_control_document o 
			inner join dbo.other_control_document_item i 
				on i.document_id = o.document_id 
			where o.document_type=1
			and o.company_code = r.company_code
			and o.receiver = r.customer_code
			and i.[year] = r.[year]
			and i.reference_no = r.reference_document_no
		) o
	where
		r.receipt_type = 'normal'
		and (o.[status] is null and r.is_active =1 
			OR o.[status] is not null
			) 
		and isnull(o.[status],'WC') = UPPER(isnull(@mode,'WC'))
		and isnull(c.user_display_name,'Customer Need User') = @user_code
	UNION all
		select 
			distinct
			r.customer_code,
			r.company_code,
			cus.customer_name,
			r.clearing_document_no,
			r.document_date,
			mcc.user_display_name as 'collection_user',
			r.currency,
			r.[year],
			r.document_amount,
			r.reference_document_no,
			NULL as [status],
			NULL as method,
			NULL as created_by,
			NULL as document_no,
			NULL as other_document_date,
	 		NULL as document_id,
			NULL as reference_no,
			r.is_active,
			com.company_name,
			NULL as reference_guid
			from dbo.other_control_document o
			inner join  dbo.other_control_document_item i
			on i.document_id  = o.document_id
			inner join dbo.receipt r
			on r.reference_document_no = i.reference_no
			and r.company_code = o.company_code
			and r.customer_code = o.receiver
			and r.[year] = i.[year]
			inner join dbo.master_customer_company mcc
			on mcc.company_code = r.company_code
			and mcc.customer_code = r.customer_code
			inner join dbo.master_customer cus
			on cus.customer_code = r.customer_code
			inner join dbo.master_company com
			on com.company_code = r.company_code
			outer apply(
				select top 1 
				1 as is_exists
				from dbo.other_control_document ao 
					inner join dbo.other_control_document_item ai 
					on ao.document_id = ai.document_id
				where ai.reference_no = r.reference_document_no
					and ao.company_code = r.company_code
					and ao.receiver = r.customer_code
					and ai.[year] = r.[year]
					and ao.[status] <>'CANCEL'
			)as a
			where 
			o.document_type = 1
			and isnull(mcc.user_display_name,'Customer Need User') = @user_code
			and o.[status] ='CANCEL'
			and isnull(r.is_active,1) = 1
			and r.receipt_type = 'normal'
			and a.is_exists is null
			
		
END
GO
