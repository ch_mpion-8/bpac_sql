SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[delete_customer_from_ems]
	-- Add the parameters for the stored procedure here
	@ems_detail_id as int
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--update dbo.ems_control_document_item
	--set is_active = 0
	--	,updated_by = @updated_by
	--	,updated_date = getdate()
	--from dbo.ems_control_document_item
	--where ems_detail_id = @ems_detail_id 

	--update dbo.ems_sub_item
	--set is_active = 0
	--	,updated_by = @updated_by
	--	,updated_date = getdate()
	--from dbo.ems_sub_item
	--where ems_detail_id = @ems_detail_id 


	delete dbo.ems_sub_item
	where ems_detail_id = @ems_detail_id 


	delete dbo.ems_control_document_item
	where ems_detail_id = @ems_detail_id 

END
GO
