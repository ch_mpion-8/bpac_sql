SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_cheque_document_control_draft_group_detail]
	-- Add the parameters for the stored procedure here
	@receipt_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	;with docList as (
		select substring(item,0,charindex('|', item)) as customer_company_id
					,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as receipt_no 
					,right(item,charindex('|', reverse(item)) -1 ) as [year]
					from dbo.SplitString(@receipt_list,',')
	)
		select distinct cc.customer_company_id
				,r.company_code
				,com.company_name
				,r.customer_code
				,cus.customer_name
				, isnull(cbb.receipt_method ,'PYC') as receipt_method
				, r.currency
				, r.reference_document_no as receipt_list
				, receiptSum.amount as document_amount
				,r.document_date as document_date
				,year(r.[document_date]) as document_year --r.[year]  as document_year
		from dbo.receipt r
		inner join dbo.master_customer_company cc on r.company_code = cc.company_code and r.customer_code = cc.customer_code
		inner join dbo.master_customer cus on r.customer_code = cus.customer_code
		inner join dbo.master_company com on r.company_code = com.company_code
		left join dbo.invoice_header i on r.assignment_no = i.invoice_no
		left join dbo.master_customer_collection_behavior cbb 
				on cc.customer_company_id = cbb.customer_company_id
						and i.collection_behavior_id = cbb.collection_behavior_id
		inner join (
			
			select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,year([document_date]) as [year]
			from dbo.receipt
			where receipt_type = 'advance'
			group by reference_document_no,customer_code,company_code,year([document_date])
		) receiptSum on r.reference_document_no = receiptSum.reference_document_no 
					and year(r.[document_date]) = receiptSum.[year]
					and r.company_code = receiptSum.company_code
					and r.customer_code = receiptSum.customer_code
		inner join docList d on cc.customer_company_id = d.customer_company_id 
					and r.reference_document_no = d.receipt_no
		where  r.receipt_type = 'advance' and year(r.[document_date])  = d.[year]
		union 
		select distinct ct.customer_company_id
				,cc.company_code
				,com.company_name
				,cc.customer_code
				,cus.customer_name
				, ct.method as receipt_method
				, ct.currency
				, ci.reference_document_no as receipt_list
				, receiptSum.amount as document_amount
				,r.document_date as document_date
				,ci.document_year as document_year --r.[year]  as document_year
		from dbo.[cheque_item_temp] ci
		inner join dbo.[cheque_temp] ct on ci.temp_id = ct.temp_id
		inner join dbo.master_customer_company cc on ct.customer_company_id = cc.customer_company_id
		inner join dbo.master_company com on cc.company_code = com.company_code
		inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
		inner join dbo.receipt r on ci.reference_document_no = r.reference_document_no 
									and ci.document_year = year(r.[document_date])
		inner join (
			
				select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,year([document_date]) as [year]
				from dbo.receipt
				where receipt_type = 'advance'
				group by reference_document_no,customer_code,company_code,year([document_date])
			) receiptSum on r.reference_document_no = receiptSum.reference_document_no 
						and  year(r.[document_date]) = receiptSum.[year]
						and r.company_code = receiptSum.company_code
						and r.customer_code = receiptSum.customer_code
		where ci.temp_id in (
			select t.temp_id
				from dbo.[cheque_temp] t
				inner join dbo.[cheque_item_temp] bi on t.temp_id = bi.temp_id
				where bi.reference_document_no in (select receipt_no from docList)
		) and ci.is_active = 1
 --   	;with docList as (
	--	select substring(item,0,charindex('|', item)) as customer_company_id
	--				,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as receipt_no 
	--				,right(item,charindex('|', reverse(item)) -1 ) as [year]
	--				from dbo.SplitString(@receipt_list,',')
	--)
	--	select distinct cc.customer_company_id
	--			,r.company_code
	--			,com.company_name
	--			,r.customer_code
	--			,cus.customer_name
	--			, isnull(cbb.receipt_method ,'PYC') as receipt_method
	--			, r.currency
	--			, r.reference_document_no as receipt_list
	--			, receiptSum.amount as document_amount
	--			,r.document_date as document_date
	--			,year(r.document_date) as document_year
	--	from dbo.receipt r
	--	inner join dbo.master_customer_company cc on r.company_code = cc.company_code and r.customer_code = cc.customer_code
	--	inner join dbo.master_customer cus on r.customer_code = cus.customer_code
	--	inner join dbo.master_company com on r.company_code = com.company_code
	--	left join dbo.invoice_header i on r.assignment_no = i.invoice_no
	--	left join dbo.master_customer_collection_behavior cbb 
	--			on cc.customer_company_id = cbb.customer_company_id
	--					and i.collection_behavior_id = cbb.collection_behavior_id
	--	inner join (
			
	--		select  reference_document_no,customer_code,company_code,sum(document_amount) as amount,[year]
	--		from dbo.receipt
	--		where receipt_type = 'advance'
	--		group by reference_document_no,customer_code,company_code,[year]
	--	) receiptSum on r.reference_document_no = receiptSum.reference_document_no 
	--				and r.[year] = receiptSum.[year]
	--				and r.company_code = receiptSum.company_code
	--				and r.customer_code = receiptSum.customer_code
	--	inner join docList d on cc.customer_company_id = d.customer_company_id 
	--				and r.reference_document_no = d.receipt_no
	--	where  r.receipt_type = 'advance' and r.[year]  = d.[year]

END
GO
