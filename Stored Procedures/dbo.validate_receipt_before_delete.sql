SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[validate_receipt_before_delete]
	-- Add the parameters for the stored procedure here
	@cheaque_detail_id as int 
	,@error_message as nvarchar(max)  output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists (select 1
		from dbo.zero_loan_item
		where cheque_control_id = @cheaque_detail_id and zero_loan_status = 3)
		begin 
			set @error_message = 'ใบเสร็จนี้มีการขอ Zero Loan แล้ว คุณแน่ใจว่าจะลบใบเสร็จนี้'
		end

END
GO
