SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_customer_holiday]
	-- Add the parameters for the stored procedure here
	@customer_code as varchar(10) =  null--'2003137' 
	,@date_from as varchar(10) =  null--'20170413'
	,@date_to as varchar(10) =  null--'20170413'
	,@holiday_name as nvarchar(200) =  null--'วันสงกรานต์' 
	,@status as bit =  null--'ACTIVE'
	,@updated_by as nvarchar(50)  = null--
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select [date]
		,day([date]) as [day]
		,month([date]) as [month]
		,year([date]) as [year]
	into #tmpDate
	from (
	SELECT  TOP (DATEDIFF(DAY, @date_from, @date_to) + 1)
			[date] = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id) - 1, @date_from)

	FROM    sys.all_objects a CROSS JOIN sys.all_objects b
	) a
	--select * from #tmpDate		

	update [dbo].[master_customer_holiday]
		set holiday_name = @holiday_name
			,[is_active] = @status
			,[updated_date] = getdate()
			,[updated_by] = @updated_by
	--select  *
	from #tmpDate tmp 
	left join dbo.master_customer_holiday h on tmp.[year]  = h.holiday_year
		and  tmp.[month] = h.holiday_month
		and tmp.[day] = h.holiday_day
	where h.holiday_year is not null and customer_code = @customer_code
		

	insert into [dbo].[master_customer_holiday]
				(customer_code
				,holiday_year
				,holiday_month
				,holiday_day
				,holiday_name
				,[is_active]
				,[created_date]
				,[created_by]
				,[updated_date]
				,[updated_by])

	select @customer_code as customer_code
		,tmp.[year]
		,tmp.[month]
		,tmp.[day]
		,@holiday_name 
		,@status 
		,getdate() 
		,@updated_by
		,getdate()
		,@updated_by
	from #tmpDate tmp 	
		left join dbo.master_customer_holiday h on tmp.[year]  = h.holiday_year
			and  tmp.[month] = h.holiday_month
			and tmp.[day] = h.holiday_day
			and h.customer_code = @customer_code

		where  h.holiday_year is null 
 
	drop table #tmpDate

END




GO
