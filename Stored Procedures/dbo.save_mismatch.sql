SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_mismatch] 
	-- Add the parameters for the stored procedure here
	@mismatch_id int = null,
	@mismatch_date date ,
	@company_code nvarchar(4),
	@customer_code nvarchar(10),
	@reason nvarchar(max),
	@solution nvarchar(max),
	@interest_rate decimal(15,10),
	@interest_day int,
	@amount decimal(18,10),
	@mismatch_status int,
	@reference_guid as uniqueidentifier = null,
	@submit_by nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cus_com_id int;
	select  @cus_com_id=customer_company_id from master_customer_company where company_code = @company_code and customer_code = @customer_code

	if(@mismatch_id = null or @mismatch_id = 0)
		insert into mismatch (mismatch_date,customer_code,company_code,reason,solution,interest_day,interest_rate,submit_by,amount,created_date,mismatch_status,[reference_guid],created_by)
		values (@mismatch_date,@customer_code,@company_code,@reason,@solution,@interest_day,@interest_rate,@submit_by,@amount,GETDATE(),@mismatch_status,@reference_guid,@submit_by);
	else
		UPDATE mismatch SET interest_rate = @interest_rate,interest_day = @interest_day ,reason = @reason
		,solution=@solution,mismatch_status = @mismatch_status,
		customer_code = @customer_code,company_code = @company_code,
		[reference_guid]=@reference_guid,updated_by=@submit_by,updated_date=GETDATE()
		where mismatch_id = @mismatch_id
END

GO
