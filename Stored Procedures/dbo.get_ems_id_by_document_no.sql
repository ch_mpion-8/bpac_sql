SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[get_ems_id_by_document_no]
	-- Add the parameters for the stored procedure here
	@document_no varchar(30) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select e.ems_control_document_id as document_id from dbo.ems_control_document e where ems_control_document_no = @document_no
END

GO
