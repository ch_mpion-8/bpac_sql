SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_print_advance_receipt_document]
	@document_id as varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select 
	isnull(conf.[configuration_value], o.method) as method ,
	o.document_date as other_document_date,
	o.remark,
	o.document_no,
	c.customer_code,
	cus.customer_name,
	r.reference_document_no,
	r.clearing_document_no,
	r.document_date,
	r.document_amount,
	r.currency,
	c.user_display_name as collection_user,
	com.company_name
	from
	dbo.other_control_document o
	inner join dbo.other_control_document_item i 
	on o.document_id = i.document_id
	inner join dbo.receipt r
	on i.reference_no = r.reference_document_no
	and o.company_code = r.company_code
	and o.receiver = r.customer_code
	and i.[year] = r.[year]
	inner join dbo.master_customer_company c
	on c.company_code = r.company_code
	and c.customer_code = r.customer_code
	inner join dbo.master_customer cus 
	on cus.customer_code = c.customer_code
	inner join dbo.master_company com
	on com.company_code = c.company_code
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where o.document_id = @document_id
		and o.company_code = r.company_code
		and o.receiver = r.customer_code
  
END
GO
