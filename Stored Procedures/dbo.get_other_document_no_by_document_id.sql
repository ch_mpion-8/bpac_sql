SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_other_document_no_by_document_id]
	-- Add the parameters for the stored procedure here
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select document_no from dbo.other_control_document where document_id = @document_id
END
GO
