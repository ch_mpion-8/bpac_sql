SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_invoice_document_status] 
	-- Add the parameters for the stored procedure here
	@invoice_list AS NVARCHAR(2000) = NULL -- '1580311650|0230|2017,1580369810|0230|2019'
	,@status AS NVARCHAR(20) = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	select substring(item,0,charindex('|', item)) as invoice_no
			,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as company_code 
			,right(item,charindex('|', reverse(item)) -1 ) as [year]
			INTO #docList
			from dbo.SplitString(@invoice_list,',')

	UPDATE ih
	SET require_document_status = @status
		,print_document_status = CASE WHEN @status = 'COMPLETE' THEN @status ELSE print_document_status END 
	FROM dbo.invoice_header ih
	INNER JOIN #docList d ON  ih.invoice_no = d.invoice_no 
			AND ih.company_code = d.company_code 
			AND YEAR(ih.invoice_date) = d.[year]

	
	UPDATE ih
	SET print_count = 1
	FROM dbo.invoice_document_detail ih
	INNER JOIN #docList d ON  ih.invoice_no = d.invoice_no 
			AND ih.company_code = d.company_code 
			AND ih.[year] = d.[year]
	WHERE ih.print_count = 0

	DROP TABLE #docList
	

END
GO
