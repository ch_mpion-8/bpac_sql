SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_customer_typeahead]
	-- Add the parameters for the stored procedure here
	@search_word varchar(100) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@search_word is null)
		set @search_word = ''

	select distinct c.customer_code, c.customer_name
	from dbo.master_customer c
	inner join dbo.master_customer_company mcc
			on c.customer_code = mcc.customer_code
	where c.is_active = 1 
			and (c.customer_name like '%'+@search_word+'%' or c.customer_code like '%'+@search_word+'%')

	
END

GO
