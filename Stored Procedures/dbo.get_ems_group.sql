SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_ems_group]
	-- Add the parameters for the stored procedure here
	@document_list as nvarchar(max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   ;with doc_list as (
		select item 
		from dbo.SplitString(@document_list,',')
	)
	, list as (
		select c.customer_code
				,cus.customer_name
				,bd.bill_presentment_id as document_id
				,bd.bill_presentment_no as document_no
				,case when a.[address] is null then '' else a.[address] + ' ' end
							 + case when a.sub_district is null then '' else  a.sub_district + ' ' end
							 + case when a.district is null then '' else a.district + ' ' end
							 + case when a.province is null then '' else a.province + ' ' end
							 + case when a.post_code is null then '' else a.post_code + ' ' end
					as [address]
		from dbo.bill_presentment_control_document bd
		inner join dbo.master_customer_company c on bd.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		left join dbo.master_customer_address a on c.customer_company_id = a.customer_company_id and a.is_ems = 1
		where bd.bill_presentment_no in (select item from doc_list)
		union
		select case when isnull(ot.contact_person_type,1) = 1 then cus.customer_code else cast(ot.contact_person_id as nvarchar(50)) end as customer_code
				,case when isnull(ot.contact_person_type,1) = 1  then  cus.customer_name else con.customer_name end as customer_name 
				,ot.document_id as document_id
				,ot.document_no as document_no
				,case when cc.customer_company_id is null then 
				 (case when cus.street is null then '' else ' ' + cus.street end
						 + case when cus.district is null then '' else ' ' + cus.district end
						 + case when cus.city is null then '' else ' ' + cus.city end
						 + case when cus.post_code is null then '' else ' ' + cus.post_code end)
					else (case when a.[address] is null then '' else ' ' + a.[address] end
						 + case when a.sub_district is null then '' else ' ' + a.sub_district end
						 + case when a.district is null then '' else ' ' + a.district end
						 + case when a.province is null then '' else ' ' + a.province end
						 + case when a.post_code is null then '' else ' ' + a.post_code end)
					end as [address]
		from dbo.other_control_document ot
		left join dbo.master_customer_company cc on ot.company_code = cc.company_code and ot.receiver = cc.customer_code
		left join dbo.master_customer cus on ot.receiver = cus.customer_code
		left join dbo.master_other_contact_person con on ot.contact_person_id = con.other_contact_id
		left join dbo.master_customer_address a on cc.customer_company_id = a.customer_company_id and a.is_ems = 1
		where ot.document_no in (select item from doc_list)
	),group_doc as (
		select customer_code
		from list 
		group by customer_code
	)
	select gd.customer_code
			,customer_name
			, [address]
			, subdraft.document_list 
			, subdraft.document_id_list 
	from group_doc gd
	outer apply (
					select top 1 customer_code
								,customer_name
								, [address]
								,	stuff(
								(
									select ',' + sub.document_no 
										from list sub
										where sub.customer_code = main.customer_code
										for xml path('')
								) ,1,1,'') as document_list
								,	stuff(
								(
									select ',' + cast(sub.document_id as varchar(10))  
										from list sub
										where sub.customer_code = main.customer_code
										for xml path('')
								) ,1,1,'') as document_id_list
					from list main
					where main.customer_code = gd.customer_code
				) subdraft

END



GO
