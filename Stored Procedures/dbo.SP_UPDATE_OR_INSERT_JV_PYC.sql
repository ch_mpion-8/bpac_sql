SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPDATE_OR_INSERT_JV_PYC]
	-- Add the parameters for the stored procedure here
	@company_id int =null,
	@document_date date =null,
	@count_trip int =null,
	@submit_by nvarchar(50) =null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if exists(select 1 from dbo.jv_pyc_process where company_id = @company_id and document_date = @document_date)
	begin
		--update
		update dbo.jv_pyc_process
		set count_trip = @count_trip
		,updated_by =@submit_by
		,updated_date = GETDATE()
		where document_date = @document_date
		and company_id = @company_id
	end
	else
	begin
		insert into dbo.jv_pyc_process(
		document_date,
		company_id,
		count_trip,
		created_by,
		created_date)
		values(
		@document_date,
		@company_id,
		@count_trip,
		@submit_by,
		GETDATE())
	end
    
END
GO
