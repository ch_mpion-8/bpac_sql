SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_cheque_document_control_detail] 
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select bcdi.cheque_control_detail_id
			,bcdi.cheque_control_id
			,bcdi.reference_document_no
			,convert(varchar, r.document_date, 103) as document_date
			,r.payment_term
			,case when r.base_line_due_date is not null then convert(varchar, r.base_line_due_date , 103) 
				else '' end as  due_date
			,sum(r.document_amount) as document_amount
			,r.currency
	from dbo.cheque_control_document_item bcdi
	inner join dbo.cheque_control_document bcd on bcdi.cheque_control_id = bcd.cheque_control_id
	inner join dbo.master_customer_company c on bcd.customer_company_id = c.customer_company_id
	inner join dbo.receipt r on bcdi.reference_document_no = r.reference_document_no 
								and r.receipt_type = 'advance'
								and c.customer_code = r.customer_code 
								and c.company_code = r.company_code
								and year(r.document_date) = bcdi.document_year
	where bcdi.cheque_control_id = @cheque_control_id
		and (bcd.cheque_control_status = 'CANCEL' or bcdi.is_active = 1)
	group by bcdi.cheque_control_detail_id
			,bcdi.cheque_control_id
			,bcdi.reference_document_no
			,convert(varchar, r.document_date, 103) 
			,r.payment_term
			,case when r.base_line_due_date is not null then convert(varchar, r.base_line_due_date , 103) 
				else '' end 
			,r.currency	
		


	select [action]
			,[description]
			,convert(varchar, [bpac_due_date], 103) as [bpac_due_date] 
			,convert(varchar, [created_date], 103)+ ' '  + convert(varchar(8), [created_date], 14) as [created_date]
			,[created_by]
	from dbo.cheque_control_document_history
	where cheque_control_id = @cheque_control_id

	
	declare @payment_term as varchar(4)
			,@collection_date as date
			,@currency as varchar(5)
			,@customer_company_id as int


	select @payment_term = r.payment_term
			,@collection_date = bcd.collection_date
			,@currency = r.currency
			,@customer_company_id = bcd.customer_company_id
	from dbo.cheque_control_document bcd
	inner join dbo.cheque_control_document_item bcdi on bcd.cheque_control_id = bcdi.cheque_control_id
	inner join dbo.receipt r on bcdi.reference_document_no = r.reference_document_no
	where bcd.cheque_control_id = @cheque_control_id


	select distinct r.reference_document_no +'|' + cast(year(r.document_date) as nvarchar(50)) as value
	from dbo.receipt r
	inner join dbo.master_customer_company cc on r.company_code = cc.company_code and r.customer_code = cc.customer_code
	left join dbo.invoice_header ih on r.assignment_no = ih.invoice_no
	outer apply(
		select bcdis.reference_document_no 
				from dbo.cheque_control_document_item bcdis
				inner join dbo.cheque_control_document bcds
					on bcds.cheque_control_id = bcdis.cheque_control_id
				inner join dbo.master_customer_company mccs
					on mccs.customer_company_id = bcds.customer_company_id
				inner join dbo.receipt rr 
					on bcdis.reference_document_no = rr.reference_document_no
					and year(rr.document_date) = bcdis.document_year
					and rr.receipt_type = 'advance'
					and rr.company_code = mccs.company_code
					and rr.customer_code = mccs.customer_code
				where bcds.cheque_control_id = @cheque_control_id 
					and bcdis.is_active = 1
					--primary key for join
					and r.clearing_document_no = rr.clearing_document_no
					and r.[year] = rr.[year]
					and r.company_code = rr.company_code
					and r.reference_document_no = rr.reference_document_no
	)as had_created
	where cc.customer_company_id = @customer_company_id 
			and r.payment_term = @payment_term
			and r.currency = @currency
			--and (ih.collection_date is null or ih.collection_date= @collection_date)
			and had_created.reference_document_no is null

END
GO
