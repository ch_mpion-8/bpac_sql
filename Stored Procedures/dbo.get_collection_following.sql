SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_collection_following]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
		,@customer as nvarchar(200) = null
		,@customer_code as nvarchar(20) = null
		,@user_name as nvarchar(50) = null
		,@bpac_date_from as date = null
		,@bpac_date_to as date = null
		,@invoice_no as nvarchar(50) = null
		,@status as nvarchar(200) = null
		,@notify_date_from as date = null
		,@notify_date_to as date = null
		,@application_tag as nvarchar(50) = null
		,@collection_method as nvarchar(50) = null
		,@received_method as nvarchar(50) = null
		,@invoice_date_from as date = null
		,@invoice_date_to as date = null
		,@is_no_doc as bit = null
		,@is_over_due as bit = null
		,@is_bpac_over_due as bit = NULL
        ,@net_due_date_from AS DATE = NULL
        ,@net_due_date_to AS DATE = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @query_string  nvarchar(max) = ''
				,@param_list nvarchar(4000) = ''
			set @query_string = N'select * from (select distinct cc.customer_company_id
		,i.company_code
		,com.company_name
		,i.customer_code
		,cus.customer_name
		,mccb.description as payment_description
		,mccb.payment_method
		,bank.bank_name
		, isnull(conf.[configuration_value] ,mccb.receipt_method) as receipt_method
		,re.reference_document_no
		, case when mccb.document is not null then (select stuff(
					(
						select '','' + docName 
							+ case when mccb.document is null or a.Item <> ''3'' then '''' else '' '' + mccb.custom_document end
							from (
							select docName = configuration_value, item = s.item
							from dbo.SplitString(mccb.document,'','') s
							inner join  dbo.configuration  c 
								on s.item = c.configuration_code and c.configuration_key = ''CollectionDocument''

						) a
							for xml path('''')
					) ,1,1,'''') as txt)
			else null end as document
		, i.invoice_no
		--, term.payment_term_name
		, case when cc.company_code in  (''7850'',''0470'') then '''' else i.payment_term end  as payment_term_name
		, i.invoice_amount + isnull(i.tax_amount,0) as invoice_amount
		, (i.invoice_amount + isnull(i.tax_amount,0) )-isnull(i.collection_amount,0.00) as remaining_amount
		, isnull(i.collection_amount,0.00)  as collection_amount
		, i.currency
		, bb.bill_presentment_no
		,bb.bill_presentment_date
		,ISNULL(ccd.cheque_control_id,ccd2.cheque_control_id) AS cheque_control_id
		,isnull(CAST(ccd.cheque_control_no  AS NVARCHAR(200)) , ccd2.cheque_control_no ) as cheque_control_no
		,ISNULL(ccd.collection_date,ccd2.collection_date) AS collection_date
		, i.invoice_date
		,  dateadd(dd,1,i.invoice_date) as delivery_date
		,case when cc.company_code in  (''7850'',''0470'') then i.net_due_date else i.due_date end  as due_date
		,i.net_due_date
		, DATEDIFF ( dd , i.net_due_date , getdate() )  as  arrears
		, i.collection_date  as bpac_date
		, i.collection_notify_date
		, replace(isnull(i.collection_status,''WAITING''),''MANUAL'',''WAITING'') as collection_status
		, cc.user_display_name
		, mccb.notify_day
		--,0 as notify_day
		, mccb.remark
		, ISNULL(cci.cheque_remark,cci2.cheque_remark) as comment
		, --mccb.collection_behavior_id
		null as collection_behavior_id
		, i.collection_remark as invoice_remark
		,app.application_tag_name
		, CASE WHEN ccd2.cheque_control_id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS is_cheque_invoice
	from dbo.invoice_header i
	inner join  dbo.master_customer_company cc on i.customer_code = cc.customer_code and i.company_code = cc.company_code
	inner join dbo.master_customer cus on i.customer_code = cus.customer_code and isnull(cus.is_ignore,0) = 0
	inner join dbo.master_company com on i.company_code = com.company_code
	left join dbo.master_application_tag app on cus.application_tag_id = app.application_tag_id	
	left join dbo.invoice_detail id on i.invoice_no = id.invoice_no
	left join dbo.receipt re on  i.invoice_no = re.assignment_no and re.[is_active] = 1 and i.customer_code = re.customer_code and i.company_code = re.company_code
	outer apply  (
		select 
			b.bill_presentment_no,
			b.bill_presentment_date
		from dbo.bill_presentment_control_document_item a 
		inner join dbo.bill_presentment_control_document b
			on a.bill_presentment_id = b.bill_presentment_id
		inner join dbo.master_customer_company c
			on c.customer_company_id = b.customer_company_id
		where 
			i.invoice_no = a.invoice_no 
			and i.company_code = c.company_code
			and i.customer_code = c.customer_code
			and a.is_active = 1
	)as bb
	left join (dbo.cheque_control_document_item cci 
		 JOIN dbo.cheque_control_document ccd 
			on cci.cheque_control_id = ccd.cheque_control_id)
	 on re.reference_document_no = cci.reference_document_no
		and year(re.[document_date]) = cci.document_year
		 and cci.is_active = 1 
		 and cc.customer_company_id = ccd.customer_company_id
		 and ccd.cheque_control_status <> ''CANCEL''
	LEFT JOIN (dbo.cheque_invoice_control_document_item cci2 
		 JOIN dbo.cheque_invoice_control_document ccd2 
			on cci2.cheque_control_id = ccd2.cheque_control_id)
	 on i.invoice_no = cci2.invoice_no
		 and cci2.is_active = 1 
		 and cc.customer_company_id = ccd2.customer_company_id
		 and ccd2.cheque_control_status <> ''CANCEL''
	left join master_customer_collection_behavior mccb
		on mccb.collection_behavior_id = i.collection_behavior_id
	
	left join dbo.configuration conf on mccb.receipt_method = conf.[configuration_code] and conf.[configuration_key] = ''ReceiveMethod''	
	left join (
					select  main.collection_behavior_id, stuff(
					(
						select '','' + bank_name 
							from dbo.SplitString(main.bank,'','') s
							inner join dbo.master_bank b on s.Item = b.bank_id
							for xml path('''')
					) ,1,1,'''') as bank_name
					from dbo.master_customer_collection_behavior main
				) bank on mccb.collection_behavior_id = bank.collection_behavior_id	
				where 1=1
				'

	print @query_string
	if @company_code is not null
		set @query_string = @query_string + ' and i.company_code  in ( select item from dbo.SplitString(@usp_company_code,'','')) '

	if @customer is not null 
		set @query_string = @query_string + ' and (i.customer_code like ''%'' + @usp_customer + ''%'' or cus.customer_name  like ''%'' + @usp_customer + ''%'') '
	
	if @customer_code is not null 
		set @query_string = @query_string + ' and (i.customer_code =  @usp_customer_code ) '


	if @user_name is not null
		set @query_string = @query_string + ' and isnull(cc.user_name,''Customer Need User'') = @usp_user_name '

/*if @bpac_date_from is not null   
		begin
			set @query_string = @query_string + ' and (
					(
						 ccd.collection_date between @usp_bpac_date_from and @usp_bpac_date_to
					))'
		end*/

		if @bpac_date_from is not null   
		begin
			set @query_string = @query_string + ' and (
					(
						 i.collection_date between @usp_bpac_date_from and @usp_bpac_date_to
					))'
		end

	if @invoice_no is not null
		set @query_string = @query_string + ' and i.invoice_no like ''%'' + @usp_invoice_no + ''%'' '

	
	if @status is not null
		set @query_string = @query_string + ' and replace(isnull(i.collection_status,''WAITING''),''MANUAL'',''WAITING'') in (select item from dbo.SplitString(@usp_status,'',''))  '

	IF @notify_date_from IS NOT NULL
	BEGIN
		SET @query_string = @query_string + ' and (i.collection_notify_date  is null or 
						(
							i.collection_notify_date = @usp_notify_date_from 
							or ((@usp_status is null or @usp_status like ''%WAITING%'') and i.collection_notify_date <= @usp_notify_date_from )
						) )'
	END
    
	if @net_due_date_from is not null   
		begin
			set @query_string = @query_string + ' and  i.net_due_date between @usp_net_due_date_from and @usp_net_due_date_to'
		end

	if @application_tag is not null
		set @query_string = @query_string + ' and isnull(cus.application_tag_id,999) in (select item from dbo.SplitString(@usp_application_tag,'',''))  '

	if @collection_method is not null
		set @query_string = @query_string + ' and exists (
						select charindex( item, mccb.payment_method, 1)
						from dbo.SplitString(@usp_collection_method,'','')
							 where charindex( item, mccb.payment_method, 1)  > 0)  '
		
	if @received_method is not null
		set @query_string = @query_string + ' and mccb.receipt_method = @usp_received_method  '


	if @invoice_date_from is not null
		set @query_string = @query_string + ' and i.invoice_date between @usp_invoice_date_from and @usp_invoice_date_to '
	
	--print @is_no_doc
	--print @status

	if @is_no_doc = 1  or @is_over_due = 1  or @is_bpac_over_due = 1 
	begin
		declare @sub_query as nvarchar(200) = ''
		if @is_no_doc = 1 and @status like '%WAITING%'
			select @sub_query =  '( cci.cheque_control_id is null and cci2.cheque_control_id is null )' 
		
		if @is_over_due = 1 
			select @sub_query = @sub_query + case when @sub_query = '' then ' ' else ' or ' end + ' (DATEDIFF ( dd , i.net_due_date , getdate() ) > 0  ) '
			
		if @is_bpac_over_due = 1 
			select @sub_query = @sub_query + case when @sub_query = '' then ' ' else ' or ' end + ' ( i.collection_date is not null and i.collection_date > i.net_due_date) '
		
		print @sub_query

		set @query_string = @query_string + ' and ( ' + @sub_query + ' )'
	end




	set @query_string = @query_string + ' ) a order by case collection_status 
		when ''ERROR'' then  1
		when ''WAITING'' then  2
		when ''WAITINGRECEIVED'' then  3
		when ''COMPLETE'' then  4
		else 5 end
		,payment_method
		,company_code
		,customer_code
		,collection_date
		,invoice_no
	'

	set @param_list = '@usp_company_code as varchar(200) = null
		,@usp_customer as nvarchar(200) = null
		,@usp_customer_code as nvarchar(50) = null
		,@usp_user_name as nvarchar(50) = null
		,@usp_bpac_date_from as date = null
		,@usp_bpac_date_to as date = null
		,@usp_invoice_no as nvarchar(50) = null
		,@usp_status as nvarchar(200) = null
		,@usp_notify_date_from as date = null
		,@usp_notify_date_to as date = null
		,@usp_application_tag as nvarchar(50) = null
		,@usp_collection_method as nvarchar(50) = null
		,@usp_received_method as nvarchar(50) = null
		,@usp_invoice_date_from as date = null
		,@usp_invoice_date_to as date = null
		,@usp_is_no_doc as bit = null
		,@usp_net_due_date_from as date =null
		,@usp_net_due_date_to as date =null'

--print @param_list
--  select @query_string
	EXEC sp_executesql @query_string, @param_list
		,@company_code
		,@customer
		,@customer_code
		,@user_name
		,@bpac_date_from
		,@bpac_date_to
		,@invoice_no
		,@status
		,@notify_date_from
		,@notify_date_to
		,@application_tag
		,@collection_method
		,@received_method
		,@invoice_date_from
		,@invoice_date_to
		,@is_no_doc
		,@net_due_date_from
		,@net_due_date_to
END
GO
