SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_invoice_document_for_print]
	-- Add the parameters for the stored procedure here
	@invoice_list AS NVARCHAR(2000) = NULL -- '1580311650|0230|2017,1580369810|0230|2019'
		,@document_type AS NVARCHAR(20) = NULL  -- = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @document_type = CASE WHEN @document_type = 'INV' THEN 'INV,DN,CN,DL' ELSE @document_type END

	select substring(item,0,charindex('|', item)) as invoice_no
			,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as company_code 
			,right(item,charindex('|', reverse(item)) -1 ) as [year]
			INTO #invList
			from dbo.SplitString(@invoice_list,',')

 --   ;with docList as (
	--	select substring(item,0,charindex('|', item)) as invoice_no
	--				,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item) - charindex('|', reverse(item))) as company_code 
	--			 ,right(item,charindex('|', reverse(item)) -1 ) as [year]
	--				from dbo.SplitString(@invoice_list,',')
	--)
	SELECT id.invoice_no
			,id.company_code
			,id.[year]
			,id.document_type
			,id.document_name
			,id.document_id
			,id.print_count
	FROM dbo.invoice_document_detail id
	INNER JOIN #invList d ON id.invoice_no = d.invoice_no AND id.company_code = d.company_code AND id.year = d.year
	WHERE @document_type IS NULL OR @document_type ='ALL' 
			OR id.document_type IN (SELECT item FROM dbo.SplitString(@document_type,','))

			
	UPDATE ih
	SET print_count = ISNULL(ih.print_count,0) + 1
	FROM dbo.invoice_document_detail ih
	INNER JOIN #invList d ON  ih.invoice_no = d.invoice_no 
			AND ih.company_code = d.company_code 
			AND ih.[year] = d.[year]

	
	DECLARE @invoice_no as nvarchar(20) = NULL -- '1580373596' -- 1580369810
	,@company_code as varchar(4) = NULL -- '0230'
	,@year as int = NULL -- 2019

	DECLARE inv_cur CURSOR FOR

		SELECT invoice_no,company_code, [year]
		FROM #invList
    
	OPEN inv_cur;  
	FETCH NEXT FROM inv_cur INTO @invoice_no, @company_code, @year;  
  
	-- Check @@FETCH_STATUS to see if there are any more rows to fetch.  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		EXECUTE dbo.validate_invoice_require_document @invoice_no ,@company_code,@year
		

	 FETCH NEXT FROM inv_cur  INTO  @invoice_no, @company_code, @year;  
	END  
	CLOSE inv_cur;  
	DEALLOCATE inv_cur;  

	DROP TABLE #invList
END
GO
