SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_status_customer_bill_presentment_behavior]
	-- Add the parameters for the stored procedure here
	@bill_presentment_behavior_id as int = null
	,@status as bit  = null 
	,@updated_by as nvarchar(20) = null--'ACTIVE'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update [dbo].[master_customer_bill_presentment_behavior]
	set [is_active] = @status
		,updated_by = @updated_by
		,updated_date = getdate()
	--select *
	from [dbo].[master_customer_bill_presentment_behavior]
	where group_id = @bill_presentment_behavior_id

END



GO
