SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_other_contact_person]
	-- Add the parameters for the stored procedure here
	 @other_contact_id as bigint = null
	,@customer_name as varchar(100) = null
	,@title as varchar(100) = null
	,@telephone as varchar(100) = null
	,@contact_person as varchar(100) = null
	,@remark as varchar(1000) = null
	,@customer_code as varchar(20) = null
	,@created_by as varchar(50) = null
	,@created_date as datetime = null
	,@updated_by as varchar(50) = null
	,@updated_date as datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if exists (select 1 from dbo.master_other_contact_person where other_contact_id = @other_contact_id)
	begin
		update dbo.master_other_contact_person set
		customer_name = @customer_name
		,title = @title
		,telephone = @telephone
		,contact_person = @contact_person
		,remark = @remark
		,customer_code = @customer_code
		,created_by = @created_by
		,created_date = @created_date
		,updated_by = @updated_by
		,updated_date = @updated_date
		where other_contact_id = @other_contact_id
	end
	else
	begin
		insert into dbo.master_other_contact_person
		(
		 customer_name
		, title
		, telephone
		, contact_person
		, remark
		, customer_code
		, created_by
		, created_date
		, updated_by
		, updated_date
		, is_active)
		values(
		  @customer_name
		, @title
		, @telephone
		, @contact_person
		, @remark
		, @customer_code
		, @created_by
		, @created_date
		, @updated_by
		, @updated_date
		, 1)
	end
END
GO
