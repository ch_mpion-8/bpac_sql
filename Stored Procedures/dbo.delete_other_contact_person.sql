SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[delete_other_contact_person]
	-- Add the parameters for the stored procedure here
	@other_contact_id bigint =null,
	@status bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update  dbo.master_other_contact_person 
	set is_active = isnull(@status,1)
	where other_contact_id =@other_contact_id
END
GO
