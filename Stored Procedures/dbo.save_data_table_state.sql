SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_data_table_state]
	-- Add the parameters for the stored procedure here
	@key nvarchar(100)=null,
	@user varchar(50)=null,
	@is_active bit = null,
	@submited_by varchar(50)=null,
	@data_table_state nvarchar(max)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @is_active is null
	begin
		Declare @f bit =null
		select top 1 @f = is_active from dbo.data_table_state where [key] like SUBSTRING(@key,CHARINDEX('/',@key),LEN(@key))
		and [user] = @user

		if @f is not null
		begin
			set @is_active = @f
		end
		else
		begin
			set @is_active = 0
		end
	end
	else
	begin
		update dbo.data_table_state 
		set is_active = @is_active
		where [user] = @user
		and [key] like SUBSTRING(@key,CHARINDEX('/',@key),LEN(@key))
	end

    if exists(select 1 from dbo.data_table_state where [user] = @user and [key]=@key)
	begin
		update dbo.data_table_state
		set data_table_state = @data_table_state,
		is_active = @is_active,
		updated_by = @submited_by,
		updated_date = getdate()
		where [user] = @user and [key]=@key
	end
	else
	begin
		insert into dbo.data_table_state(
		[user],
		[key],
		data_table_state,
		created_by,
		created_date)
		values(
		@user,
		@key,
		@data_table_state,
		@submited_by,
		getdate()
		)
	end
END
GO
