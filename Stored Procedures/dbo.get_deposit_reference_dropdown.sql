SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_deposit_reference_dropdown] 
	-- Add the parameters for the stored procedure here
	@company_code varchar(4) =null,
	@customer_code varchar(10)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
	select  distinct
	dep.dp_no as reference_no from dbo.deposit dep
	left join dbo.other_control_document_item i 
	on i.reference_no = dep.dp_no
	left join dbo.other_control_document o
	on o.document_id = i.document_id
	where 
	dep.company_code = @company_code
	and dep.customer_code =@customer_code
	and  isnull(o.[status],'CANCEL') = 'CANCEL' 
	and (select TOP 1 1 from dbo.other_control_document_item ai inner join dbo.other_control_document ao on ai.document_id= ao.document_id
	where ai.reference_no = dep.dp_no
	and ao.document_type = 3
	and ao.[status] <>'CANCEL') is null

		
END
GO
