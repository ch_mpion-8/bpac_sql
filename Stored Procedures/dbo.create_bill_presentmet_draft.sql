SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_bill_presentmet_draft]
	-- Add the parameters for the stored procedure here
	 @document_control_list  as [dbo].[document_control_table_type] readonly
	 ,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	declare @customer_company_id [int]  = NULL,
		@bill_presentment_date [date] =  NULL,
		@method [nvarchar](20) = NULL,
		@payment_term [varchar](4) = NULL,
		@currency [varchar](5) = NULL,
		@remark [nvarchar](2000) = NULL,
		@reference_no_list [nvarchar](max) = NULL

	--select * from dbo.bill_presentment_temp

	--select * from dbo.bill_presentment_item_temp

	declare table_cursor cursor for 

	select [customer_company_id]
			,[date]
			,[method]
			,[payment_term]
			,[currency]
			,[remark]
			,[reference_no_list]
	from @document_control_list

	open table_cursor;
	fetch next from table_cursor into @customer_company_id,@bill_presentment_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	while @@fetch_status = 0
	   begin
      
		  declare @tempId as int = null

		  if exists (select 1 from dbo.bill_presentment_temp 
								where customer_company_id = @customer_company_id
									and payment_term = @payment_term
									and method = @method
									and currency = @currency
									and bill_presentment_date = @bill_presentment_date )
			begin
			
				select @tempId = temp_id from dbo.bill_presentment_temp 
								where customer_company_id = @customer_company_id
									and payment_term = @payment_term
									and method = @method
									and currency = @currency
									and bill_presentment_date = @bill_presentment_date

				update dbo.bill_presentment_temp
				set remark = @remark
					,updated_by = @updated_by
					,updated_date = getdate()
				where temp_id = @tempId
				--PRINT 'Update temp Id' + @tempId
			end
			else
			begin
			
				insert into  dbo.bill_presentment_temp
				(
					[customer_company_id]
					,[bill_presentment_date]
					,[method]
					,[payment_term]
					,[currency]
					,[remark]
					,created_by
					,created_date
					,updated_by
					,updated_date
				)
				values
				(
					@customer_company_id
					,@bill_presentment_date
					,@method
					,@payment_term
					,@currency
					,@remark
					,@updated_by
					,getdate()
					,@updated_by
					,getdate()
				)

				select @tempId = SCOPE_IDENTITY()
				--PRINT 'Insert temp Id' + @tempId
			end

			select   substring(item,0,charindex('|', item)) as item
			into #invoice
			from dbo.SplitString(@reference_no_list,',')
			 

			update dbo.bill_presentment_item_temp
			set is_active = 0
				,updated_by = @updated_by
				,updated_date = getdate()	
			--where temp_id <> @tempId and invoice_no in (select rtrim(ltrim(item)) as item from dbo.SplitString(@reference_no_list,','))
			where temp_id <> @tempId and invoice_no in (select rtrim(ltrim(item)) as item from #invoice)

			update dbo.bill_presentment_item_temp
			set is_active = 1
				,updated_by = @updated_by
				,updated_date = getdate()
			--where temp_id = @tempId and invoice_no in (select rtrim(ltrim(item)) as item from dbo.SplitString(@reference_no_list,','))
			where temp_id = @tempId and invoice_no in (select rtrim(ltrim(item)) as item from #invoice)

			insert into dbo.bill_presentment_item_temp
			(
				temp_id
				,invoice_no
				,created_by
				,created_date
				,updated_by
				,updated_date
				,is_active
			)
			select @tempId
					,rtrim(ltrim(item)) as item
					,@updated_by
					,getdate()
					,@updated_by
					,getdate()
					,1
			--from dbo.SplitString(@reference_no_list,',') ss
			from #invoice ss
			left join dbo.bill_presentment_item_temp tmp on rtrim(ltrim(ss.Item)) = tmp.invoice_no and tmp.temp_id = @tempId
			where tmp.invoice_no is null

			drop table #invoice
	   
		  fetch next from table_cursor into  @customer_company_id,@bill_presentment_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	   end;

	close table_cursor;
	deallocate table_cursor;

END





GO
