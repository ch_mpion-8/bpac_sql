SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[create_ems_document_control]
	-- Add the parameters for the stored procedure here
	 @control_list  as [dbo].[ems_table_type]  readonly
	 ,@company_code as nvarchar(4) = null
	 ,@department as nvarchar(50) = null
	 ,@cost_center as nvarchar(50) = null
	 ,@cost_code as nvarchar(50) = null
	 ,@send_date as date = null
	 ,@updated_by as nvarchar(50) = null
	 ,@is_manual as bit = null
	 ,@control_id as int = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @customer_code [varchar](10)  = NULL,
		@remark [nvarchar](2000) = NULL,
		@reference_no_list [nvarchar](max) = NULL

	--select * from ems_sub_item
	
	--declare @control_id as int = null

	
	declare @ems_id_new table(new_id int)
			
	insert into  dbo.ems_control_document
	(
		[send_date]
		,company_code
		,department
		,cost_center
		,cost_code
		,is_manual
		,[status]
		,created_by
		,created_date
		,updated_by
		,updated_date
	)
	output inserted.ems_control_document_id into @ems_id_new
	values
	(
		@send_date
		,@company_code
		,@department
		,@cost_center
		,@cost_code
		,@is_manual
		,'WAITINGRECEIVED'
		,@updated_by
		,getdate()
		,@updated_by
		,getdate()
	)

	--select @control_id = IDENT_CURRENT('ems_control_document')
	select @control_id = new_id
	from @ems_id_new


	declare table_cursor cursor for 

	select cast([customer_code] as [varchar](10) ) 
			,[remark]
			,[reference_no_list]
	from @control_list

	open table_cursor;
	fetch next from table_cursor into @customer_code,@remark,@reference_no_list
	while @@fetch_status = 0
	   begin
      
	   declare @control_detail_id as int = null
	   
		declare @ems_detail_id_new table(new_id int)
			

			insert into dbo.ems_control_document_item
			(
				[ems_control_document_id]
				,[customer_code]
				,[remark]
				,[is_active]
				,created_by
				,created_date
				,updated_by
				,updated_date
			)
			output inserted.ems_detail_id into @ems_detail_id_new
			values(
				@control_id
				,@customer_code
				,@remark
				,1
				,@updated_by
				,getdate()
				,@updated_by
				,getdate()
			)
			--select @control_detail_id = IDENT_CURRENT('ems_control_document_item')
			select @control_detail_id = new_id
			from @ems_detail_id_new

			insert into dbo.ems_sub_item
			(
				ems_detail_id
				,reference_id
				,reference_type
				,is_active
				,created_by
				,created_date
				,updated_by
				,updated_date
			)
			select @control_detail_id
					,isnull(bd.bill_presentment_id,ot.document_id) as item
					,LEFT(item, 1)
					,1
					,@updated_by
					,getdate()
					,@updated_by
					,getdate()
			from dbo.SplitString(@reference_no_list,',') s
			left join dbo.bill_presentment_control_document bd on s.Item = bd.bill_presentment_no
			left join dbo.other_control_document ot on s.Item = ot.document_no


	   
		  fetch next from table_cursor into   @customer_code,@remark,@reference_no_list
	   end;

	close table_cursor;
	deallocate table_cursor;
END
GO
