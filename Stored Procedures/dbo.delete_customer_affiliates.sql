SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_customer_affiliates]
	-- Add the parameters for the stored procedure here
	--@source_customer_code as varchar(10) = null --'2010550'
	--	,@destination_customer_code as varchar(10) =  null --'2011271'
	--	,@affiliates_id as int = null -- 2
		@affiliate_detail_id as int = null -- 2
		,@updated_by as nvarchar(20) = null--'ACTIVE'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --  update [dbo].master_customer_affiliates 
	--set [status] = 'CANCEL'
	--	,updated_by = @updated_by
	--	,updated_date = getdate()
	----select *
	--from [dbo].master_customer_affiliates aff 
	--where customer_code = @destination_customer_code
	
	--if( (select count(1) from [dbo].master_customer_affiliates 
	--			where affiliates_id = @affiliates_id and [status] = 'ACTIVE') <= 1)
	--begin
	--	update [dbo].master_customer_affiliates 
	--	set [status] = 'CANCEL'
	--		,updated_by = @updated_by
	--		,updated_date = getdate()
	--	--select *
	--	from [dbo].master_customer_affiliates aff 
	--	where customer_code = @source_customer_code
	--end

	 update [dbo].master_customer_affiliate_list 
	set [is_active] = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	--select *
	from [dbo].master_customer_affiliate_list aff 
	where affiliate_detail_id = @affiliate_detail_id

END




GO
