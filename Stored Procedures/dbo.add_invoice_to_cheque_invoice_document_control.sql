SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[add_invoice_to_cheque_invoice_document_control]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
	,@invoice_no as varchar(16)
	,@amount as DECIMAL(15,2)
	,@updated_by as nvarchar(50)
	-- ,@year as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists(select 1 from dbo.cheque_invoice_control_document_item 
					where cheque_control_id = @cheque_control_id 
							and invoice_no = @invoice_no)
	begin
		update  dbo.cheque_invoice_control_document_item
		set is_active = 1
			,amount = @amount
			,updated_by = @updated_by
			,updated_date = getdate()
		where cheque_control_id = @cheque_control_id 
			and invoice_no = @invoice_no
	end
	else
	begin
		insert into dbo.cheque_invoice_control_document_item
		(
			cheque_control_id
			,invoice_no
			, amount
			,is_active
			,created_date
			,created_by
			,updated_date
			,updated_by
			--,document_year
		)
		values
		(
			@cheque_control_id
			,@invoice_no
			,@amount
			,1
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
			--,@year
		)
	end

	
	update invoice_header
	set remaining_amount = remaining_amount - @amount
	where invoice_no = @invoice_no

	--declare @description as nvarchar(100)
	--set @description = 'Add receipt ' + @receipt_no

	exec dbo.insert_cheque_invoice_control_history 
			@cheque_control_id = @cheque_control_id
			,@description = @invoice_no
			,@action  = 'Add invoice'
			,@updated_by  = @updated_by
END

GO
