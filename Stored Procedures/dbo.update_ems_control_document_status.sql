SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[update_ems_control_document_status]
	-- Add the parameters for the stored procedure here
	@ems_doucment_id as int
	,@status as nvarchar(20) 
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update dbo.ems_control_document
	set [status] = @status
		,updated_by = @updated_by
		,updated_date = getdate()
	where ems_control_document_id = @ems_doucment_id

	
		
	declare @change_status_name as nvarchar(200) = null
			,@id as nvarchar(200)
			,@type as nvarchar(200)
	select @change_status_name = case @status when 'WAITINGRECEIVED' then 'Reverse status to Waiting for receive control doc.'
														  when 'COMPLETE' then 'Complete'
														  when 'CANCEL' then 'Cancel'
								 end
		
	
	declare table_cursor cursor for 

		
	select reference_id, reference_type
	from  dbo.ems_control_document e
	inner join dbo.ems_control_document_item i on e.ems_control_document_id = i.ems_control_document_id
	inner join dbo.ems_sub_item s on i.ems_detail_id = s.ems_detail_id
	where i.is_active= 1 and s.is_active = 1 and e.ems_control_document_id = @ems_doucment_id

	open table_cursor;
	fetch next from table_cursor into @id,@type
	while @@fetch_status = 0
	   begin

			
			if @type = 'B'
			begin
					
				if exists (
					select 1
					from dbo.bill_presentment_control_document
					where bill_presentment_status = 'COMPLETE' and bill_presentment_id = @id and reference_guid is null)
				begin 
					exec dbo.update_bill_presentment_control_document_status
							@bill_presentment_id = @id
							,@status  = 'WAITINGRECEIVED'
							,@updated_by  = @updated_by


					exec dbo.insert_bill_presentment_history 
							@bill_presentment_id = @id
							,@description = 'Reverse status from ems document control.'
							,@action  = 'Reverse status.'
							,@updated_by  = @updated_by
				end
			end
			else if  @type = 'O'
			begin
				if exists (
					select 1
					from dbo.other_control_document
					where status = 'C' and document_id = @id and reference_guid is null)
				begin 
					
					update dbo.other_control_document
						   set updated_by = @updated_by,
						   update_date = getdate(),
						   [status] = case when document_type = 4 then 'Waiting' else 'WR'end
						   from dbo.other_control_document
						   where document_id= @id

					insert into dbo.other_control_document_history(document_id,created_by,created_date,[description],[action])
					 values(@id,@updated_by,getdate(),'Reverse status from ems document control.','Reverse.')
				end
			end

		  fetch next from table_cursor into @id,@type
	   end;

	close table_cursor;
	deallocate table_cursor;

	exec  dbo.insert_ems_control_history
			@ems_control_document_id = @ems_doucment_id
			,@description = @change_status_name
			,@action  = 'Update status'
			,@updated_by  = @updated_by



END


GO
