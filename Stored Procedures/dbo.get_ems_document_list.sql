SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_ems_document_list]
	@company_code as varchar(4) = null
		,@customer as nvarchar(50) = null
		,@customer_code as varchar(20) = null
		,@bill_presentment_no as nvarchar(50) = null
		,@bill_presentment_date_from as date = null
		,@bill_presentment_date_to as date = null
		,@other_document_no as nvarchar(50) = null
		,@other_document_date_from as date = null
		,@other_document_date_to as date = null
		,@running_no as nvarchar(50) = null
		,@send_date as date = null
		,@status as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;with list as (
		select bd.customer_company_id
				,c.company_code
				,c.customer_code
				,cus.customer_name
				,case when a.[address] is null then '' else ' ' + a.[address] end
						 + case when a.sub_district is null then '' else ' ' + a.sub_district end
						 + case when a.district is null then '' else ' ' + a.district end
						 + case when a.province is null then '' else ' ' + a.province end
						 + case when a.post_code is null then '' else ' ' + a.post_code end
					as [address]
				,bd.bill_presentment_id as document_id
				,bd.bill_presentment_no as document_no
				,bd.bill_presentment_date as document_date
				,'ใบคุมวางบิล' as document_type
				,ems.ems_control_document_id
				,ems.ems_control_document_no as running_no
				,ems.send_date
				,ems.ems_doc_status as status
				,ems.reference_guid
				,ems.item_is_active as item_is_active
				,ems.sub_is_active as sub_is_active
				,bd.bill_presentment_status as doc_status
		from dbo.bill_presentment_control_document bd
		inner join dbo.master_customer_company c on bd.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		left join dbo.master_customer_address a on c.customer_company_id = a.customer_company_id and a.is_ems = 1
		--left join dbo.ems_sub_item esi on bd.bill_presentment_id = esi.reference_id and esi.reference_type = 'B' and esi.is_active = 1
		--left join dbo.ems_control_document_item es on  esi.ems_detail_id = es.ems_detail_id  and es.is_active = 1
		--left join dbo.ems_control_document ems on es.ems_control_document_id = ems.ems_control_document_id 
		left join (			
		select ems.ems_control_document_id	
				,ems_control_document_no	
				,company_code	
				,send_date	
				,reference_guid	
				,is_manual	
				,status as ems_doc_status
				,es.ems_detail_id
				,customer_code
				,es.is_active as sub_is_active
				,ems_sub_detail_id
				,reference_id
				,reference_type
				,esi.is_active as item_is_active
			from dbo.ems_control_document ems 
			left join dbo.ems_control_document_item es on ems.ems_control_document_id  = es.ems_control_document_id
			left join dbo.ems_sub_item esi on es.ems_detail_id = esi.ems_detail_id
			where reference_type = 'B' 
					and (isnull(esi.is_active,1) = 1 or status = 'CANCEL' )
		) ems on bd.bill_presentment_id = ems.reference_id --and ems.reference_type = 'B'
		where bd.method = 'EMS'
				and (@company_code is null or c.company_code = @company_code)
				and (
					@customer_code is not null AND c.customer_code = @customer_code
					OR
					@customer_code is null AND  (c.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%')
				)
				and (@other_document_no is null or bd.bill_presentment_no  like '%' + @other_document_no + '%' )
				and (@other_document_date_from is null or 
					(
						@other_document_date_from is not null 
						and @other_document_date_to is not null 
						and bd.bill_presentment_date between @other_document_date_from and @other_document_date_to
					)
				)
				--and (@running_no is null or ems.ems_control_document_no  like '%' + @running_no + '%' )		
				--and (@send_date is null or send_date  = @send_date )		
				--and (@status is null or isnull(ems.ems_doc_status,'WAITINGCREATE')  = @status )
				and isnull(ems.is_manual,0) = 0
				and (bd.bill_presentment_status <> 'CANCEL' or ems.reference_id is not null)
				--and (
				--		(isnull(bd.bill_presentment_status,'') <> 'CANCEL' /*and isnull(ems.item_is_active,1) = 1*/)
				--		or (isnull(bd.bill_presentment_status,'') = 'CANCEL' and isnull(ems.ems_doc_status,'') = 'CANCEL' )
				--	)
		union	
		select c.customer_company_id
				,ot.company_code
				/*  show only document from customer
				,case when isnull(ot.contact_person_type,1) = 1 then cus.customer_code else cast(ot.contact_person_id as nvarchar(50)) end as customer_code
				,case when isnull(ot.contact_person_type,1) = 1  then  cus.customer_name else con.customer_name end as customer_name
				*/
				,cus.customer_code as customer_code
				, cus.customer_name  customer_name
				,case when c.customer_company_id is null then 
				 (case when cus.street is null then '' else ' ' + cus.street end
						 + case when cus.district is null then '' else ' ' + cus.district end
						 + case when cus.city is null then '' else ' ' + cus.city end
						 + case when cus.post_code is null then '' else ' ' + cus.post_code end)
					else (case when a.[address] is null then '' else ' ' + a.[address] end
						 + case when a.sub_district is null then '' else ' ' + a.sub_district end
						 + case when a.district is null then '' else ' ' + a.district end
						 + case when a.province is null then '' else ' ' + a.province end
						 + case when a.post_code is null then '' else ' ' + a.post_code end)
					end as [address]
				,ot.document_id as document_id
				,ot.document_no as document_no
				,ot.document_date as document_date
				,case when ot.document_type = 1 then 'ใบนำส่งใบเสร็จรับเงิน' 
					when ot.document_type = 2 then 'ใบนำส่งเอกสารเซ็นบิล'
					when ot.document_type = 3 then 'ใบนำส่งใบส่งคืนสินค้าฝาก'
					when ot.document_type = 4 then 'ใบนำส่งเอกสารอื่นๆ'
					else 'ใบคุมใบฝากสินค้า'
					end as document_type
				,ems.ems_control_document_id
				,ems.ems_control_document_no as running_no
				,ems.send_date
				,ems.ems_doc_status as status
				,ems.reference_guid
				,ems.item_is_active as item_is_active
				,ems.sub_is_active as sub_is_active
				,ot.status as doc_status
		from dbo.other_control_document ot
	/* Change inner join to Left join for condition
		if receiver in master_customer_company then get address from master_customer_addres
		else if receiver in master_customer_company and don't have address in master_customer_addres then show empty
		else if receiver not in master_customer_company then get address from master_customer
		else show empty
		inner join dbo.master_customer_company c on ot.company_code = c.company_code and ot.receiver = c.customer_code
		inner join dbo.master_customer cus on ot.receiver = cus.customer_code
		*/
		left join dbo.master_customer_company c on ot.company_code = c.company_code and ot.receiver = c.customer_code
		inner join dbo.master_customer cus on ot.receiver = cus.customer_code
		/* show only document from customer
		--left join dbo.master_other_contact_person con on ot.contact_person_id = con.other_contact_id
		*/
		left join dbo.master_customer_address a on c.customer_company_id = a.customer_company_id and a.is_ems = 1
		left join (			
		select ems.ems_control_document_id	
				,ems_control_document_no	
				,company_code	
				,send_date	
				,reference_guid	
				,is_manual	
				,status as ems_doc_status
				,es.ems_detail_id
				,customer_code
				,es.is_active as sub_is_active
				,ems_sub_detail_id
				,reference_id
				,reference_type
				,esi.is_active as item_is_active
			from dbo.ems_control_document ems 
			left join dbo.ems_control_document_item es on ems.ems_control_document_id  = es.ems_control_document_id
			left join dbo.ems_sub_item esi on es.ems_detail_id = esi.ems_detail_id
			where reference_type = 'O'
					and (isnull(esi.is_active,1) = 1 or status = 'CANCEL' )
		) ems on ot.document_id = ems.reference_id --and ems.reference_type = 'O'
		where ot.method = 'EMS'
				and (@company_code is null or c.company_code = @company_code)
				and (
					@customer_code is not null AND c.customer_code = @customer_code
					OR
					@customer_code is null AND  (c.customer_code like '%' + @customer + '%' or cus.customer_name  like '%' + @customer + '%')
				)
				and (@other_document_no is null or ot.document_no  like '%' + @other_document_no + '%' )
				and (@other_document_date_from is null or 
					(
						@other_document_date_from is not null 
						and @other_document_date_to is not null 
						and ot.document_date between @other_document_date_from and @other_document_date_to
					)
				)
				--and (@running_no is null or ems.ems_control_document_no  like '%' + @running_no + '%' )				
				--and (@send_date is null or ems.send_date  = @send_date )		
				--and (@status is null or isnull(ems.ems_doc_status,'WAITINGCREATE')  = @status )
				and isnull(ems.is_manual,0) = 0
				and (ot.status <> 'CANCEL' or ems.reference_id is not null)
				--and (
				--		(isnull(ot.status,'') <> 'CANCEL'  /*and isnull(ems.item_is_active,1) = 1*/)
				--		or (isnull(ot.status,'') = 'CANCEL' and isnull(ems.ems_doc_status,'') = 'CANCEL' )
				--	)
		),list2 as (
		select *
		from list
		union
		select l1.customer_company_id	
				,l1.company_code	
				,l1.customer_code	
				,l1.customer_name	
				,l1.address	
				,l1.document_id	
				,l1.document_no	
				,l1.document_date	
				,l1.document_type	
				,null as ems_control_document_id	
				,null as running_no	
				,null as send_date	
				,'WAITINGCREATE' as status	
				,null as reference_guid	
				,null as item_is_active	
				,null as sub_is_active	
				,l1.doc_status
		from list l1
		where 
		isnull(status,'WAITINGCREATE') not in( 'COMPLETE' ,'WAITINGRECEIVED','WAITINGCREATE')
		and l1.doc_status <> 'CANCEL'
		and not exists(
			select 1 from
			list l2
			where l2.document_no = l1.document_no
			and isnull(status,'WAITINGCREATE') in( 'COMPLETE' ,'WAITINGRECEIVED','WAITINGCREATE')
			)
		)
		select *
		from list2
		where	 (@running_no is null or running_no  like '%' + @running_no + '%' )				
				and (@send_date is null or send_date  = @send_date )		
				and (@status is null or isnull(status,'WAITINGCREATE')  = @status )

END

GO
