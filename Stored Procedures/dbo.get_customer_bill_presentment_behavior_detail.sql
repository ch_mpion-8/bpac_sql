SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[get_customer_bill_presentment_behavior_detail]
	-- Add the parameters for the stored procedure here
	@bill_presentment_behavior_id as int = null
	--,@customer_company_id as int = null --4
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     select distinct cb.group_id as bill_presentment_behavior_id
		,cb.[customer_company_id]
		,cc.company_code
		,cc.customer_code
		, valid_date_from
		--, cb.payment_term_code
		,(
				select stuff(
						(
							select ',' + paymentCode 
								from (
								select paymentCode = b.[payment_term_code]
								from [dbo].[master_customer_bill_presentment_behavior] b
								where cb.group_id = b.group_id
									and b.is_active = 1
							) a
								for xml path('')
						) ,1,1,'') as txt
				) as [payment_term_code]

		--, term.payment_term_name
		, cb.is_bill_presentment
		, invoice_period_from
		, invoice_period_to
		, cb.address_id
		, ad.title  as address_title
		, [time]
		, [description]
		, remark
		, condition_date_by
		, date_from
		, date_to
		, last_x_date
		, last_x_business_day
		, custom_date
		, custom_day
		, condition_week_by
		, custom_week
		, next_x_week
		, condition_month_by
		, custom_month
		, next_x_month
		, is_next_bill_presentment_date
		, billing_method
		, is_signed_document
		, is_billing_wiith_truck
		, notify_day
		, is_skip_customer_holiday
		, is_skip_scg_holiday
		, is_billing_with_bill
		, document_type
		, cb.[is_active]
		, cb.[created_date]
		, cb.[created_by]
		, cb.[updated_date]
		, cb.[updated_by]
		, cb.custom_date_next_x_month
		, cc.[user_name]
	from [dbo].[master_customer_bill_presentment_behavior] cb
	inner join  dbo.master_customer_company cc on cb.customer_company_id = cc.customer_company_id
	--inner join dbo.master_payment_term term on cb.payment_term_code = term.payment_term_code
	left join dbo.master_customer_address ad on cb.address_id = ad.address_id
	--inner join dbo.master_bank b on cb.bank_id = b.bank_id
	where cb.group_id = @bill_presentment_behavior_id and cb.is_active = 1
	
	select bd.bill_presentment_document_id
			,bd.document_code
			,c.configuration_value as document_name
			,bd.custom_document
	from dbo.master_customer_bill_presentment_document bd
	inner join dbo.[master_customer_bill_presentment_behavior] b on bd.bill_presentment_behavior_id = b.bill_presentment_behavior_id
	inner join dbo.configuration  c on bd.document_code = c.configuration_code and c.configuration_key = 'BillingDocument'
	where b.group_id = @bill_presentment_behavior_id 
			and bd.[is_active] = 1

	select bill_presentment_custom_date_id
			,[date] as custom_date 
	from dbo.master_customer_bill_presentment_custom_date bd
	inner join dbo.[master_customer_bill_presentment_behavior] b on bd.bill_presentment_behavior_id = b.bill_presentment_behavior_id
	where b.group_id = @bill_presentment_behavior_id
			and bd.[is_active] = 1

	--select address_id
	--		,customer_company_id	
	--		,title	
	--		,[address]	
	--		,sub_district	
	--		,district	
	--		,province	
	--		,post_code	
	--		,is_ems	
	--		,is_allowed_edit
	--		,[status] 
	--from dbo.master_customer_address 
	--where customer_company_id = @customer_company_id
	--	and [status] = 'ACTIVE'

END
GO
