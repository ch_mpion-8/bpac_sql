SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[check_valid_reference_other_document]
	@reference_no varchar(500)=null,
	@document_id int=null,
	@error_message nvarchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @customer_code varchar(20),@company_code varchar(10),@customer_name nvarchar(50),@document_date datetime
    if @reference_no is null or @reference_no =''
	begin 
		set @error_message = 'โปรดกรอก Reference No.'
		return
	end

	--check dupicate referecnce in otherdoc.document_type = 4 for send
	if (select count(*) from dbo.other_control_document o 
		inner join dbo.other_control_document_item i 
		on i.document_id=o.document_id
		where 
		i.reference_no = @reference_no
		and o.document_id = @document_id) >1
	begin
		set @error_message = 'พบ Rerfence No ในOtherDocument นี้แล้ว.'
		return
	end


	Declare @recevier_type int = null,@contact_person_id int =null


	--select 
	--@customer_code = o.receiver,
	--@company_code = o.company_code,
	--@document_date = o.document_date,
	--@recevier_type = o.contact_person_type
	----@contact_person_id = o.contact_person_id
	--from dbo.other_control_document o
	--left join dbo.master_customer_company c
	--on c.company_code = o.company_code
	--and c.customer_code = o.receiver
	--where o.document_id = @document_id

	--if isnull(@recevier_type,1) = 1
	--begin
	--	select @customer_name = customer_name from dbo.master_customer where customer_code = @customer_code
	--end
	--else
	--begin
	--	select @customer_name = customer_name from dbo.master_other_contact_person where other_contact_id = @contact_person_id
	--end

	
		Declare @temp Table(
			customer_code varchar(20),
			customer_name nvarchar(50),
			company_code varchar(10),
			document_date datetime,
			reference_no varchar(50)
		)

		insert into @temp values(
		@customer_code,
		@customer_name,
		@company_code,
		@document_date,
		@reference_no
		)

		select * from @temp
	
END
GO
