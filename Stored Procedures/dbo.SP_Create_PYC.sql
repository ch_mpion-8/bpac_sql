SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Create_PYC]
	-- Add the parameters for the stored procedure here
	@reference_no_id as int = null
	,@reference_type as int = null
	,@trip_date as date = null
	,@count_trip as int = null
	,@is_skip as bit = null
	,@remark as varchar(max) = null
	,@created_by as varchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if exists (select 1 from dbo.scg_pyc_process where reference_type = @reference_type and reference_no_id = @reference_no_id and trip_date = @trip_date)
		return;

	insert into dbo.scg_pyc_process(
	  reference_no_id
	, reference_type
	, trip_date
	, count_trip
	, is_skip
	, remark
	, created_by
	, created_date)
	values(
	  @reference_no_id
	, @reference_type
	, @trip_date
	, @count_trip
	, @is_skip
	, @remark
	, @created_by
	, GETDATE())
END
GO
