SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_bill_presentment_detail_for_email]
    -- Add the parameters for the stored procedure here
    @company_code VARCHAR(30) = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    SELECT bp.bill_presentment_id,
           inv.invoice_no,
           inv.require_document_status
    FROM dbo.bill_presentment_control_document bp
        INNER JOIN dbo.bill_presentment_control_document_item bpi
            ON bpi.bill_presentment_id = bp.bill_presentment_id
        INNER JOIN dbo.invoice_header inv
            ON inv.invoice_no = bpi.invoice_no
        INNER JOIN dbo.master_customer_company mcc
            ON mcc.customer_company_id = bp.customer_company_id
        INNER JOIN dbo.master_company mc
            ON mc.company_code = mcc.company_code
        INNER JOIN dbo.master_email_config mec
            ON mec.customer_company_id = mcc.customer_company_id
    WHERE bpi.is_active = 1
          AND mc.company_code = @company_code
          AND mec.output_type_code LIKE 'send%';
END;
GO
