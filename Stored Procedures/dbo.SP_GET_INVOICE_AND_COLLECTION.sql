SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_INVOICE_AND_COLLECTION] 
	-- Add the parameters for the stored procedure here
	
	@company_customer_code as varchar(10) = null,
	@payment_term as varchar(10) =null,
--	@invoice_period_to as int = 0,
--@invoice_period_from as int = 0,
@valid_from as varchar(15) = null,
@valid_to as varchar(15) = null,
@recalculate as bit =0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @bill TABLE(
    bill_id INT
    );

	select 
	ih.invoice_no,
	ih.invoice_type,
	ih.invoice_date,
	--DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))
	--as load_date,
	DATEADD(dd,1,ih.invoice_date)
	as load_date,
	--ih.bill_presentment_date,
	case when 
		case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
		then substring(ih.bill_presentment_remark,0,11)
		else ih.bill_presentment_remark end
		= 'สร้างใบคุม' then ih.bill_presentment_date
	else bill_date.bill_presentment_date end as bill_presentment_date,
	ih.due_date,
	ih.collection_date,
	ih.company_code,
	ih.customer_code,
	ih.payment_term,
	ih.invoice_amount,
	ih.tax_amount,
	ih.currency,
	ih.referenece_document_no,
	ih.assignment_no,
	ih.net_due_date,
	mccb.collection_behavior_id,
	mccb.customer_company_id as collection_customer_company_id,
	mccb.collection_condition_date,
	mccb.condition_by,
	mccb.condition_month_by as collection_condition_month_by,
	mccb.condition_week_by as collection_condition_week_by,
	mccb.custom_date as collection_custom_date,
	mccb.custom_day as collection_custom_day,
	mccb.custom_month as collection_custom_month,
	mccb.custom_week as collection_custom_week,
	mccb.date_from as collection_date_from,
	mccb.date_to as collection_date_to,
	mccb.is_skip_customer_holiday  as collection_is_skip_customer_holiday,
	mccb.is_skip_scg_holiday  as collection_is_skip_scg_holiday,
	mccb.is_business_day as collection_is_business_day,
	mccb.last_x_date as collection_last_x_date,
	mccb.next_x_month as collection_next_x_month,
	mccb.notify_day as collection_notify_date,
	mccb.period_from as collection_period_from,
	mccb.period_to as collection_period_to,
	mccb.payment_term_code as collection_payment_term_code,
	mccb.is_next_collection_date,
	mccb.payment_day,
    mccb.next_x_week,
	mccb.last_x_business_day,
	mccb.custom_date_next_x_month,
	ih.collection_status,
	ih.created_date_time as created_date

		into #temp1
		from invoice_header ih 
		left join master_customer_company mcc 
		on ih.company_code = mcc.company_code 
		and ih.customer_code =  mcc.customer_code
		left join (
			Select b.bill_presentment_date,bi.invoice_no from bill_presentment_control_document_item bi 
			inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
			where bi.is_active = 1
					and b.bill_presentment_status <> 'CANCEL'
		) bill_date on  ih.invoice_no =  bill_date.invoice_no
		left join master_customer_collection_behavior mccb
		on mcc.customer_company_id = mccb.customer_company_id
		and ih.payment_term = mccb.payment_term_code
		AND (
		(mccb.collection_condition_date = 'Due Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.due_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.due_date) >= 0)
		and DAY(ih.due_date) between mccb.period_from and mccb.period_to)
		or
		(mccb.collection_condition_date = 'Billing Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.invoice_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
		and DAY(ih.invoice_date) between mccb.period_from and mccb.period_to)
		or
		(mccb.collection_condition_date = 'BPAC Bill Date'
		 AND 
		 (DATEDIFF(DAY,mccb.valid_date_from,
		(case when 
			case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
			then substring(ih.bill_presentment_remark,0,11) else ih.bill_presentment_remark end
			= 'สร้างใบคุม' then ih.bill_presentment_date
		else bill_date.bill_presentment_date end )
		) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,
		(case when 
			case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
			then substring(ih.bill_presentment_remark,0,11) else ih.bill_presentment_remark end
			= 'สร้างใบคุม' then ih.bill_presentment_date
		else bill_date.bill_presentment_date end )
		) >= 0)
		and DAY(case when 
					case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
					then substring(ih.bill_presentment_remark,0,11) else ih.bill_presentment_remark end
					= 'สร้างใบคุม' then ih.bill_presentment_date
				else bill_date.bill_presentment_date end) between mccb.period_from and mccb.period_to
		)
		or
		(mccb.collection_condition_date = 'Delivery Date' AND (DATEDIFF(DAY,mccb.valid_date_from,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
		and DAY(DATEADD(dd,1,ih.invoice_date)) between mccb.period_from and mccb.period_to)
		--(mccb.collection_condition_date = 'Delivery Date' 
		--AND (DATEDIFF(DAY,mccb.valid_date_from,
		--DATEADD(dd, 1, (Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))) >= 0)
		--and (mccb.valid_date_to is null or (DATEDIFF(DAY,mccb.valid_date_to,
		--DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))) >= 0))
		--and DAY(DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)))
		-- between mccb.period_from and mccb.period_to)
		)
		--select DATEADD(dd, 1, '2010-07-29')
		where 
		--(ih.collection_status is null or ih.collection_status not in (select * from dbo.SplitString('COMPLETE,MANUAL,CANCEL',',')) )
		--((ih.collection_status is null or ih.collection_status not in (select * from dbo.SplitString('COMPLETE,MANUAL,CANCEL',',')) )
		--or ih.collection_status = 'MANUAL' and ih.collection_behavior_id is null
		--or ih.invoice_no not in (select distinct invoice_no from log_manual_update_collection_date where is_manual_update = 1)
		--)
			(
		(ih.collection_status is null or ih.collection_status not in (select * from dbo.SplitString('COMPLETE,MANUAL,CANCEL',',')) )
		or 
		ih.collection_status = 'MANUAL' and ih.collection_behavior_id is null
		--or (ih.invoice_no not in (select distinct invoice_no from log_manual_update_collection_date where is_manual_update = 1)
		--and ih.collection_status not in (select * from dbo.SplitString('COMPLETE,MANUAL,CANCEL',',')) 
		--)
		)
		and (@company_customer_code is null or mcc.customer_company_id = @company_customer_code)
		and (@payment_term is null or (ih.payment_term in (select * from dbo.SplitString(@payment_term,','))))
		and (ih.collection_date is null or @recalculate = 1)
		and (mccb.is_active = 1)


		and ((mccb.collection_condition_date = 'Due Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.due_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.due_date) >= 0)
		)
		or
		(mccb.collection_condition_date = 'Billing Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.invoice_date) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
		)

		or
		(mccb.collection_condition_date = 'BPAC Bill Date'
		 AND 
		 (DATEDIFF(DAY,mccb.valid_date_from,
		(case when 
				case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
				then substring(ih.bill_presentment_remark,0,11) else ih.bill_presentment_remark end
				= 'สร้างใบคุม' then ih.bill_presentment_date
			else bill_date.bill_presentment_date end)
		) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,
		(case when 
			case when len(isnull(ih.bill_presentment_remark,' ')) > 11 
			then substring(ih.bill_presentment_remark,0,11) else ih.bill_presentment_remark end
			= 'สร้างใบคุม' then ih.bill_presentment_date
		else bill_date.bill_presentment_date end )
		) >= 0)
	
		)
		or
	
		(mccb.collection_condition_date = 'Delivery Date' AND (DATEDIFF(DAY,mccb.valid_date_from,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
		and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
		and DAY(DATEADD(dd,1,ih.invoice_date)) between mccb.period_from and mccb.period_to)
			or mccb.collection_behavior_id is null
		)
		--and ih.company_code = '0480'
		--and ih.customer_code = '2000256'
		--and ih.collection_behavior_id = 0
		and ih.invoice_no = '1580366583'
		--and ih.bill_presentment_remark like '%ÊÃéÒ§ãº¤ØÁ%'

 --   select 
	--ih.invoice_no,
	--ih.invoice_type,
	--ih.invoice_date,
	----DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))
	----as load_date,
	--DATEADD(dd,1,ih.invoice_date)
	--as load_date,
	----ih.bill_presentment_date,
 --   (Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no ) as bill_presentment_date,
	--ih.due_date,
	--ih.collection_date,
	--ih.company_code,
	--ih.customer_code,
	--ih.payment_term,
	--ih.invoice_amount,
	--ih.tax_amount,
	--ih.currency,
	--ih.referenece_document_no,
	--ih.assignment_no,
	--ih.net_due_date,
	--mccb.collection_behavior_id,
	--mccb.customer_company_id as collection_customer_company_id,
	--mccb.collection_condition_date,
	--mccb.condition_by,
	--mccb.condition_month_by as collection_condition_month_by,
	--mccb.condition_week_by as collection_condition_week_by,
	--mccb.custom_date as collection_custom_date,
	--mccb.custom_day as collection_custom_day,
	--mccb.custom_month as collection_custom_month,
	--mccb.custom_week as collection_custom_week,
	--mccb.date_from as collection_date_from,
	--mccb.date_to as collection_date_to,
	--mccb.is_skip_customer_holiday  as collection_is_skip_customer_holiday,
	--mccb.is_skip_scg_holiday  as collection_is_skip_scg_holiday,
	--mccb.is_business_day as collection_is_business_day,
	--mccb.last_x_date as collection_last_x_date,
	--mccb.next_x_month as collection_next_x_month,
	--mccb.notify_day as collection_notify_date,
	--mccb.period_from as collection_period_from,
	--mccb.period_to as collection_period_to,
	--mccb.payment_term_code as collection_payment_term_code,
	--mccb.is_next_collection_date,
	--mccb.payment_day,
 --   mccb.next_x_week,
	--mccb.last_x_business_day,
	--mccb.custom_date_next_x_month

	--	into #temp1
	--	from invoice_header ih 
	--	left join master_customer_company mcc 
	--	on ih.company_code = mcc.company_code 
	--	and ih.customer_code =  mcc.customer_code
	--	left join master_customer_collection_behavior mccb
	--	on mcc.customer_company_id = mccb.customer_company_id
	--	and ih.payment_term = mccb.payment_term_code
	--	AND (
	--	(mccb.collection_condition_date = 'Due Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.due_date) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.due_date) >= 0)
	--	and DAY(ih.due_date) between mccb.period_from and mccb.period_to)
	--	or
	--	(mccb.collection_condition_date = 'Billing Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.invoice_date) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
	--	and DAY(ih.invoice_date) between mccb.period_from and mccb.period_to)
	--	or
	--	(mccb.collection_condition_date = 'BPAC Bill Date'
	--	 AND 
	--	 (DATEDIFF(DAY,mccb.valid_date_from,
	--	(Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no )
	--	) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,
	--	(Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no )
	--	) >= 0)
	--	and DAY((Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no )) between mccb.period_from and mccb.period_to
	--	)
	--	or
	--	(mccb.collection_condition_date = 'Delivery Date' AND (DATEDIFF(DAY,mccb.valid_date_from,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
	--	and DAY(DATEADD(dd,1,ih.invoice_date)) between mccb.period_from and mccb.period_to)
	--	--(mccb.collection_condition_date = 'Delivery Date' 
	--	--AND (DATEDIFF(DAY,mccb.valid_date_from,
	--	--DATEADD(dd, 1, (Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))) >= 0)
	--	--and (mccb.valid_date_to is null or (DATEDIFF(DAY,mccb.valid_date_to,
	--	--DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc))) >= 0))
	--	--and DAY(DATEADD(dd,1,(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)))
	--	-- between mccb.period_from and mccb.period_to)
	--	)
	--	--select DATEADD(dd, 1, '2010-07-29')
	--	where 
	--	(ih.collection_status is null or ih.collection_status not in (select * from dbo.SplitString('COMPLETE,MANUAL,CANCEL',',')) )
	--	and (@company_customer_code is null or mcc.customer_company_id = @company_customer_code)
	--	and (@payment_term is null or (ih.payment_term in (select * from dbo.SplitString(@payment_term,','))))
	--	and (ih.collection_date is null or @recalculate = 1)
	--	--and mcc.customer_code = '2000086'
	--	--and (@invoice_period_from = 0 or @invoice_period_to = 0 or  
	--	--(DAY(ih.invoice_date) between @invoice_period_from and @invoice_period_to)
	--	--)
	--	--and (
	--	--(@valid_from is null or (DATEDIFF(DAY,convert(datetime, LEFT(@valid_from,10), 103)  ,ih.invoice_date) >= 0 and @valid_to is null))
	--	--or
	--	--((@valid_from is null and @valid_to is null) or 
	--	--(DATEDIFF(DAY,convert(datetime, LEFT(@valid_from,10), 103)  ,ih.invoice_date) >= 0
	--	--and
	--	--DATEDIFF(DAY,convert(datetime, LEFT(@valid_to,10), 103)  ,ih.invoice_date) < 0))
	--	--)

	--	and ((mccb.collection_condition_date = 'Due Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.due_date) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.due_date) >= 0)
	--	)
	--	or
	--	(mccb.collection_condition_date = 'Billing Date' AND (DATEDIFF(DAY,mccb.valid_date_from,ih.invoice_date) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,ih.invoice_date) >= 0)
	--	)

	--	or
	--	(mccb.collection_condition_date = 'BPAC Bill Date'
	--	 AND 
	--	 (DATEDIFF(DAY,mccb.valid_date_from,
	--	(Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no )
	--	) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,
	--	(Select distinct top(1) b.bill_presentment_date from bill_presentment_control_document_item bi 
	--	inner join bill_presentment_control_document b on bi.bill_presentment_id = b.bill_presentment_id
	--	where bi.invoice_no = ih.invoice_no )
	--	) >= 0)
	
	--	)
	--	or
	--	--(mccb.collection_condition_date = 'Delivery Date' 
	--	--AND (DATEDIFF(DAY,mccb.valid_date_from,
	--	--(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)) >= 0)
	--	--and (mccb.valid_date_to is null or (DATEDIFF(DAY,mccb.valid_date_to,
	--	--(Select distinct top(1) load_date from invoice_detail where invoice_no = ih.invoice_no order by load_date asc)) >= 0))
		
	--	--)
	--	(mccb.collection_condition_date = 'Delivery Date' AND (DATEDIFF(DAY,mccb.valid_date_from,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
	--	and (mccb.valid_date_to is null or DATEDIFF(DAY,  mccb.valid_date_to,(DATEADD(dd,1,ih.invoice_date)) ) >= 0)
	--	and DAY(DATEADD(dd,1,ih.invoice_date)) between mccb.period_from and mccb.period_to)
	--		or mccb.collection_behavior_id is null
	--	)
	--	--and invoice_no = '1680478860'
	--	--and invoice_no = '1580296598'

	
	select * from #temp1 
	select * from master_customer_collection_custom_date
	where collection_behavior_id in ( select collection_behavior_id  from #temp1)
	and is_active =1

	select h.customer_holiday_id as holiday_id 
			,h.customer_code
			,h.holiday_year
			,h.holiday_month
			,h.holiday_day
			,h.holiday_name
			,'CUSTOMER' as holiday_type
			,h.is_active
			,h.created_date
			,h.created_by
			,h.updated_date
			,h.updated_by
	from dbo.master_customer_holiday h
	where   customer_code in (select distinct customer_code from #temp1 where collection_is_skip_customer_holiday is null or collection_is_skip_customer_holiday = 0)
			and is_active = 1

	

	drop table #temp1
	--exec SP_INSERT_CUSTOMER_TO_MASTER_CUSTOMER_COMPANY

END
GO
