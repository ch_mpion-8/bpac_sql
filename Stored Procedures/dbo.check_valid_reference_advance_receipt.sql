SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[check_valid_reference_advance_receipt]
	@reference_no varchar(50)=null,
	@document_id int=null,
	@error_message nvarchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    if @reference_no is null or @reference_no =''
	begin 
		set @error_message = 'โปรดกรอก Reference No.'
		return
	end

	declare @reference_year varchar(4) =  SUBSTRING(@reference_no,CHARINDEX('_',@reference_no)+1,4)
	set @reference_no =  SUBSTRING(@reference_no,1,CHARINDEX('_',@reference_no)-1)

	Declare @d_customer_code varchar(20),@d_company_code varchar(10),@d_currency varchar(10),@amount decimal
	Declare @customer_code varchar(20),@company_code varchar(10),@currency varchar(10),@customer_name nvarchar(50),@document_date datetime,@clearing_doc_no varchar(20)=null
    Declare @active bit

	Select TOP 1 
		@d_company_code = r.company_code,
		@d_customer_code = r.customer_code,
		@d_currency = r.currency
	from dbo.other_control_document o
	inner join dbo.other_control_document_item i
	on i.document_id=o.document_id
	inner join dbo.receipt r
	on r.reference_document_no = i.reference_no
	and r.[year] = i.[year]
	and r.company_code = o.company_code
	and r.customer_code = o.receiver
	where o.document_id = @document_id
	

	Select 
		@amount=document_amount
		,@active=is_active
		,@company_code = company_code
		,@customer_code = customer_code
		,@currency = currency
		,@document_date = document_date
		,@clearing_doc_no = clearing_document_no 
	from dbo.receipt
	where 
		reference_document_no = @reference_no 
		and [year] = @reference_year
		and receipt_type ='normal' 
		and company_code = @d_company_code 
		and customer_code = @d_customer_code 
		and currency = @d_currency
		and isnull(is_active,1) = 1

	if @clearing_doc_no is null OR @clearing_doc_no =''
	begin
		set @error_message = 'ไม่พบ Reference No ไม่ระบุ '+@reference_no+':'+@reference_year
		return
	end

	----check dupicate referecnce in  this document_id
	--if exists(select 1 from dbo.other_control_document o 
	--	inner join dbo.other_control_document_item i 
	--	on i.document_id=o.document_id
	--	where o.document_type = 1
	--	and i.reference_no = @reference_no
	--	and o.document_id = @document_id)
	--begin
	--	set @error_message ='Reference No: '+@reference_no+' มีอยู่แล้วใน OtherDocument นี้'
	--	return
	--end
	


	

	if @d_company_code =@company_code AND @customer_code=@d_customer_code AND @d_currency = @currency
	begin
		Declare @temp Table(
			customer_code varchar(20),
			customer_name nvarchar(50),
			company_code varchar(10),
			clearing_document_no varchar(20),
			document_date datetime,
			reference_no varchar(50),
			[year] varchar(4),
			is_active bit,
			document_amount decimal
		)

		select @customer_name = customer_name 
		from dbo.master_customer
		where customer_code = @customer_code

		insert into @temp values(
		@customer_code,
		@customer_name,
		@company_code,
		@clearing_doc_no,
		@document_date,
		@reference_no,
		@reference_year,
		@active,
		@amount)

		select * from @temp
	end
	ELSE
	begin
		set @error_message ='CompanyCode CustomerCode Current ของ Reference No : '+@reference_no+' ไม่ตรงกัน'
	end	
END

SET ANSI_NULLS ON
GO
