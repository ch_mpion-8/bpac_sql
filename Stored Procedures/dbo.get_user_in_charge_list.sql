SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[get_user_in_charge_list]
 @company_code varchar(200) =  null --  '2220' -- 
		,@user_code nvarchar(100) = null  -- '10005' -- 
		,@assign_status nvarchar(10) = null  -- 'NEW' -- 
		,@application_tag_id nvarchar(100) = null --  2  --  
		,@customer nvarchar(50) = null  --  'a' --  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select cc.customer_company_id
			,cc.company_code
			,com.company_name
			,cc.customer_code
			,cus.customer_name
			,cc.[user_name]
			,cc.[user_display_name]
			,cus.application_tag_id
			,tag.application_tag_name
			,cc.created_date
			,cc.updated_date
			, case when  [user_name] is null then 'UNASSIGNED' 
				    when  datediff(day,isnull(cc.updated_date, cast('1753-1-1' as date)) ,getdate()) >0 
							and [user_name] is not null then 'ASSIGNED' 
					else 'NEW' end  as [status]
			--,*
	from dbo.master_customer_company cc
	inner join dbo.master_company com on cc.company_code = com.company_code  
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code --and isnull(cus.is_ignore,0) = 0
	left join dbo.master_application_tag tag on cus.application_tag_id = tag.application_tag_id
	left join (select distinct customer_code, company_code from invoice_header) ih 
		on ih.customer_code = cc.customer_code and ih.company_code = cc.company_code
	left join (select distinct customer_code, company_code from receipt) rp
		on rp.customer_code = cc.customer_code and rp.company_code = cc.company_code
	where (@company_code is null or cc.company_code  in ( select item from dbo.SplitString(@company_code,',')))
		and (@user_code is null or cc.[user_name] in (SELECT Item FROM dbo.SplitString(@user_code, ',')) )
		and (@assign_status is null 
				or case when  [user_name] is null then 'UNASSIGNED' 
				         when  datediff(day,isnull(cc.updated_date, cast('1753-1-1' as date)) ,getdate()) >0 
							and [user_name] is not null then 'ASSIGNED' 
						else 'NEW' end = @assign_status)
		and (@application_tag_id is null or isnull(cus.application_tag_id,999) in (SELECT Item FROM dbo.SplitString(@application_tag_id, ',')) )
		and (@customer is null or cc.customer_code like '%' + @customer + '%' or cus.customer_name like '%' + @customer + '%' )
		and ([user_name] is not null or ([user_name] is null and (ih.customer_code is not null or rp.customer_code is not null)))

END
GO
