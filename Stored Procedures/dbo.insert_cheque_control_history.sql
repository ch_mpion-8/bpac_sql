SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_cheque_control_history]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
	,@description as nvarchar(2000) 
	,@action as nvarchar(2000) 
	,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	insert into [dbo].[cheque_control_document_history]
	(
		[cheque_control_id]
		,[action]
		,bpac_due_date
		,[description]
		,created_by
		,created_date
	)
	select @cheque_control_id
			,@action
			,collection_date
			,@description
			,@updated_by
			,getdate()
	from dbo.cheque_control_document 
	where cheque_control_id = @cheque_control_id


END


GO
