SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[save_other_control_document]
	-- Add the parameters for the stored procedure here
	@document_no as varchar(20)=null,
	@document_type as int =null,
	@action_type as int = null,
	@company_code as varchar(4)=null,
	@receiver as varchar(10)=null,
	@method as varchar(20) =null,
	@document_date as datetime=null,
	@user_code as varchar(50)=null,
	@remark as nvarchar(2000) = null,
	@status nvarchar(20)=null,
	@created_date as datetime=null,
	@created_by as nvarchar(50)=null,
	@updated_date as datetime=null,
	@updated_by as nvarchar(50)=null,
	@document_receive as nvarchar(max)=null,
	@document_send as nvarchar(max)=null,
	@document_sendreceive as nvarchar(max)=null,
	@error_message as nvarchar(200) =null output,
	@document_id as int =null output,
	@contact_person_id as int =null,
	@contact_person_type as int =null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS(select 1 from dbo.master_company where company_code=@company_code)
	begin
		
		--check dupicate reference send_doc
		If(@document_send is not null)
		begin
			declare @t_ref as varchar(500)
			declare cursor_chk CURSOR FOR
			select Item from [dbo].[SplitString](@document_send,'|')
			
			open cursor_chk
			Fetch Next From cursor_chk
			INTO @t_ref

			While @@FETCH_STATUS=0 begin
				
				if @document_type =1
				begin
					--find year of reference no
					--only send reference [advance receipt]
					declare @year varchar(4) =null
					declare @split_idx int = charindex(',',@t_ref)
					if @split_idx is not null and @split_idx > 0
					begin
						set @year = SUBSTRING(@t_ref,@split_idx+1,4)
						set @t_ref = SUBSTRING(@t_ref,1,@split_idx-1)
					end
						
					if exists (
								select 1 from dbo.other_control_document o 
									inner join dbo.other_control_document_item i 
										on o.document_id = i.document_id 
								where i.reference_no = @t_ref
									and o.company_code = @company_code 
									and o.receiver = @receiver 
									and i.[year] = @year
									and o.document_type=1 
									and o.[status] <>'CANCEL'
					)
					begin
						set @error_message = '[S_1]พบข้อมูล Reference No: '+@t_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end
				else if @document_type <> 4
				begin
					IF EXISTS(Select 1 from dbo.other_control_document_item i
					inner join dbo.other_control_document o on i.document_id = o.document_id and o.document_type=@document_type
					where i.reference_no = @t_ref and i.action_type=1 and o.[status] <> 'CANCEL')
					begin
						set @error_message = '[S_O]พบข้อมูล Reference No: '+@t_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end
				
				

				Fetch Next From cursor_chk
				INTO @t_ref
			end
			CLOSE cursor_chk
			DEALLOCATE cursor_chk
		end
		--check dupicate reference receive_doc
		If(@document_receive is not null)
		begin
			declare @r_ref as varchar(500)
			declare cursor_chkr CURSOR FOR
			select Item from [dbo].[SplitString](@document_receive,'|')
			
			open cursor_chkr
			Fetch Next From cursor_chkr
			INTO @r_ref

			While @@FETCH_STATUS=0 begin
				
				if @document_type =1 
				begin
					if exists (select 1 from dbo.other_control_document o inner join dbo.other_control_document_item i on o.document_id = i.document_id where i.reference_no = @r_ref
					and o.company_code = @company_code and o.receiver = @receiver and o.document_type=1 and o.[status] <>'CANCEL')
					begin
						set @error_message = '[R_1]พบข้อมูล Reference No: '+@r_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end
				else if @document_type <> 4
				begin
					IF EXISTS(Select 1 from dbo.other_control_document_item i
					inner join dbo.other_control_document o on i.document_id = o.document_id and o.document_type=@document_type
					where i.reference_no = @r_ref and i.action_type=2 and o.[status] <>'CANCEL')
					begin
						set @error_message = '[R_O]พบข้อมูล Reference No: '+@r_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end



				Fetch Next From cursor_chkr
				INTO @r_ref
			end
			CLOSE cursor_chkr
			DEALLOCATE cursor_chkr
		end
		--check dupicate reference send_receive_doc
		If(@document_sendreceive is not null)
		begin
			declare @sr_ref as varchar(500)
			declare cursor_chksr CURSOR FOR
			select Item from [dbo].[SplitString](@document_sendreceive,'|')
			
			open cursor_chksr
			Fetch Next From cursor_chksr
			INTO @sr_ref

			While @@FETCH_STATUS=0 begin

				if @document_type = 1
				begin
					if exists (select 1 from dbo.other_control_document o inner join dbo.other_control_document_item i on o.document_id = i.document_id where i.reference_no = @sr_ref
					and o.company_code = @company_code and o.receiver = @receiver and o.document_type=1 and o.[status] <>'CANCEL')
					begin
						set @error_message = '[SR_1]พบข้อมูล Reference No: '+@sr_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end
				else if @document_type <> 4
				begin
					IF EXISTS(Select 1 from dbo.other_control_document_item i
					inner join dbo.other_control_document o on i.document_id = o.document_id and o.document_type=@document_type
					where i.reference_no = @sr_ref and i.action_type=0 and o.[status] <>'CANCEL')
					begin
						set @error_message = '[SR_O]พบข้อมูล Reference No: '+@sr_ref+' ที่มีอยู่แล้ว ไม่สามารถเพิ่มซ้้ำได้'
						Return
					end
				end



				Fetch Next From cursor_chksr
				INTO @sr_ref
			end
			CLOSE cursor_chksr
			DEALLOCATE cursor_chksr
		end

		-- search collection user 
		select @user_code = user_display_name from dbo.master_customer_company where company_code = @company_code and customer_code = @receiver

		Declare @contact_name varchar(120),@reciver_name varchar(120)
		if @document_type = 4
		begin
			if @contact_person_type = 0
			begin
				select @receiver = customer_code,@reciver_name=customer_name,@contact_name = contact_person from dbo.master_other_contact_person where other_contact_id = @contact_person_id
			end
			else if @contact_person_type = 1
				select @contact_name = contact_name from dbo.master_customer_contact where contact_id = @contact_person_id
		end
		--create 
		declare @new_id as int
		declare @tmp table(new_id int)
		insert into [dbo].[other_control_document] 
			(document_no,
			document_type,
			company_code,
			receiver,
			method,
			document_date,
			user_code,
			remark,
			[status],
			receiver_name,
			contact_person_id,
			contact_person,
			contact_person_type,
			created_date,
			created_by,
			update_date,
			updated_by
			)
		output inserted.document_id into @tmp
		select
			@document_no,
			@document_type,
			@company_code,
			@receiver,
			@method,
			@document_date,
			@user_code,
			@remark,
			@status,
			@reciver_name,
			@contact_person_id,
			@contact_name,
			@contact_person_type,
			@created_date,
			@created_by,
			@updated_date,
			@updated_by
		
		select @new_id =  new_id from @tmp 
		set @document_id = @new_id
		declare @reference_no as varchar(500)
				--Create History Log
		insert into dbo.other_control_document_history(
		document_id,
		[action],
		[description],
		created_by,
		created_date)
		values(@new_id,
		'Create',
		'',
		@created_by,
		@created_date)

		declare @sequence_no int =1
		if (@document_send is not null)
		begin
		--0 is send/receive
		--1 is send
		--2 is recive
			
			declare cursor_doc CURSOR FOR
			select Item from [dbo].[SplitString](@document_send,'|')
			
			set @sequence_no =1
			open cursor_doc
			Fetch Next From cursor_doc
			INTO @reference_no

			While @@FETCH_STATUS=0 begin
				exec [dbo].[save_other_control_document_item] 
				@new_id,
				@reference_no,
				1,--1 is send
				@document_type,
				@status,
				@created_date,
				@created_by,
				@updated_date,
				@updated_by,
				@sequence_no

				set @sequence_no = @sequence_no+1

				Fetch Next From cursor_doc
				INTO @reference_no
			end

			CLOSE cursor_doc
			DEALLOCATE cursor_doc
		end

		if (@document_receive is not null)
		begin
		--0 is send/receive
		--1 is send
		--2 is recive
			set @sequence_no =1
			declare cursor_doc CURSOR FOR
			select Item from [dbo].[SplitString](@document_receive,'|')
			
			open cursor_doc
			Fetch Next From cursor_doc
			INTO @reference_no

			While @@FETCH_STATUS=0 begin
				exec [dbo].[save_other_control_document_item] @new_id,
				@reference_no,
				2,--2 is recive
				@document_type,
				@status,
				@created_date,
				@created_by,
				@updated_date,
				@updated_by,
				@sequence_no

				set @sequence_no = @sequence_no+1
				Fetch Next From cursor_doc
				INTO @reference_no
			end

			CLOSE cursor_doc
			DEALLOCATE cursor_doc
		end
		if (@document_sendreceive is not null)
		begin
		--0 is send/receive
		--1 is send
		--2 is recive
			set @sequence_no =1
			declare cursor_doc CURSOR FOR
			select Item from [dbo].[SplitString](@document_sendreceive,'|')
			
			open cursor_doc
			Fetch Next From cursor_doc
			INTO @reference_no

			While @@FETCH_STATUS=0 begin
				exec [dbo].[save_other_control_document_item] @new_id,
				@reference_no,
				0,--0 is send/recive
				@document_type,
				@status,
				@created_date,
				@created_by,
				@updated_date,
				@updated_by,
				@sequence_no

				set @sequence_no = @sequence_no+1
				Fetch Next From cursor_doc
				INTO @reference_no
			end

			CLOSE cursor_doc
			DEALLOCATE cursor_doc
		end

	end
	ELSE
	begin
		set @error_message = 'ไม่พบ Company Code ที่ระบุ'
	end
	
END
GO
