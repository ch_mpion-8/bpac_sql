SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_GET_DETIAL_SUMMARY]
	-- Add the parameters for the stored procedure here
	@date_month_year varchar(30)=null,
	@date date =null,
	@company int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		set LANGUAGE us_english
		Declare @company_code varchar(4) = null
		set @company_code = case @company 
		when 1 then '0480'
		when 2 then '0230'
		when 3 then '2220' 
		when 5 then '0470' 
		when 6 then '7850' 
		end

		if @date is not null
		begin
			with other_doc as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.document_id as [document_id],o.document_no,mcom.company_code,mcus.customer_code,o.document_date,document_date as trip_date ,1 as reference_type 
				from [dbo].[other_control_document] o left join master_company mcom
				on o.company_code = mcom.company_code
				left join master_customer mcus on mcus.customer_code = o.receiver
				left join dbo.scg_pyc_process pyc on pyc.reference_no_id = o.document_id and pyc.reference_type = 1
				where o.method = 'PYC' 
				and convert(date,document_date) = @date
				and o.company_code = isnull(@company_code,o.company_code)
				),
				 cheque as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.cheque_control_id as [document_id],o.[cheque_control_no] as document_no,mcc.company_code,mcc.customer_code,o.collection_date as document_date,o.collection_date as trip_date,2 as reference_type from cheque_control_document o left join master_customer_company mcc
				on o.customer_company_id = mcc.customer_company_id 
				left join dbo.scg_pyc_process pyc
				on pyc.reference_no_id = o.cheque_control_id
				and pyc.reference_type = 2
				where o.method = 'PYC'
				and convert(date,collection_date) = @date
				and company_code = isnull(@company_code,company_code)
				),
				 bill as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.bill_presentment_id as [document_id],o.bill_presentment_no as document_no,mcc.company_code,mcc.customer_code,bill_presentment_date  as document_date,o.bill_presentment_date as trip_date,3 as reference_type from bill_presentment_control_document o left join master_customer_company mcc
				on o.customer_company_id = mcc.customer_company_id 
				left join dbo.scg_pyc_process pyc
				on pyc.reference_no_id = o.bill_presentment_id
				and pyc.reference_type =3
				where o.method = 'PYC'
				and convert(date,bill_presentment_date) = @date
				and company_code = isnull(@company_code,company_code)
				)
				,
				 all_doc as (
					select * from other_doc 
					union all
					select * from cheque
					union all
					select * from bill
				)
				select 
				a.*
				,mcus.customer_name,mcom.company_name,mcc.user_display_name
				,pcg.*
				,(select top 1 [address] from dbo.master_customer_address addr where addr.customer_company_id = mcc.customer_company_id) as [address]
				from all_doc a 
				left join master_customer mcus on mcus.customer_code = a.customer_code
				left join master_company mcom on mcom.company_code = a.company_code
				left join master_customer_company mcc on mcc.company_code = a.company_code and mcc.customer_code = a.customer_code 
				left join pyc_customer_group pcg on pcg.customer_code = a.customer_code
		end
		else
		begin
			with other_doc as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.document_id as [document_id],o.document_no,mcom.company_code,mcus.customer_code,o.document_date,document_date as trip_date ,1 as reference_type 
				from [dbo].[other_control_document] o left join master_company mcom
				on o.company_code = mcom.company_code
				left join master_customer mcus on mcus.customer_code = o.receiver
				left join dbo.scg_pyc_process pyc on pyc.reference_no_id = o.document_id and pyc.reference_type = 1
				where o.method = 'PYC' 
				and MONTH(@date_month_year) = MONTH(document_date)
				and YEAR(@date_month_year) = YEAR(document_date)
				and o.company_code = isnull(@company_code,o.company_code)
				),
				 cheque as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.cheque_control_id as [document_id],o.[cheque_control_no] as document_no,mcc.company_code,mcc.customer_code,o.collection_date as document_date,o.collection_date as trip_date,2 as reference_type from cheque_control_document o left join master_customer_company mcc
				on o.customer_company_id = mcc.customer_company_id 
				left join dbo.scg_pyc_process pyc
				on pyc.reference_no_id = o.cheque_control_id
				and pyc.reference_type = 2
				where o.method = 'PYC'
				and MONTH(@date_month_year) = MONTH(collection_date)
				and YEAR(@date_month_year) = YEAR(collection_date)
				and company_code = isnull(@company_code,company_code)
				),
				 bill as (
				select pyc.process_id,pyc.remark,isnull(pyc.is_skip,0) as is_skip,o.bill_presentment_id as [document_id],o.bill_presentment_no as document_no,mcc.company_code,mcc.customer_code,bill_presentment_date  as document_date,o.bill_presentment_date as trip_date,3 as reference_type from bill_presentment_control_document o left join master_customer_company mcc
				on o.customer_company_id = mcc.customer_company_id 
				left join dbo.scg_pyc_process pyc
				on pyc.reference_no_id = o.bill_presentment_id
				and pyc.reference_type =3
				where o.method = 'PYC'
				and MONTH(@date_month_year) = MONTH(bill_presentment_date)
				and YEAR(@date_month_year) = YEAR(bill_presentment_date)
				and company_code = isnull(@company_code,company_code)
				)
				,
				 all_doc as (
					select * from other_doc 
					union all
					select * from cheque
					union all
					select * from bill
				)
				select 
				a.*
				,mcus.customer_name,mcom.company_name,mcc.user_display_name
				,pcg.*
				,(select top 1 [address] from dbo.master_customer_address addr where addr.customer_company_id = mcc.customer_company_id) as [address]
				from all_doc a 
				left join master_customer mcus on mcus.customer_code = a.customer_code
				left join master_company mcom on mcom.company_code = a.company_code
				left join master_customer_company mcc on mcc.company_code = a.company_code and mcc.customer_code = a.customer_code 
				left join pyc_customer_group pcg on pcg.customer_code = a.customer_code
		end

    
END
GO
