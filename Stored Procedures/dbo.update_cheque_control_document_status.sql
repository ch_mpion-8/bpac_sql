SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[update_cheque_control_document_status]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int
	,@status as nvarchar(20) 
	,@updated_by as nvarchar(50) = null
	,@clear_cheque_data as bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update dbo.cheque_control_document
	set cheque_control_status = @status
		,updated_by = @updated_by
		,updated_date = getdate()
	where cheque_control_id = @cheque_control_id

	--if @clear_cheque_data = 1 
	--begin
	--	update dbo.cheque_control_document_item
	--	set  cheque_no = null
	--		,cheque_amount = null
	--		,cheque_date = null
	--		,cheque_currency = null
	--		,cheque_bank = null
	--		,cheque_remark = null
	--		,updated_by = @updated_by
	--		,updated_date = getdate()
	--	where cheque_control_id = @cheque_control_id

	--end
		
	declare @change_status_name as nvarchar(200) = null
	select @change_status_name = case @status when 'WAITINGRECEIVED' then 'Reverse status to Waiting for receive control doc.'
														  when 'COMPLETE' then 'Complete'
														  when 'CANCEL' then 'Cancel'
								 end
		
	exec dbo.insert_cheque_control_history 
			@cheque_control_id = @cheque_control_id
			,@description = @change_status_name
			,@action  = 'Update status'
			,@updated_by  = @updated_by
END

GO
