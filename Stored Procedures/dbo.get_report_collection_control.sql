SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[get_report_collection_control]
	-- Add the parameters for the stored procedure here
	@company_code varchar(100)=null,
	@document_date date=null,
	@method varchar(100)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Declare @t_method Table(method varchar(15))
	if @company_code = 'All'
		set @company_code = NULL

	insert into @t_method select * from dbo.SplitString(@method,',')
	

	/*select 
	o.cheque_control_no
	,o.collection_date
	,c.company_code
	,com.company_name
	,c.customer_code
	,cus.customer_name as user_display_name
	,i.reference_document_no
	,r.document_date
	,r.assignment_no
	,r.document_amount as invoice_amount
	,r.currency
	,isnull(conf.[configuration_value], o.method) as method
	,i.cheque_control_detail_id
	,o.created_by
	,o.created_date
	 from dbo.cheque_control_document_item i 
	inner join 	dbo.cheque_control_document o 	on o.cheque_control_id = i.cheque_control_id
	inner join dbo.master_customer_company c on c.customer_company_id = o.customer_company_id
	inner join dbo.master_customer cus on cus.customer_code = c.customer_code
	inner join dbo.master_company com on c.company_code = com.company_code
	inner join  dbo.receipt r on r.reference_document_no = i.reference_document_no
							and r.company_code = c.company_code
							and r.customer_code = c.customer_code
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where 
	(convert(date,o.created_date) = @document_date or @document_date is null)
	and (o.method in (select * from @t_method) or @method is null or @method ='')
	and (c.company_code = @company_code or @company_code is null or @company_code = '')
	and o.cheque_control_status <>'CANCEL'
	and i.is_active =1
	order by o.collection_date,c.customer_code,r.reference_document_no,o.cheque_control_no*/
	select 
	o.cheque_control_no
	,o.collection_date
	,c.company_code
	,com.company_name
	,c.customer_code
	,cus.customer_name as user_display_name
	,i.reference_document_no
	,r.document_date
	,r.assignment_no
	,r.document_amount as invoice_amount
	,r.currency
	,isnull(conf.[configuration_value], o.method) as method
	,i.cheque_control_detail_id
	,o.created_by
	,o.created_date
	 from dbo.cheque_control_document_item i 
	inner join 	dbo.cheque_control_document o 	on o.cheque_control_id = i.cheque_control_id
	inner join dbo.master_customer_company c on c.customer_company_id = o.customer_company_id
	inner join dbo.master_customer cus on cus.customer_code = c.customer_code
	inner join dbo.master_company com on c.company_code = com.company_code
	inner join  dbo.receipt r on r.reference_document_no = i.reference_document_no
							and r.company_code = c.company_code
							and r.customer_code = c.customer_code
							AND r.year = i.document_year
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where 
	(convert(date,o.created_date) = @document_date or @document_date is null)
	and (o.method in (select * from @t_method) or @method is null or @method ='')
	and (@company_code is null OR @company_code ='' or c.company_code in ( select item from dbo.SplitString(@company_code,',')))
	-- and (c.company_code = @company_code or @company_code is null or @company_code = '')
	and o.cheque_control_status <>'CANCEL'
	and i.is_active =1
	UNION
    select 
	o.cheque_control_no
	,o.collection_date
	,c.company_code
	,com.company_name
	,c.customer_code
	,cus.customer_name as user_display_name
	,o.cheque_control_no AS reference_document_no
	,o.collection_date AS [document_date]
	,i.invoice_no 
	,i.amount AS [invoice_amount]
	--,r.document_amount as invoice_amount
	,r.currency
	,isnull(conf.[configuration_value], o.method) as method
	,i.cheque_control_detail_id
	,o.created_by
	,o.created_date
	 from dbo.cheque_invoice_control_document_item i 
	inner join 	dbo.cheque_invoice_control_document o 	on o.cheque_control_id = i.cheque_control_id
	inner join dbo.master_customer_company c on c.customer_company_id = o.customer_company_id
	inner join dbo.master_customer cus on cus.customer_code = c.customer_code
	inner join dbo.master_company com on c.company_code = com.company_code
	inner join  dbo.invoice_header r on r.invoice_no = i.invoice_no
							and r.company_code = c.company_code
							and r.customer_code = c.customer_code
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'ReceiveMethod'
	where 
	(convert(date,o.created_date) = @document_date or @document_date is null)
	and (o.method in (select * from @t_method) or @method is null or @method ='')
	and (@company_code is null OR @company_code ='' or c.company_code in ( select item from dbo.SplitString(@company_code,',')))
	-- and (c.company_code = @company_code or @company_code is null or @company_code = '')
	and o.cheque_control_status <>'CANCEL'
	and i.is_active =1
	order by collection_date,customer_code,reference_document_no,cheque_control_no


END
GO
