SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_default_receipt_method]
	-- Add the parameters for the stored procedure here
	@company_code varchar(4) =null,
	@customer_code varchar(20) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select top 1 isnull(b.receipt_method,'PYC') as receipt_method 
	from dbo.master_customer_company c
	inner join dbo.master_customer_collection_behavior b
	on c.customer_company_id = b.customer_company_id
	where c.customer_code = @customer_code
	and c.company_code =@company_code
	and b.is_active = 1
END
GO
