SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[get_print_envelope]
	-- Add the parameters for the stored procedure here
	@contact_list as nvarchar(max) = null --'1,2,3,4'
		,@is_customer as bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	;with items as (	
	select substring(item,0,charindex('|', item)) as receiver_id
		,substring( item, charindex('|', item) + 1, len(item) - charindex('|', item)) as receiver_type 
	from dbo.SplitString(@contact_list,',')
	)
	select cus.customer_name
				,ct.contact_name as contact_name
				,[address]	
				,sub_district	
				,ad.district	
				,province	
				,ad.post_code	
				,0 as customer_company_id --ad.customer_company_id 
		from dbo.master_customer_address ad
		inner join dbo.master_customer_company c on ad.customer_company_id = c.customer_company_id
		inner join dbo.master_customer cus on c.customer_code = cus.customer_code
		left join dbo.master_customer_contact ct on c.customer_company_id = ct.customer_company_id and ct.is_show_in_ems = 1 and ct.is_active =1
		where ad.customer_company_id in (select receiver_id from items where receiver_type = 'C')
				and ad.is_ems = 1
				and ad.is_active = 1
	union
		select null as customer_name 
				,contact_name
				,[address]	
				,sub_district	
				,district	
				,province	
				,post_code	
				, 0
		from dbo.master_other_contact 
		where contact_id in (select receiver_id from items where receiver_type = 'R')
				and is_active = 1
	



	--if(@is_customer = 1)
	--begin
	--	select cus.customer_name
	--			,ct.contact_name as contact_name
	--			,[address]	
	--			,sub_district	
	--			,ad.district	
	--			,province	
	--			,ad.post_code	
	--	from dbo.master_customer_address ad
	--	inner join dbo.master_customer_company c on ad.customer_company_id = c.customer_company_id
	--	inner join dbo.master_customer cus on c.customer_code = cus.customer_code
	--	left join dbo.master_customer_contact ct on c.customer_company_id = ct.customer_company_id and ct.is_show_in_ems = 1
	--	where c.customer_code in (select item from dbo.SplitString(@contact_list,','))
	--			and ad.is_ems = 1
	--			and ad.is_active = 1
	--end
	--else
	--begin
	--	select null as customer_name
	--			,contact_name
	--			,[address]	
	--			,sub_district	
	--			,district	
	--			,province	
	--			,post_code	
	--	from dbo.master_other_contact 
	--	where contact_id in (select item from dbo.SplitString(@contact_list,','))
	--			and is_active = 1
	--end
END
GO
