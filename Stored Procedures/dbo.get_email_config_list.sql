SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_email_config_list]
	-- Add the parameters for the stored procedure here
	@company_code as varchar(200) = null
	,@customer_code as nvarchar(20) =null
	,@customer as nvarchar(100) =  null--'ha' --'b'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select ec.customer_company_id
			,sales_org
			,sold_to
			,ship_to
			,number_of_copies
			,output_type_text
			,output_type_code
			,send_email_type
			,(select stuff(
					(
						select distinct ', ' + sub.email_address 
							from [dbo].[master_email_config_detail] sub
							where sub.customer_company_id = ec.customer_company_id
									and sub.document_type = 'Cash In Account' and sub.email_type = 'To'
							for xml path('')
					) ,1,1,'') )  as cash_in_account_to
			,(select stuff(
					(
						select distinct ', ' + sub.email_address 
							from [dbo].[master_email_config_detail] sub
							where sub.customer_company_id = ec.customer_company_id
									and sub.document_type = 'Cash In Account' and sub.email_type = 'CC'
							for xml path('')
					) ,1,1,'') )  as cash_in_account_CC
			,(select stuff(
					(
						select distinct ', ' + sub.email_address 
							from [dbo].[master_email_config_detail] sub
							where sub.customer_company_id = ec.customer_company_id
									and sub.document_type = 'eTax' and sub.email_type = 'To'
							for xml path('')
					) ,1,1,'') )  as etaxinvoice_to
			,(select stuff(
					(
						select distinct ', ' + sub.email_address 
							from [dbo].[master_email_config_detail] sub
							where sub.customer_company_id = ec.customer_company_id
									and sub.document_type = 'eTax' and sub.email_type = 'CC'
							for xml path('')
					) ,1,1,'') )  as etaxinvoice_CC
			,ec.is_active
			,ec.created_date
			,ec.created_by
			,ec.updated_date
			,ec.updated_by
			,cc.company_code
			,cc.customer_code
			,c.customer_name
	from master_email_config ec
	inner join master_customer_company cc on ec.customer_company_id = cc.customer_company_id
	inner join master_customer c on cc.customer_code = c.customer_code
	where (@company_code is null or cc.company_code  in ( select item from dbo.SplitString(@company_code,',')))
			and (@customer is null or cc.customer_code +' : '+ c.customer_name like '%' + @customer + '%')
			and cc.customer_code  = isnull(@customer_code,cc.customer_code)


END


GO
