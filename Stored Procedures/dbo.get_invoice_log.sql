SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_invoice_log]
	-- Add the parameters for the stored procedure here
	 @invoice_no nvarchar(50) = null
	 ,@company_code nvarchar(20) = null
	 ,@is_bill_presentment bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	if (@is_bill_presentment = 1)
	begin 
		--select [invoice_no]
		--	,case when old_date is null then '-' else convert(varchar, old_date, 103) end as old_date
		--	,case when old_date is null then '-' else convert(varchar, new_date, 103) end as new_date
		--	,[updated_by]
		--	,convert(varchar, [updated_time], 103) + ' '  + convert(varchar(8), [updated_time], 14) as [updated_time]
		--from (
		select  [invoice_no]
			  ,case when [old_billing_date] is null then '-' else convert(varchar, [old_billing_date], 103) end as old_date
			  ,case when [new_billing_date] is null then '-' else convert(varchar, [new_billing_date], 103) end as new_date
			  ,[remark]
			  ,[updated_by] as [updated_by]
			  , convert(varchar, [updated_time], 103) + ' '  + convert(varchar(8), [updated_time], 14) as [updated_time]
		from [SCGBPAC].[dbo].[log_manual_update_bill_date]
		where invoice_no = @invoice_no
				and (company_code is null or company_code = @company_code)
		--group by  [invoice_no]
		--	  ,[old_billing_date]
		--	  ,[new_billing_date]   
		--) a
		order by [invoice_no],[updated_time]
	end
	else
	begin 
	select  [invoice_no]
			  ,case when old_collection_date is null then '-' else convert(varchar, old_collection_date, 103) end   as old_date
			  ,case when new_collection_date is null then '-' else convert(varchar, new_collection_date, 103) end  as new_date
			  ,[remark]
			  ,[updated_by] as [updated_by]
			  , convert(varchar, [updated_time], 103) + ' '  + convert(varchar(8), [updated_time], 14) as [updated_time]
		from [SCGBPAC].[dbo].log_manual_update_collection_date
		where invoice_no = @invoice_no
				and (company_code is null or company_code = @company_code)
		order by [invoice_no],[updated_time]
		--select [invoice_no]
		--	,case when old_date is null then '-' else convert(varchar, old_date, 103) end as old_date
		--	,case when old_date is null then '-' else convert(varchar, new_date, 103) end as new_date
		--	,[updated_by]
		--	,convert(varchar, [updated_time], 103) + ' '  + convert(varchar(8), [updated_time], 14) as [updated_time]
		--from (
		--select  [invoice_no]
		--	  ,[old_collection_date]  as old_date
		--	  , [new_collection_date] as new_date
		--	  ,min([updated_by]) as [updated_by]
		--	  , min([updated_time]) as [updated_time]
		--from [SCGBPAC].[dbo].[log_manual_update_collection_date]
		--where invoice_no = @invoice_no
		--		and (company_code is null or company_code = @company_code)
		--group by  [invoice_no]
		--	  ,[old_collection_date]
		--	  ,[new_collection_date]   
		--) a
		--order by [invoice_no],[updated_time]
	end
	
END
GO
