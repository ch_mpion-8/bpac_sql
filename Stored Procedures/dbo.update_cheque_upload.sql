SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_cheque_upload]
	-- Add the parameters for the stored procedure here
	@cheque_control_id as int = null
		,@reference_guid as uniqueidentifier = null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   declare @status as nvarchar(20) = 'COMPLETE'

	update dbo.cheque_control_document
	set cheque_control_status = @status
		,reference_guid = @reference_guid
		,updated_by = @updated_by
		,updated_date = getdate()
	where cheque_control_id = @cheque_control_id


		exec dbo.insert_cheque_control_history
			@cheque_control_id= @cheque_control_id
			,@description =  'Upload cheque.'
			,@action  = 'Complete'
			,@updated_by  = @updated_by

END


GO
