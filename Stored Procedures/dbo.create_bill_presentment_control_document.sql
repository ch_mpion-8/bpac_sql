SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[create_bill_presentment_control_document]
	-- Add the parameters for the stored procedure here
	 @document_control_list  as [dbo].[document_control_table_type] readonly
	 ,@updated_by as nvarchar(50) = null
	,@document_control_id as nvarchar(2000) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
declare @customer_company_id [int]  = NULL,
		@bill_presentment_date [date] =  NULL,
		@method [nvarchar](20) = NULL,
		@payment_term [varchar](4) = NULL,
		@currency [varchar](5) = NULL,
		@remark [nvarchar](2000) = NULL,
		@reference_no_list [nvarchar](max) = NULL,
		@new_status as nvarchar(20) = 'WAITINGRECEIVED'

		declare table_cursor cursor for 

	select [customer_company_id]
			,[date]
			,[method]
			,[payment_term]
			,[currency]
			,[remark]
			,[reference_no_list]
	from @document_control_list

	open table_cursor;
	fetch next from table_cursor into @customer_company_id,@bill_presentment_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	while @@fetch_status = 0
	   begin
      
		  declare @bill_id as int = null

			declare @bill_id_new table(new_id int)
			
				insert into [dbo].[bill_presentment_control_document]
					([customer_company_id]
					,[method]
					,[remark]
					,[bill_presentment_date]
					,[bill_presentment_status]
					,[created_date]
					,[created_by]
					,[updated_date]
					,[updated_by])
				output inserted.bill_presentment_id into @bill_id_new
				values
				(
					@customer_company_id
					,@method
					,@remark
					,@bill_presentment_date
					,@new_status
					,getdate()
					,@updated_by
					,getdate()
					,@updated_by
				)

				--select @bill_id = ident_current('bill_presentment_control_document')
				select @bill_id = new_id
				from @bill_id_new

				set @document_control_id = case when @document_control_id is null or @document_control_id = '' 
							then cast(@bill_id as nvarchar(200)) else @document_control_id + ',' + cast(@bill_id as nvarchar(200)) end
				

			select *
			into #tmp
			from (
				--select item as ref_no
				--from dbo.SplitString(@reference_no_list,',') ss
				select   substring(item,0,charindex('|', item)) as ref_no
					--,right(item,charindex('|', reverse(item)) -1 ) as com
				from dbo.SplitString(@reference_no_list,',')ss
				union
				select i.invoice_no as ref_no
				from dbo.bill_presentment_temp c
				inner join dbo.bill_presentment_item_temp i on c.temp_id = i.temp_id and i.is_active = 1
				where c.customer_company_id = @customer_company_id 
							--and method = @method 
							and bill_presentment_date = @bill_presentment_date 
							and currency = @currency
							and c.payment_term = @payment_term

			) a


				
			insert into [dbo].[bill_presentment_control_document_item]
				([bill_presentment_id]
				,[invoice_no]
				,is_active
				,[created_date]
				,[created_by]
				,[updated_date]
				,[updated_by])
			select @bill_id
					,rtrim(ltrim(ref_no)) as item
					,1
					,getdate()
					,@updated_by
					,getdate()
					,@updated_by
			from #tmp

			 

			update dbo.bill_presentment_item_temp
			set is_active = 0
				,updated_by = @updated_by
				,updated_date = getdate()	
			where invoice_no in  (select rtrim(ltrim(ref_no)) as ref_no from #tmp)

			exec dbo.update_invoice_bill_presentment_status
				@invoice_no = null
				,@bill_presentment_id = @bill_id
				,@status  = 'COMPLETE'
				,@updated_by  = @updated_by
		
	--update dbo.invoice_header
	--set bill_presentment_status = 'COMPLETE'
	--where invoice_no in (select invoice_no from dbo.bill_presentment_control_document_item with(nolock)
	--										where bill_presentment_id = @bill_id)
		exec dbo.insert_bill_presentment_history 
			@bill_presentment_id = @bill_id
			,@description = 'Create bill presentment.'--1580269083
			,@action  = 'Create'
			,@updated_by  = @updated_by


	   
	   drop table #tmp
		  fetch next from table_cursor into  @customer_company_id,@bill_presentment_date,@method,@payment_term
										,@currency,@remark,@reference_no_list
	   end;

	close table_cursor;
	deallocate table_cursor;

END

GO
