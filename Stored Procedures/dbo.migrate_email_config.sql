SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[migrate_email_config]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	insert into master_customer_company (company_code,customer_code,is_active,created_by,created_date,updated_by,updated_date)
	select distinct temp.[COMPANYCODE],c.customer_code,1,'SYSTEM',getdate(),'SYSTEM',getdate()
	from [dbo].[CUSTOMERCONTACT_PRD_20191001$] temp
	left join master_customer_company cc on temp.[COMPANYCODE] = cc.company_code 
		and temp.[CUSTOMERCODE] = right('0000000000' + cc.customer_code, 10)
	left join master_customer c on temp.customercode =  right('0000000000' +c.customer_code,10)
		where [COMPANYCODE] in (select company_code from master_company)
				and c.customer_code is not null
				and cc.customer_company_id is null


	insert into [master_email_config]
	([customer_company_id]
		  ,[sales_org]
		  ,[sold_to]
		  ,[ship_to]
		  ,[number_of_copies]
		  ,[output_type_text]
		  ,[output_type_code]
		  ,[send_email_type]
		  ,[is_active]
		  ,[created_date]
		  ,[created_by]
		  ,[updated_date]
		  ,[updated_by])
	select distinct cc.customer_company_id 
		  ,temp.[SALESORGANIZE]
		  ,temp.[SOLDTO]
		  ,temp.[SHIPTO]
		  ,temp.[NUMBEROFCOPIES]
		  ,temp.[OUTPUTTYPE]
		  ,temp.[OUTPUTTYPE]
		  ,case when temp.[SENDEMAILTYPE] = 'online' then 'Single File' else 'Bundled Files' end
		  ,temp.[ISACTIVECONTACT]
		  ,getdate() as w
		  ,temp.[UPDATEBY]
		  ,getdate() as w
		  ,temp.[UPDATEBY]
	from [dbo].[CUSTOMERCONTACT_PRD_20191001$] temp
	inner join master_customer_company cc on temp.[COMPANYCODE] = cc.company_code 
		and temp.[CUSTOMERCODE] = right('0000000000' + cc.customer_code, 10)

		 
 insert into [master_email_config_detail](
[customer_company_id]
      ,[document_type]
      ,[email_type]
      ,[email_address]
      ,[is_active]
      ,[created_date]
      ,[created_by]
      ,[updated_date]
      ,[updated_by])
select distinct cc.customer_company_id 
      ,case when temp.[DOCUMENTTYPE] = 'e-Tax Invoice' then 'eTax' else temp.[DOCUMENTTYPE] end 
	  ,case when temp.[to] = 1 then 'to' else 'cc' end
      ,temp.[EMAILADDRESS]
      ,temp.[ISACTIVECONTACT]
      ,getdate() as w
      ,temp.[UPDATEBY]
      ,getdate() as w
      ,temp.[UPDATEBY]
from [dbo].[CUSTOMERCONTACT_PRD_20191001$] temp
inner join master_customer_company cc on temp.[COMPANYCODE] = cc.company_code 
	and temp.[CUSTOMERCODE] = right('0000000000' + cc.customer_code, 10)

END
GO
