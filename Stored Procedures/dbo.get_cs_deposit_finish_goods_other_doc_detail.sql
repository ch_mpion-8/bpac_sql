SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_cs_deposit_finish_goods_other_doc_detail]
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
	distinct  
	d.order_no as reference_no,
	o.created_by,
	o.document_no,
	o.remark,
	Convert(varchar(2),o.method) as method,
	d.order_no,
	d.company_code,
	d.customer_code,
	cus.customer_name,
	d.invoice_date,
	o.document_date as other_doc_date,
	com.company_name
	from dbo.other_control_document o
	left join dbo.other_control_document_item i 
	on i.document_id=o.document_id
	inner join dbo.deposit d
	on d.order_no = i.reference_no
	inner join dbo.master_customer_company c
	on c.customer_code = d.customer_code 
	and c.company_code = d.company_code
	inner join dbo.master_customer cus
	on c.customer_code = cus.customer_code
	inner join dbo.master_company com
	on com.company_code = d.company_code
	where o.document_id = @document_id
END
GO
