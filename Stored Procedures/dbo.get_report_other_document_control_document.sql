SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[get_report_other_document_control_document]
	-- Add the parameters for the stored procedure here
	@otherdoc_date date =null,
	@company_code varchar(100)=null,
	@user_code varchar(100)=null,
	@method varchar(100)=null,
	@otherdoc_type varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @t_method Table(method varchar(20))
	Declare @t_document_type Table([type] varchar(1))
	insert into @t_method select * from dbo.SplitString(@method,',')
	insert into @t_document_type select * from dbo.SplitString(@otherdoc_type,',')

	if @company_code = 'All'
		set @company_code = NULL

	if @user_code = 'All'
		set @user_code = null

	select 
	o.document_no,
	o.document_date,
	o.receiver,
	isnull(c.customer_name,cp.customer_name) as customer_name,
	o.document_type,
	o.user_code,
	isnull(conf.[configuration_value], o.method) as method
	,(select Top 1 action_type from dbo.other_control_document_item i where i.document_id = o.document_id and (i.action_type=1 or i.action_type=0) ) as has_send
	,(select Top 1 action_type from dbo.other_control_document_item i where i.document_id = o.document_id and (i.action_type=2 or i.action_type=0) ) as has_receive
	, o.created_date
	, com.company_code
	, com.company_name
	from dbo.other_control_document o	
	left join dbo.master_customer c on c.customer_code = o.receiver
	inner join dbo.master_company com on com.company_code = o.company_code
	left join dbo.master_other_contact_person cp on cp.other_contact_id=o.contact_person_id
	left join dbo.configuration conf on o.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where (method in (select * from @t_method) or @method is null or @method = '' or o.document_type=5)
	and (Convert(date,o.created_date) = @otherdoc_date or @otherdoc_date is null)
	and document_type in (select * from @t_document_type)
	and (@company_code is null OR @company_code ='' or o.company_code in ( select item from dbo.SplitString(@company_code,',')))
	and isnull(o.user_code,'') = isnull(@user_code,isnull(o.user_code,''))
	and o.[status] <>'CANCEL'
	order by o.document_date,c.customer_code,o.document_no


    
END
GO
