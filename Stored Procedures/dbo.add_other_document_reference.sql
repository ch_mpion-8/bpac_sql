SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[add_other_document_reference]
	-- Add the parameters for the stored procedure here
	@reference_no varchar(50)=null,
	@document_id int =null,
	@submited_by varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if exists (select 1 from dbo.other_control_document_item i inner join dbo.other_control_document o on i.document_id=o.document_id where i.reference_no = @reference_no and o.document_id = @document_id)
		return

	insert into dbo.other_control_document_item (reference_no,document_id,created_by,created_date,action_type)
	values(
	@reference_no,
	@document_id,
	@submited_by,
	GETDATE(),
	1)
END
GO
