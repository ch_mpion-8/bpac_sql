SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[save_customer_contact]
	-- Add the parameters for the stored procedure here
	@contact_id as int = null
		,@customer_company_id as int = null
		,@contact_name as nvarchar(100) = null--'john'
		,@position as nvarchar(100) =null-- 'king'
		,@email as nvarchar(100) = null
		,@telephone as nvarchar(100) = null
		,@fax as nvarchar(100) = null
		,@remark as nvarchar(2000) = null
		,@is_show_in_ems as bit = 1
		,@is_show_in_other_document as bit = null
		,@status as bit = null
		,@updated_by as nvarchar(50) = null
		,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if ( @is_show_in_ems = 1 
				and exists(select 1 from [dbo].master_customer_contact 
							where customer_company_id = @customer_company_id 
									and isnull(is_show_in_ems,0) = 1 
									and (@contact_id is null or contact_id <> @contact_id)
									and is_active = 1
					)
		)
		begin
			set @error_message = 'ขื่อ Contact ที่ใช้แสดงใน ems มีได้เพียง Contact เดียวเท่านั้น'
			return;
		end

		--if ( @is_show_in_other_document = 1 
		--		and exists(select 1 from [dbo].master_customer_contact 
		--					where customer_company_id = @customer_company_id 
		--							and isnull(is_show_in_other_document,0) = 1 
		--							and (@contact_id is null or contact_id <> @contact_id)
		--							and is_active = 1
		--			)
		--)
		--begin
		--	set @error_message = 'ขื่อ Contact ที่ใช้แสดงใน Other Doc. มีได้เพียง Contact เดียวเท่านั้น'
		--	return;
		--end

		if exists (select 1 from [dbo].master_customer_contact where contact_id = @contact_id)
		 begin
			
			update dbo.master_customer_contact
			set contact_name = @contact_name
				,position = @position
				,email = @email
				,telephone = @telephone
				,fax = @fax
				,remark = @remark
				,is_show_in_ems = @is_show_in_ems
				,is_show_in_other_document = @is_show_in_other_document
				,[is_active] = @status
				,updated_date = getdate()
				,updated_by = @updated_by
			--select 
			--	@contact_name
			--	,@position
			--	,@email
			--	,@telephone
			--	,@fax
			--	,@remark
			--	,@is_show_in_ems
			--	,@is_show_in_other_document
			--	,@status
			--	,getdate()
			--	,@updated_by
			from dbo.master_customer_contact
			where contact_id = @contact_id

			
			if exists(select 1 from dbo.master_customer_contact where copy_from_id = @contact_id and is_active = 1)
			begin
				update dbo.master_customer_contact
				set contact_name = @contact_name
					,position = @position
					,email = @email
					,telephone = @telephone
					,fax = @fax
					,remark = @remark
					,is_show_in_ems = @is_show_in_ems
					,is_show_in_other_document = @is_show_in_other_document
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_contact
				where copy_from_id = @contact_id and is_active = 1
			end

			declare @copy_from_id int
			select @copy_from_id = copy_from_id
			from dbo.master_customer_contact
			where contact_id = @contact_id

			if exists(select 1 from dbo.master_customer_contact where contact_id = @copy_from_id and is_active = 1)
			begin
				update dbo.master_customer_contact
				set contact_name = @contact_name
					,position = @position
					,email = @email
					,telephone = @telephone
					,fax = @fax
					,remark = @remark
					,is_show_in_ems = @is_show_in_ems
					,is_show_in_other_document = @is_show_in_other_document
					,[is_active] = @status
					,updated_date = getdate()
					,updated_by = @updated_by
				from dbo.master_customer_contact
				where contact_id = @copy_from_id and is_active = 1
			end




		 end
		 else
		 begin
			
			insert into dbo.master_customer_contact
			([customer_company_id]
			   ,contact_name
			   ,position
			   ,email
			   ,telephone
			   ,fax
			   ,remark
			   ,is_show_in_ems
			   ,is_show_in_other_document
			   ,[is_active]
			   ,[created_date]
			   ,[created_by]
			   ,[updated_date]
			   ,[updated_by])
			select 
				@customer_company_id
				,@contact_name
				,@position
				,@email
				,@telephone
				,@fax
				,@remark
				,@is_show_in_ems
				,@is_show_in_other_document
				,@status
				,getdate()
				,@updated_by
				,getdate()
				,@updated_by

		 end
END


GO
