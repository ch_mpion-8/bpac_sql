SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[copy_customer_address]
	-- Add the parameters for the stored procedure here
	 @from_customer_company_id as int = null--4 -- '2011271' --null
		,@to_customer_company_id as int = null--18 -- '0230' --null
		,@updated_by as nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @is_has_ems as bit = null

	if exists (select 1 from dbo.master_customer_address where customer_company_id = @to_customer_company_id and is_ems = 1)
	begin
		set @is_has_ems = 1
	end

	insert into dbo.master_customer_address
				([customer_company_id]
				   ,[title]
				   ,[address]
				   ,[sub_district]
				   ,[district]
				   ,[province]
				   ,[post_code]
				   ,[is_ems]
				   ,[is_allowed_edit]
				   ,[copy_from_id]
				   ,[is_active]
				   ,[created_date]
				   ,[created_by]
				   ,[updated_date]
				   ,[updated_by])
	select @to_customer_company_id	
			,a.title	
			,a.[address]	
			,a.sub_district	
			,a.district	
			,a.province	
			,a.post_code	
			,case when @is_has_ems =1 then null else is_ems end		
			,a.[is_allowed_edit]
			,a.address_id
			,a.[is_active]
			,getdate()
			,@updated_by
			,getdate()
			,@updated_by
	from dbo.master_customer_address a
	where customer_company_id = @from_customer_company_id
			and a.[is_active] = 1
			and a.is_allowed_edit = 1
END



GO
