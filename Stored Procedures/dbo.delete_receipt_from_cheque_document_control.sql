SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[delete_receipt_from_cheque_document_control]
	-- Add the parameters for the stored procedure here
	@cheque_control_detail_id as int
	,@updated_by as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.cheque_control_document_item
	set is_active = 0
		,updated_by = @updated_by
		,updated_date = getdate()
	from dbo.cheque_control_document_item
	where cheque_control_detail_id = @cheque_control_detail_id 

	declare @receipt_no as varchar(16) 
			,@cheque_control_id as int

	select @receipt_no = reference_document_no ,@cheque_control_id = cheque_control_id
	from dbo.cheque_control_document_item
	where cheque_control_detail_id = @cheque_control_detail_id 



	--declare @description as nvarchar(100)
	--set @description = 'Remove receipt ' + @receipt_no
	exec dbo.insert_cheque_control_history
			@cheque_control_id= @cheque_control_id
			,@description = @receipt_no
			,@action  = 'Remove'
			,@updated_by  = @updated_by

	if not exists ( select 1 from dbo.cheque_control_document_item where cheque_control_id = @cheque_control_id and cheque_no is null)
	begin
		exec dbo.update_cheque_control_document_status
				@cheque_control_id = @cheque_control_id
				,@status = 'COMPLETE'
				,@updated_by  = @updated_by
	end

END
GO
