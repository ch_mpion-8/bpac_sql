SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[insert_file_temp]
	-- Add the parameters for the stored procedure here
	@file_name as nvarchar(200) = null
	,@file_type as nvarchar(200) = null
	,@file_content as varbinary(max) = null
	,@document_type as nvarchar(200) = null
	,@size as int = null
	,@is_active as bit = null
	,@updated_by as nvarchar(200) = null
	,@id as uniqueidentifier = null output
   
          
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @T table(ID uniqueidentifier)

	if exists( select 1 from [files_temp] where [id] = @id)
	begin
		update [files_temp]
		set 
			[file_name] = @file_name
			,file_type = @file_type
			,file_content = @file_content
			,document_type = @document_type
			,size = @size
			,is_active = @is_active
			,updated_date = getdate()
			,updated_by = @updated_by
		where [id] = @id
	end
	else
	begin
	
    -- Insert statements for procedure here
		INSERT INTO [dbo].[files_temp]
			   ( [file_name]
				, file_type
				, file_content
				, document_type
				, size
				, is_active
				, created_date
				, created_by
				, updated_date
				, updated_by)
			   OUTPUT inserted.id into @T
		 VALUES
			   (
			   @file_name
				, @file_type
				, @file_content
				, @document_type
				, @size
				, @is_active
				, getdate()
				, @updated_by
				, getdate()
				, @updated_by)

		select @id = ID from @T
	end

END
GO
