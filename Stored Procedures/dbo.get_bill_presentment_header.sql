SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_bill_presentment_header]
	-- Add the parameters for the stored procedure here
	@bill_presentment_id as INT
    ,@is_from_email AS BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @payment_method nvarchar(100)
	select  @payment_method =	stuff(
			(
				select ', ' + case when cc.payment_method is null then '-' else cc.payment_method end
				--select cc.payment_method,b.bill_presentment_id--,b.bill_presentment_no
					from [dbo].[bill_presentment_control_document] b
					inner join [dbo].[bill_presentment_control_document_item] bi on b.bill_presentment_id = bi.bill_presentment_id
					inner join dbo.master_customer_company c on b.customer_company_id = c.customer_company_id
					inner join dbo.invoice_header i on bi.invoice_no = i.invoice_no and c.company_code = i.company_code
					left join dbo.master_customer_collection_behavior cc on i.collection_behavior_id = cc.collection_behavior_id
					where b.bill_presentment_id = @bill_presentment_id 
						and bi.is_active = 1
						and cc.payment_method is not null
					group by b.bill_presentment_id,cc.payment_method--,b.bill_presentment_no
					for xml path('')
			) ,1,1,'') 

	select bcd.bill_presentment_id
			,bcd.bill_presentment_no
			,bcd.customer_company_id
			,cc.company_code
			,CASE WHEN @is_from_email = 1 THEN com.company_name_th ELSE com.company_name END AS company_name
			,cc.customer_code
			,cus.customer_name
			,isnull(conf.[configuration_value], bcd.method) as bill_presentment_method
			-- ,bcd.method as bill_presentment_method
			,bcd.bill_presentment_date
			,bcd.remark
			,bcd.bill_presentment_status
			,@payment_method as payment_method
	from dbo.bill_presentment_control_document bcd
	inner join dbo.master_customer_company cc on bcd.customer_company_id = cc.customer_company_id
	inner join dbo.master_customer cus on cc.customer_code = cus.customer_code
	inner join dbo.master_company com on cc.company_code = com.company_code
	left join dbo.configuration conf on bcd.method = conf.[configuration_code] and conf.[configuration_key] = 'Method'
	where bill_presentment_id = @bill_presentment_id


	--declare @payment_term as varchar(4)
	--		,@bill_date as date
	--		,@currency as varchar(5)
	--		,@customer_company_id as int


	--select @payment_term = ih.payment_term
	--		,@bill_date = ih.bill_presentment_date
	--		,@currency = ih.currency
	--		,@customer_company_id = bcd.customer_company_id
	--from dbo.bill_presentment_control_document bcd
	--inner join dbo.bill_presentment_control_document_item bcdi on bcd.bill_presentment_id = bcdi.bill_presentment_id
	--inner join dbo.invoice_header ih on bcdi.invoice_no = ih.invoice_no
	--where bcd.bill_presentment_id = @bill_presentment_id


	--select ih.invoice_no as value
	--from dbo.invoice_header ih
	--inner join dbo.master_customer_company cc on ih.company_code = cc.company_code and ih.customer_code = cc.customer_code
	--where cc.customer_company_id = @customer_company_id 
	--		and ih.payment_term = @payment_term
	--		and ih.currency = @currency
	--		and ih.bill_presentment_date = @bill_date
	--		and (ih.bill_presentment_status is null or ih.bill_presentment_status = 'WAITINGCREATE')
END


GO
