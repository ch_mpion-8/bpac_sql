SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_deposit_finish_goods_other_doc_detail]
	@document_id int =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
	distinct d.dp_no,
	o.created_by,
	o.document_no,
	o.remark,
	o.method,
	d.company_code,
	d.customer_code,
	cus.customer_name,
	i.reference_no,
	o.document_date as other_doc_date,
	com.company_name,
	STUFF(
					(
					  SELECT ',' + invoice_no
					  from dbo.deposit where dp_no = i.reference_no
					  FOR XML PATH(''), TYPE
					 ).value('.', 'NVARCHAR(MAX)')
					,1
					,1
					,''
	) as invoice_no
	from dbo.other_control_document o
	left join dbo.other_control_document_item i 
	on i.document_id=o.document_id
	inner join dbo.deposit d
	on d.dp_no = i.reference_no
	inner join dbo.master_customer_company c
	on c.customer_code = d.customer_code 
	and c.company_code = d.company_code
	inner join dbo.master_customer cus
	on cus.customer_code = c.customer_code
	inner join dbo.master_company com
	on com.company_code = d.company_code
	where o.document_id = @document_id
END
GO
