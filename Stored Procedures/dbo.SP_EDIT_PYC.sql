SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_EDIT_PYC]
	-- Add the parameters for the stored procedure here
	@document_id int =null,
	@reference_type int =null,
	@process_id int =null,
	@count_trip int =null,
	@remark varchar(max)=null,
	@is_skip bit =null,
	@submitby varchar(50)=null,
	@trip_date date =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @process_id =0 or @process_id is null
	begin
		exec dbo.[SP_Create_PYC] @document_id,@reference_type,@trip_date,@count_trip,@is_skip,@remark,@submitby
	end
	else
	begin
		update dbo.scg_pyc_process
		set count_trip = @count_trip
			,remark = @remark
			,is_skip = @is_skip
			,trip_date = @trip_date
			,updated_date = GETDATE()
			,updated_by = @submitby
		where process_id = @process_id
	end
END
GO
