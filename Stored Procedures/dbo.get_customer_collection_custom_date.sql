SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_customer_collection_custom_date]
	-- Add the parameters for the stored procedure here
	@collection_behavior_id as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select collection_custom_date_id
		,[date] as custom_date 
	from dbo.master_customer_collection_custom_date  bd
	inner join dbo.master_customer_collection_behavior b on bd.collection_behavior_id = b.collection_behavior_id
	where b.group_id = @collection_behavior_id
			and bd.[is_active] = 1

END



GO
