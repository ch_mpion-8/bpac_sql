SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[get_other_contact]
	@contact as nvarchar(50) = null
	,@status as bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select [contact_id]
      ,[contact_name]
      ,[address]
      ,[sub_district]
      ,[district]
      ,[province]
      ,[post_code]
      ,[remark]
	  ,[is_active]
  from [dbo].[master_other_contact]
  where (@contact is null or contact_name like '%' + @contact + '%')
		and (@status is null or is_active = @status)

END
GO
