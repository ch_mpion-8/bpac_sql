SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  CREATE VIEW [dbo].[view_do_not_delete_inquiry_duplicated_dp] AS
 SELECT *
 FROM dbo.deposit
 WHERE dp_no in ( SELECT dp_no
 FROM [dbo].[deposit]
 Group by dp_no
 HAVING count(dp_no) > 1)
GO
