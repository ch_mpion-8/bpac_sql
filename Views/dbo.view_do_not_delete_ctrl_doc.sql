SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[view_do_not_delete_ctrl_doc] AS

SELECT bpcd.[bill_presentment_id]
      ,bpcd.[bill_presentment_no] presentment_control_doc_no
	  ,bpcdi.invoice_no
        , dp.customer_code
		, cst.customer_name
		 , dp.company_code
		 , cmp.company_name
	  ,bpcd.[customer_company_id]
	  ,bpcd.[method]
      ,ISNULL(bpcd.[remark],'') AS remark
      ,bpcd.[bill_presentment_date]
      ,bpcd.[bill_presentment_status]
	  ,bpcdi.is_active
	  , dp.order_no
	  , dp.order_date
	  , dp.product_id
	  , dp.total_quantity
	  , dp.dp_no
	 
  FROM [SCGBPAC].[dbo].[bill_presentment_control_document] bpcd
  INNER JOIN [dbo].[bill_presentment_control_document_item] bpcdi
  ON bpcd.bill_presentment_id = bpcdi.bill_presentment_id
  INNER JOIN [dbo].[deposit] dp
  ON bpcdi.invoice_no = dp.invoice_no
  INNER JOIN (
		SELECT company_code, company_name AS company_name FROM [dbo].[master_company]
		UNION
		SELECT company_code, company_full_name AS company_name FROM [dbo].[jv_company]
  ) cmp
  ON   dp.company_code = cmp.company_code
  INNER JOIN [dbo].[master_customer] cst
  on dp.customer_code = cst.customer_code
	


GO
