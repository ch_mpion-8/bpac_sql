SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	CREATE VIEW  [dbo].[view_do_not_delete_invoice] AS
	SELECT DISTINCT invhd.clearing_document_no,  invhd.invoice_no, invhd.invoice_type, 
	
	invhd.invoice_date, 
	
	invhd.company_code, invhd.customer_code, invhd.payment_term, term.payment_term_name,
	invhd.invoice_amount, invhd.tax_amount, invhd.currency,		


	invhd.bill_presentment_behavior_id, 
	invhd.bill_presentment_date,
	invhd.bill_presentment_notify_date,
	invhd.bill_presentment_status,
	invhd.bill_presentment_remark,

		invhd.collection_behavior_id,
		 invhd.collection_date,
		 invhd.collection_notify_date,
		 invhd.due_date,
		 invhd.net_due_date,
		 invhd.collection_amount,
		 invhd.collection_status,
		 invhd.collection_remark,

	
	invdt.dp_no, invdt.load_date, invdt.order_no, invdt.bid_no, invdt.po_no, invdt.material_no,

	dps.total_quantity,
		   invdt.net_value, invdt.tax_value
	FROM  [dbo].[invoice_header] invhd
	INNER join [dbo].[invoice_detail] invdt
	ON invhd.invoice_no = invdt.invoice_no
	INNER JOIN [deposit] dps
	ON invdt.dp_no = dps.dp_no
	INNER JOIN [dbo].[master_payment_term] term
	on invhd.payment_term = term.payment_term_code
	WHERE invdt.net_value <> 0 and  invdt.tax_value <> 0
--	ORDER BY invhd.clearing_document_no,  invhd.invoice_no
GO
