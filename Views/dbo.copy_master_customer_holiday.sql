SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[copy_master_customer_holiday] AS

SELECT  cust.CUSTOMER_NAME,
		cust_hol.*
FROM master_customer_holiday cust_hol
INNER JOIN (

		SELECT DISTINCT customer_code
		FROM master_customer_company
		WHERE company_code in ('7850','0470')
) cust_comp
ON cust_hol.customer_code = cust_comp.customer_code

LEFT JOIN master_customer cust
ON cust_hol.customer_code = cust.customer_code

WHERE 
	   CAST(cust_hol.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(cust_hol.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)
   

GO
