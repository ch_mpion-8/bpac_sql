SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[view_company]
AS
SELECT c.company_code, c.company_name, 'COM' AS company_type
FROM     dbo.master_company c
WHERE  isnull(c.is_active, 1) = 1
UNION
SELECT j.company_code AS company_code, j.company_full_name as company_name, 'JV' AS company_type
FROM     dbo.jv_company j
WHERE  isnull(j.is_active, 1) = 1
GO
