SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[copy_master_customer_affiliate_list] AS

SELECT aff.*
  FROM [SCGBPAC].[dbo].[master_customer_affiliate_list] aff
  INNER JOIN (
  		SELECT DISTINCT CUSTOMER_CODE
		FROM master_customer_company
		WHERE company_code in ('7850','0470')
  ) cust
  ON aff.customer_code = cust.customer_code
  WHERE   CAST(aff.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(aff.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)





GO
