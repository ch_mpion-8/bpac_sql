SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
  CREATE VIEW [dbo].[copy_master_customer_affiliate_group] AS
  
  SELECT aff_grp.*
  FROM [SCGBPAC].[dbo].[master_customer_affiliate_group] aff_grp
  INNER JOIN [copy_master_customer_affiliate_list] aff_list 
  ON aff_grp.affiliate_id = aff_list.affiliate_id
  WHERE CAST(aff_grp.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(aff_grp.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)
GO
