SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[copy_master_customer_contact] AS

SELECT cust_comp.CUSTOMER_CODE, cust.CUSTOMER_NAME, cust_comp.COMPANY_CODE, comp.COMPANY_NAME,
		cust_cont.*
  FROM [SCGBPAC].[dbo].[master_customer_contact] cust_cont
INNER JOIN (
		SELECT customer_company_id, customer_code, company_code
		FROM master_customer_company
		WHERE company_code in ('7850','0470')
) cust_comp
ON cust_cont.customer_company_id = cust_comp.customer_company_id
LEFT JOIN master_customer cust
ON cust_comp.customer_code = cust.customer_code
LEFT JOIN master_company comp
ON cust_comp.company_code = comp.company_code
WHERE 
   (
	   CAST(cust_cont.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(cust_cont.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)
   )
GO
