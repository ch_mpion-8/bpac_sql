SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  CREATE VIEW [dbo].[copy_master_customer_collection_custom_date] AS
  SELECT behv.CUSTOMER_CODE, behv.CUSTOMER_NAME, behv.COMPANY_CODE, behv.COMPANY_NAME,
	cstm_date.*
  FROM [master_customer_collection_custom_date] cstm_date
  INNER JOIN  [SCGBPAC].[dbo].[copy_master_customer_collection_behavior] behv
  ON cstm_date.collection_behavior_id = behv.collection_behavior_id
GO
