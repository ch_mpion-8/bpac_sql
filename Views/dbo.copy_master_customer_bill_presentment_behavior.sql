SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[copy_master_customer_bill_presentment_behavior] AS

SELECT cust_comp.CUSTOMER_CODE, cust.CUSTOMER_NAME, cust_comp.COMPANY_CODE, comp.COMPANY_NAME,
	behv.*
FROM master_customer_bill_presentment_behavior behv
INNER JOIN (
		SELECT customer_company_id, customer_code, company_code
		FROM master_customer_company
		WHERE company_code in ('7850','0470')
) cust_comp

LEFT JOIN master_customer cust
ON cust_comp.customer_code = cust.customer_code
LEFT JOIN master_company comp
ON cust_comp.company_code = comp.company_code

ON behv.customer_company_id = cust_comp.customer_company_id
WHERE 
	  (CAST(behv.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(behv.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)
	  ) and behv.is_active = 1
GO
