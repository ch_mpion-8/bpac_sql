SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[view_do_not_delete_inquiry_inconsistent_date_status] AS
 
--  SELECT *
--  FROM [dbo].[invoice_header]
--WHERE invoice_no = '1580324171'

  SELECT cdh.bill_presentment_id
	,cdh.bill_presentment_no
	,cdh.customer_company_id
	,cdh.method
	,cdh.remark
	,cdh.bill_presentment_date
	,cdh.bill_presentment_status
	,cdh.created_date
	,cdh.created_by
	,cdh.updated_date
	,cdh.updated_by
	,cdh.reference_guid
	,cdi.bill_presentment_detail_id
	,cdi.invoice_no
	,cdi.created_date item_created_date
	,cdi.created_by item_created_by
	,cdi.updated_date item_updated_date
	,cdi.updated_by item_updated_by
	,cdi.is_active
  FROM [bill_presentment_control_document] cdh
  INNER JOIN [bill_presentment_control_document_item] cdi
  on cdh.bill_presentment_id= cdi.bill_presentment_id
  WHERE cdi.invoice_no = '1580324171'
GO
