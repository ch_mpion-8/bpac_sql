SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[view_company_cost_center]
AS
SELECT cost_center_id, company, company_type, department, cost_center, cost_code, is_active
FROM     dbo.master_company_cost_center
WHERE  (ISNULL(is_active, 1) = 1)

GO
