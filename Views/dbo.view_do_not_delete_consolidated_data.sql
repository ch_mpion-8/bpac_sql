SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[view_do_not_delete_consolidated_data] as
SELECT
      bpcd.[bill_presentment_no] presentment_control_doc_no
	   ,bpcd.bill_presentment_date
	   ,bpcd.[method]
	   ,bpcd.bill_presentment_status
	   ,ISNULL(bpcd.[remark],'') AS control_doc_remark
	    ,bpcdi.is_active
	  ,bpcdi.invoice_no
	  , invhd.invoice_type
	  , invhd.customer_code
	  , cst.customer_name
	  ,invhd.company_code
		 , cmp.company_name
		 
	  , invhd.payment_term
	  , term.payment_term_name
	  , invhd.invoice_amount
	  , invhd.tax_amount
	  , invhd.currency
	  ,invhd.bill_presentment_notify_date
	  --,invhd.bill_presentment_status invoice_status
	,ISNULL(invhd.bill_presentment_remark,'') AS Bill_presentment_remark

	  ,invdt.order_no
	  ,invdt.bid_no
	  ,invdt.po_no
	  ,invdt.material_no
	  ,invdt.dp_no
	  ,dp.total_quantity
	  ,invdt.net_value
	  ,invdt.tax_value
	  ,dp.order_date
	  ,dp.invoice_date
	  ,invdt.load_date

  FROM [SCGBPAC].[dbo].[bill_presentment_control_document] bpcd
  INNER JOIN [dbo].[bill_presentment_control_document_item] bpcdi
  ON bpcd.bill_presentment_id = bpcdi.bill_presentment_id

  INNER JOIN [dbo].[invoice_header] invhd
  on invhd.invoice_no = bpcdi.invoice_no



  INNER JOIN [dbo].[invoice_detail] invdt
  ON invdt.invoice_no = bpcdi.invoice_no


  INNER JOIN [master_payment_term] term
	on invhd.payment_term = term.payment_term_code

    INNER JOIN [dbo].[deposit] dp
  ON dp.dp_no = invdt.dp_no




  INNER JOIN (
		SELECT company_code, company_name AS company_name FROM [dbo].[master_company]
		UNION
		SELECT company_code, company_full_name AS company_name FROM [dbo].[jv_company]
  ) cmp
  ON   invhd.company_code = cmp.company_code
  INNER JOIN [dbo].[master_customer] cst
  on invhd.customer_code = cst.customer_code

GO
