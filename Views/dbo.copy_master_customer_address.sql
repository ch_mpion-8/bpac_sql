SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[copy_master_customer_address] AS
  
SELECT cust_comp.CUSTOMER_CODE, cust.CUSTOMER_NAME, cust_comp.COMPANY_CODE, comp.COMPANY_NAME,
	cust_addr.*
FROM [master_customer_address] cust_addr
INNER JOIN (
		SELECT customer_company_id, customer_code, company_code
		FROM master_customer_company
		WHERE company_code in ('7850','0470')
) cust_comp

ON cust_addr.customer_company_id = cust_comp.customer_company_id

LEFT JOIN master_customer cust
ON cust_comp.customer_code = cust.customer_code
LEFT JOIN master_company comp
ON cust_comp.company_code = comp.company_code

WHERE  
	  CAST(cust_addr.updated_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE) OR
	   CAST(cust_addr.created_date AS DATE) BETWEEN CAST('24 Jul 2019' AS date) and CAST( GETDATE() AS DATE)
	  
GO
